<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : services.php
 * @Created_at  : 15/03/2018
 * @Update_at   : 09/11/2024
 * ----------------------------------------------------------------
 */

/**
 * Services configuration.
 */
return [
];
