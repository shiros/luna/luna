<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : handler.php
 * @Created_at  : 15/03/2018
 * @Update_at   : 06/05/2024
 * ----------------------------------------------------------------
 */

/**
 * Handler configuration.
 *
 * To handle an event, you need to define the name of the event you wish to handle as the key.
 * If you wish to handle a specific exception, you must use the exception class name as the key.
 *
 * Handle definition looks like :
 * - 'MyEvent' => MyEventHandler::class
 *
 * Example :
 * - RouteException::class => RouteExceptionHandler::class
 */
return [
];
