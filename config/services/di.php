<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : di.php
 * @Created_at  : 09/11/2024
 * @Update_at   : 09/11/2024
 * ----------------------------------------------------------------
 */

use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\Module\ConfigModule;
use Luna\Component\DI\Module\ContainerModule;
use Luna\Component\DI\Module\DispatcherModule;
use Luna\Component\DI\Module\KernelModule;
use Luna\Component\DI\Module\LoggerModule;
use Luna\Component\Dispatcher\Dispatcher;
use Luna\Component\Dispatcher\DispatcherInterface;
use Luna\Component\Logger\LoggerInterface;
use Luna\Component\Logger\LunaLogger;
use Luna\Config\LunaConfig;
use Luna\Kernel;
use Luna\KernelInterface;

/**
 * Dependency injector configuration.
 */
return [
    /**
     * DI Modules.
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * The module definition looks like :
     *  - MyClass::class => MyClassModule::class
     */
    'Modules' => [
        Dispatcher::class          => DispatcherModule::class,
        DispatcherInterface::class => DispatcherModule::class,
        Kernel::class              => KernelModule::class,
        KernelInterface::class     => KernelModule::class,
        LoggerInterface::class     => LoggerModule::class,
        LunaConfig::class          => ConfigModule::class,
        LunaContainer::class       => ContainerModule::class,
        LunaLogger::class          => LoggerModule::class,
    ]
];
