<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : env.php
 * @Created_at  : 01/06/2019
 * @Update_at   : 06/05/2024
 * ----------------------------------------------------------------
 */

/**
 * Environment configuration.
 */
return [
    /**
     * Environment name.
     *
     * Production       => 'prod'
     * Pre-Production   => 'pre-prod'
     * Development      => 'dev'
     * Test             => 'test'
     *
     */
    'Environment' => 'dev',

    # --------------------------------
    # Parameters

    /**
     * The parameters format looks like :
     *  - 'Key' => 'SecretValue'
     */
];
