<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : env.test.php
 * @Created_at  : 20/09/2021
 * @Update_at   : 06/05/2024
 * ----------------------------------------------------------------
 */

/**
 * Environment configuration.
 */
return [
    /**
     * Environment name.
     *
     * Production       => 'prod'
     * Pre-Production   => 'pre-prod'
     * Development      => 'dev'
     * Test             => 'test'
     *
     */
    'Environment' => 'test',

    # --------------------------------
    # Parameters

    /**
     * The parameters format looks like :
     *  - 'Key' => 'SecretValue'
     */
];
