<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : config.php
 * @Created_at  : 02/12/2017
 * @Update_at   : 06/05/2024
 * ----------------------------------------------------------------
 */

/**
 * General configuration.
 */
return [
];
