[![pipeline status](https://gitlab.com/shiros/luna/luna/badges/master/pipeline.svg)](https://gitlab.com/shiros/luna/luna/commits/master)
[![coverage report](https://gitlab.com/shiros/luna/luna/badges/master/coverage.svg)](https://gitlab.com/shiros/luna/luna/commits/master)

# Luna Framework

## Table of Contents

[[_TOC_]]

## Information

This framework is inspired by _**Symfony**_.

This project is in ***Php 8.2***.  
You can access to the wiki [here](https://gitlab.com/shiros/luna/luna/-/wikis/home).

### License

This project is licensed under the MIT license.  
See the [LICENSE](./LICENSE) file for more details.

### Dependencies

In this project, we'll use some external libs. You can see them below.

- [Monolog](https://github.com/Seldaek/monolog): You can access to the MIT
  license [here](https://github.com/Seldaek/monolog/blob/main/LICENSE)
- [Symfony YAML](https://github.com/symfony/yaml): You can access to the MIT
  license [here](https://github.com/symfony/yaml?tab=MIT-1-ov-file#readme)

## Installation

***Coming soon***

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)

## Contributors

- [Maxime Mazet (ElBidouilleur)](https://gitlab.com/ElBidouilleur)
