<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigBuilder.php
 * @Created_at  : 12/10/2018
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Config\Builder;

use Luna\Component\Bag\BagInterface;
use Luna\Config\Config;
use Luna\Config\LunaConfig;
use Luna\Config\Resolver\ConfigResolver;
use Luna\Environment\EnvironmentInterface;

class ConfigBuilder
{
    # --------------------------------
    # Core methods

    /**
     * Build config.
     *
     * @param BagInterface|array        $data
     * @param EnvironmentInterface|null $environment
     *
     * @return Config
     */
    public static function build(
        BagInterface|array    $data = [],
        ?EnvironmentInterface $environment = null
    ): Config {
        return new Config(
            data       : $data,
            environment: $environment
        );
    }

    /**
     * Build config from files.
     *
     * @see ConfigResolver::resolveFiles() for more information
     *
     * @param string                    $path
     * @param BagInterface|array        $files
     * @param EnvironmentInterface|null $environment
     *
     * @return Config
     */
    public static function buildFromFiles(
        string                $path,
        BagInterface|array    $files,
        ?EnvironmentInterface $environment = null
    ): Config {
        // ----------------
        // Process

        // Build config
        $config = self::build(
            environment: $environment
        );

        // Load files
        $config->load(
            path : $path,
            files: $files
        );

        return $config;
    }

    /**
     * Build luna configuration.
     *
     * @see ConfigResolver::resolveFiles() for more information
     *
     * @param EnvironmentInterface|null $environment
     *
     * @return LunaConfig
     */
    public static function buildLuna(
        ?EnvironmentInterface $environment = null
    ): LunaConfig {
        // ----------------
        // Process

        // Build config
        $config = new LunaConfig(
            environment: $environment
        );

        // Load Luna configuration directory (Base)
        $config->load(
            path : LUNA_CONFIG_DIR
        );

        return $config;
    }
}
