<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigResolver.php
 * @Created_at  : 18/07/2024
 * @Update_at   : 28/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Config\Resolver;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Bag\Bag;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Config\Reader\ConfigReader;

class ConfigResolver
{
    # --------------------------------
    # Constants

    /**
     * Available file extensions.
     *
     * Extensions are also classified by index.
     * The higher the index, the more important the extension.
     */
    public const AVAILABLE_EXTENSIONS = [
        'php',
        'json',
        'yaml'
    ];

    # --------------------------------
    # Attributes

    protected ConfigReader $reader;

    # --------------------------------
    # Constructor

    /**
     * ConfigResolver constructor.
     *
     * @param ConfigReader $reader
     */
    public function __construct(
        ConfigReader $reader
    ) {
        // Set attributes
        $this->reader = $reader;
    }

    # --------------------------------
    # Core methods

    /**
     * Resolve files.
     * Detect automatically the file extension.
     *
     * Depending on the file extension, the system will try to analyze the file differently.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $rootPath = '/var/www/html/config';
     * $files = [
     *      'config',
     *      'Routing'           => 'routing',
     *      'Services'          => 'services/services',
     *      'Services.Bridge'   => 'services/bridge',
     * ];
     *
     * // Resolve files
     * $settings = $resolver->resolveFiles($rootPath, $files);
     *
     * // Output is bag with this structure :
     * // [
     * //   ...,                        // Configuration of 'config' file
     * //   'Routing' => [ ... ],       // Configuration of 'routing' file
     * //   'Services' => [
     * //       ...,                    // Configuration of 'services/services' file
     * //       'Bridge' => [ ... ],    // Configuration of 'services/bridge' file
     * //   ]
     * // ]
     * ```
     *
     * @param string             $path
     * @param BagInterface|array $files
     *
     * @return BagInterface
     */
    public function resolveFiles(
        string             $path,
        BagInterface|array $files
    ): BagInterface {
        // ----------------
        // Vars

        // Attributes
        $files = ValueManager::getArray($files);

        // Get information
        $settings = new Bag();

        // ----------------
        // Process

        // Sanitize path
        $path = $this->sanitizePath($path);

        // Security check : Path doesn't exist
        if (!FileManager::exists($path)) return $settings;

        // Browse files
        foreach ($files as $key => $file) {
            // Get information
            $_file = ValueManager::getString($file);
            $_path = ValueManager::join(
                value    : [$path, $_file],
                separator: DIRECTORY_SEPARATOR
            );

            // Resolve file & compute extracted config
            $_config   = $this->resolveFile($_path);
            $_settings = $this->generateSettings($key, $_config);

            // Merge into settings
            $settings->replaceRecursive($_settings->all());
        }

        return $settings;
    }

    /**
     * Resolve file.
     * Detect automatically the file extension and read the file.
     *
     * Depending on the file extension, the system will try to analyze the file differently.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     *
     * // Returns an instance of the bag
     * $config = $resolver->resolveFile('./config/services');
     * ```
     *
     * @see self::resolvePath() for more information
     *
     * @param string $path
     *
     * @return BagInterface
     */
    public function resolveFile(string $path): BagInterface
    {
        // ----------------
        // Process

        // Resolve the real file path
        $realPath = $this->resolvePath($path);

        return $this->reader->read($realPath);
    }

    /**
     * Resolve the file path.
     * Returns the real file path.
     *
     * Depending on the file extension, the system will try to analyze the file differently.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $path     = './config/my-config';
     *
     * // Case : Is a PHP file
     * $realPath  = $resolver->resolveFile($path);
     * var_dump($realPath);
     * // Output : './config/my-config.php'
     *
     * // Case : Is a JSON file
     * $realPath  = $resolver->resolveFile($path);
     * var_dump($realPath);
     * // Output : './config/my-config.json'
     *
     * // Case : Is a YAML file
     * $realPath  = $resolver->resolveFile($path);
     * var_dump($realPath);
     * // Output : './config/my-config.yaml'
     * ```
     *
     * @see self::AVAILABLE_EXTENSIONS for available extensions & priority order
     *
     * @param string $path
     *
     * @return string|null
     */
    public function resolvePath(string $path): ?string
    {
        // ----------------
        // Vars

        // Get information
        $extensions = self::AVAILABLE_EXTENSIONS;
        $realPath   = null;

        // ----------------
        // Process

        // Sanitize path
        $path = $this->sanitizePath($path);

        // Generate the real path
        foreach ($extensions as $extension) {
            // Build real path
            $_realPath = "{$path}.{$extension}";

            // Break condition : File exist
            if (FileManager::exists($_realPath)) {
                $realPath = $_realPath;
                break;
            }
        }

        return $realPath;
    }

    # --------------------------------
    # Utils methods

    /**
     * Sanitize the given path.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $path1    = './good/path';
     * $path2    = './slash-terminated/path/';
     * $path3    = '.\path\with\slashes\different\from\system';
     *
     * // Sanitize
     * $path1 = $resolver->sanitizePath($path1);
     * $path2 = $resolver->sanitizePath($path2);
     * $path3 = $resolver->sanitizePath($path3);
     *
     * // Outputs :
     * // - For '$path1' => './good/path'
     * // - For '$path2' => './slash-terminated/path'
     * // - For '$path3' => './path/with/slashes/different/from/system'
     * ```
     *
     * @param string $path
     *
     * @return BagInterface
     */
    protected function sanitizePath(string $path): string
    {
        // ----------------
        // Process

        // Sanitize path
        $path = ValueManager::trim($path);
        $path = ValueManager::regexp_replace('/[\/\\\]/', DIRECTORY_SEPARATOR, $path);
        $path = ValueManager::rtrim($path, DIRECTORY_SEPARATOR);

        return $path;
    }

    /**
     * Generate correct settings structure.
     * Builds a tree from the key to contain the given configuration.
     * When the key is an integer, the prepare process ignore the key and return the config as a root config.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $key      = 'Test.Multiple.Keys'
     * $config   = ['welcome-message' => 'Hello World !'];
     *
     * // Generate settings
     * $settings = $resolver->generateSettings($key, $config);
     *
     * // Output is bag with this structure :
     * // [
     * //   'Test' => [
     * //       'Multiple' => [
     * //           'Keys' => ['welcome-message' => 'Hello World !']
     * //       ]
     * //   ]
     * // ]
     * ```
     *
     * @param int|string         $key
     * @param BagInterface|array $config
     *
     * @return BagInterface
     */
    protected function generateSettings(
        int|string         $key,
        BagInterface|array $config
    ): BagInterface {
        // ----------------
        // Vars

        // Attributes
        $config = ValueManager::getArray($config);

        // ----------------
        // Process

        // Case : The key is an integer or empty
        if (TypeManager::isInteger($key) || TypeManager::isEmpty($key)) {
            return new Bag($config);
        }

        // Generate settings
        $settings = new Bag();

        // Set config in settings
        $settings->set(
            key  : $key,
            value: $config
        );

        return $settings;
    }
}
