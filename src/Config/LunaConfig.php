<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaConfig.php
 * @Created_at  : 11/11/2018
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Config;

use Luna\Component\Bag\BagInterface;
use Luna\Environment\EnvironmentInterface;

class LunaConfig extends Config
{
    # --------------------------------
    # Constants

    /**
     * Luna basic configuration files.
     */
    public const BASIC_FILES = [
        'config',
        'Services'         => 'services/services',
        'Services.Bridge'  => 'services/bridge',
        'Services.DI'      => 'services/di',
        'Services.Handler' => 'services/handler'
    ];

    # --------------------------------
    # Constructor

    public function __construct(
        BagInterface|array    $data = [],
        ?EnvironmentInterface $environment = null
    ) {
        // Call parent constructor
        parent::__construct(
            data       : $data,
            environment: $environment
        );

        // Setup files
        $this
            ->setFiles(files: self::BASIC_FILES)
            ->setPaths(paths: [])
        ;
    }

    # --------------------------------
    # Getters

    /**
     * Get the dependency injector settings.
     *
     * @param string|null $key
     * @param null        $default
     *
     * @return mixed
     */
    public function getDI(?string $key = null, $default = null): mixed
    {
        // ----------------
        // Process

        // Build key
        $key = $this->computeKey(
            group: 'Services.DI',
            key  : $key
        );

        return $this->get($key, $default);
    }

    /**
     * Get the handler settings.
     *
     * @param string|null $key
     * @param null        $default
     *
     * @return mixed
     */
    public function getHandler(?string $key = null, $default = null): mixed
    {
        // ----------------
        // Process

        // Build key
        $key = $this->computeKey(
            group: 'Services.Handler',
            key  : $key
        );

        return $this->get($key, $default);
    }

    # --------------------------------
    # Utils methods

    /**
     * Compute key.
     * Add the group before the key.
     *
     * @param string      $group
     * @param null|string $key
     *
     * @return string
     */
    protected function computeKey(string $group, ?string $key = null): string
    {
        // ----------------
        // Vars

        // Get information
        $fullKey = $group;

        // ----------------
        // Process

        // If the key isn't null
        if (!is_null($key)) {
            $fullKey .= ".{$key}";
        }

        return $fullKey;
    }
}
