<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Config.php
 * @Created_at  : 24/11/2016
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Config;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;
use Luna\Config\Reader\ConfigReader;
use Luna\Config\Resolver\ConfigResolver;
use Luna\Environment\EnvironmentInterface;

class Config extends AbstractConfig
{
    # --------------------------------
    # Attributes

    /**
     * The config reader.
     * It's a mandatory component of the resolver.
     */
    protected ConfigReader $reader;

    /**
     * The config resolver.
     * It's used to read the given path and extract its content.
     */
    protected ConfigResolver $resolver;

    # --------------------------------
    # Constructor

    /**
     * Config constructor.
     *
     * @param BagInterface|array        $data
     * @param EnvironmentInterface|null $environment
     */
    public function __construct(
        BagInterface|array    $data = [],
        ?EnvironmentInterface $environment = null
    ) {
        // Call parent constructor
        parent::__construct(
            data       : $data,
            environment: $environment
        );

        // Set services
        $this->reader   = new ConfigReader();
        $this->resolver = new ConfigResolver($this->reader);
    }

    # --------------------------------
    # Utils methods

    /**
     * @inheritDoc
     */
    public function refresh(bool $hardReset = false): static
    {
        // ----------------
        // Vars

        // Get information
        $files = $this->getFiles();
        $paths = $this->getPaths();

        // ----------------
        // Process

        // Reset the configuration
        if ($hardReset) {
            $this->clear();
        }

        // Browse paths
        foreach ($paths as $path) {
            // Reload configuration
            $this->load(
                path : $path,
                files: $files
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function load(
        string             $path,
        BagInterface|array $files = []
    ): static {
        // ----------------
        // Vars

        // Get configuration files
        $_files = $this->getFiles();

        // ----------------
        // Process

        // Sanitize path
        $path = PathManager::sanitize(path: $path);

        // Security check : Path doesn't exist
        if (!FileManager::exists($path)) return $this;

        // Set information
        $this
            ->addFiles(files: $files)
            ->addPath(path: $path)
        ;

        // Resolve files
        $data = $this->resolver->resolveFiles(
            path : $path,
            files: TypeManager::isEmpty($files) ? $_files : $files
        );

        // Merges with current configuration
        $this->replaceRecursive($data->all());

        return $this;
    }
}
