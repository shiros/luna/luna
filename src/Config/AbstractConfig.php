<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AbstractConfig.php
 * @Created_at  : 11/11/2018
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Config;

use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Environment\EnvironmentInterface;

abstract class AbstractConfig extends Bag implements ConfigInterface
{
    # --------------------------------
    # Attributes

    /**
     * Environment instance.
     * It used to build config values.
     */
    protected ?EnvironmentInterface $environment;

    /**
     * Configuration files.
     * Files represents the structure of the configuration.
     *
     * @var BagInterface<string, string>
     */
    protected BagInterface $files;

    /**
     * Configuration paths.
     * Paths are used to load configurations.
     *
     * @var BagInterface<string>
     */
    protected BagInterface $paths;

    # --------------------------------
    # Constructor

    /**
     * Config constructor.
     *
     * @param BagInterface|array        $data
     * @param EnvironmentInterface|null $environment
     */
    public function __construct(
        BagInterface|array    $data = [],
        ?EnvironmentInterface $environment = null
    ) {
        // Call parent constructor
        parent::__construct($data);

        // Set attributes
        $this
            ->setEnvironment($environment)
            ->setFiles(files: [])
            ->setPaths(paths: [])
        ;
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     */
    public function buildValue(mixed $value): mixed
    {
        // ----------------
        // Process

        return match (true) {
            // Case : If it's an array
            TypeManager::isArray($value)  => $this->computeAsArray($value),

            // Case : If it's a string
            TypeManager::isString($value) => $this->computeAsString($value),

            // Case : Default
            default                       => $value
        };
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function get(mixed $key, $default = null): mixed
    {
        // Get value
        $value = parent::get($key, $default);

        // Build
        return $this->buildValue($value);
    }

    /**
     * @inheritDoc
     */
    public function getEnvironment(): ?EnvironmentInterface
    {
        return $this->environment;
    }

    /**
     * @inheritDoc
     */
    public function getFiles(): BagInterface
    {
        return $this->files;
    }

    /**
     * @inheritDoc
     */
    public function getPaths(): BagInterface
    {
        return $this->paths;
    }

    # --------------------------------
    # Setters

    /**
     * @inheritDoc
     */
    public function setEnvironment(?EnvironmentInterface $environment): static
    {
        $this->environment = $environment;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setFiles(BagInterface|array $files): self
    {
        $this->files = ValueManager::getBag(value: $files);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addFiles(BagInterface|array $files): self
    {
        // ----------------
        // Process

        // Security check : Empty files
        if (TypeManager::isEmpty(value: $files)) return $this;

        // Get files as array
        $files = ValueManager::getArray($files);

        // Add files to the current configuration
        $this->files
            ->replace($files)
            ->unique()
        ;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPaths(BagInterface|array $paths): self
    {
        $this->paths = ValueManager::getBag(value: $paths);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addPaths(BagInterface|array $paths): self
    {
        // ----------------
        // Process

        // Security check : Empty paths
        if (TypeManager::isEmpty(value: $paths)) return $this;

        // Get paths as array
        $paths = ValueManager::getArray($paths);

        // Add paths
        foreach ($paths as $path) {
            $this->files->push(
                value: ValueManager::getString($path)
            );
        }

        // Apply the uniqueness constraint
        $this->paths->unique();

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addPath(string $path): self
    {
        // ----------------
        // Process

        // Sanitize path
        $path = PathManager::sanitize(path: $path);

        // Security check : Empty path
        if (TypeManager::isEmpty(value: $path)) return $this;

        // Add paths
        $this->paths
            ->push($path)
            ->unique()
        ;

        return $this;
    }

    # --------------------------------
    # Process methods

    /**
     * Compute : Array value.
     * If the value match with an array structure, prepare array correctly.
     *
     * @param array $value
     *
     * @return array
     */
    protected function computeAsArray(array $value): array
    {
        // ----------------
        // Vars

        $computedValue = [];

        // ----------------
        // Process

        // Compute
        foreach ($value as $key => $item) {
            $computedValue[$key] = $this->buildValue($item);
        }

        return $computedValue;
    }

    /**
     * Compute : String value.
     * If the value match with the environment key, get the corresponding environment variable.
     *
     * @param string $value
     *
     * @return mixed
     */
    protected function computeAsString(string $value): mixed
    {
        // ----------------
        // Vars

        // Get information
        $key         = ValueManager::replace('%', '', $value);
        $environment = $this->getEnvironment();

        // ----------------
        // Process

        // Security check : Environment instance
        if (TypeManager::isNull($environment)) {
            return $value;
        }

        // Security check : Value
        if (!ValueManager::match(self::REGEXP_ENV_KEY, $value)) {
            return $value;
        }

        // Get the environment value
        return $environment->get($key, $value);
    }
}
