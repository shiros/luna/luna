<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AbstractConfig.php
 * @Created_at  : 24/05/2024
 * @Update_at   : 24/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Config;

use Luna\Component\Bag\BagInterface;
use Luna\Environment\EnvironmentInterface;

interface ConfigInterface extends BagInterface
{
    # --------------------------------
    # Constants

    public const REGEXP_ENV_KEY = '/^%.*%$/';

    # --------------------------------
    # Core methods

    /**
     * Get a value in the environment parameters.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function buildValue(mixed $value): mixed;

    # --------------------------------
    # Getters

    /**
     * Get environment.
     *
     * @return ?EnvironmentInterface|null
     */
    public function getEnvironment(): ?EnvironmentInterface;

    /**
     * Returns configuration files.
     *
     * @return BagInterface
     */
    public function getFiles(): BagInterface;

    /**
     * Returns configuration paths.
     *
     * @return BagInterface
     */
    public function getPaths(): BagInterface;

    # --------------------------------
    # Setters

    /**
     * Set environment.
     *
     * @param EnvironmentInterface|null $environment
     *
     * @return self
     */
    public function setEnvironment(?EnvironmentInterface $environment): self;

    /**
     * Set configuration files.
     * Files represents the structure of the configuration.
     *
     * **WARNING :** This setter will replace all the configuration files to be used.
     *
     * @param BagInterface|array $files
     *
     * @return self
     */
    public function setFiles(BagInterface|array $files): self;

    /**
     * Add new configuration files.
     *
     * **WARNING :** If a file key exists in the current configuration, the file will be overwritten.
     *
     * @param BagInterface|array $files
     *
     * @return self
     */
    public function addFiles(BagInterface|array $files): self;

    /**
     * Set configuration paths.
     * Paths are used to load configurations.
     *
     * **WARNING :** This setter will replace all the configuration paths to be used.
     *
     * @param BagInterface|array $paths
     *
     * @return self
     */
    public function setPaths(BagInterface|array $paths): self;

    /**
     * Add new configuration paths.
     *
     * @param BagInterface|array $paths
     *
     * @return self
     */
    public function addPaths(BagInterface|array $paths): self;

    /**
     * Add new configuration path.
     *
     * @param string $path
     *
     * @return self
     */
    public function addPath(string $path): self;

    # --------------------------------
    # Utils methods

    /**
     * Refresh the configuration.
     * If the 'hardReset' parameter is supplied, it will reset and refresh the configuration.
     * Any manual additions will be deleted.
     *
     * @param bool $hardReset
     *
     * @return self
     */
    public function refresh(bool $hardReset = false): static;

    /**
     * Load another configuration.
     * Resolves files and merges retrieved configuration with current configuration.
     *
     * By default, if no files are supplied, the configuration files will be used.
     *
     * Usage :
     * ```
     * // Get config
     * $config = ...;
     *
     * // Load new configuration
     * $config->load(
     *   path : '/var/www/html/config-2'
     *   files: [
     *      'config',
     *      'Services'         => 'services/services',
     *      'Services.Bridge'  => 'services/bridge',
     *      'Services.DI'      => 'services/di',
     *      'Services.Handler' => 'services/handler',
     *   ]
     * );
     *  ```
     *
     * @see ConfigResolver::resolveFiles()
     *
     * @param string             $path
     * @param BagInterface|array $files
     *
     * @return static
     */
    public function load(
        string             $path,
        BagInterface|array $files = []
    ): static;
}
