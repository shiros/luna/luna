<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : BridgeInterface.php
 * @Created_at  : 01/06/2019
 * @Update_at   : 08/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Bridge;

interface BridgeInterface
{
    # --------------------------------
    # Core methods

    /**
     * Launch the bridge process.
     *
     * This process allows the redefinition of components of the framework or project.
     * It's also called by the bridge constructor.
     *
     * ### Usage
     *
     * It's possible to call the 'boot' method in different ways.
     *
     * _**By arguments**_
     * ```
     * $bridge->boot($myArgument1, $myArgument2);
     * ```
     *
     * _**By named arguments**_
     * ```
     * $bridge->boot(
     *     argument1: $myArgument1,
     *     argument2: $myArgument2
     * );
     * ```
     *
     * _**By an array**_
     * ```
     * $bridge->boot(...[
     *     argument1 => $myArgument1,
     *     argument2 => $myArgument2
     * ]);
     * ```
     *
     * @param ...$arguments // Use to build the class. This is the constructor arguments
     *
     * @return void
     */
    public function boot(...$arguments): void;

    /**
     * Call a class's method managed by the bridge.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function call(string $name, array $arguments): mixed;

    /**
     * Call a static class's method managed by the bridge.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function callStatic(string $name, array $arguments): mixed;

    # --------------------------------
    # Getters

    /**
     * Get the configuration key used to retrieve the class.
     *
     * @return string
     */
    public static function getKey(): string;

    /**
     * Get the interface managed by the bridge.
     *
     * @return string
     */
    public static function getInterface(): string;

    /**
     * Get the class managed by the bridge.
     * If no class has been defined in the configuration, an exception will be thrown.
     *
     * @return string
     */
    public static function getClass(): string;

    /**
     * Get the class instance managed by the bridge.
     *
     * @return mixed
     */
    public function getInstance(): mixed;

    # --------------------------------
    # Magic methods

    /**
     * Fallback for undefined methods.
     * Use the 'call' method to invoke the provided method.
     *
     * @see BridgeInterface::call()
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call(string $name, array $arguments): mixed;

    /**
     * Fallback for undefined static methods.
     * Use the 'callStatic' method to invoke the provided method.
     *
     * @see BridgeInterface::callStatic()
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments): mixed;
}
