<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Bridge.php
 * @Created_at  : 14/03/2018
 * @Update_at   : 08/12/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Bridge;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\InterfaceManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Config\LunaConfig;
use Luna\Exception\Bridge\BridgeException;
use ReflectionException;

abstract class Bridge implements BridgeInterface
{
    # --------------------------------
    # Attributes

    protected string $interface;
    protected string $class;
    protected mixed  $instance;

    # --------------------------------
    # Constructor

    /**
     * Bridge constructor.
     *
     * @see BridgeInterface::boot() It's explained how to give the arguments.
     *
     * @param mixed ...$arguments
     *
     * @throws BridgeException
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function __construct(...$arguments)
    {
        // Launch bridge process
        $this->boot(...$arguments);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws BridgeException
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function boot(...$arguments): void
    {
        // ----------------
        // Vars

        // Get information
        $interface = static::getInterface();
        $class     = static::getClass();

        // ----------------
        // Process

        // Check information
        static::checkInterface($interface);
        static::checkClass($class, $interface);

        // Set information
        $this->interface = $interface;
        $this->class     = $class;

        // Build instance
        $this->instance = DependencyInjector::callConstructor($class, $arguments);
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function call(
        string                  $name,
        BagInterface|array|null $arguments = null
    ): mixed {
        // ----------------
        // Process

        return DependencyInjector::callMethod(
            method       : $name,
            objectOrClass: $this->instance,
            arguments    : $arguments
        );
    }

    /**
     * @inheritDoc
     *
     * @throws BridgeException
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public static function callStatic(
        string                  $name,
        BagInterface|array|null $arguments = null
    ): mixed {
        // ----------------
        // Vars

        // Get information
        $interface = static::getInterface();
        $class     = static::getClass();

        // ----------------
        // Process

        // Check information
        static::checkInterface($interface);
        static::checkClass($class, $interface);

        // Call static method
        return DependencyInjector::callMethod(
            method       : $name,
            objectOrClass: $class,
            arguments    : $arguments
        );
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public abstract static function getKey(): string;

    /**
     * @inheritDoc
     */
    public abstract static function getInterface(): string;

    /**
     * @inheritDoc
     *
     * @throws BridgeException
     * @throws ContainerException
     */
    public static function getClass(): string
    {
        // ----------------
        // Vars

        // Get bridge key
        $bridgeKey = static::getKey();

        // Get luna information
        $config = static::getConfig();

        // ----------------
        // Process

        // Get class
        $class = $config->getAsString(key: $bridgeKey, default: null);

        // Security check : No class provided
        if (TypeManager::isEmpty($class)) {
            throw new BridgeException(
                message   : "You must provide a valid class to use the bridge system.",
                parameters: ['key' => $bridgeKey]
            );
        }

        return $class;
    }

    public function getInstance(): mixed
    {
        return $this->instance;
    }

    /**
     * Get the bridge config.
     *
     * @return LunaConfig
     * @throws ContainerException
     */
    protected static function getConfig(): BagInterface
    {
        return ValueManager::getBag(
            value: LunaContainer
                ::getInstance()
                ->getConfig()
        );
    }

    # --------------------------------
    # Utils methods

    /**
     * Check interface information.
     *
     * @param string $interface
     *
     * @return void
     * @throws BridgeException
     */
    protected static function checkInterface(string $interface): void
    {
        // ----------------
        // Process

        if (!InterfaceManager::exist($interface)) {
            throw new BridgeException(
                message   : "The provided interface '{$interface}' doesn't exist.",
                parameters: ['interface' => $interface]
            );
        }
    }

    /**
     * Check class information.
     *
     * @param string $class
     * @param string $interface
     *
     * @return void
     * @throws BridgeException
     */
    protected static function checkClass(string $class, string $interface): void
    {
        // ----------------
        // Process

        // Security check : Class doesn't exist
        if (!ClassManager::exist($class)) {
            throw new BridgeException(
                message   : "The provided class '{$class}' doesn't exist.",
                parameters: ['class' => $class]
            );
        }

        // Security check : Class doesn't implement interface
        if (!ClassManager::implement($interface, $class)) {
            throw new BridgeException(
                message   : "The provided class '{$class}' doesn't implement '{$interface}'.",
                parameters: [
                    'class'     => $class,
                    'interface' => $interface,
                ]
            );
        }
    }

    # --------------------------------
    # Magic methods

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function __call(string $name, array $arguments): mixed
    {
        return $this->call(
            name     : $name,
            arguments: $arguments
        );
    }

    /**
     * @inheritDoc
     *
     * @throws BridgeException
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public static function __callStatic(string $name, array $arguments): mixed
    {
        return static::callStatic(
            name     : $name,
            arguments: $arguments
        );
    }
}
