<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaConstant.php
 * @Created_at  : 18/07/2021
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Constant;

class LunaConstant
{
    # --------------------------------
    # Regexp

    /**
     * Regexp : Luna Root
     * Extracts the framework's root path.
     */
    public const REGEXP_LUNA_ROOT = '#(\\\|/)src([^<]*)$#';

    /**
     * Regexp : Application Root
     * Extracts the application's root path.
     */
    public const REGEXP_APPLICATION_ROOT = '#(\\\|/)vendor(\\\|/)([^<]*)$#';

    /**
     * Regexp : LOCK file
     * Checks whether the provided value is a LOCK file.
     */
    public const REGEXP_LOCK_FILE = '/^.*\.lock$/';

    /**
     * Regexp : PHP file
     * Checks whether the provided value is a PHP file.
     */
    public const REGEXP_PHP_FILE = '/^.*\.php$/';

    /**
     * Regexp : JSON file
     * Checks whether the provided value is a JSON file.
     */
    public const REGEXP_JSON_FILE = '/^.*\.json$/';

    /**
     * Regexp : YAML file
     * Checks whether the provided value is a YAML file.
     */
    public const REGEXP_YAML_FILE = '/^.*\.(yml|yaml)$/';

    /**
     * Regexp : URL
     * Checks whether the provided value is a URL.
     */
    public const REGEXP_URL = '/^(http|ftp)s?:\/\/.*$/';
}
