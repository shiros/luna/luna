<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Kernel.php
 * @Created_at  : 03/12/2017
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\Dispatcher\Dispatcher;
use Luna\Component\Dispatcher\DispatcherInterface;
use Luna\Component\Kernel\Resolver\KernelResolver;
use Luna\Component\Lock\LunaLock;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;
use Luna\Component\Module\Exception\ModuleException;
use Luna\Component\Module\LunaModule;
use Luna\Constant\LunaConstant;
use Luna\Environment\EnvironmentInterface;
use Luna\Exception\KernelException;
use ReflectionException;
use Throwable;

class Kernel implements KernelInterface
{
    # --------------------------------
    # Attributes

    /**
     * Kernel started time.
     */
    protected float $startTime;

    /**
     * Kernel options.
     *
     * It may include :
     * - A specific environment
     * - An additional configuration
     */
    protected BagInterface $options;

    /**
     * Kernel resolver.
     *
     * Allow the kernel to resolve some components.
     */
    protected KernelResolver $resolver;

    /**
     * Luna environment.
     *
     * It contains environment variables.
     */
    protected EnvironmentInterface $environment;

    /**
     * Luna lock file.
     *
     * It contains runtime configuration.
     */
    protected LunaLock $lock;

    /**
     * Luna module.
     *
     * This is the main module that loads registered modules.
     */
    protected LunaModule $module;

    /**
     * Luna container.
     *
     * The container contains all Luna's basic components.
     * These components are instantiated only once.
     * They can be accessed from anywhere.
     */
    protected LunaContainer $container;

    /**
     * Luna dispatcher.
     *
     * The dispatcher is used to emit various events within the framework.
     */
    protected DispatcherInterface $dispatcher;

    # --------------------------------
    # Constructor

    /**
     * Kernel constructor.
     *
     * @param BagInterface|array|null $options
     *
     * @throws KernelException
     */
    public function __construct(BagInterface|array|null $options = null)
    {
        // Init kernel system
        $this->init($options);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws KernelException
     */
    public function init(BagInterface|array|null $options = null): self
    {
        // ----------------
        // Process

        try {
            // Set attributes
            $this->startTime = microtime(as_float: true);
            $this->options   = ValueManager::getBag($options);
            $this->resolver  = new KernelResolver();

            // Resolve primary components
            $this->environment = $this->resolver->resolveEnvironment(
                applicationPath: $this->getAppRoot(),
                value          : $this->options->get(key: self::OPT_ENVIRONMENT)
            );
            $this->lock        = $this->resolver->resolveLock(
                applicationPath: $this->getAppRoot(),
                value          : $this->options->get(key: self::OPT_LOCK)
            );

            // Initialize components
            $this
                ->initializeConstants()
                ->initializeComponents()
                ->initializeModules()
            ;
        } catch (Throwable $throwable) {
            throw new KernelException(
                message   : "Failed to initialize kernel",
                parameters: [
                    'options' => $options
                ],
                previous  : $throwable
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function start(): self
    {
        // ----------------
        // Process

        try {
            $this->module->start();
        } catch (Throwable $throwable) {
            $this->dispatcher->dispatch($throwable);
        }

        return $this;
    }

    # --------------------------------
    # Init methods

    /**
     * Initialize constants.
     *
     * Constants loaded :
     *  - LUNA constants
     *  - Application constants
     *
     * @return self
     */
    protected function initializeConstants(): self
    {
        // ----------------
        // Process

        // Generate constants
        $constants = [
            // Luna
            'LUNA_ROOT'       => $this->getLunaRoot(),
            'LUNA_CONFIG_DIR' => $this->getLunaConfigDir(),

            // Application
            'APP_ROOT'        => $this->getAppRoot(),
            'APP_ENV'         => $this->getEnvironment()->getEnv(),
            'APP_CONFIG_DIR'  => $this->getAppConfigDir(),
            'APP_LOG_DIR'     => $this->getAppLogDir()
        ];

        // Initialize constants
        foreach ($constants as $key => $value) {
            // Security check : The constant is already defined
            if (defined($key)) continue;

            // Defines constant
            define($key, $value);
        }

        return $this;
    }

    /**
     * Initialize Luna components.
     *  - Container
     *  - Unique instances to add in the container
     *
     * @return self
     * @throws ContainerException
     */
    protected function initializeComponents(): self
    {
        // ----------------
        // Process

        // Creates basic components
        $config     = $this->resolver->resolveConfiguration(environment: $this->environment);
        $logger     = $this->resolver->resolveLogger(path: $this->getAppLogDir());
        $dispatcher = new Dispatcher();

        // Create container
        $this->container = LunaContainer::build([
            // Base
            LunaContainer::KEY_KERNEL      => $this,
            LunaContainer::KEY_ENVIRONMENT => $this->environment,
            LunaContainer::KEY_LOCK        => $this->lock,

            // Components
            LunaContainer::KEY_CONFIG      => $config,
            LunaContainer::KEY_LOGGER      => $logger,
            LunaContainer::KEY_DISPATCHER  => $dispatcher,
        ], true);

        // Set kernel attributes
        $this->dispatcher = $dispatcher;
        $this->module     = new LunaModule(
            container: $this->container,
            lock     : $this->lock
        );

        return $this;
    }

    /**
     * Initialize Luna's modules.
     *
     * These modules are instantiated only once.
     * They are contained in the Luna container.
     *  - Key   : LunaContainer::KEY_MODULES
     *  - Value : BagInterface<string, ModuleInterface>
     *
     * **WARNING :** If the kernel is in standalone mode, no module will be initialized.
     *
     * @see LunaContainer
     * @see LunaModule
     *
     * @return self
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ModuleException
     * @throws ReflectionException
     */
    protected function initializeModules(): self
    {
        // ----------------
        // Process

        // Security check : Standalone mode
        if ($this->isStandalone()) return $this;

        // Boot module
        $this->module->boot();

        // Set initialized modules
        $this->container->set(
            key  : LunaContainer::KEY_MODULES,
            value: $this->module->getModules()
        );

        return $this;
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function isStandalone(): bool
    {
        return $this->options->getAsBoolean(key: self::OPT_STANDALONE);
    }

    /**
     * @inheritDoc
     */
    public function getLunaRoot(): string
    {
        return ValueManager::regexp_replace(
            pattern: LunaConstant::REGEXP_LUNA_ROOT,
            replace: '',
            value  : __DIR__
        );
    }

    /**
     * @inheritDoc
     */
    public function getLunaConfigDir(): string
    {
        return PathManager::sanitize(
            path: "{$this->getLunaRoot()}/config"
        );
    }

    /**
     * @inheritDoc
     */
    public function getAppRoot(): string
    {
        return ValueManager::regexp_replace(
            pattern: LunaConstant::REGEXP_APPLICATION_ROOT,
            replace: '',
            value  : $this->getLunaRoot()
        );
    }

    /**
     * @inheritDoc
     */
    public function getAppConfigDir(): string
    {
        return PathManager::sanitize(
            path: "{$this->getAppRoot()}/config"
        );
    }

    /**
     * @inheritDoc
     */
    public function getAppLogDir(): string
    {
        return PathManager::sanitize(
            path: "{$this->getAppRoot()}/var/log/{$this->environment->getEnv()}"
        );
    }

    /**
     * @inheritDoc
     */
    public function getContainer(): LunaContainer
    {
        return $this->container;
    }

    /**
     * @inheritDoc
     */
    public function getEnvironment(): EnvironmentInterface
    {
        return $this->environment;
    }

    /**
     * @inheritDoc
     */
    public function getLock(): LunaLock
    {
        return $this->lock;
    }
}
