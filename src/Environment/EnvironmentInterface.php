<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentInterface.php
 * @Created_at  : 06/01/2023
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Environment;

use Luna\Component\Bag\BagInterface;

interface EnvironmentInterface extends BagInterface
{
    # --------------------------------
    # Constants

    // Environments
    public const PRODUCTION     = 'production';
    public const PRE_PRODUCTION = 'pre-production';
    public const DEVELOPMENT    = 'development';
    public const TEST           = 'test';

    // Keys
    public const KEY_ENVIRONMENT = 'Environment';

    // Keywords
    public const KEYWORDS_PRODUCTION     = [
        'production',
        'prod'
    ];
    public const KEYWORDS_PRE_PRODUCTION = [
        'pre-production',
        'preproduction',
        'pre-prod',
        'preprod'
    ];
    public const KEYWORDS_DEVELOPMENT    = [
        'development',
        'dev'
    ];
    public const KEYWORDS_TEST           = [
        'test'
    ];

    # --------------------------------
    # Getters

    /**
     * Get environment.
     *
     * If the defined environment matches the keywords, the corresponding environment constant will be returned.
     * By default, if no environment is given, the development environment will be returned.
     *
     * @return string
     */
    public function getEnv(): string;
}
