<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentBuilder.php
 * @Created_at  : 12/10/2018
 * @Update_at   : 07/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Environment\Builder;

use Luna\Component\Bag\BagInterface;
use Luna\Environment\Environment;
use Luna\Environment\Reader\EnvironmentReader;

class EnvironmentBuilder
{
    # --------------------------------
    # Core methods

    /**
     * Build environment.
     *
     * @param BagInterface|array $parameters
     *
     * @return Environment
     */
    public static function build(
        BagInterface|array $parameters = []
    ): Environment {
        return new Environment(
            parameters: $parameters
        );
    }

    /**
     * Build environment from files.
     * Browse files, if then exists try to read then and get the settings.
     *
     * @see EnvironmentReader::read() for more information
     *
     * @param string $path
     *
     * @return Environment
     */
    public static function buildFromFiles(
        string $path,
    ): Environment {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Read environment file
        $parameters = $reader->read($path);

        // Build environment
        return self::build(
            parameters: $parameters
        );
    }
}
