<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentReader.php
 * @Created_at  : 17/07/2021
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Environment\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\File\Manager\JSONFileManager;
use Luna\Component\File\Manager\PHPFileManager;
use Luna\Component\File\Manager\YAMLFileManager;
use Luna\Component\Manager\ValueManager;
use Throwable;

class EnvironmentReader
{
    # --------------------------------
    # Core methods

    /**
     * Read environment file and returns its contents.
     *
     * @param string|null $path
     *
     * @return BagInterface
     */
    public function read(?string $path): BagInterface
    {
        // ----------------
        // Process

        try {
            // Reading file
            $config = match (true) {
                FileManager::isPHPFile($path)  => PHPFileManager::read($path),
                FileManager::isJSONFile($path) => JSONFileManager::read($path, true),
                FileManager::isYAMLFile($path) => YAMLFileManager::read($path),
                default                        => []
            };
        } catch (Throwable) {
            $config = [];
        }

        return ValueManager::getBag($config);
    }
}
