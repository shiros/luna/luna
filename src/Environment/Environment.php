<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Environment.php
 * @Created_at  : 17/07/2021
 * @Update_at   : 03/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Environment;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Manager\ValueManager;

class Environment extends ReadOnlyBag implements EnvironmentInterface
{
    # --------------------------------
    # Constants

    protected const MESSAGE_IMMUTABLE_EXCEPTION = "You can't modify the environment settings.";

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getEnv(): string
    {
        // ----------------
        // Process

        // Get environment value
        $value = $this->getAsString(
            key    : static::KEY_ENVIRONMENT,
            default: static::DEVELOPMENT
        );

        // ----------------
        // Process

        // Matching process
        return match (true) {
            ValueManager::contains(static::KEYWORDS_PRODUCTION, $value)     => static::PRODUCTION,
            ValueManager::contains(static::KEYWORDS_PRE_PRODUCTION, $value) => static::PRE_PRODUCTION,
            ValueManager::contains(static::KEYWORDS_DEVELOPMENT, $value)    => static::DEVELOPMENT,
            ValueManager::contains(static::KEYWORDS_TEST, $value)           => static::TEST,
            default                                                         => $value
        };
    }
}
