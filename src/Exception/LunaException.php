<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaException.php
 * @Created_at  : 16/03/2018
 * @Update_at   : 01/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Exception;

use Exception;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Manager\JSONManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Stringable;
use Throwable;

class LunaException extends Exception implements Stringable
{
    # --------------------------------
    # Constants

    public const DEFAULT_CODE    = 1;
    public const DEFAULT_MESSAGE = 'An error is occurred during the process.';

    # --------------------------------
    # Attributes

    protected BagInterface $parameters;

    # --------------------------------
    # Constructor

    /**
     * LunaException constructor.
     *
     * @param string|null        $message
     * @param int|null           $code
     * @param BagInterface|array $parameters
     * @param Throwable|null     $previous
     */
    public function __construct(
        ?string            $message = null,
        ?int               $code = null,
        BagInterface|array $parameters = [],
        ?Throwable         $previous = null
    ) {
        // Set attributes
        $code             = ValueManager::getInteger($code, static::DEFAULT_CODE);
        $message          = ValueManager::getString($message, static::DEFAULT_MESSAGE);
        $this->parameters = ValueManager::getBag($parameters);

        // Call parent constructor
        parent::__construct($message, $code, $previous);
    }

    # --------------------------------
    # Getters

    /**
     * Get exception information.
     *
     * @return string
     */
    public function getInformation(): string
    {
        // ----------------
        // Vars

        $className = __CLASS__;

        // Exception information
        $code    = $this->code;
        $message = $this->message;
        $file    = $this->getFile();
        $line    = $this->getLine();

        // ----------------
        // Process

        return "{$className} [{$code}]: {$message} in '{$file}:{$line}'";
    }

    /**
     * Get exception parameters.
     *
     * @return BagInterface
     */
    public function getParameters(): BagInterface
    {
        return $this->parameters;
    }

    # --------------------------------
    # Stringable methods

    /**
     * Get exception's display in string form.
     *
     * @return string
     */
    public function __toString(): string
    {
        // ----------------
        // Vars

        // Constants
        $eol = PHP_EOL;

        // Exception information
        $information      = $this->getInformation();
        $parameters       = $this->getParameters();
        $traceAsString    = $this->getTraceAsString();
        $previousAsString = $this->getPrevious()?->__toString();

        // ----------------
        // Process

        // Compute information
        $parameters = JSONManager::encode($parameters);

        // Generate messages
        $messages = [
            $information,
            " {$parameters}",
            " {$traceAsString}"
        ];

        // Push previous
        if (!TypeManager::isNull($previousAsString)) {
            $messages[] = "";
            $messages[] = $previousAsString;
        }

        return ValueManager::join($messages, $eol);
    }
}
