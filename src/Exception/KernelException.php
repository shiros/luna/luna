<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KernelException.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 14/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Exception;

class KernelException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the kernel process.';
}
