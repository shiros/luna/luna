<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : BridgeException.php
 * @Created_at  : 22/03/2018
 * @Update_at   : 07/12/2022
 * ----------------------------------------------------------------
 */

namespace Luna\Exception\Bridge;

use Luna\Exception\LunaException;

class BridgeException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the bridge process.';
}
