<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : HandlerInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Exception\Handler;

use Luna\Component\Dispatcher\Entity\EventInterface;
use Luna\Component\Dispatcher\Entity\ThrowableEvent;
use Luna\Component\Handler\AbstractHandler;
use Luna\Component\Logger\Exception\LoggerException;
use Luna\Component\Manager\ClassManager;
use Luna\Exception\LunaException;

class ThrowableHandler extends AbstractHandler
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function handle(EventInterface $event): void
    {
        // ----------------
        // Process

        // Security check
        if (!ClassManager::is(ThrowableEvent::class, $event)) {
            return;
        }

        /** @var ThrowableEvent $event */
        $this->handleThrowable($event);
    }

    /**
     * Handle a throwable event.
     *
     * @param ThrowableEvent $event
     *
     * @return void
     * @throws LoggerException
     */
    public function handleThrowable(ThrowableEvent $event): void
    {
        // ----------------
        // Vars

        // Get information
        $throwable  = $event->getData();
        $parameters = $event->getParameters()?->all();
        $options    = $event->getOptions();

        /**
         * Get throwable parameters
         *
         * Use 'is_a(...)' instead of 'ClassManager::is(...)' to use auto-completion and avoid php runtime error.
         */
        $throwableParameters = match (true) {
            is_a($throwable, LunaException::class) => $throwable->getParameters()->all(),
            default                                => []
        };

        // Options
        $logLevel = $options->get(static::OPT_LOG_LEVEL, 'error');

        // ----------------
        // Process

        // Logs
        $this->logger->log($logLevel, $throwable, [
            'event_parameters'     => $parameters,
            'throwable_parameters' => $throwableParameters
        ]);
    }
}