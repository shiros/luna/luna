<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KernelInterface.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 31/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Lock\LunaLock;
use Luna\Environment\EnvironmentInterface;

interface KernelInterface
{
    # --------------------------------
    # Constants

    /**
     * Framework name.
     */
    public const NAME = 'Luna';

    /**
     * Framework version.
     */
    public const VERSION       = self::VERSION_MAJOR . '.' . self::VERSION_MINOR . '.' . self::VERSION_FIX;
    public const VERSION_MAJOR = 4;
    public const VERSION_MINOR = 0;
    public const VERSION_FIX   = 0;

    /**
     * Option : Environment
     */
    public const OPT_ENVIRONMENT = "environment";

    /**
     * Option : Lock
     */
    public const OPT_LOCK = "lock";

    /**
     * Option : Standalone.
     * By default, the standalone options is set to 'false'.
     */
    public const OPT_STANDALONE = "standalone";

    # --------------------------------
    # Core methods

    /**
     * Initialize kernel.
     *  - Constants
     *  - Components
     *  - Modules
     *
     * Options can be passed to configure the system.
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Initialize the kernel
     * $kernel->init([
     *      LunaConstant::OPT_ENVIRONMENT => 'dev'
     * ]);
     * ```
     *
     * @param BagInterface|array|null $options
     *
     * @return self
     */
    public function init(BagInterface|array|null $options = null): self;

    /**
     * Access point of the application.
     * Allow to start the routing component and settings the Luna Framework
     *
     * - DI (Dependency Injector)
     * - Routing
     * - Templating
     * - Constant
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Start the kernel
     * $kernel->start();
     * ```
     *
     * @return self
     */
    public function start(): self;

    # --------------------------------
    # Getters

    /**
     * Returns whether the kernel is in standalone mode.
     *
     * @return bool
     */
    public function isStandalone(): bool;

    /**
     * Returns the Luna kernel path.
     * This is the location of the framework kernel file. (Vendor)
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Get the luna root path
     * $path = $kernel->getLunaRoot();
     *
     * // Output will be :
     * // "<project_dir>/vendor/shiros/luna"
     * ```
     *
     * @return string
     */
    public function getLunaRoot(): string;

    /**
     * Returns the Luna configuration path.
     * This is the location of the framework configuration files. (Vendor)
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Get the luna configuration path
     * $path = $kernel->getLunaConfigDir();
     *
     * // Output will be :
     * // "<project_dir>/vendor/shiros/luna/config"
     * ```
     *
     * @return string
     */
    public function getLunaConfigDir(): string;

    /**
     * Returns the application path.
     * This is the location of the project.
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Get the application root path
     * $path = $kernel->getAppRoot();
     *
     * // Output will be :
     * // "<project_dir>"
     * ```
     *
     * @return string
     */
    public function getAppRoot(): string;

    /**
     * Returns the application path.
     * This is the location of the project.
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Get the application root path
     * $path = $kernel->getAppConfigDir();
     *
     * // Output will be :
     * // "<project_dir>/config"
     * ```
     *
     * @return string
     */
    public function getAppConfigDir(): string;

    /**
     * Returns the application path.
     * This is the location of the project.
     *
     * Usage :
     * ```
     * // Get kernel
     * $kernel = ...;
     *
     * // Get the application root path
     * $path = $kernel->getAppLogDir();
     *
     * // Output will be :
     * // "<project_dir>/var/log/<environment>"
     * ```
     *
     * @return string
     */
    public function getAppLogDir(): string;

    /**
     * Get luna container.
     * It contains the basic components of the framework.
     *
     * @return LunaContainer
     */
    public function getContainer(): LunaContainer;

    /**
     * Get luna environment.
     * It contains the environment variables.
     *
     * @return EnvironmentInterface
     */
    public function getEnvironment(): EnvironmentInterface;

    /**
     * Get luna lock.
     * It contains the runtime configuration.
     *
     * @return LunaLock
     */
    public function getLock(): LunaLock;
}
