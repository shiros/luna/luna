<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaTestCase.php
 * @Created_at  : 12/10/2018
 * @Update_at   : 01/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Test;

use LogicException;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Constant\LunaConstant;
use Luna\Exception\LunaException;
use Luna\KernelInterface;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use RuntimeException;

class LunaTestCase extends TestCase
{
    # --------------------------------
    # Constants

    /**
     * Luna kernel
     */
    protected ?KernelInterface $kernel;

    # --------------------------------
    # Core methods

    /**
     * Instantiate kernel for tests.
     *
     * @return KernelInterface
     */
    protected function bootKernel(): KernelInterface
    {
        // ----------------
        // Vars

        // Get information
        $kernelClassName = static::getKernelClass();
        $environmentPath = static::getEnvironmentPath();

        // ----------------
        // Process

        // Create kernel
        $this->kernel = new $kernelClassName([
            KernelInterface::OPT_ENVIRONMENT => $environmentPath
        ]);

        return $this->kernel;
    }

    /**
     * Call protected/private method of a class.
     * If it's the class name, the invocation will only concern static methods.
     * If it's the instance, the invocation will only concern classic methods.
     *
     * @param string|object $object     The instance or class name containing the required method
     * @param string        $method     The method's name
     * @param array         $parameters The method's parameters
     *
     * @return ReflectionMethod
     * @throws LunaException
     * @throws ReflectionException
     */
    protected function call(string|object $object, string $method, array $parameters = []): mixed
    {
        // ----------------
        // Vars

        // Get information
        $className = match (true) {
            TypeManager::isString($object) => $object,
            TypeManager::isObject($object) => ClassManager::nameOf($object),
            default                        => throw new LunaException(
                "The parameter 'object' must be an object or a class name."
            )
        };

        // ----------------
        // Process

        // Get the reflection class
        $class = new ReflectionClass($className);

        // Get the reflection method
        $method = $class->getMethod($method);

        // Call the method
        return TypeManager::isString($object)
            ? $method->invokeArgs(object: null, args: $parameters)      // Static call
            : $method->invokeArgs(object: $object, args: $parameters);  // Instance call
    }

    # --------------------------------
    # Getters

    /**
     * Get the kernel.
     *
     * @return KernelInterface
     */
    protected function getKernel(): KernelInterface
    {
        // ----------------
        // Process

        // Security check : Kernel not defined
        if (!isset($this->kernel)) {
            throw new LogicException(
                message: "You must call 'bootKernel' method before accessing to the kernel."
            );
        }

        return $this->kernel;
    }

    /**
     * Get the kernel class name.
     *
     * @return string
     *
     * @throws RuntimeException
     * @throws LogicException
     */
    protected function getKernelClass(): string
    {
        // ----------------
        // Process

        // Security check : Environment 'KERNEL_CLASS' not defined
        if (!isset($_ENV['KERNEL_CLASS'])) {
            throw new LogicException(
                "You must set the 'KERNEL_CLASS' variable to the fully-qualified class name of your Kernel in phpunit.xml."
            );
        }

        // Get kernel class
        $class = $_ENV['KERNEL_CLASS'];

        // Security check : The kernel class doesn't exist
        if (!class_exists($class)) {
            throw new RuntimeException(
                "Class '{$class}' doesn't exist or cannot be autoloaded. Check that the KERNEL_CLASS value in phpunit.xml match with the fully-qualified class name of your Kernel."
            );
        }

        return $class;
    }

    /**
     * Get the kernel class name.
     *
     * @return string
     *
     * @throws RuntimeException
     * @throws LogicException
     */
    protected function getEnvironmentPath(): string
    {
        // ----------------
        // Process

        // Security check : Environment 'ENV_PATH' not defined
        if (!isset($_ENV['ENV_PATH'])) {
            throw new LogicException("You must set the 'ENV_PATH' variable to the environment path in phpunit.xml.");
        }

        // Get environment path
        $envPath = ValueManager::getString(
            value: $_ENV['ENV_PATH']
        );

        // Security check : The environment file doesn't exist
        if (!file_exists($envPath)) {
            throw new RuntimeException(
                "The environment path '{$envPath}' doesn't exist. Check that the ENV_PATH value in phpunit.xml matches with the path of your environment file."
            );
        }

        return $envPath;
    }

    # --------------------------------
    # Utils methods

    /**
     * Compute path.
     * It will compute the given path for the current OS.
     *
     * @param string $path
     *
     * @return string
     */
    protected function computePath(string $path): string
    {
        // ----------------
        // Process

        // Sanitize path
        return ValueManager::replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    }

    /**
     * Do nothing function.
     *
     * @return null
     */
    protected function doNothing(): null
    {
        return null;
    }
}
