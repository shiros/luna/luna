<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ModuleException.php
 * @Created_at  : 17/10/2024
 * @Update_at   : 26/10/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Module\Exception;

use Luna\Exception\LunaException;

class ModuleException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_CODE    = 1;
    public const DEFAULT_MESSAGE = 'An error is occurred during the module process.';
}
