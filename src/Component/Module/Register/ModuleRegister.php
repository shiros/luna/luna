<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ModuleRegister.php
 * @Created_at  : 28/10/2024
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Module\Register;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\LOCKFileWriterException;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Lock\LunaLock;
use Luna\Component\Lock\Resolver\LockResolver;

class ModuleRegister
{
    # --------------------------------
    # Attributes

    /**
     * Luna lock instance.
     * Contains registered modules.
     */
    protected LunaLock $lock;

    /**
     * Lock resolver.
     */
    protected LockResolver $resolver;

    # --------------------------------
    # Constructor

    /**
     * ModuleRegister constructor.
     *
     * @param LunaLock $lock
     */
    public function __construct(LunaLock $lock)
    {
        // Set attributes
        $this->lock = $lock;
    }

    # --------------------------------
    # Core methods

    /**
     * Returns available modules.
     *
     * @return BagInterface<string, LockModule>
     */
    public function list(): BagInterface
    {
        return $this->lock->getModules();
    }

    /**
     * Adds a new module.
     * Register it in the lock file.
     *
     * @param LockModule $module
     *
     * @return self
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public function add(LockModule $module): self
    {
        // ----------------
        // Vars

        // Get information
        $modules = $this->list();

        // ----------------
        // Process

        // Add module
        $modules->set(
            key  : $module->getName(),
            value: $module
        );

        // Update lock
        $this->lock->setModules($modules);

        return $this;
    }

    /**
     * Removes a module.
     * Unregister it in the lock file.
     *
     * @param LockModule $module
     *
     * @return self
     * @throws LOCKFileWriterException
     * @throws FileWriterException
     */
    public function remove(LockModule $module): self
    {
        // ----------------
        // Vars

        // Get information
        $modules = $this->list();
        $key     = $module->getName();

        // ----------------
        // Process

        // Security check : Module not found
        if (!$modules->has($key)) return $this;

        // Remove the module
        $modules->remove($key);

        // Update lock
        $this->lock->setModules($modules);

        return $this;
    }
}
