<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AbstractModule.php
 * @Created_at  : 04/10/2024
 * @Update_at   : 16/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Module;

use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;
use Luna\Config\Reader\ConfigReader;
use Luna\Config\Resolver\ConfigResolver;
use Luna\Constant\LunaConstant;
use Luna\KernelInterface;
use ReflectionClass;

abstract class AbstractModule implements ModuleInterface
{
    # --------------------------------
    # Attributes

    /**
     * Luna container.
     */
    protected LunaContainer $container;

    /**
     * The config resolver.
     * It's used to resolve config paths.
     */
    protected ConfigResolver $configResolver;

    # --------------------------------
    # Constructor

    /**
     * Module constructor.
     *
     * @param LunaContainer $container
     */
    public function __construct(
        LunaContainer $container,
    ) {
        // Set attributes
        $this->container      = $container;
        $this->configResolver = new ConfigResolver(
            reader: new ConfigReader()
        );
    }

    # --------------------------------
    # Life-Cycle methods

    /**
     * @inheritDoc
     */
    public function install(LockModule $module): void
    {
        // Can be overridden in subclasses if event is required.
    }

    /**
     * @inheritDoc
     */
    public function update(LockModule $module): void
    {
        // Can be overridden in subclasses if event is required.
    }

    /**
     * @inheritDoc
     */
    public function delete(LockModule $module): void
    {
        // Can be overridden in subclasses if event is required.
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getContainer(): LunaContainer
    {
        return $this->container;
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function getKernel(): KernelInterface
    {
        return $this
            ->getContainer()
            ->getKernel()
        ;
    }

    /**
     * @inheritDoc
     */
    public function getRootDir(): string
    {
        // ----------------
        // Process

        // Generate reflection class
        $reflector = new ReflectionClass($this);

        // Generate module path
        return ValueManager::regexp_replace(
            pattern: LunaConstant::REGEXP_LUNA_ROOT,
            replace: '',
            value  : $reflector->getFileName()
        );
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function getAppDir(): string
    {
        return $this
            ->getKernel()
            ->getAppRoot()
        ;
    }

    # --------------------------------
    # Utils methods

    /**
     * Creates a directory.
     * Does nothing if the file exists.
     *
     * @param string $directory
     *
     * @return self
     * @throws FileWriterException
     */
    protected function createDirectory(
        string $directory
    ): self {
        // ----------------
        // Process

        // Sanitize directory path
        $directory = PathManager::sanitize(path: $directory);

        // Checks that folder exists
        if (FileManager::isDirectory(path: $directory)) return $this;

        // The path exist, but it's not a directory, then remove it
        if (FileManager::exists(path: $directory)) {
            FileManager::deleteDirectory(path: $directory);
        }

        // Create the directory
        FileManager::createDirectory(
            path     : $directory,
            recursive: true
        );

        return $this;
    }

    /**
     * Copy a file.
     *
     * @param string $source      // This is the file to copy.
     * @param string $destination // This is the destination file.
     *
     * @return self
     * @throws FileWriterException
     */
    protected function copyFile(
        string $source,
        string $destination,
    ): self {
        // ----------------
        // Process

        // Sanitize paths
        $source      = PathManager::sanitize(path: $source);
        $destination = PathManager::sanitize(path: $destination);

        // Get information
        $destinationDir = pathinfo(path: $destination, flags: PATHINFO_DIRNAME);

        // Security check : Destination file already exists
        if (FileManager::exists(path: $destination)) return $this;

        // Create the destination directory
        $this->createDirectory(directory: $destinationDir);

        // Copy file
        FileManager::copy(
            source     : $source,
            destination: $destination
        );

        return $this;
    }

    /**
     * Copy a config file.
     *
     * @param string $source      // This is the config file to copy
     * @param string $destination // This is the destination folder to which the file will be copied.
     *
     * @return self
     * @throws FileWriterException
     */
    protected function copyConfigFile(
        string $source,
        string $destination,
    ): self {
        // ----------------
        // Process

        // Sanitize paths
        $source      = PathManager::sanitize(path: $source);
        $destination = PathManager::sanitize(path: $destination);

        // Resolve source
        $source = $this->configResolver->resolvePath(path: $source);

        // Generate destination file
        $filename        = basename($source);
        $destinationFile = PathManager::sanitize(path: "{$destination}/{$filename}");

        // Copy config
        $this->copyFile(
            source     : $source,
            destination: $destinationFile
        );

        return $this;
    }

    /**
     * Copy a file.
     *
     * @param string $path
     *
     * @return self
     * @throws FileWriterException
     */
    protected function removeFile(
        string $path
    ): self {
        // ----------------
        // Process

        // Sanitize paths
        $path = PathManager::sanitize(path: $path);

        // Security check : File doesn't exist
        if (!FileManager::isFile(path: $path)) return $this;

        // Remove file
        FileManager::delete(path: $path);

        return $this;
    }
}
