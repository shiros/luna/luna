<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaModule.php
 * @Created_at  : 04/10/2024
 * @Update_at   : 13/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Module;

use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Lock\LunaLock;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Module\Exception\ModuleException;
use ReflectionException;

class LunaModule extends AbstractModule
{
    # --------------------------------
    # Attributes

    /**
     * Luna lock file.
     * Modules are contains in the lock data.
     */
    protected LunaLock $lock;

    /**
     * Luna modules.
     *
     * This contains all Luna's modules.
     * These modules are instantiated only once.
     *
     * @var BagInterface<string, ModuleInterface>
     */
    protected BagInterface $modules;

    # --------------------------------
    # Constructor

    /**
     * LunaModule constructor.
     *
     * @param LunaContainer $container
     * @param LunaLock      $lock
     */
    public function __construct(
        LunaContainer $container,
        LunaLock      $lock
    ) {
        // Call parent constructor
        parent::__construct(container: $container);

        // Set services
        $this->lock    = $lock;
        $this->modules = new Bag();
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Boot up luna modules.
     * The 'boot' method is called to enable the module to start up correctly.
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ModuleException
     * @throws ReflectionException
     */
    public function boot(): void
    {
        // ----------------
        // Process

        // Initialize modules
        $this->initializeModules();

        /** @var ModuleInterface $module */
        foreach ($this->modules as $module) {
            $module->boot();
        }
    }

    /**
     * Start the luna module.
     *
     * Starts :
     *  - The routing system if the web module is registered.
     *
     * @return void
     */
    public function start(): void
    {
        // ----------------
        // Vars

        // Get modules
        $web = $this->modules->get(key: 'shiros/web');

        // ----------------
        // Process

        // Security check : No web module
        if (TypeManager::isNull($web)) return;

        // Starting the web module
        $web->start();
    }

    # --------------------------------
    # Getters

    /**
     * Returns whether the class is a valid module.
     *
     * @param string $class
     *
     * @return bool
     */
    public function isModule(string $class): bool
    {
        return ClassManager::exist(class: $class)
            && ClassManager::implement(
                interface    : ModuleInterface::class,
                objectOrClass: $class
            );
    }

    /**
     * Returns the application module.
     * If it's not found or does not implement the correct interface, null is returned.
     *
     * @return LockModule|null
     */
    public function getAppModule(): ?LockModule
    {
        // ----------------
        // Process

        // Generate application module
        $module = new LockModule(
            name   : 'application',
            version: '1.0.0',
            class  : 'App\AppModule'
        );

        return $this->isModule(class: $module->getClass())
            ? $module
            : null;
    }

    /**
     * Returns initialized modules.
     *
     * @return BagInterface<string, ModuleInterface>
     */
    public function getModules(): BagInterface
    {
        return $this->modules;
    }

    # --------------------------------
    # Utils methods

    /**
     * Initialize modules.
     * Use the lock files to extract registered modules.
     *
     * @return void
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ModuleException
     * @throws ReflectionException
     */
    protected function initializeModules(): void
    {
        // ----------------
        // Vars

        // Get information
        $appModule = $this->getAppModule();
        $modules   = $this->lock->getModules();

        // ----------------
        // Process

        // Initialize modules
        foreach ($modules as $module) {
            // Get information
            $name  = $module->getName();
            $class = $module->getClass();

            // Security check : Is a module
            if (!$this->isModule($class)) {
                throw new ModuleException(
                    message: "The provided module '{$class}' isn't a Luna module, so you need to implement the 'Luna\Component\Module\ModuleInterface'."
                );
            }

            // Instantiate module
            $_module = DependencyInjector::callConstructor(class: $class);

            // Save module
            $this->modules->set(key: $name, value: $_module);
        }

        // Instantiate application module at the end
        if (!TypeManager::isNull($appModule)) {
            // Get information
            $name  = $appModule->getName();
            $class = $appModule->getClass();

            // Create instance
            $_module = DependencyInjector::callConstructor(class: $class);

            // Save application module
            $this->modules->set(
                key  : $name,
                value: $_module
            );
        }
    }
}
