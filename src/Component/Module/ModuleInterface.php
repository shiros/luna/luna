<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ModuleInterface.php
 * @Created_at  : 04/10/2024
 * @Update_at   : 23/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Module;

use Luna\Component\Composer\LunaComposer;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Lock\Entity\LockModule;
use Luna\KernelInterface;

interface ModuleInterface
{
    # --------------------------------
    # Life-Cycle methods

    /**
     * Used to initialize some elements after module installation.
     * It's called after the module is installed.
     *
     * @see LunaComposer::run()
     *
     * @param LockModule $module
     *
     * @return void
     */
    public function install(LockModule $module): void;

    /**
     * Used to initialize some elements after updating the module.
     * It's called after the module is updated.
     *
     * @see LunaComposer::run()
     *
     * @param LockModule $module
     *
     * @return void
     */
    public function update(LockModule $module): void;

    /**
     * Used to initialize some elements after uninstalling the module.
     * It's called after the module is uninstalled.
     *
     * @see LunaComposer::run()
     *
     * @param LockModule    $module
     *
     * @return void
     */
    public function delete(LockModule $module): void;

    # --------------------------------
    # Core methods

    /**
     * Boot the luna module.
     *
     * It's called during kernel initialization.
     * Enables the module to prepare components or save a certain configuration.
     *
     * @return void
     */
    public function boot(): void;

    # --------------------------------
    # Getters

    /**
     * Returns the luna container.
     *
     * Usage :
     * ```
     * // Get module
     * $module = ...;
     *
     * // Get the container
     * $path = $module->getContainer();
     *
     * // Output will be an instance of Luna container.
     *  ```
     *
     * @return LunaContainer
     */
    public function getContainer(): LunaContainer;

    /**
     * Returns the luna kernel.
     *
     * Usage :
     * ```
     * // Get module
     * $module = ...;
     *
     * // Get the kernel
     * $path = $module->getKernel();
     *
     * // Output will be an instance of Luna kernel.
     *  ```
     *
     * @return KernelInterface
     */
    public function getKernel(): KernelInterface;

    /**
     * Returns the module root directory.
     * This is the location of the module file.
     *
     * Usage :
     * ```
     * // Get module
     * $module = ...;
     *
     * // Get the module root path
     * $path = $module->getRootDir();
     *
     * // Output will be :
     * // "<module_dir>/src"
     *  ```
     *
     * @return string
     */
    public function getRootDir(): string;

    /**
     * Returns the module application directory.
     * This is the location of the module file.
     *
     * Usage :
     * ```
     * // Get module
     * $module = ...;
     *
     * // Get the module application path
     * $path = $module->getAppDir();
     *
     * // Output will be :
     * // "<application_dir>/src"
     *  ```
     *
     * @return string
     */
    public function getAppDir(): string;
}
