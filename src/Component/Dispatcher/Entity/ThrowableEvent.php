<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ThrowableEvent.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Entity;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Manager\ClassManager;
use Throwable;

class ThrowableEvent extends AbstractEvent
{
    # --------------------------------
    # Attributes

    protected string    $type;
    protected Throwable $data;

    # --------------------------------
    # Constructor

    /**
     * ThrowableEvent constructor.
     *
     * @param Throwable               $data
     * @param BagInterface|array|null $parameters
     * @param BagInterface|array|null $options
     */
    public function __construct(
        Throwable     $data,
        BagInterface|array|null $parameters = null,
        BagInterface|array|null $options = null
    ) {
        // Call parent constructor
        parent::__construct(
            type      : ClassManager::nameOf($data),
            parameters: $parameters,
            options   : $options
        );

        // Set attributes
        $this->data = $data;
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     *
     * @return Throwable
     */
    public function getData(): Throwable
    {
        return $this->data;
    }
}