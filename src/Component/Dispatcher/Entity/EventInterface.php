<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : EventInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 26/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Entity;

use Luna\Component\Bag\BagInterface;

interface EventInterface
{
    # --------------------------------
    # Getters

    /**
     * Get the event type.
     *
     * @return mixed
     */
    public function getType(): string;

    /**
     * Get the event data.
     *
     * @return mixed
     */
    public function getData(): mixed;

    /**
     * Get the event parameters.
     *
     * @return BagInterface|null
     */
    public function getParameters(): ?BagInterface;

    /**
     * Get the event options.
     *
     * @return BagInterface
     */
    public function getOptions(): BagInterface;
}