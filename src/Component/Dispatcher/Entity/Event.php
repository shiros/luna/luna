<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ThrowableEvent.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Entity;

use Luna\Component\Bag\BagInterface;

class Event extends AbstractEvent
{
    # --------------------------------
    # Attributes

    protected mixed $data;

    # --------------------------------
    # Constructor

    /**
     * Event constructor.
     *
     * @param mixed                   $data
     * @param BagInterface|array|null $parameters
     * @param BagInterface|array|null $options
     */
    public function __construct(
        mixed                   $data,
        BagInterface|array|null $parameters = null,
        BagInterface|array|null $options = null
    ) {
        // Call parent constructor
        parent::__construct(
            type      : 'Default',
            parameters: $parameters,
            options   : $options
        );

        // Set attributes
        $this->data = $data;
    }


    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getData(): string
    {
        return $this->data;
    }
}