<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : AbstractEvent.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Entity;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Manager\ValueManager;

abstract class AbstractEvent implements EventInterface
{
    # --------------------------------
    # Attributes

    protected string       $type;
    protected BagInterface $parameters;
    protected BagInterface $options;

    # --------------------------------
    # Getters

    /**
     * AbstractEvent constructor.
     *
     * @param string                  $type
     * @param BagInterface|array|null $parameters
     * @param BagInterface|array|null $options
     */
    public function __construct(
        string                  $type,
        BagInterface|array|null $parameters = null,
        BagInterface|array|null $options = null
    ) {
        // Set attributes
        $this->type       = $type;
        $this->parameters = ValueManager::getBag($parameters);
        $this->options    = ValueManager::getBag($options);
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @inheritDoc
     */
    public function getParameters(): ?BagInterface
    {
        return $this->parameters;
    }

    /**
     * @inheritDoc
     */
    public function getOptions(): BagInterface
    {
        return $this->options;
    }
}