<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Dispatcher.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher;

use Luna\Component\Bag\BagInterface;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\Dispatcher\Builder\EventBuilder;
use Luna\Component\Dispatcher\Entity\ThrowableEvent;
use Luna\Component\Dispatcher\Resolver\DispatcherResolver;
use Luna\Component\Logger\Exception\LoggerException;
use Luna\Exception\Handler\ThrowableHandler;
use Throwable;

class Dispatcher implements DispatcherInterface
{
    # --------------------------------
    # Core methods

    /**
     * Dispatch a throwable event.
     *
     * @param mixed                   $event
     * @param BagInterface|array|null $parameters
     *
     * @return void
     * @throws LoggerException
     */
    public function dispatch(mixed $event, BagInterface|array|null $parameters = null): void
    {
        // ----------------
        // Vars

        // Get information
        $event = EventBuilder::build(
            data      : $event,
            parameters: $parameters
        );

        // Handlers
        $throwableHandler = new ThrowableHandler();

        // ----------------
        // Process

        try {
            // Resolve handler
            $handler = DispatcherResolver::resolveHandler($event);

            // Call handler
            DependencyInjector::callMethod('handle', $handler, [
                'event' => $event
            ]);
        } catch (Throwable $throwable) {
            $throwableHandler->handle(
                event: new ThrowableEvent($throwable)
            );
        }
    }
}