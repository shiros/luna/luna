<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DispatcherException.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 26/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Exception;

use Luna\Exception\LunaException;

class DispatcherException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the dispatcher process.';
}
