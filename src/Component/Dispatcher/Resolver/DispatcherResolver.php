<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Dispatcher.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Resolver;

use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Dispatcher\Entity\EventInterface;
use Luna\Component\Dispatcher\Entity\ThrowableEvent;
use Luna\Component\Handler\Handler;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Exception\Handler\ThrowableHandler;
use Throwable;

class DispatcherResolver
{
    # --------------------------------
    # Core methods

    /**
     * Returns the throwable handler.
     *
     * @param EventInterface $event
     *
     * @return string
     * @throws ContainerException
     */
    public static function resolveHandler(EventInterface $event): string
    {
        // ----------------
        // Vars

        // Get information
        $container = LunaContainer::getInstance();
        $config    = $container->getConfig();

        // Get event information
        $type = $event->getType();

        // ----------------
        // Process

        // Get handlers
        $handler          = ValueManager::getString(
            value: $config->getHandler(key: $type)
        );
        $throwableHandler = ValueManager::getString(
            value: $config->getHandler(key: Throwable::class)
        );

        // Conditions
        $hasHandler          = !TypeManager::isEmpty($handler);
        $hasThrowableHandler = !TypeManager::isEmpty($throwableHandler);
        $isThrowableEvent    = ClassManager::is(ThrowableEvent::class, $event);

        // Compute handler
        return match (true) {
            $hasHandler          => $handler,
            $hasThrowableHandler => $throwableHandler,
            $isThrowableEvent    => ThrowableHandler::class,
            default              => Handler::class
        };
    }
}