<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : DispatcherInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher;

use Luna\Component\Bag\BagInterface;

interface DispatcherInterface
{
    # --------------------------------
    # Core methods

    /**
     * Dispatch an event.
     *
     * @param object                  $event
     * @param BagInterface|array|null $parameters
     *
     * @return void
     */
    public function dispatch(object $event, BagInterface|array|null $parameters = null): void;
}