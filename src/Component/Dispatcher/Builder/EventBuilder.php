<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : EventBuilder.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 07/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Dispatcher\Builder;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Dispatcher\Entity\Event;
use Luna\Component\Dispatcher\Entity\EventInterface;
use Luna\Component\Dispatcher\Entity\ThrowableEvent;
use Luna\Component\Manager\ClassManager;
use Throwable;

class EventBuilder
{
    # --------------------------------
    # Core methods

    /**
     * Build an event object.
     *
     * @param mixed                   $data
     * @param BagInterface|array|null $parameters
     *
     * @return EventInterface
     */
    public static function build(mixed $data, BagInterface|array|null $parameters = null): EventInterface
    {
        // ----------------
        // Vars

        // Conditions
        $isEvent     = ClassManager::implement(EventInterface::class, $data);
        $isThrowable = ClassManager::is(Throwable::class, $data);

        // ----------------
        // Process

        return match (true) {
            $isEvent     => $data,
            $isThrowable => new ThrowableEvent($data, $parameters),
            default      => new Event($data, $parameters)
        };
    }
}