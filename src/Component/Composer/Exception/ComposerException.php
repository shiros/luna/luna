<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ComposerException.php
 * @Created_at  : 16/11/2024
 * @Update_at   : 16/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Composer\Exception;

use Luna\Exception\LunaException;

class ComposerException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the composer process.';
}
