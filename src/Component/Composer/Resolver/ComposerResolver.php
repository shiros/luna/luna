<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ComposerResolver.php
 * @Created_at  : 28/10/2024
 * @Update_at   : 14/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Composer\Resolver;

use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UninstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\Installer\InstallationManager;
use Composer\Installer\PackageEvent;
use Composer\Package\PackageInterface;
use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\File\Exception\Reader\JSONFileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\File\Manager\JSONFileManager;
use Luna\Component\Lock\Builder\LockModuleBuilder;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Component\Module\ModuleInterface;
use ReflectionException;

class ComposerResolver
{
    # --------------------------------
    # Attributes

    /**
     * Luna container.
     */
    protected LunaContainer $container;

    /**
     * Installation manager.
     */
    protected InstallationManager $installationManager;

    # --------------------------------
    # Constructor

    /**
     * Package constructor.
     *
     * @param LunaContainer       $container
     * @param InstallationManager $installationManager
     */
    public function __construct(
        LunaContainer       $container,
        InstallationManager $installationManager
    ) {
        // Set attributes
        $this->container           = $container;
        $this->installationManager = $installationManager;
    }

    # --------------------------------
    # Core methods

    /**
     * Resolve the package from provided composer event.
     * Returns an instance of package, or null if the event operation isn't supported.
     *
     * Usage :
     * ```
     * // Get components
     * $resolver = ...;
     * $event    = ...; // Coming from composer
     *
     * // Resolve module
     * $package = $resolver->resolvePackage($event);
     * var_dump($package);
     * // Output : This is a composer package instance.
     *  ```
     *
     * @param PackageEvent $event
     *
     * @return PackageInterface|null
     */
    public function resolvePackage(PackageEvent $event): ?PackageInterface
    {
        // ----------------
        // Vars

        // Get information
        $operation = $event->getOperation();

        // ----------------
        // Process

        /**
         * Get target package.
         *
         * Use 'is_a(...)' instead of 'ClassManager::is(...)' to use auto-completion and avoid php runtime error.
         */
        return match (true) {
            is_a($operation, class: InstallOperation::class)   => $operation->getPackage(),
            is_a($operation, class: UpdateOperation::class)    => $operation->getTargetPackage(),
            is_a($operation, class: UninstallOperation::class) => $operation->getPackage(),
            default                                            => null
        };
    }

    /**
     * Resolve the composer.json file.
     * Returns composer parameters.
     *
     * **WARNING :**If the composer.json file doesn't exist, the bag will be empty.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $package  = ...; // Coming from composer
     *
     * // Resolve composer.json
     * $parameters = $resolver->resolveComposerFile($package);
     * var_dump($parameters);
     * // Output : Is a bag object that contains the composer.json parameters.
     * ```
     *
     * @param PackageInterface $package
     *
     * @return BagInterface
     * @throws JSONFileReaderException
     */
    public function resolveComposerFile(PackageInterface $package): BagInterface
    {
        // ----------------
        // Vars

        // Get information
        $path = $this->installationManager->getInstallPath($package);
        $file = PathManager::sanitize("{$path}/composer.json");

        // ----------------
        // Process

        // Security check : No composer.json
        if (!FileManager::exists($path)) {
            return new Bag();
        }

        // Read composer.json
        return ValueManager::getBag(
            value: JSONFileManager::read(
                path       : $file,
                associative: true
            )
        );
    }

    /**
     * Resolve the potential module.
     * Returns an instance of module, or null if isn't a valid module.
     *
     * Usage :
     * ```
     * // Get components
     * $resolver = ...;
     * $package  = ...; // Coming from composer
     *
     * // Resolve module
     * $lockModule = $resolver->resolveLockModule($package);
     * var_dump($lockModule);
     * // Output : This is a lock-module instance.
     *  ```
     *
     * @param PackageInterface $package
     *
     * @return ModuleInterface|null
     * @throws JSONFileReaderException
     */
    public function resolveLockModule(PackageInterface $package): ?LockModule
    {
        // ----------------
        // Vars

        // Get information
        $composer = $this->resolveComposerFile($package);
        $class    = $composer->getAsString(key: 'luna.module', default: null);

        // ----------------
        // Process

        // Security check : No module register
        if (TypeManager::isEmpty(value: $class)) return null;

        return LockModuleBuilder::buildFromComposer(
            package: $package,
            class  : $class
        );
    }

    /**
     * Resolve the potential module.
     * Returns an instance of module, or null if isn't a valid module.
     *
     * Usage :
     * ```
     * // Get components
     * $resolver    = ...;
     * $lockModule  = new LockModule(...);
     *
     * // Resolve module
     * $module = $resolver->resolveModule($lockModule);
     * var_dump($module);
     * // Output : This is a module instance.
     *  ```
     *
     * @param LockModule $lockModule
     *
     * @return ModuleInterface|null
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function resolveModule(LockModule $lockModule): ?ModuleInterface
    {
        // ----------------
        // Vars

        // Get information
        $class = $lockModule->getClass();

        // ----------------
        // Process

        // Security check : Isn't a module
        if (TypeManager::isNull(value: $class)
            || !ClassManager::exist(class: $class)
            || !ClassManager::implement(interface: ModuleInterface::class, objectOrClass: $class)
        ) return null;

        return DependencyInjector::callConstructor(
            class    : $class,
            arguments: [
                'container' => $this->container
            ]
        );
    }
}
