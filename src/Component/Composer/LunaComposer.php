<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaComposer.php
 * @Created_at  : 23/10/2024
 * @Update_at   : 23/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Composer;

use Composer\Composer;
use Composer\Installer\PackageEvent;
use Composer\Package\PackageInterface;
use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Composer\Exception\ComposerException;
use Luna\Component\Composer\Resolver\ComposerResolver;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Lock\LunaLock;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Component\Module\Register\ModuleRegister;
use Luna\Exception\KernelException;
use Luna\Kernel;
use Luna\KernelInterface;
use Stringable;
use Throwable;

class LunaComposer
{
    # --------------------------------
    # Attributes

    /**
     * Composer instance.
     * Manages composer information.
     */
    protected Composer $composer;

    /**
     * Output stream.
     * Allow to write message(s) in the terminal.
     */
    protected $outputStream;

    /**
     * Resolver.
     * Resolves some components required by luna composer.
     */
    protected ComposerResolver $resolver;

    /**
     * Composer packages.
     *
     * @var BagInterface<string, PackageInterface>
     */
    protected BagInterface $packages;

    /**
     * Luna kernel.
     */
    protected KernelInterface $kernel;

    /**
     * Luna container.
     */
    protected LunaContainer $container;

    /**
     * Luna lock.
     */
    protected LunaLock $lock;

    /**
     * Module register.
     * Allow to register/un-register a module in Luna.
     */
    protected ModuleRegister $moduleRegister;

    # --------------------------------
    # Constructor

    /**
     * LunaComposer constructor.
     *
     * @param Composer $composer
     *
     * @throws KernelException
     */
    public function __construct(Composer $composer)
    {
        // Initialize the Composer instance
        $this->composer = $composer;

        // Set attributes
        $this->outputStream   = $this->openOutputStream();
        $this->packages       = new Bag();
        $this->kernel         = new Kernel(
            options: [KernelInterface::OPT_STANDALONE => true]
        );
        $this->container      = $this->kernel->getContainer();
        $this->lock           = $this->kernel->getLock();
        $this->moduleRegister = new ModuleRegister(lock: $this->lock);
        $this->resolver       = new ComposerResolver(
            container          : $this->container,
            installationManager: $this->composer->getInstallationManager(),
        );

        // Init component
        $this->init();
    }

    # --------------------------------
    # Core methods

    /**
     * Initialize luna composer
     *
     * @return self
     */
    public function init(): self
    {
        // ----------------
        // Vars

        // Get information
        $packages = $this->composer
            ->getRepositoryManager()
            ->getLocalRepository()
            ->getPackages()
        ;

        // ----------------
        // Process

        // Clean packages
        $this->packages->clear();

        // Setup package
        foreach ($packages as $package) {
            $this->packages->set(
                key  : $package->getName(),
                value: $package
            );
        }

        return $this;
    }

    /**
     * Occurs after a composer command was performed.
     *
     * @link https://getcomposer.org/doc/articles/scripts.md
     *
     * @param BagInterface|array|null $arguments
     *
     * @return void
     */
    public function run(
        BagInterface|array|null $arguments = null
    ): void {
        // ----------------
        // Vars

        // Attributes
        $arguments = ValueManager::getBag($arguments);

        // Get information
        $action = $arguments->getAsString(0, null);
        $name   = $arguments->getAsString(1, null);

        // ----------------
        // Process

        try {
            // Call action
            switch ($action) {
                case 'post-install-cmd':
                case 'post-update-cmd':
                    $this->registerPackages();
                    break;

                case 'pre-package-uninstall':
                    $this->deleteModule(name: $name);
                    break;

                default:
                    $this
                        ->write(messages: "[luna-cmd] : Unknown action '{$action}'")
                        ->writeln()
                    ;
                    break;
            }
        } catch (Throwable $throwable) {
            $this
                ->write(messages: "[luna-cmd] : {$throwable->getMessage()}")
                ->writeln()
            ;
        }
    }

    /**
     * Occurs before a package is uninstalled.
     *
     * @link https://getcomposer.org/doc/articles/scripts.md#package-events
     *
     * @param PackageEvent $event
     *
     * @return void
     */
    public static function prePackageUninstall(PackageEvent $event): void
    {
        // ----------------
        // Process

        try {
            // Get components
            $composer    = new LunaComposer(
                composer: $event->getComposer()
            );
            $modules     = $composer->getModules();
            $package     = $composer->getPackage($event);
            $packageName = $package?->getName();

            // Security check : Module not found
            if (!$modules->has($packageName)) {
                echo "[luna-cmd] : The package '$packageName' doesn't contains a Luna module. Abort process." . PHP_EOL;
                return;
            }

            // Generate arguments
            $arguments = [
                'pre-package-uninstall',
                $packageName
            ];

            // Run the luna composer
            $composer->run(arguments: $arguments);
        } catch (Throwable $throwable) {
            echo "[luna-cmd] : {$throwable->getMessage()}" . PHP_EOL;
        }
    }

    # --------------------------------
    # Getters

    /**
     * Returns the composer's package obtained by the event.
     *
     * @param PackageEvent $event
     *
     * @return PackageInterface|null
     */
    public function getPackage(PackageEvent $event): ?PackageInterface
    {
        return $this->resolver->resolvePackage(event: $event);
    }

    /**
     * Returns composer's packages.
     *
     * @return BagInterface<string, PackageInterface>
     */
    public function getPackages(): BagInterface
    {
        return $this->packages;
    }

    /**
     * Returns luna modules.
     *
     * @see LunaLock::getModules()
     *
     * @return BagInterface<string, LockModule>
     */
    public function getModules(): BagInterface
    {
        return $this->lock->getModules();
    }

    # --------------------------------
    # Utils methods

    /**
     * Register packages.
     * It's called when the 'run' method for 'install' or 'update' actions.
     *
     * This action will :
     *  - Initialize modules
     *  - Call life-cycle methods
     *  - Register them in the lock file
     *
     * @return self
     */
    public function registerPackages(): self
    {
        // ----------------
        // Vars

        // Get information
        $packages    = $this->getPackages();
        $lockModules = $this->getModules();

        // ----------------
        // Process

        // Browse packages to install/update modules
        foreach ($packages as $package) {
            try {
                // Get information
                $name          = $package->getName();
                $lockModule    = $this->resolver->resolveLockModule($package);
                $oldLockModule = $lockModules->get(key: $name);

                // Security check : No lock-module
                if (TypeManager::isNull($lockModule)) continue;

                // Resolve module
                $module = $this->resolver->resolveModule($lockModule);

                // Conditions
                $isAlreadyRegistered = !TypeManager::isNull($oldLockModule);
                $hasSameVersion      = $oldLockModule?->getVersion() === $lockModule->getVersion();

                // Launch module process
                switch (true) {
                    case !$isAlreadyRegistered:
                        // Call install life-cycle method
                        $module->install(module: $lockModule);

                        // Register module
                        $this->moduleRegister->add(module: $lockModule);

                        // Logs
                        $this->writeln(
                            messages: "[luna-cmd] : Module '{$lockModule->getName()}' registered."
                        );
                        break;

                    case !$hasSameVersion:
                        // Call update life-cycle method
                        $module->update(module: $lockModule);

                        // Update the registered module
                        $this->moduleRegister->add(module: $lockModule);

                        // Logs
                        $this->writeln(messages: "[luna-cmd] : Module '{$lockModule->getName()}' updated.");
                        break;
                }
            } catch (Throwable $throwable) {
                $this
                    ->write(messages: "[luna-cmd] : {$package->getName()} - {$throwable->getMessage()}")
                    ->writeln()
                ;
            }
        }

        return $this;
    }

    /**
     * Deletes a module.
     * It's called when the 'run' method.
     *
     * This action will :
     * - Calling module life-cycle method
     * - Un-register it from luna
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function deleteModule(?string $name): self
    {
        // ----------------
        // Vars

        // Get information
        $lockModules = $this->getModules();
        $lockModule  = $lockModules->get(key: $name);

        // ----------------
        // Process

        try {
            // Security check : No lock-module
            if (TypeManager::isNull($lockModule)) {
                throw new ComposerException(
                    message: "The package '{$lockModule->getName()}' isn't a luna module. Abort process."
                );
            }

            // Resolve module
            $module = $this->resolver->resolveModule($lockModule);

            // Security check : No module
            if (TypeManager::isNull($lockModule)) {
                throw new ComposerException(
                    message: "Module '{$lockModule->getName()}' not found. Abort process."
                );
            }

            // Call delete life-cycle method
            $module->delete(module: $lockModule);

            // Uh-registered module
            $this->moduleRegister->remove(module: $lockModule);

            // Logs
            $this->writeln(messages: "[luna-cmd] : Module '{$lockModule->getName()}' un-registered.");
        } catch (Throwable $throwable) {
            $this
                ->write(messages: "[luna-cmd] : {$name} - {$throwable->getMessage()}")
                ->writeln()
            ;
        }

        return $this;
    }

    # --------------------------------
    # Output methods

    /**
     * Open an output stream.
     * Using "php://stdout" or "php://output".
     *
     * @link https://php.net/manual/en/wrappers.php.php
     *
     * @return resource
     */
    protected function openOutputStream()
    {
        // ----------------
        // Process

        // Check STDOUT constant
        if (defined('STDOUT')) {
            return STDOUT;
        }

        // Open stream
        return @fopen('php://stdout', 'w') ?: fopen('php://output', 'w');
    }

    /**
     * Writes a message(s) to the output.
     *
     * @param iterable|Stringable|string $messages
     * @param bool                       $newline
     *
     * @return $this
     */
    protected function write(
        iterable|Stringable|string $messages = '',
        bool                       $newline = false
    ): static {
        // ----------------
        // Vars

        // Attributes
        $messages = TypeManager::isIterable($messages) ? $messages : [$messages];

        // ----------------
        // Process

        // Write messages
        foreach ($messages as $message) {
            // Prepare message
            $message = ValueManager::getString($message);

            // Add PHP end of line
            if ($newline) $message .= PHP_EOL;

            // Write message
            @fwrite($this->outputStream, $message);
            fflush($this->outputStream);
        }

        return $this;
    }

    /**
     * Writes a message(s) to the output and adds a newline at the end.
     *
     * @param iterable|Stringable|string $messages
     *
     * @return $this
     */
    protected function writeln(
        iterable|Stringable|string $messages = ''
    ): static {
        return $this->write(
            messages: $messages,
            newline : true
        );
    }
}
