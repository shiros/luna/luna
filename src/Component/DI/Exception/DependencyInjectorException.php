<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DependencyInjectorException.php
 * @Created_at  : 22/03/2018
 * @Update_at   : 14/01/2023
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\DI\Exception;

use Luna\Exception\LunaException;

class DependencyInjectorException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the dependency injector process.';
}
