<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DependencyInjector.php
 * @Created_at  : 03/12/2017
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI;

use Closure;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\DI\Processor\DIProcessor;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionMethod;

class DependencyInjector
{
    # --------------------------------
    # Core methods

    /**
     * Allow to instantiate an object dynamically.
     *
     * @param string                  $class
     * @param BagInterface|array|null $arguments
     *
     * @return mixed
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public static function callConstructor(
        string                  $class,
        BagInterface|array|null $arguments = null
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $arguments = ValueManager::getBag($arguments);

        // ----------------
        // Process

        // Security check
        if (!ClassManager::exist($class)) {
            throw new DependencyInjectorException("Class {$class} doesn't exist.");
        }

        // Get processor & class
        $processor       = new DIProcessor($arguments);
        $reflectionClass = new ReflectionClass($class);

        // Instantiate class
        return $processor->construct($reflectionClass);
    }

    /**
     * Allow to call a method with automatic instantiation of parameters.
     *
     * @param string                  $method
     * @param object|string           $objectOrClass
     * @param BagInterface|array|null $arguments
     *
     * @return mixed
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public static function callMethod(
        string                  $method,
        object|string           $objectOrClass,
        BagInterface|array|null $arguments = null
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $arguments = ValueManager::getBag($arguments);

        // Get information
        $className = ValueManager::getString($objectOrClass);

        // ----------------
        // Process

        // Case : Try to call method
        if (ClassManager::hasMethod($method, $objectOrClass)) {
            // Get processor & method
            $processor        = new DIProcessor($arguments);
            $reflectionMethod = new ReflectionMethod($className, $method);

            // Calls the method
            return $processor->method($reflectionMethod, $objectOrClass);
        }

        // Case : Try to call method thanks to __call
        if (ClassManager::isCallable([$objectOrClass, $method])) {
            return call_user_func_array(
                callback: [$objectOrClass, $method],
                args    : $arguments->all()
            );
        }

        // Default
        throw new DependencyInjectorException(
            message   : "Please check if the object/class is correct and if the method exists or if the magic methods '__call' / '__callStatic' are correctly written.",
            parameters: [
                'class'  => $className,
                'method' => $method
            ]
        );
    }

    /**
     * Allow to call a function with automatic instantiation of parameters.
     *
     * @param string|Closure          $function
     * @param BagInterface|array|null $arguments
     *
     * @return mixed
     *
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public static function callFunction(
        string|Closure          $function,
        BagInterface|array|null $arguments = null
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $arguments = ValueManager::getBag($arguments);

        // Get information
        $functionName = ValueManager::getString($function);

        // ----------------
        // Process

        // Security check
        if (!ClassManager::hasFunction($functionName) && !TypeManager::isFunction($function)) {
            throw new DependencyInjectorException("Function {$function} doesn't exist.");
        }

        // Get processor & function
        $processor          = new DIProcessor($arguments);
        $reflectionFunction = new ReflectionFunction($function);

        // Calls the function
        return $processor->function($reflectionFunction);
    }
}
