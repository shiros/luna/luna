<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DispatcherModule.php
 * @Created_at  : 08/06/2024
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\DI\Processor\ModuleProcessor;
use Luna\Component\Dispatcher\DispatcherInterface;

class DispatcherModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * Get the luna dispatcher.
     *
     * @inheritDoc
     *
     * @return DispatcherInterface
     * @throws ContainerException
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): DispatcherInterface
    {
        return $this->container->getDispatcher();
    }
}
