<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KernelModule.php
 * @Created_at  : 10/05/2018
 * @Update_at   : 03/05/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\DI\Processor\ModuleProcessor;
use Luna\KernelInterface;

class KernelModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * Get the luna kernel.
     *
     * @inheritDoc
     *
     * @return KernelInterface
     * @throws ContainerException
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): KernelInterface
    {
        return $this->container->getKernel();
    }
}
