<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ModuleInterface.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 03/05/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\DI\Processor\ModuleProcessor;

interface ModuleInterface
{
    # --------------------------------
    # Core methods

    /**
     * Dependency Injector module process.
     *
     * @param ReadOnlyBag     $arguments
     * @param ModuleProcessor $moduleProcessor
     *
     * @return mixed
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): mixed;
}
