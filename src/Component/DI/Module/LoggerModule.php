<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LoggerModule.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 27/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\DI\Processor\ModuleProcessor;
use Luna\Component\Logger\LunaLogger;

class LoggerModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * Get the luna logger.
     *
     * @inheritDoc
     *
     * @return LunaLogger
     * @throws ContainerException
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): LunaLogger
    {
        return $this->container->getLogger();
    }
}
