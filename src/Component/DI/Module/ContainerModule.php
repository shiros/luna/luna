<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ContainerModule.php
 * @Created_at  : 12/05/2018
 * @Update_at   : 03/05/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\Processor\ModuleProcessor;

class ContainerModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * Get the luna container.
     *
     * @inheritDoc
     *
     * @return LunaContainer
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): LunaContainer
    {
        return $this->container;
    }
}
