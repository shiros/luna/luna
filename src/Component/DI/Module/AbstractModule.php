<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AbstractModule.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 02/05/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Container\LunaContainer;

abstract class AbstractModule implements ModuleInterface
{
    # --------------------------------
    # Attributes

    protected LunaContainer $container;

    # --------------------------------
    # Core methods

    /**
     * AbstractModule constructor.
     */
    public function __construct()
    {
        $this->container = LunaContainer::getInstance();
    }
}
