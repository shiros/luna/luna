<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigModule.php
 * @Created_at  : 10/05/2018
 * @Update_at   : 03/05/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Module;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\DI\Processor\ModuleProcessor;
use Luna\Config\LunaConfig;

class ConfigModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * Get the luna config.
     *
     * @inheritDoc
     *
     * @return LunaConfig
     * @throws ContainerException
     */
    public function process(ReadOnlyBag $arguments, ModuleProcessor $moduleProcessor): LunaConfig
    {
        return $this->container->getConfig();
    }
}
