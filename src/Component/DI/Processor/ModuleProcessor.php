<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ModuleProcessor.php
 * @Created_at  : 22/03/2023
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Processor;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\DI\Module\ModuleInterface;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionType;
use ReflectionUnionType;

class ModuleProcessor
{
    # --------------------------------
    # Attributes

    /**
     * Dependency injector modules.
     *
     * @var BagInterface<string>
     */
    protected BagInterface $modules;

    # --------------------------------
    # Constructor

    /**
     * ModuleProcessor constructor.
     *
     * @param BagInterface|array $modules
     */
    public function __construct(
        BagInterface|array $modules = []
    ) {
        // Set attributes
        $this->modules = ValueManager::getBag($modules);
    }

    # --------------------------------
    # Core methods

    /**
     * Compute a module.
     * Launch the process method of the module to build the requested element.
     *
     * @param ModuleInterface $module
     * @param ReadOnlyBag     $arguments
     *
     * @return object
     * @throws DependencyInjectorException
     */
    public function compute(ModuleInterface $module, ReadOnlyBag $arguments): object
    {
        // ----------------
        // Vars

        // Get module information
        $moduleName = ClassManager::nameOf($module);

        // ----------------
        // Process

        // Instantiate object
        $object = $module->process($arguments, $this);

        // Security check
        if (!TypeManager::isObject($object)) {
            throw new DependencyInjectorException(
                "The dependency injector module '{$moduleName}' must be return an object."
            );
        }

        return $object;
    }

    # --------------------------------
    # Getters

    /**
     * Get modules.
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @return BagInterface
     */
    public function all(): BagInterface
    {
        return $this->modules;
    }

    /**
     * Get the dependency module corresponding to the key.
     * Mostly, the key is a class name.
     *
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @param string $key
     *
     * @return ModuleInterface|null
     */
    public function get(string $key): ?ModuleInterface
    {
        // ----------------
        // Process

        // Security check
        if ($this->modules->isEmpty()) {
            return null;
        }

        // Get the module
        $module = $this->modules->get($key);

        return match (true) {
            !TypeManager::isEmpty($module) && ClassManager::is(ModuleInterface::class, $module) => new $module(),
            default                                                                             => null
        };
    }

    /**
     * Get the dependency module corresponding to the type.
     *
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @param ReflectionType $type
     *
     * @return ModuleInterface|null
     */
    public function getByType(ReflectionType $type): ?ModuleInterface
    {
        // ----------------
        // Process

        // Security check
        if ($this->modules->isEmpty()) {
            return null;
        }

        /**
         * Get module
         *
         * Use 'is_a(...)' instead of 'ClassManager::is(...)' to use auto-completion and avoid php runtime error.
         */
        return match (true) {
            is_a($type, ReflectionNamedType::class)        => $this->getByNamedType($type),
            is_a($type, ReflectionUnionType::class)        => $this->getByUnionType($type),
            is_a($type, ReflectionIntersectionType::class) => $this->getByIntersectionType($type),
            default                                        => null
        };
    }

    /**
     * Get the dependency module corresponding to the named type.
     *
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @param ReflectionNamedType $type
     *
     * @return ModuleInterface|null
     */
    public function getByNamedType(ReflectionNamedType $type): ?ModuleInterface
    {
        // ----------------
        // Vars

        // Get information
        $name      = $type->getName();
        $isBuiltIn = $type->isBuiltin();

        // ----------------
        // Process

        // Case : Is a built-in type
        if ($isBuiltIn) {
            return null;
        }

        // Case : Get the associate module
        return $this->get($name);
    }

    /**
     * Get the dependency module corresponding to the union type.
     *
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @param ReflectionUnionType $type
     *
     * @return ModuleInterface|null
     */
    public function getByUnionType(ReflectionUnionType $type): ?ModuleInterface
    {
        // ----------------
        // Vars

        // Get information
        $types      = $type->getTypes();
        $definition = ValueManager::join($types, '|');

        // ----------------
        // Process

        // Get module by definition
        $module = $this->get($definition);

        // Case : The module exist for the given definition
        if (!TypeManager::isNull($module)) {
            return $module;
        }

        // Case : Search a module for at least one type
        foreach ($types as $_type) {
            $module = $this->getByNamedType($_type);

            // Module found, break the process
            if (!TypeManager::isNull($module)) {
                return $module;
            }
        }

        // Case : Default (No module found)
        return null;
    }

    /**
     * Get the dependency module corresponding to the intersection type.
     *
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * @param ReflectionIntersectionType $type
     *
     * @return ModuleInterface|null
     */
    public function getByIntersectionType(ReflectionIntersectionType $type): ?ModuleInterface
    {
        // ----------------
        // Vars

        // Get information
        $types      = $type->getTypes();
        $definition = ValueManager::join($types, '&');

        // ----------------
        // Process

        // Get module by definition
        return $this->get($definition);
    }
}
