<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DIProcessor.php
 * @Created_at  : 03/12/2017
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Processor;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Config\LunaConfig;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionParameter;

class DIProcessor
{
    # --------------------------------
    # Attributes

    protected LunaContainer $container;
    protected LunaConfig    $config;

    // Processors
    protected ArgumentProcessor  $argumentProcessor;
    protected ModuleProcessor    $moduleProcessor;
    protected ParameterProcessor $parameterProcessor;

    // Data
    protected ReadOnlyBag $arguments;

    # --------------------------------
    # Constructor

    /**
     * DIProcessor constructor.
     *
     * @param BagInterface|array|null $arguments
     *
     * @throws ContainerException
     */
    public function __construct(BagInterface|array|null $arguments = null)
    {
        // Set attributes
        $this->container = LunaContainer::getInstance();
        $this->config    = $this->container->getConfig();

        // Get config data
        $_arguments = ValueManager::getBag(
            value: $this->config->getDI(key: 'Arguments')
        );
        $_modules   = ValueManager::getBag(
            value: $this->config->getDI(key: 'Modules')
        );

        // Set processors
        $this->argumentProcessor  = new ArgumentProcessor($_arguments);
        $this->moduleProcessor    = new ModuleProcessor($_modules);
        $this->parameterProcessor = new ParameterProcessor($this, $this->moduleProcessor);

        // Set data
        $this->arguments = $this->argumentProcessor->compute($arguments);
    }

    # --------------------------------
    # Core methods

    /**
     * System to construct the object recursively
     *
     * @param ReflectionClass $reflectionClass
     *
     * @return mixed
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function construct(ReflectionClass $reflectionClass): mixed
    {
        // ----------------
        // Vars

        // Get information
        $className            = $reflectionClass->getName();
        $classConstructor     = $reflectionClass->getConstructor();
        $classConstructorArgs = ValueManager::getArray($classConstructor?->getParameters());

        // ----------------
        // Process

        // Case : The module is known by class name
        if (!TypeManager::isNull($module = $this->moduleProcessor->get($className))) {
            return $this->moduleProcessor->compute($module, $this->arguments);
        }

        // Security check
        $this->validateClass($reflectionClass);

        // Compute parameters
        $parameters = $this->computeParameters($classConstructorArgs);

        return TypeManager::isEmpty($parameters)
            ? $reflectionClass->newInstance()
            : $reflectionClass->newInstanceArgs($parameters);
    }

    /**
     * System to call the method.
     *
     * @param ReflectionMethod $reflectionMethod
     * @param object|string    $objectOrClass
     *
     * @return mixed
     * @throws ContainerException
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function method(ReflectionMethod $reflectionMethod, object|string $objectOrClass): mixed
    {
        // ----------------
        // Vars

        // Get information
        $reflectionMethodArgs = $reflectionMethod->getParameters();
        $className            = ValueManager::getString($objectOrClass);

        // ----------------
        // Process

        // Prepare object
        $object = match (true) {
            $reflectionMethod->isStatic()         => null,
            TypeManager::isObject($objectOrClass) => $objectOrClass,
            default                               => DependencyInjector::callConstructor(
                $className,
                $this->arguments
            )
        };

        // Compute parameters
        $parameters = $this->computeParameters($reflectionMethodArgs);

        // Call method
        return TypeManager::isEmpty($parameters)
            ? $reflectionMethod->invoke($object)
            : $reflectionMethod->invokeArgs($object, $parameters);
    }

    /**
     * System to call the function.
     *
     * @param ReflectionFunction $reflectionFunction
     *
     * @return mixed
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function function (ReflectionFunction $reflectionFunction): mixed
    {
        // ----------------
        // Vars

        // Get information
        $reflectionFunctionArgs = $reflectionFunction->getParameters();

        // ----------------
        // Process

        // Compute parameters
        $parameters = $this->computeParameters($reflectionFunctionArgs);

        return empty($parameters)
            ? $reflectionFunction->invoke()
            : $reflectionFunction->invokeArgs($parameters);
    }

    # --------------------------------
    # Utils methods

    /**
     * Validate a class.
     *
     * Verify if :
     *  - Is an interface
     *  - Is an abstract class
     *  - Is a trait
     *  - Isn't an instantiable class
     *
     * @param ReflectionClass $class
     *
     * @return void
     * @throws DependencyInjectorException
     */
    public function validateClass(ReflectionClass $class): void
    {
        // ----------------
        // Vars

        // Get class information
        $name = $class->getName();

        // ----------------
        // Process

        // Case : Is an interface
        if ($class->isInterface()) {
            throw new DependencyInjectorException(
                message   : "The class {$name} is an interface.",
                parameters: ['name' => $name]
            );
        }

        // Case : Is an abstract class
        if ($class->isAbstract()) {
            throw new DependencyInjectorException(
                message   : "The class {$name} is an abstract class.",
                parameters: ['name' => $name]
            );
        }

        // Case : Is a trait
        if ($class->isTrait()) {
            throw new DependencyInjectorException(
                message   : "The class {$name} is a trait.",
                parameters: ['name' => $name]
            );
        }

        // Case : Isn't instantiable
        if (!$class->isInstantiable()) {
            throw new DependencyInjectorException(
                message   : "Class {$name} not instantiable. (private or protected constructor)",
                parameters: ['name' => $name]
            );
        }
    }

    /**
     * Compute parameters.
     *
     * @param BagInterface|array $parameters
     *
     * @return array
     * @throws DependencyInjectorException
     * @throws ReflectionException
     */
    public function computeParameters(BagInterface|array $parameters): array
    {
        // ----------------
        // Vars

        // Get information
        $parameters         = ValueManager::getArray($parameters);
        $computedParameters = [];

        // ----------------
        // Process

        foreach ($parameters as $index => $parameter) {
            // Security check
            if (!ClassManager::is(ReflectionParameter::class, $parameter)) {
                break;
            }

            // Get information
            $isVariadic = $this->parameterProcessor->isVariadic($parameter);
            $value      = $this->parameterProcessor->compute(
                parameter: $parameter,
                arguments: $this->arguments,
                index    : $index
            );

            // Case : Is variadic parameter
            if ($isVariadic) {
                $computedParameters = [...$computedParameters, ...$this->arguments->all()];
                break;
            }

            // Case : Default
            $computedParameters[$parameter->getName()] = $value;
        }

        return $computedParameters;
    }
}
