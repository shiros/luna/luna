<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ArgumentProcessor.php
 * @Created_at  : 22/03/2023
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Processor;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Manager\ValueManager;

class ArgumentProcessor
{
    # --------------------------------
    # Attributes

    /**
     * Dependency injector arguments.
     *
     * @var BagInterface<string>
     */
    protected BagInterface $arguments;

    # --------------------------------
    # Constructor

    /**
     * ArgumentProcessor constructor.
     */
    public function __construct(
        BagInterface|array $arguments = []
    ) {
        // Set attributes
        $this->arguments = ValueManager::getBag($arguments);
    }

    # --------------------------------
    # Core methods

    /**
     * Compute arguments.
     * Merge given arguments with arguments in the configuration.
     * Returns a read only bag.
     *
     * @param BagInterface|array|null $arguments
     *
     * @return ReadOnlyBag
     */
    public function compute(BagInterface|array|null $arguments = null): ReadOnlyBag
    {
        // ----------------
        // Vars

        // Get information
        $arguments = ValueManager::getArray($arguments);

        // ----------------
        // Process

        return new ReadOnlyBag([
            ...$this->arguments->all(),
            ...$arguments
        ]);
    }
}
