<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ParameterProcessor.php
 * @Created_at  : 02/04/2023
 * @Update_at   : 08/06/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\DI\Processor;

use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\DI\Exception\DependencyInjectorException;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionParameter;

class ParameterProcessor
{
    # --------------------------------
    # Attributes

    /**
     * Dependency injector processor.
     * It's used to build a parameter.
     */
    protected DIProcessor $DIProcessor;

    /**
     * Dependency injector module processor.
     * It's to get a DI module.
     */
    protected ModuleProcessor $moduleProcessor;

    # --------------------------------
    # Constructor

    /**
     * ParameterProcessor constructor.
     *
     * @param DIProcessor     $DIProcessor
     * @param ModuleProcessor $moduleProcessor
     */
    public function __construct(
        DIProcessor     $DIProcessor,
        ModuleProcessor $moduleProcessor
    ) {
        // Set processors
        $this->DIProcessor     = $DIProcessor;
        $this->moduleProcessor = $moduleProcessor;
    }

    # --------------------------------
    # Core methods

    /**
     * Compute a reflection parameter.
     * Returns the constructed parameter or its default value.
     *
     * If an index is given, the process will check, at first, if there is a parameter for it in the given arguments.
     *
     * @param ReflectionParameter $parameter
     * @param ReadOnlyBag         $arguments
     * @param int|null            $index This is the position of the parameter in the call.
     *
     * @return mixed
     * @throws DependencyInjectorException  Cannot inject the parameter.
     * @throws ReflectionException          Issue when using reflection elements.
     */
    public function compute(
        ReflectionParameter $parameter,
        ReadOnlyBag         $arguments,
        ?int                $index = null
    ): mixed {
        // ----------------
        // Vars

        // Get parameter information
        $name            = $parameter->getName();
        $type            = $parameter->getType();
        $isVariadic      = $this->isVariadic($parameter);
        $isOptional      = $this->isOptional($parameter);
        $hasDefaultValue = $this->hasDefaultValue($parameter);

        // ----------------
        // Process

        // Case : Is a variadic parameter.
        if ($isVariadic) {
            return $arguments->all();
        }

        // Case : The parameter is known by its index
        if (!TypeManager::isNull($index) && !TypeManager::isNull($argument = $arguments->get($index))) {
            return $argument;
        }

        // Case : The parameter is known by name
        if (!TypeManager::isNull($argument = $arguments->get($name))) {
            return $argument;
        }

        // Case : The module is known by name
        if (!TypeManager::isNull($module = $this->moduleProcessor->get($name))) {
            return $this->moduleProcessor->compute($module, $arguments);
        }

        // Case : The module is known by type
        if (!TypeManager::isNull($type)
            && !TypeManager::isNull($module = $this->moduleProcessor->getByType($type))
        ) {
            return $this->moduleProcessor->compute($module, $arguments);
        }

        // Case : It's a named type and an existing class
        if (ClassManager::is(ReflectionNamedType::class, $type)
            && (ClassManager::exist($typeName = $type->getName()))
        ) {
            return $this->DIProcessor->construct(
                reflectionClass: new ReflectionClass($typeName)
            );
        }

        // Case : Is an optional parameter.
        if ($isOptional) {
            return $hasDefaultValue
                ? $parameter->getDefaultValue()
                : null;
        }

        // Case : Default
        throw new DependencyInjectorException("Cannot inject a value for attribute '{$name}'.");
    }

    # --------------------------------
    # Getter methods

    /**
     * Returns whether parameter is variadic.
     *
     * @param ReflectionParameter $parameter
     *
     * @return bool
     */
    public function isVariadic(ReflectionParameter $parameter): bool
    {
        return $parameter->isVariadic();
    }

    /**
     * Returns whether parameter is optional.
     *
     * @param ReflectionParameter $parameter
     *
     * @return bool
     */
    public function isOptional(ReflectionParameter $parameter): bool
    {
        return $parameter->isOptional();
    }

    /**
     * Returns whether parameter has a default value.
     *
     * @param ReflectionParameter $parameter
     *
     * @return bool
     */
    public function hasDefaultValue(ReflectionParameter $parameter): bool
    {
        return $parameter->isDefaultValueAvailable();
    }
}
