<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Lock.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Lock;

use Luna\Component\Bag\Bag;
use Luna\Component\File\Exception\Reader\LOCKFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\LOCKFileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\File\Manager\LOCKFileManager;
use Luna\Component\Lock\Exception\LockException;
use Luna\Component\Manager\PathManager;

class Lock extends Bag implements LockInterface
{
    # --------------------------------
    # Attributes

    /**
     * This is the path to the managed lock file.
     */
    protected string $path;

    # --------------------------------
    # Constructor

    /**
     * Lock constructor.
     *
     * @param string $path
     *
     * @throws FileWriterException
     * @throws LOCKFileReaderException
     * @throws LockException
     */
    public function __construct(string $path)
    {
        // Call parent constructor
        parent::__construct();

        // Set attributes
        $this->path = PathManager::sanitize($path);

        // Read file
        $this
            ->create()
            ->read()
        ;
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws FileWriterException
     * @throws LockException
     */
    public function create(): static
    {
        // ----------------
        // Process

        try {
            // Security check : File exists
            if (FileManager::exists($this->path)) {
                return $this;
            }

            // Generate file
            $this
                ->change(parameters: static::DEFAULT_STRUCTURE)
                ->save()
            ;

            return $this;
        } catch (LOCKFileWriterException $exception) {
            throw new LockException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: ['path' => $this->path],
                previous  : $exception,
            );
        }
    }

    /**
     * @inheritDoc
     *
     * @throws LOCKFileReaderException
     */
    public function read(): static
    {
        // ----------------
        // Process

        // Read lock file
        $data = LOCKFileManager::read($this->path, true);

        // Stored content
        $this
            ->change(parameters: static::DEFAULT_STRUCTURE)
            ->replaceRecursive($data)
        ;

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @throws LOCKFileWriterException
     * @throws FileWriterException
     */
    public function save(): static
    {
        // ----------------
        // Process

        // Write data in lock file
        LOCKFileManager::create(
            path    : $this->path,
            contents: $this->all(),
            flags   : JSON_PRETTY_PRINT
        );

        return $this;
    }
}