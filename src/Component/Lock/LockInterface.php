<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Lock.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 26/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Lock;

use Luna\Component\Bag\BagInterface;

interface LockInterface extends BagInterface
{
    # --------------------------------
    # Constants

    /**
     * This is the default structure of the lock file.
     */
    public const DEFAULT_STRUCTURE = [];

    # --------------------------------
    # Core methods

    /**
     * Generate the lock file.
     *
     * @return static
     */
    public function create(): static;

    /**
     * Read the lock file.
     *
     * **WARNING :** When this method is executed, it replaces stored data in the class.
     *
     * @return static
     */
    public function read(): static;

    /**
     * Save the data in the lock file.
     *
     * **WARNING :** When this method is executed, it replaces stored data in the lock file.
     *
     * @return static
     */
    public function save(): static;
}