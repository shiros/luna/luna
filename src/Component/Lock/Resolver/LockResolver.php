<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LockResolver.php
 * @Created_at  : 14/11/2024
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Lock\Resolver;

use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Lock\Builder\LockModuleBuilder;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

class LockResolver
{
    # --------------------------------
    # Core methods

    /**
     * Resolve modules.
     * Transforms provided data in a bag of LockModule.
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $data = [
     *      ['name' => 'shiros/luna-console', 'version' => '1.0.0', 'class' => 'Luna\Console\Module'],
     *      'shiros/luna-web' => ['name' => 'shiros/luna-web', 'version' => '1.0.0', 'class' => 'Lune\Web\Module']
     * ];
     * $wrongData = [
     *      'App\AppModule',
     *      10,
     *      true,
     *      ....
     * ];
     *
     * // Resolve modules
     * $modules = $resolver->resolveModules($data);
     * // Output is a bag with LockModule instance
     *
     * // Resolve wrong modules
     * $modules = $resolver->resolveFiles($wrongData);
     * // Output is an empty bag
     * ```
     *
     * @param BagInterface|array $modules
     *
     * @return BagInterface<string, LockModule>
     */
    public function resolveModules(BagInterface|array $modules): BagInterface
    {
        // ----------------
        // Vars

        // Get information
        $_modules = new Bag();

        // ----------------
        // Process

        // Browse modules
        foreach ($modules as $module) {
            // Get information
            $_module = LockModuleBuilder::build(
                data: ValueManager::getBag($module),
            );

            // Security check : Wrong module
            if (TypeManager::isNull($_module)) continue;

            // Set module
            $_modules->set(
                key  : $_module->getName(),
                value: $_module
            );
        }

        return $_modules;
    }
}
