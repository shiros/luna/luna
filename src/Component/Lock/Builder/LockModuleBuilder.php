<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LockModuleBuilder.php
 * @Created_at  : 14/11/2024
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Lock\Builder;

use Composer\Package\PackageInterface;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

class LockModuleBuilder
{
    # --------------------------------
    # Core methods

    /**
     * Build a lock-module from data.
     * If the data is incorrect, null is returned.
     *
     * @param BagInterface|array $data
     *
     * @return LockModule|null
     */
    public static function build(BagInterface|array $data): ?LockModule
    {
        // ----------------
        // Vars

        // Attributes
        $data = ValueManager::getBag($data);

        // ----------------
        // Process

        // Extract information
        $name    = $data->getAsString(LockModule::KEY_NAME);
        $version = $data->getAsString(LockModule::KEY_VERSION);
        $class   = $data->getAsString(LockModule::KEY_CLASS);

        // Security check : Wrong data
        if (TypeManager::isEmpty($name)) return null;
        if (TypeManager::isEmpty($version)) return null;
        if (TypeManager::isEmpty($class)) return null;

        // Create route
        return new LockModule(
            name   : $name,
            version: $version,
            class  : $class
        );
    }

    /**
     * Build a lock-module from composer data.
     *
     * @param PackageInterface $package
     * @param string           $class
     *
     * @return LockModule
     */
    public static function buildFromComposer(
        PackageInterface $package,
        string           $class
    ): LockModule {
        // ----------------
        // Vars

        // Get information
        $name    = $package->getName();
        $version = $package->getVersion();

        // ----------------
        // Process

        // Create route
        return new LockModule(
            name   : $name,
            version: $version,
            class  : $class
        );
    }
}
