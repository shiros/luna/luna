<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Lock.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 14/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Lock;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\LOCKFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\LOCKFileWriterException;
use Luna\Component\File\Manager\LOCKFileManager;
use Luna\Component\Lock\Entity\LockModule;
use Luna\Component\Lock\Exception\LockException;
use Luna\Component\Lock\Resolver\LockResolver;
use Luna\Component\Manager\ValueManager;

class LunaLock extends Lock
{
    # --------------------------------
    # Constants

    /**
     * Luna modules are stored on this key.
     */
    public const KEY_MODULES = 'modules';

    /**
     * This is the default structure.
     */
    public const DEFAULT_STRUCTURE = [
        self::KEY_MODULES => []
    ];

    # --------------------------------
    # Attributes

    /**
     * Lock resolver.
     */
    protected LockResolver $resolver;

    # --------------------------------
    # Constructor

    /**
     * LunaLock constructor.
     *
     * @param string $path
     *
     * @throws LockException
     * @throws LOCKFileReaderException
     * @throws FileWriterException
     */
    public function __construct(string $path)
    {
        // Set attributes
        $this->resolver = new LockResolver();

        // Call parent constructor
        parent::__construct($path);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public function read(): static
    {
        // ----------------
        // Process

        // Read lock file
        $data = ValueManager::getBag(
            value: LOCKFileManager::read($this->path, true)
        );

        // Set configuration
        $this
            ->change(parameters: static::DEFAULT_STRUCTURE)
            ->set(
                key  : self::KEY_MODULES,
                value: $this->resolver->resolveModules(
                    modules: $data->getAsArray(self::KEY_MODULES)
                )
            )
            ->save()
        ;

        return $this;
    }

    # --------------------------------
    # Getters

    /**
     * Returns registered modules.
     *
     * **WARNING :** The returned instance is a clone.
     *
     * @return BagInterface<string, LockModule>
     */
    public function getModules(): BagInterface
    {
        return clone $this->getAsBag(key: self::KEY_MODULES);
    }

    # --------------------------------
    # Setters

    /**
     * Set modules.
     *
     * @param BagInterface $modules
     *
     * @return self
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public function setModules(BagInterface $modules): self
    {
        return $this
            ->set(
                key  : self::KEY_MODULES,
                value: $modules
            )
            ->save()
        ;
    }
}