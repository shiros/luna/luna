<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LockModule.php
 * @Created_at  : 13/11/2024
 * @Update_at   : 14/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Lock\Entity;

use JsonSerializable;

class LockModule implements JsonSerializable
{
    // --------------------------------
    // Constants

    /**
     * Lock key for module name.
     */
    public const KEY_NAME = 'name';

    /**
     * Lock key for module version.
     */
    public const KEY_VERSION = 'version';

    /**
     * Lock key for module class.
     */
    public const KEY_CLASS = 'class';

    # --------------------------------
    # Attributes

    /**
     * This is the module name. (shiros/luna)
     */
    protected string $name;

    /**
     * This is the module version. (1.0.0)
     */
    protected string $version;

    /**
     * This is the module class. (Luna\Module)
     */
    protected string $class;

    # --------------------------------
    # Constructor

    /**
     * LockModule constructor.
     *
     * @param string $name
     * @param string $version
     * @param string $class
     */
    public function __construct(
        string $name,
        string $version,
        string $class
    ) {
        $this->name    = $name;
        $this->version = $version;
        $this->class   = $class;
    }

    # --------------------------------
    # Getters

    /**
     * Get the module name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the module version.
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * Get the module class.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    # --------------------------------
    # Serialize methods

    /**
     * Implementation of JsonSerializable interface.
     *
     * Serialize the lock-module.
     * Returns as valid JSON object.
     *
     * @see JsonSerializable::jsonSerialize()
     *
     * @return mixed
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_NAME    => $this->getName(),
            self::KEY_VERSION => $this->getVersion(),
            self::KEY_CLASS   => $this->getClass(),
        ];
    }
}
