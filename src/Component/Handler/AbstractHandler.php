<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : HandlerInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 26/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Handler;

use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Logger\LoggerInterface;
use Luna\Config\LunaConfig;

abstract class AbstractHandler implements HandlerInterface
{
    # --------------------------------
    # Attributes


    protected LunaContainer   $container;
    protected LunaConfig      $config;
    protected LoggerInterface $logger;

    # --------------------------------
    # Constructor

    /**
     * Handler constructor
     *
     * @throws ContainerException
     */
    public function __construct()
    {
        // Set attributes
        $this->container = LunaContainer::getInstance();
        $this->config    = $this->container->getConfig();
        $this->logger    = $this->container->getLogger();
    }
}