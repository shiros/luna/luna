<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : HandlerInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 26/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Handler;

use Luna\Component\Dispatcher\Entity\EventInterface;

interface HandlerInterface
{
    # --------------------------------
    # Constants

    // Options
    public const OPT_LOG_LEVEL = 'log_level';

    # --------------------------------
    # Core methods

    /**
     * Handler an event.
     *
     * @param EventInterface $event
     *
     * @return void
     */
    public function handle(EventInterface $event): void;
}