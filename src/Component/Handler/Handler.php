<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : HandlerInterface.php
 * @Created_at  : 26/11/2023
 * @Update_at   : 28/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Handler;

use Luna\Component\Dispatcher\Entity\EventInterface;
use Luna\Component\Logger\Exception\LoggerException;

class Handler extends AbstractHandler
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function handle(EventInterface $event): void
    {
        // ----------------
        // Vars

        // Get information
        $data       = $event->getData();
        $parameters = $event->getParameters()->all();
        $options    = $event->getOptions();

        // Options
        $logLevel = $options->get(static::OPT_LOG_LEVEL, 'info');

        // ----------------
        // Process

        // Logs
        $this->logger->log($logLevel, $data, $parameters);
    }
}