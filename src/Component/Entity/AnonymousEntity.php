<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AnonymousEntity.php
 * @Created_at  : 24/11/2016
 * @Update_at   : 20/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Entity;

use Luna\Component\Manager\ClassManager;

class AnonymousEntity
{
    # --------------------------------
    # Constants

    protected const METHOD_GET_PREFIX = 'get';
    protected const METHOD_SET_PREFIX = 'set';

    # --------------------------------
    # Constructor

    /**
     * AnonymousEntity constructor.
     *
     * @param array|null $array
     */
    public function __construct(?array $array = null)
    {
        // Prepare class attributes
        if (!is_null($array)) {
            foreach ($array as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    # --------------------------------
    # Getters

    /**
     * Magic getter.
     *
     * @param $key
     *
     * @return mixed
     */
    public function __get($key): mixed
    {
        // ----------------
        // Prepare

        // Key
        $key = lcfirst($key);

        // Method
        $method = self::METHOD_GET_PREFIX . ucfirst($key);

        // ----------------
        // Process

        // Case : Getter method
        if (ClassManager::hasMethod($method, $this)) {
            return $this->$method();
        }

        // Case : Key exists
        if (property_exists($this, $key)) {
            return $this->$key;
        }

        // Case : Key exists (ucfisrt)
        if (property_exists($this, ucfirst($key))) {
            $key = ucfirst($key);
            return $this->$key;
        }

        // Case : Default
        return null;
    }

    # --------------------------------
    # Setters

    /**
     * Magic setter.
     *
     * @param $key
     * @param $value
     *
     * @return void
     */
    public function __set($key, $value): void
    {
        // ----------------
        // Prepare

        // Key
        $key = lcfirst($key);

        // Method
        $method = self::METHOD_SET_PREFIX . ucfirst($key);

        // ----------------
        // Process

        // Case : Setter method
        if (ClassManager::hasMethod($method, $this)) {
            $this->$method($value);
        }

        // Case : Key exists
        if (property_exists($this, $key)) {
            $this->$key = $value;
        }

        // Case : Key exists (ucfisrt)
        if (property_exists($this, ucfirst($key))) {
            $key        = ucfirst($key);
            $this->$key = $value;
        }

        // Case : Default
        $this->$key = $value;
    }
}
