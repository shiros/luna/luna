<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PathManager.php
 * @Created_at  : 03/11/2024
 * @Update_at   : 06/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

class PathManager
{
    # --------------------------------
    # Core methods

    /**
     * Sanitize the given path.
     *
     * Usage :
     * ```
     * // Get paths
     * $path1    = './good/path';
     * $path2    = './slash-terminated/path/';
     * $path3    = '.\path\with\slashes\different\from\system';
     * $path4    = null;
     * $path5    = '';
     *
     * // Sanitize paths
     * $path1 = PathManager::sanitize($path1);
     * $path2 = PathManager::sanitize($path2);
     * $path3 = PathManager::sanitize($path3);
     * $path4 = PathManager::sanitize($path4);
     * $path5 = PathManager::sanitize($path5);
     *
     * // Outputs :
     * // - For '$path1' => './good/path'
     * // - For '$path2' => './slash-terminated/path'
     * // - For '$path3' => './path/with/slashes/different/from/system'
     * // - For '$path4' => ''
     * // - For '$path5' => ''
     * ```
     *
     * @param string|null $path
     *
     * @return string
     */
    public static function sanitize(?string $path): string
    {
        // ----------------
        // Process

        // Security check : Path is null or empty
        if (TypeManager::isEmpty($path)) return '';

        // Sanitize path
        $path = ValueManager::trim($path);
        $path = ValueManager::regexp_replace('/[\/\\\]/', DIRECTORY_SEPARATOR, $path);
        $path = ValueManager::rtrim($path, DIRECTORY_SEPARATOR);

        return $path;
    }
}
