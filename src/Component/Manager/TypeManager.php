<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : TypeManager.php
 * @Created_at  : 04/07/2021
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

use DateTimeInterface;
use Luna\Component\Bag\BagInterface;
use Stringable;

class TypeManager
{
    # --------------------------------
    # Primary methods

    /**
     * Check if the value is null.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isNull(mixed $value): bool
    {
        return is_null($value);
    }

    /**
     * Get if the value is empty.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isEmpty(mixed $value): bool
    {
        // Case : Is Null
        if (self::isNull($value)) {
            return true;
        }

        // Case : Is a bag
        if (self::isBag($value)) {
            return $value->isEmpty();
        }

        return empty($value);
    }

    /**
     * Get if the value is a string.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isString(mixed $value): bool
    {
        return is_string($value);
    }

    /**
     * Get if the value is a number.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isNumber(mixed $value): bool
    {
        return self::isInteger($value) || self::isLong($value) || self::isFloat($value) || self::isDouble($value);
    }

    /**
     * Get if the value is a integer.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isInteger(mixed $value): bool
    {
        return is_int($value)
            || (self::isString($value) && preg_match('#^[0-9]+$#', $value));
    }

    /**
     * Get if the value is a long.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isLong(mixed $value): bool
    {
        return is_long($value)
            || (self::isString($value) && preg_match('#^[0-9]+$#', $value));
    }

    /**
     * Get if the value is a float.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isFloat(mixed $value): bool
    {
        return is_float($value)
            || (self::isString($value) && preg_match('#^[0-9]+(\.|,)^[0-9]+$#', $value));
    }

    /**
     * Get if the value is a double.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isDouble(mixed $value): bool
    {
        return is_double($value)
            || (self::isString($value) && preg_match('#^[0-9]+(\.|,)^[0-9]+$#', $value));
    }

    /**
     * Get if the value is boolean.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isBoolean(mixed $value): bool
    {
        return is_bool($value);
    }

    # --------------------------------
    # Complex methods

    /**
     * Get if value implement stringable interface.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isStringable(mixed $value): bool
    {
        return !self::isNull($value) && ClassManager::implement(Stringable::class, $value);
    }

    /**
     * Get if value is iterable.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isIterable(mixed $value): bool
    {
        return is_iterable($value);
    }

    /**
     * Get if value implement date time interface.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isDate(mixed $value): bool
    {
        return !self::isNull($value) && ClassManager::implement(DateTimeInterface::class, $value);
    }

    /**
     * Get if value is an array.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isArray(mixed $value): bool
    {
        return !self::isNull($value) && is_array($value);
    }

    /**
     * Get if value is a bag.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isBag(mixed $value): bool
    {
        return !self::isNull($value)
            && ClassManager::implement(interface: BagInterface::class, objectOrClass: $value);
    }

    /**
     * Get if value is an object.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isObject(mixed $value): bool
    {
        return !self::isNull($value) && is_object($value);
    }

    /**
     * Get if value is a function.
     *
     * @param string $value
     *
     * @return bool
     */
    public static function isFunction(mixed $value): bool
    {
        return !self::isNull($value) && is_callable($value);
    }

    /**
     * Get if value is a resource.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isResource(mixed $value): bool
    {
        return is_resource($value);
    }

    /**
     * Get if value is a stream.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isStream(mixed $value): bool
    {
        return self::isResource($value) && get_resource_type($value) === 'stream';
    }
}
