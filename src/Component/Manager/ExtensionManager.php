<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 * @Contributor : Maxime Mazet
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ExtensionManager.php
 * @Created_at  : 22/05/2024
 * @Update_at   : 22/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

class ExtensionManager
{
    # --------------------------------
    # Core methods

    /**
     * Verify is a PHP extension is enabled.
     *
     * @param string $extension
     *
     * @return bool
     */
    public static function has(string $extension): bool
    {
        return extension_loaded($extension);
    }
}
