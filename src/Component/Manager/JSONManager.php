<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : JSONManager.php
 * @Created_at  : 28/07/2021
 * @Update_at   : 24/06/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

class JSONManager
{
    # --------------------------------
    # Core methods

    /**
     * Encode a value into JSON string.
     *
     * @see  json_encode() for more information
     *
     * @link https://php.net/manual/en/function.json-encode.php
     *
     * @param mixed $value
     * @param int   $flags
     * @param int   $depth
     *
     * @return string|null
     */
    public static function encode(mixed $value, int $flags = 0, int $depth = 512): ?string
    {
        return json_encode($value, $flags, $depth);
    }

    /**
     * Decode JSON string.
     *
     * @see  json_decode() for more information
     *
     * @link https://php.net/manual/en/function.json-decode.php
     *
     * @param string    $json
     * @param bool|null $associative
     * @param int       $flags
     * @param int       $depth
     *
     * @return mixed
     */
    public static function decode(string $json, ?bool $associative = false, int $flags = 0, int $depth = 512): mixed
    {
        return json_decode($json, $associative, $depth, $flags);
    }
}
