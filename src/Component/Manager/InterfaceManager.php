<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : InterfaceManager.php
 * @Created_at  : 02/12/2020
 * @Update_at   : 02/12/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

class InterfaceManager
{
    # --------------------------------
    # Definition methods

    /**
     * Returns whether the class exists.
     *
     * @param string $class The class name
     *
     * @return bool
     */
    public static function exist(string $class): bool
    {
        return interface_exists($class);
    }
}
