<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ClassManager.php
 * @Created_at  : 05/04/2020
 * @Update_at   : 02/12/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

class ClassManager
{
    # --------------------------------
    # Naming methods

    /**
     * Get the name of the object.
     *
     * @param object $object
     *
     * @return string
     */
    public static function nameOf(object $object): string
    {
        return get_class($object);
    }

    # --------------------------------
    # Definition methods

    /**
     * Returns whether the class exists.
     *
     * @param string $class The class name
     *
     * @return bool
     */
    public static function exist(string $class): bool
    {
        return class_exists($class);
    }

    /**
     * Returns whether the class/object is a specific class or extends/implement a specific class/interface.
     *
     * @param string $class         The class name
     * @param mixed  $objectOrClass An object instance or a class name
     *
     * @return bool
     */
    public static function is(string $class, mixed $objectOrClass): bool
    {
        return is_a($objectOrClass, $class, true);
    }

    /**
     * Returns whether the class implement a specific interface.
     *
     * @param string $interface     The interface name
     * @param mixed  $objectOrClass An object instance or a class name
     *
     * @return bool
     */
    public static function implement(string $interface, mixed $objectOrClass): bool
    {
        return is_subclass_of($objectOrClass, $interface);
    }

    /**
     * Returns whether the class extend a specific class.
     *
     * @param string $parentClass   The parent name
     * @param mixed  $objectOrClass An object instance or a class name
     *
     * @return bool
     */
    public static function extend(string $parentClass, mixed $objectOrClass): bool
    {
        return is_subclass_of($objectOrClass, $parentClass);
    }

    # --------------------------------
    # Has/Is methods

    /**
     * Checks if function exist.
     *
     * @param string $function The function name
     *
     * @return bool
     */
    public static function hasFunction(string $function): bool
    {
        return function_exists($function);
    }

    /**
     * Checks if class/object have specific method.
     *
     * @param string $method The methode name
     * @param mixed  $object An object instance or a class name
     *
     * @return bool
     */
    public static function hasMethod(string $method, mixed $object): bool
    {
        return method_exists($object, $method);
    }

    /**
     * Checks if value could be callable.
     *
     * @link https://php.net/manual/en/function.is-callable.php
     *
     * @param mixed $value
     * @param bool  $syntaxOnly
     *
     * @return bool
     */
    public static function isCallable(mixed $value, bool $syntaxOnly = false): bool
    {
        return is_callable($value, $syntaxOnly);
    }
}
