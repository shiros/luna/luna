<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ObjectManager.php
 * @Created_at  : 05/04/2020
 * @Update_at   : 11/03/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

use Luna\Component\Manager\Exception\ManagerException;
use ReflectionClass;
use Throwable;

class ObjectManager
{
    # --------------------------------
    # Core methods

    /**
     * Check if an object has an attribute.
     *
     * @param mixed  $object
     * @param string $attribute
     *
     * @return bool
     * @throws ManagerException
     */
    public static function hasAttribute(mixed $object, string $attribute): bool
    {
        // ----------------
        // Vars

        // Information
        $methods = [
            'get' . ucfirst($attribute),
            'get' . strtolower($attribute),
            'get_' . ucfirst($attribute),
            'get_' . strtolower($attribute)
        ];

        // ----------------
        // Process

        // Security check
        if (!TypeManager::isObject($object)) {
            return false;
        }

        try {
            // Reflection information
            $reflection = new ReflectionClass($object);

            // Case : Property
            if ($reflection->hasProperty($attribute)) {
                return true;
            }

            // Case : Method
            foreach ($methods as $method) {
                if ($reflection->hasMethod($method)) {
                    return true;
                }
            }

            // Case : Default
            return false;
        } catch (Throwable $throwable) {
            throw new ManagerException(
                message   : $throwable->getMessage(),
                parameters: [ClassManager::nameOf($object)],
                previous  : $throwable
            );
        }
    }

    # --------------------------------
    # Getters

    /**
     * Get the value of the attribute of an object.
     *
     * @param mixed  $object
     * @param string $attribute
     * @param mixed  $default
     *
     * @return mixed
     * @throws ManagerException
     */
    public static function getValue(mixed $object, string $attribute, mixed $default = null): mixed
    {
        // ----------------
        // Vars

        // Information
        $methods = [
            'get' . ucfirst($attribute),
            'get' . strtolower($attribute),
            'get_' . ucfirst($attribute),
            'get_' . strtolower($attribute)
        ];

        // ----------------
        // Process

        // Security check
        if (!TypeManager::isObject($object)) {
            return $default;
        }

        try {
            // Reflection information
            $reflection = new ReflectionClass($object);
            $property   = $reflection->hasProperty($attribute) ? $reflection->getProperty($attribute) : null;

            // Case : Property
            if (!TypeManager::isNull($property) && $property->isPublic()) {
                return $property->isStatic()
                    ? $property->getValue()
                    : $property->getValue($object);
            }

            // Case : Method
            foreach ($methods as $method) {
                // Get method
                $method = $reflection->hasMethod($method) ? $reflection->getMethod($method) : null;

                // Security check
                if (TypeManager::isNull($method)
                    || $method->isAbstract()
                    || $method->isConstructor()
                    || $method->isDestructor()
                    || !$method->isPublic()
                ) {
                    continue;
                }

                // Invoke
                return $method->isStatic()
                    ? $method->invoke(null)
                    : $method->invoke($object);
            }

            // Case : Default
            return $default;
        } catch (Throwable $throwable) {
            throw new ManagerException(
                message   : $throwable->getMessage(),
                parameters: [ClassManager::nameOf($object)],
                previous  : $throwable
            );
        }
    }

    # --------------------------------
    # Setters

    /**
     * Set value to an object attribute.
     *
     * @param mixed  $object
     * @param string $attribute
     * @param mixed  $value
     *
     * @return void
     * @throws ManagerException
     */
    public static function setValue(mixed $object, string $attribute, mixed $value): void
    {
        // ----------------
        // Vars

        // Information
        $methods = [
            'set' . ucfirst($attribute),
            'set' . strtolower($attribute),
            'set_' . ucfirst($attribute),
            'set_' . strtolower($attribute)
        ];

        // ----------------
        // Process

        // Security check
        if (!TypeManager::isObject($object)) {
            return;
        }

        try {
            // Reflection information
            $reflection = new ReflectionClass($object);
            $property   = $reflection->hasProperty($attribute) ? $reflection->getProperty($attribute) : null;

            // Case : Property
            if (!TypeManager::isNull($property) && $property->isPublic() && !$property->isReadOnly()) {
                $property->isStatic()
                    ? $property->setValue(null, $value)
                    : $property->setValue($object, $value);
            }

            // Case : Method
            foreach ($methods as $method) {
                // Get method
                $method = $reflection->hasMethod($method) ? $reflection->getMethod($method) : null;

                // Security check
                if (TypeManager::isNull($method)
                    || $method->isAbstract()
                    || $method->isConstructor()
                    || $method->isDestructor()
                    || !$method->isPublic()
                ) {
                    continue;
                }

                // Invoke
                $method->isStatic()
                    ? $method->invoke(null, $value)
                    : $method->invoke($object, $value);
            }

            // Case : Default
            throw new ManagerException('Value set failed, no correct setters was found.');
        } catch (Throwable $throwable) {
            throw new ManagerException(
                message   : $throwable->getMessage(),
                parameters: [ClassManager::nameOf($object)],
                previous  : $throwable
            );
        }
    }
}
