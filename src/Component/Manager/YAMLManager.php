<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : YAMLManager.php
 * @Created_at  : 24/09/2021
 * @Update_at   : 17/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

use Luna\Component\Manager\Exception\ManagerException;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class YAMLManager
{
    # --------------------------------
    # Core methods

    /**
     * Encode a value into YAML string.
     *
     * @see Yaml for flags
     * @see Yaml::dump() for more information
     *
     * @param mixed $value
     * @param int   $inline The level where you switch to inline YAML
     * @param int   $indent The indentation used for nested nodes
     * @param int   $flags  Encoding flags, to define YAML string generation
     *
     * @return string
     */
    public static function encode(
        mixed $value,
        int   $inline = 2,
        int   $indent = 4,
        int   $flags = 0
    ): string {
        return Yaml::dump(
            input : $value,
            inline: $inline,
            indent: $indent,
            flags : $flags
        );
    }

    /**
     * Decode YAML string.
     *
     * @see Yaml for flags
     * @see Yaml::parse() for more information
     *
     * @param string $yaml
     * @param int    $flags Decoding flags
     *
     * @return mixed
     *
     * @throws ManagerException Parsing issue
     */
    public static function decode(
        string $yaml,
        int    $flags = 0
    ): mixed {
        try {
            return Yaml::parse(
                input: $yaml,
                flags: $flags
            );
        } catch (ParseException $exception) {
            throw new ManagerException(
                message   : $exception->getMessage(),
                parameters: [
                    'yaml'  => $yaml,
                    'flags' => $flags
                ],
                previous  : $exception
            );
        }
    }
}
