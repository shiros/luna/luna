<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 * @Contributor : Maxime Mazet
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ValueManager.php
 * @Created_at  : 04/07/2021
 * @Update_at   : 10/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Manager;

use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Manager\Exception\ManagerException;
use Throwable;

class ValueManager
{
    # --------------------------------
    # Constants

    public const SEPARATOR = ',';

    # --------------------------------
    # Core methods

    /**
     * Try to cast value in string.
     * If the cast process failed, this method returns empty string as default.
     *
     * @param mixed       $value
     * @param string|null $default
     *
     * @return string|null
     */
    public static function getString(mixed $value, ?string $default = ''): ?string
    {
        // ----------------
        // Process

        // Case : Is boolean
        if (TypeManager::isBoolean($value)) {
            return $value ? 'true' : 'false';
        }

        // Case : Is string
        if (TypeManager::isString($value)) {
            return $value;
        }

        // Case : Is number
        if (TypeManager::isNumber($value)) {
            return strval($value);
        }

        // Case : Is stringable
        if (TypeManager::isStringable($value)) {
            return $value->__toString();
        }

        // Case : Is object
        if (TypeManager::isObject($value)) {
            return ClassManager::nameOf($value);
        }

        return $default;
    }

    /**
     * Try to cast value in integer.
     * If the cast process failed, this method returns 0 as default.
     *
     * @param mixed    $value
     * @param int|null $default
     *
     * @return int|null
     */
    public static function getInteger(mixed $value, ?int $default = 0): ?int
    {
        // ----------------
        // Process

        // Case : Is null
        if (TypeManager::isNull($value) || TypeManager::isEmpty($value)) {
            return $default;
        }

        // Case : Is boolean
        if (TypeManager::isBoolean($value)) {
            return $value ? 1 : 0;
        }

        // Case : Is integer
        if (TypeManager::isInteger($value)) {
            return $value;
        }

        return intval($value);
    }

    /**
     * Try to cast value in float.
     * If the cast process failed, this method returns 0 as default.
     *
     * @param mixed      $value
     * @param float|null $default
     *
     * @return float|null
     */
    public static function getFloat(mixed $value, ?float $default = 0): ?float
    {
        // ----------------
        // Process

        // Case : Is null
        if (TypeManager::isNull($value) || TypeManager::isEmpty($value)) {
            return $default;
        }

        // Case : Is boolean
        if (TypeManager::isBoolean($value)) {
            return $value ? 1 : 0;
        }

        // Case : Is float
        if (TypeManager::isFloat($value)) {
            return $value;
        }

        return floatval($value);
    }

    /**
     * Try to cast value in boolean.
     * If the cast process failed, this method returns a false boolean as default.
     *
     * @param mixed     $value
     * @param bool|null $default
     *
     * @return bool|null
     */
    public static function getBoolean(mixed $value, ?bool $default = false): ?bool
    {
        // ----------------
        // Process

        // Case : Is boolean
        if (TypeManager::isBoolean($value)) {
            return $value;
        }

        // Case : Is string
        if (TypeManager::isString($value)) {
            return ($value === 'true' || $value === '1');
        }

        // Case : Is number
        if (TypeManager::isNumber($value)) {
            return $value === 1;
        }

        // Case : Is array
        if (TypeManager::isArray($value)) {
            return !TypeManager::isEmpty($value);
        }

        return $default;
    }

    /**
     * Try to cast value in array.
     * If the cast process failed, this method returns an empty array as default.
     *
     * @param mixed      $value
     * @param array|null $default
     *
     * @return array|null
     */
    public static function getArray(mixed $value, ?array $default = []): ?array
    {
        // ----------------
        // Process

        // Case : Is Array
        if (TypeManager::isArray($value)) {
            return $value;
        }

        // Case : Implement Bag interface
        if (ClassManager::implement(BagInterface::class, $value)) {
            return $value->all();
        }

        return $default;
    }

    /**
     * Try to cast value in a bag.
     * If the value implements the bag interface, the returned value will be the corresponding bag.
     * If the value is an array, the return value will be a parameter bag.
     * If the cast process failed, this method returns an empty parameter bag as default.
     *
     * @param mixed    $value
     * @param Bag|null $default
     *
     * @return Bag|null
     */
    public static function getBag(mixed $value, ?BagInterface $default = new Bag()): ?BagInterface
    {
        // ----------------
        // Process

        // Case : Implement Bag interface
        if (ClassManager::implement(BagInterface::class, $value)) {
            return $value;
        }

        // Case : Is Array
        if (TypeManager::isArray($value)) {
            return new Bag($value);
        }

        return $default;
    }

    # --------------------------------
    # Utils methods

    /**
     * Make the first string's letter to upper case.
     *
     * @link https://php.net/manual/en/function.ucfirst.php
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function ucFirst(mixed $value): string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return ucfirst($value);
    }

    /**
     * Make a string to upper case.
     *
     * @link https://php.net/manual/en/function.strtoupper.php
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function toUpper(mixed $value): string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return strtoupper($value);
    }

    /**
     * Make a string to lower case.
     *
     * @link https://php.net/manual/en/function.strtolower.php
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function toLower(mixed $value): string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return strtolower($value);
    }

    /**
     * Returns whether the value starts with needle parameter.
     *
     * @link https://php.net/manual/en/function.str-starts-with.php
     *
     * @param mixed  $value
     * @param string $needle
     *
     * @return bool
     */
    public static function startsWith(mixed $value, string $needle): bool
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return str_starts_with($value, $needle);
    }

    /**
     * Returns whether the value ends with needle parameter.
     *
     * @link https://php.net/manual/en/function.str-ends-with.php
     *
     * @param mixed  $value
     * @param string $needle
     *
     * @return bool
     */
    public static function endsWith(mixed $value, string $needle): bool
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return str_ends_with($value, $needle);
    }

    /**
     * Get the value's length.
     * If the length of the value cannot be calculated, false is returned.
     *
     * @link https://php.net/manual/en/function.strlen.php
     *
     * @param mixed $value
     *
     * @return int|false
     */
    public static function lengthOf(mixed $value): int|false
    {
        // ----------------
        // Process

        // Case : String
        if (TypeManager::isString($value)) {
            return strlen($value);
        }

        // Case : Array
        if (TypeManager::isArray($value)) {
            return count($value);
        }

        // Case : Array
        if (ClassManager::implement(BagInterface::class, $value)) {
            return $value->count();
        }

        return false;
    }

    /**
     * Get a random integer between 2 numbers.
     *
     * @param mixed $min
     * @param mixed $max
     *
     * @return int
     * @throws ManagerException
     */
    public static function randomInteger(mixed $min, mixed $max): int
    {
        // ----------------
        // Vars

        // Attributes
        $min = self::getInteger($min);
        $max = self::getInteger($max, 1);

        // ----------------
        // Process

        // Generate
        try {
            return random_int($min, $max);
        } catch (Throwable $throwable) {
            throw new ManagerException(
                message   : 'An error is occurred when trying to generate a random integer.',
                parameters: ['min' => $min, 'max' => $max],
                previous  : $throwable
            );
        }
    }

    /**
     * Get a random string.
     * The length can be defined by thanks to the function parameters.
     *
     * @param int $length
     *
     * @return int
     * @throws ManagerException
     */
    public static function randomString(int $length = 32): int
    {
        // ----------------
        // Vars

        $nbBytes = $length / 2;

        // ----------------
        // Process

        // Generate
        try {
            // Generate random bytes
            $bytes = random_bytes($nbBytes);

            // Convert to hex
            $string = bin2hex($bytes);

            return self::truncate($string, $length);
        } catch (Throwable $throwable) {
            throw new ManagerException(
                message   : 'An error is occurred when trying to generate a random string.',
                parameters: ['length' => $length],
                previous  : $throwable
            );
        }
    }

    /**
     * Repeat a character string, as many times as the multiplier indicates.
     *
     * @link https://php.net/manual/en/function.str_repeat.php
     *
     * @param mixed $value
     * @param int   $times
     *
     * @return string
     */
    public static function repeat(mixed $value, int $times = 1): string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        return str_repeat($value, $times);
    }

    /**
     * Truncate a string.
     *
     * @link https://php.net/manual/en/function.substr.php
     *
     * @param mixed $value
     * @param mixed $size
     *
     * @return string
     */
    public static function truncate(mixed $value, mixed $size): string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);
        $size  = self::getInteger($size);

        // ----------------
        // Process

        if (strlen($value) > $size) {
            $value = substr($value, 0, $size);
        }

        return $value;
    }

    /**
     * Split a string by a separator.
     *
     * If separator is null or empty, the value will be split character by character.
     * The limit is used to split the value by the limit value.
     *
     * If limit is set to null, it will be ignored.
     *
     * @link https://php.net/manual/en/function.explode.php
     *
     * @param mixed    $value
     * @param mixed    $separator
     * @param int|null $limit
     *
     * @return array|string
     */
    public static function split(
        mixed $value,
        mixed $separator = self::SEPARATOR,
        ?int  $limit = null
    ): array|string {
        // ----------------
        // Vars

        // Attributes
        $value     = self::getString($value);
        $separator = self::getString($separator);

        // ----------------
        // Process

        // Case : Separator is empty
        if (TypeManager::isEmpty($separator)) {
            return TypeManager::isNull($limit)
                ? str_split($value)
                : str_split($value, $limit);
        }

        // Case : Default
        $result = TypeManager::isNull($limit)
            ? explode($separator, $value)
            : explode($separator, $value, $limit);

        return $result ?: $value;
    }

    /**
     * Join an array by a separator.
     *
     * @link https://php.net/manual/en/function.implode.php
     *
     * @param mixed $value
     * @param mixed $separator
     *
     * @return string
     */
    public static function join(mixed $value, array|string $separator = self::SEPARATOR): string
    {
        return implode(
            separator: $separator,
            array    : self::getArray($value)
        );
    }

    /**
     * Check if a value match with a regexp.
     *
     * @link https://php.net/manual/en/function.preg_match.php
     *
     * @param string $pattern
     * @param mixed  $value
     * @param array  $matches
     * @param int    $flags
     * @param int    $offset
     *
     * @return bool
     */
    public static function match(
        string $pattern,
        mixed  $value,
        array  &$matches = [],
        int    $flags = 0,
        int    $offset = 0
    ): bool {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value);

        // ----------------
        // Process

        // Match process
        $match = preg_match($pattern, $value, $matches, $flags, $offset);

        return self::getBoolean($match);
    }

    /**
     * Trim a string.
     *
     * @link https://php.net/manual/en/function.trim.php
     *
     * @param mixed  $value
     * @param string $characters
     *
     * @return string|null
     */
    public static function trim(mixed $value, string $characters = " \t\n\r\0\x0B"): ?string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value, null);

        // ----------------
        // Process

        // Case : Is null
        if (TypeManager::isNull($value)) {
            return null;
        }

        // Case : Default
        return trim($value, $characters);
    }

    /**
     * Left trim a string.
     *
     * @link https://php.net/manual/en/function.ltrim.php
     *
     * @param mixed  $value
     * @param string $characters
     *
     * @return string|null
     */
    public static function ltrim(mixed $value, string $characters = " \t\n\r\0\x0B"): ?string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value, null);

        // ----------------
        // Process

        // Case : Is null
        if (TypeManager::isNull($value)) {
            return null;
        }

        // Case : Default
        return ltrim($value, $characters);
    }

    /**
     * Right trim a string.
     *
     * @link https://php.net/manual/en/function.rtrim.php
     *
     * @param mixed  $value
     * @param string $characters
     *
     * @return string|null
     */
    public static function rtrim(mixed $value, string $characters = " \t\n\r\0\x0B"): ?string
    {
        // ----------------
        // Vars

        // Attributes
        $value = self::getString($value, null);

        // ----------------
        // Process

        // Case : Is null
        if (TypeManager::isNull($value)) {
            return null;
        }

        // Case : Default
        return rtrim($value, $characters);
    }

    /**
     * Check if the needle exist in your value
     *
     * @link https://php.net/manual/en/function.str-contains.php
     *
     * @param mixed $value
     * @param mixed $needle
     * @param bool  $keyOnly  Used when the value is an array or a bag
     * @param bool  $nullable Used when the value is an array or a bag
     *
     * @return bool
     */
    public static function contains(
        mixed $value,
        mixed $needle,
        bool  $keyOnly = false,
        bool  $nullable = false,
    ): bool {
        // ----------------
        // Vars

        // Attributes
        $subject      = self::getString($value);
        $needleString = self::getString($needle);

        // ----------------
        // Process

        /**
         * Case : Value is an array.
         *
         * Process explanation
         *   - If 'Key Only' : Apply contains on keys only
         *   - Else          : Search if the array contains the value
         *
         * The 'nullable' option allows to match a key even if its value is null.
         */
        if (TypeManager::isArray($value)) {
            $subject = self::getBag($value);
            return $keyOnly
                ? $subject->has($needle, $nullable)
                : $subject->contains($needle);
        }

        /**
         * Case : Value is an array or bag.
         *
         * Process explanation
         *   - If 'Key Only' : Apply contains on keys only
         *   - Else          : Search if the array contains the value
         *
         * The 'nullable' option allows to match a key even if its value is null.
         *
         * @var BagInterface $value
         */
        if (TypeManager::isBag($value)) {
            return $keyOnly
                ? $value->has($needle, $nullable)
                : $value->contains($needle);
        }

        // Default : Using PHP function 'str_contains'
        return str_contains($subject, $needleString);
    }

    /**
     * Replace the characters in your value.
     *
     * @link https://php.net/manual/en/function.str-replace.php
     *
     * @param array|string $search
     * @param array|string $replace
     * @param mixed        $value
     *
     * @return array|string
     */
    public static function replace(array|string $search, array|string $replace, mixed $value): array|string
    {
        // ----------------
        // Vars

        $subject = self::getString($value);

        // ----------------
        // Process

        // Case : Value is an array
        if (TypeManager::isArray($value)) {
            $subject = self::getArray($value);
        }

        /**
         * Case : Value is a bag
         *
         * @var BagInterface $value
         */
        if (TypeManager::isBag($value)) {
            $subject = $value->all();
        }

        return str_replace($search, $replace, $subject);
    }

    /**
     * Replace the characters with regexp in your value.
     *
     * @link https://php.net/manual/en/function.preg_replace.php
     *
     * @param array|string $pattern
     * @param array|string $replace
     * @param mixed        $value
     *
     * @return array|string|null
     */
    public static function regexp_replace(array|string $pattern, array|string $replace, mixed $value): array|string|null
    {
        // ----------------
        // Vars

        $subject = self::getString($value);

        // ----------------
        // Process

        // Case : Value is an array
        if (TypeManager::isArray($value)) {
            $subject = self::getArray($value);
        }

        /**
         * Case : Value is a bag
         *
         * @var BagInterface $value
         */
        if (TypeManager::isBag($value)) {
            $subject = $value->all();
        }

        return preg_replace($pattern, $replace, $subject);
    }
}
