<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ManagerException.php
 * @Created_at  : 10/06/2022
 * @Update_at   : 01/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Manager\Exception;

use Luna\Exception\LunaException;

class ManagerException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the manager process.';
}
