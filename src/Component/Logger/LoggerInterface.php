<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : LoggerInterface.php
 * @Created_at  : 27/11/2023
 * @Update_at   : 27/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Logger;

use Luna\Component\Bag\BagInterface;

interface LoggerInterface
{
    # --------------------------------
    # Constants

    /**
     * Debug event.
     */
    public const DEBUG           = 'debug';
    public const DEBUG_VERBOSITY = 100;

    /**
     * Information event.
     *
     * Examples :
     *  - User logs in / logs out
     *  - Matched routes
     *  - SQL logs
     *  - ...
     */
    public const INFO           = 'info';
    public const INFO_VERBOSITY = 200;

    /**
     * Warning event.
     *
     * Examples :
     *  - Deprecated API / methods
     *  - Undesirable things that are not necessarily wrong
     *  - ...
     */
    public const WARNING           = 'warning';
    public const WARNING_VERBOSITY = 300;

    /**
     * Error event.
     *
     * Examples :
     *  - Wrong data
     *  - Runtime error
     *  - ...
     */
    public const ERROR           = 'error';
    public const ERROR_VERBOSITY = 400;

    /**
     * Critical event.
     *
     * Examples :
     *  - Application component unavailable
     *  - Unexpected exception
     */
    public const CRITICAL           = 'critical';
    public const CRITICAL_VERBOSITY = 500;

    /**
     * Verbosity allows to control the levels displayed.
     * The lower the verbosity level, the higher the logging level.
     */
    public const VERBOSITY_LEVELS = [
        self::DEBUG    => self::DEBUG_VERBOSITY,
        self::INFO     => self::INFO_VERBOSITY,
        self::WARNING  => self::WARNING_VERBOSITY,
        self::ERROR    => self::ERROR_VERBOSITY,
        self::CRITICAL => self::CRITICAL_VERBOSITY,
    ];

    # --------------------------------
    # Core methods

    /**
     * Logs a message with an arbitrary level.
     * Parameters can be supplied for additional information.
     *
     * @param string             $level
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function log(
        string             $level,
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    /**
     * Log : Debug event.
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function debug(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    /**
     * Log : Information event.
     *
     * Examples :
     *  - User logs in / logs out
     *  - Matched routes
     *  - SQL logs
     *  - ...
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function info(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    /**
     * Log : Warning event.
     *
     * Examples :
     *  - Deprecated API / methods
     *  - Undesirable things that are not necessarily wrong
     *  - ...
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function warning(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    /**
     * Log : Error event.
     *
     * Examples :
     *  - Wrong data
     *  - Runtime error
     *  - ...
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function error(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    /**
     * Log : Critical event.
     *
     * Examples :
     *  - Application component unavailable
     *  - Unexpected exception
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return static
     */
    public function critical(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static;

    # --------------------------------
    # Getters

    /**
     * Returns the logger name.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Returns the logger verbosity.
     *
     * @return int
     */
    public function getVerbosity(): int;

    /**
     * Returns the level verbosity.
     *
     * @param string $level
     *
     * @return int|null
     */
    public function getLevelVerbosity(string $level): ?int;
}
