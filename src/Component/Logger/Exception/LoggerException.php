<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LoggerException.php
 * @Created_at  : 27/11/2023
 * @Update_at   : 27/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Logger\Exception;

use Luna\Exception\LunaException;

class LoggerException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the logger process.';
}
