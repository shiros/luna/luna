<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : LunaLogger.php
 * @Created_at  : 27/11/2023
 * @Update_at   : 20/05/2024
 *
 * This file uses components licensed under the MIT license.
 * See README file for more information.
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Logger;

use Luna\Component\Container\LunaContainer;
use Luna\KernelInterface;
use Monolog\Handler\StreamHandler;

/**
 * This logger use a stream handler to save logs in the given path.
 */
class LunaLogger extends Logger
{
    # --------------------------------
    # Attributes

    protected LunaContainer   $container;
    protected KernelInterface $kernel;

    # --------------------------------
    # Constructor

    /**
     * LunaLogger constructor.
     *
     * @param string $name
     * @param string $path
     * @param int    $verbosity
     */
    public function __construct(
        string $name,
        string $path,
        int    $verbosity = self::DEBUG_VERBOSITY,
    ) {
        parent::__construct(
            name     : $name,
            verbosity: $verbosity,
            handlers : [
                new StreamHandler(
                    stream: $path,
                    level : $verbosity
                )
            ]
        );
    }
}
