<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 *
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Logger.php
 * @Created_at  : 27/11/2023
 * @Update_at   : 20/05/2024
 *
 * This file uses components licensed under the MIT license.
 * See README file for more information.
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Logger;

use DateTimeZone;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Logger\Exception\LoggerException;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\JSONManager;
use Luna\Component\Manager\ValueManager;
use Luna\Exception\LunaException;
use Monolog\Logger as MonologLogger;
use Throwable;

class Logger implements LoggerInterface
{
    # --------------------------------
    # Attributes

    /**
     * Logger name.
     */
    protected string $name;

    /**
     * Verbosity allows to control the levels displayed.
     * The lower the verbosity level, the higher the logging level.
     */
    protected int $verbosity;

    /**
     * Monolog Logger instance.
     */
    protected MonologLogger $logger;

    # --------------------------------
    # Constructor

    /**
     * Logger constructor.
     *
     * @see \Monolog\Logger
     *
     * @param string            $name       The logging channel
     * @param int               $verbosity  The logging verbosity
     * @param array             $handlers   Optional handlers, the first one in the array is called first, etc.
     * @param array             $processors Optional processors array
     * @param DateTimeZone|null $timezone   Optional timezone, if not provided date_default_timezone_get() will be used
     */
    public function __construct(
        string        $name,
        int           $verbosity = self::DEBUG_VERBOSITY,
        array         $handlers = [],
        array         $processors = [],
        ?DateTimeZone $timezone = null
    ) {
        // Set attributes
        $this->name      = $name;
        $this->verbosity = $verbosity;

        // Create logger
        $this->logger = new MonologLogger(
            name      : $name,
            handlers  : $handlers,
            processors: $processors,
            timezone  : $timezone
        );
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function log(
        string             $level,
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Vars

        // Get information
        $message        = $this->computeMessage($message);
        $parameters     = $this->computeParameters($message, $parameters);
        $verbosity      = $this->getVerbosity();
        $levelVerbosity = $this->getLevelVerbosity($level);

        // ----------------
        // Process

        // Security check : Verbosity
        if ($verbosity >= $levelVerbosity) {
            return $this;
        }

        // Logs
        switch ($level) {
            case static::DEBUG:
                $this->logger->debug($message, $parameters->all());
                break;

            case static::INFO:
                $this->logger->info($message, $parameters->all());
                break;

            case static::WARNING:
                $this->logger->warning($message, $parameters->all());
                break;

            case static::ERROR:
                $this->logger->error($message, $parameters->all());
                break;

            case static::CRITICAL:
                $this->logger->critical($message, $parameters->all());
                break;

            default:
                throw new LoggerException(
                    message   : "The level '{$level}' isn't supported. Please refer to the logger interface.",
                    parameters: [
                        'level' => $level
                    ]
                );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function debug(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Process

        // Logs : Debug event
        return $this->log(
            level     : self::DEBUG,
            message   : $message,
            parameters: $parameters
        );
    }

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function info(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Process

        // Logs : Info event
        return $this->log(
            level     : self::INFO,
            message   : $message,
            parameters: $parameters
        );
    }

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function warning(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Process

        // Logs : Warning event
        return $this->log(
            level     : self::WARNING,
            message   : $message,
            parameters: $parameters
        );
    }

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function error(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Process

        // Logs : Error event
        return $this->log(
            level     : self::ERROR,
            message   : $message,
            parameters: $parameters
        );
    }

    /**
     * @inheritDoc
     *
     * @throws LoggerException
     */
    public function critical(
        mixed              $message,
        BagInterface|array $parameters = []
    ): static {
        // ----------------
        // Process

        // Logs : Critical event
        return $this->log(
            level     : self::CRITICAL,
            message   : $message,
            parameters: $parameters
        );
    }

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getVerbosity(): int
    {
        return $this->verbosity;
    }

    /**
     * @inheritDoc
     */
    public function getLevelVerbosity(string $level): ?int
    {
        return static::VERBOSITY_LEVELS[$level] ?? null;
    }

    # --------------------------------
    # Utils methods

    /**
     * Compute message.
     *
     * @param mixed $message
     *
     * @return string
     */
    public function computeMessage(mixed $message): string
    {
        // ----------------
        // Vars

        // Conditions
        $isThrowable = ClassManager::implement(Throwable::class, $message);

        // ----------------
        // Process

        // Convert message
        $_message = match (true) {
            $isThrowable => JSONManager::encode($message->getMessage()),
            default      => ValueManager::getString($message)
        };

        // Sanitize message
        $_message = ValueManager::trim($_message);
        $_message = ValueManager::regexp_replace('#^[\'"](.*)[\'"]$#', "$1", $_message);

        return $_message;
    }

    /**
     * Compute parameters.
     *
     * @param mixed              $message
     * @param BagInterface|array $parameters
     *
     * @return BagInterface
     */
    public function computeParameters(mixed $message, BagInterface|array $parameters = []): BagInterface
    {
        // ----------------
        // Vars

        // Attributes
        $parameters = ValueManager::getBag($parameters);

        // Conditions
        $isLunaException = ClassManager::is(LunaException::class, $message);

        // ----------------
        // Process

        // Convert message
        return match (true) {
            $isLunaException => $message->getParameters()->mergeRecursive($parameters),
            default          => $parameters
        };
    }
}
