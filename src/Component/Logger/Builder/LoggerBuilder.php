<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LoggerBuilder.php
 * @Created_at  : 10/05/2018
 * @Update_at   : 20/05/2024
 *
 * This file uses components licensed under the MIT license.
 * See README file for more information.
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Logger\Builder;

use DateTimeZone;
use Luna\Component\Logger\Logger;
use Luna\Component\Logger\LoggerInterface;
use Luna\Component\Logger\LunaLogger;

class LoggerBuilder
{
    # --------------------------------
    # Core methods

    /**
     * Create a logger.
     *
     * @see \Monolog\Logger
     *
     * @param string            $name       The logging channel
     * @param int               $verbosity  The logging verbosity
     * @param array             $handlers   Optional handlers, the first one in the array is called first, etc.
     * @param array             $processors Optional processors array
     * @param DateTimeZone|null $timezone   Optional timezone, if not provided date_default_timezone_get() will be used
     */
    public static function build(
        string        $name,
        int           $verbosity = LoggerInterface::DEBUG_VERBOSITY,
        array         $handlers = [],
        array         $processors = [],
        ?DateTimeZone $timezone = null
    ): LoggerInterface {
        return new Logger(
            name      : $name,
            verbosity : $verbosity,
            handlers  : $handlers,
            processors: $processors,
            timezone  : $timezone,
        );
    }

    /**
     * Create a luna logger.
     * This logger use a stream handler to save logs in the given path.
     *
     * @param string $name
     * @param string $path
     *
     * @return LunaLogger
     */
    public static function buildLuna(string $name, string $path): LunaLogger
    {
        // ----------------
        // Process

        return new LunaLogger(
            $name,
            $path
        );
    }
}
