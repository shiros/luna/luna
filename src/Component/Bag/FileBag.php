<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileBag.php
 * @Created_at  : 07/05/2018
 * @Update_at   : 27/09/2022
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

class FileBag extends Bag
{
    // TODO : Make some methods for files bag
}
