<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ServerBag.php
 * @Created_at  : 19/04/2018
 * @Update_at   : 25/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

class ServerBag extends ReferenceBag
{
    # --------------------------------
    # Core methods

    /**
     * Auto-build of a server bag using the $_SERVER constant.
     *
     * @return self
     */
    public static function build(): self
    {
        // ----------------
        // Vars

        // Get server information
        $server        = $_SERVER ?? [];

        // ----------------
        // Process

        return new ServerBag($server);
    }
}
