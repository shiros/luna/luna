<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 * @Contributor : Maxime Mazet
 *
 * @File        : Bag.php
 * @Created_at  : 25/05/2018
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

use ArrayIterator;
use Iterator;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\Exception\ManagerException;
use Luna\Component\Manager\JSONManager;
use Luna\Component\Manager\ObjectManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

class Bag implements BagInterface
{
    # --------------------------------
    # Attributes

    protected array $parameters;

    # --------------------------------
    # Constructor

    /**
     * Bag constructor.
     *
     * @param BagInterface|array|null $parameters
     */
    public function __construct(
        BagInterface|array|null $parameters = []
    ) {
        // Set parameters
        $this->processSetParameters($parameters);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return $this->parameters;
    }

    /**
     * @inheritDoc
     */
    public function keys(): array
    {
        return array_keys($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function values(): array
    {
        return array_values($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function change(mixed $parameters): static
    {
        $this->processSetParameters($parameters);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function replace(...$arrays): static
    {
        // ----------------
        // Process

        // Add at first the current parameters array
        array_unshift($arrays, $this->parameters);

        // Apply map to arrays (Each entry need to be an array)
        $arrays = array_map(fn($array) => ValueManager::getArray($array), $arrays);

        // Replace
        $this->parameters = call_user_func_array('array_replace', $arrays);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function replaceRecursive(...$arrays): static
    {
        // ----------------
        // Process

        // Add at first the current parameters array
        array_unshift($arrays, $this->parameters);

        // Apply map to arrays (Each entry need to be an array)
        $arrays = array_map(fn($array) => ValueManager::getArray($array), $arrays);

        // Replace
        $this->parameters = call_user_func_array('array_replace_recursive', $arrays);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function merge(...$arrays): static
    {
        // ----------------
        // Process

        // Add at first the current parameters array
        array_unshift($arrays, $this->parameters);

        // Apply map to arrays (Each entry need to be an array)
        $arrays = array_map(fn($array) => ValueManager::getArray($array), $arrays);

        // Merge
        $this->parameters = call_user_func_array('array_merge', $arrays);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function mergeRecursive(...$arrays): static
    {
        // ----------------
        // Process

        // Add at first the current parameters array
        array_unshift($arrays, $this->parameters);

        // Apply map to arrays (Each entry need to be an array)
        $arrays = array_map(fn($array) => ValueManager::getArray($array), $arrays);

        // Merge
        $this->parameters = call_user_func_array('array_merge_recursive', $arrays);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return empty($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function indexOf(mixed $value): int|null
    {
        // ----------------
        // Process

        // Search index
        $index = array_search($value, $this->parameters);

        // Case : No index is found or index isn't an int.
        if ($index === false || !TypeManager::isInteger($index)) {
            return null;
        }

        return $index;
    }

    /**
     * @inheritDoc
     */
    public function has(mixed $key, bool $nullable = false): bool
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return false;
        }

        // Case : Is an array
        if (TypeManager::isArray($key)) {
            return $this->processHasArray($key, $nullable);
        }

        // Case : Is a bag
        if (ClassManager::implement(interface: BagInterface::class, objectOrClass: $key)) {
            return $this->processHasArray($key->all(), $nullable);
        }

        // Case : Integer or String
        if (TypeManager::isInteger($key) || TypeManager::isString($key)) {
            return $this->processHas($this->parameters, $key, $nullable);
        }

        // Case : Default
        return false;
    }

    /**
     * @inheritDoc
     */
    public function contains(mixed $value): bool
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return false;
        }

        // Case : Default
        return in_array($value, $this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function get(mixed $key, mixed $default = null): mixed
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return $default;
        }

        // Case : Integer or String
        if (TypeManager::isInteger($key) || TypeManager::isString($key)) {
            return $this->processGet($this->parameters, $key, $default);
        }

        // Case : Default
        return $default;
    }

    /**
     * @inheritDoc
     */
    public function getAsString(mixed $key, ?string $default = ''): ?string
    {
        return ValueManager::getString($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getAsInteger(mixed $key, ?int $default = 0): ?int
    {
        return ValueManager::getInteger($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getAsFloat(mixed $key = null, ?float $default = 0): ?float
    {
        return ValueManager::getFloat($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getAsBoolean(mixed $key, ?bool $default = false): ?bool
    {
        return ValueManager::getBoolean($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getAsArray(mixed $key = null, ?array $default = []): ?array
    {
        return ValueManager::getArray($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getAsBag(mixed $key = null, ?BagInterface $default = new self()): ?BagInterface
    {
        return ValueManager::getBag($this->get($key, $default), $default);
    }

    /**
     * @inheritDoc
     */
    public function getFirst(): mixed
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return null;
        }

        // Case : Default
        return reset($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function getLast(): mixed
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return null;
        }

        // Case : Default
        return end($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function set(mixed $key, mixed $value): static
    {
        // ----------------
        // Process

        // Case : Integer or String
        if (TypeManager::isInteger($key) || TypeManager::isString($key)) {
            $this->processSet($this->parameters, $key, $value);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function remove(int|string $key): static
    {
        // TODO : Make process recursivly

        unset($this->parameters[$key]);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear(): static
    {
        // ----------------
        // Process

        // Case : Is empty
        if ($this->isEmpty()) {
            return $this;
        }

        // Case : Default
        $this->parameters = [];
        return $this;
    }

    # --------------------------------
    # Heap methods

    /**
     * @inheritDoc
     */
    public function unshift(mixed ...$values): static
    {
        array_unshift($this->parameters, ...$values);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function push(mixed ...$values): static
    {
        array_push($this->parameters, ...$values);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function shift(): mixed
    {
        return array_shift($this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function pop(): mixed
    {
        return array_pop($this->parameters);
    }

    # --------------------------------
    # Sort methods

    /**
     * @inheritDoc
     */
    public function keySort(): static
    {
        $this->parameters = $this->processKeySort($this->parameters);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function keySortDesc(): static
    {
        $this->parameters = $this->processKeySort($this->parameters, true);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function valueSort(): static
    {
        $this->parameters = $this->processValueSort($this->parameters);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function valueSortDesc(): static
    {
        $this->parameters = $this->processValueSort($this->parameters, true);
        return $this;
    }

    # --------------------------------
    # Utils methods

    /**
     * @inheritDoc
     */
    public function unique(int $flags = SORT_STRING): static
    {
        $this->parameters = array_unique($this->parameters, $flags);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function join(array|string $separator = self::SEPARATOR): string
    {
        return ValueManager::join(
            value    : $this->all(),
            separator: $separator
        );
    }

    /**
     * @inheritDoc
     */
    public function maxKeyLength(): int
    {
        // ----------------
        // Vars

        $maxLength = 0;

        // ----------------
        // Process

        foreach ($this->parameters as $key => $v) {
            $length = 0;

            // Case : Is String
            if (TypeManager::isString($key)) {
                $length = strlen($key);
            }

            if ($length > $maxLength) {
                $maxLength = $length;
            }
        }

        return $maxLength;
    }

    /**
     * @inheritDoc
     *
     * @throws ManagerException
     */
    public function maxValueLength(?string $propertyName = null): int
    {
        // ----------------
        // Vars

        $maxLength = 0;

        // ----------------
        // Process

        foreach ($this->parameters as $parameter) {
            $length = 0;

            // Case : Is String
            if (TypeManager::isString($parameter)) {
                $length = strlen($parameter);
            }

            // Case : Is Object
            if (TypeManager::isObject($parameter) && !TypeManager::isNull($propertyName)) {
                $value = ObjectManager::getValue($parameter, $propertyName);

                if (TypeManager::isNull($value) or !TypeManager::isString($value)) {
                    continue;
                }

                $length = strlen($value);
            }

            if ($length > $maxLength) {
                $maxLength = $length;
            }
        }

        return $maxLength;
    }

    # --------------------------------
    # Access methods

    /**
     * @inheritDoc
     */
    public function offsetExists(mixed $offset): bool
    {
        return $this->has($offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->get($offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->set($offset, $value);
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset(mixed $offset): void
    {
        $this->remove($offset);
    }

    # --------------------------------
    # Countable methods

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count(
            value: $this->all()
        );
    }

    # --------------------------------
    # Serialize methods

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return $this->all();
    }

    # --------------------------------
    # Stringable methods

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return JSONManager::encode(
            value: $this->all()
        );
    }

    # --------------------------------
    # Traversable methods

    /**
     * @inheritDoc
     */
    public function getIterator(): Iterator
    {
        return new ArrayIterator(
            array: $this->all()
        );
    }

    # --------------------------------
    # Process methods

    /**
     * Process : Set parameters.
     *
     * @param BagInterface|array|null $parameters
     *
     * @return static
     */
    protected function processSetParameters(
        BagInterface|array|null $parameters
    ): static {
        // ----------------
        // Process

        // Set parameters
        $this->parameters = match (true) {
            // Case : Is a bag
            ClassManager::is(
                class        : BagInterface::class,
                objectOrClass: $parameters
            )                                 => $parameters->all(),

            // Case : Is an array
            TypeManager::isArray($parameters) => $parameters,

            // Default
            default                           => []
        };

        return $this;
    }

    /**
     * Process : Has.
     * Check if a key exist. (Recursive)
     *
     * @param array      $parameters
     * @param int|string $key
     * @param bool       $nullable
     *
     * @return bool
     */
    protected function processHas(array $parameters, int|string $key, bool $nullable): bool
    {
        // ----------------
        // Vars

        $path = null;

        // ----------------
        // Process

        // Extract key & path
        if (TypeManager::isString($key)) {
            // Split key by the delimiter '.'
            $keys = ValueManager::split($key, '.');

            // Get first key
            $key = array_shift($keys);

            // Join the rest of the key
            $path = ValueManager::join($keys, '.');
        }

        // Check if key exists
        if (!array_key_exists($key, $parameters)) {
            return false;
        }

        // Get value
        $value = $parameters[$key];

        // Case : Recursive system
        if (!TypeManager::isEmpty($path)) {
            // Case : Is Array
            if (TypeManager::isArray($value)) {
                return $this->processHas($value, $path, $nullable);
            }

            // Case : Is Bag
            if (ClassManager::implement(interface: BagInterface::class, objectOrClass: $value)) {
                return $value->has($path);
            }

            return false;
        }

        // Case : Nullable
        if (!$nullable && TypeManager::isNull($value)) {
            return false;
        }

        return true;
    }

    /**
     * Process : Has array mode.
     *
     * @param array $keys
     * @param bool  $nullable
     *
     * @return bool
     */
    protected function processHasArray(array $keys, bool $nullable): bool
    {
        // ----------------
        // Process

        // Check if each keys exists
        foreach ($keys as $key) {
            if (!$this->has($key, $nullable)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Process : Get.
     * Get value for a specific key. (Recursive)
     *
     * @param array      $parameters
     * @param int|string $key
     * @param mixed      $default
     *
     * @return mixed
     */
    protected function processGet(array $parameters, int|string $key, mixed $default = null): mixed
    {
        // ----------------
        // Vars

        $path = null;

        // ----------------
        // Process

        // Extract key & path
        if (TypeManager::isString($key)) {
            // Split key by the delimiter '.'
            $keys = ValueManager::split($key, '.');

            // Get first key
            $key = array_shift($keys);

            // Join the rest of the key
            $path = ValueManager::join($keys, '.');
        }

        // Check if key exists
        if (!isset($parameters[$key])) {
            return $default;
        }

        // Get value
        $value = $parameters[$key];

        // Recursive system
        if (!TypeManager::isEmpty($path)) {
            // Case : Is Array
            if (TypeManager::isArray($value)) {
                return $this->processGet($value, $path, $default);
            }

            // Case : Is Bag
            if (ClassManager::implement(interface: BagInterface::class, objectOrClass: $value)) {
                return $value->get($path);
            }

            return $default;
        }

        return $value;
    }

    /**
     * Process : Set.
     * Set value for a specific key. (Recursive)
     *
     * @param array      $parameters
     * @param int|string $key
     * @param mixed      $value
     *
     * @return void
     */
    protected function processSet(array &$parameters, int|string $key, mixed $value): void
    {
        // ----------------
        // Vars

        $path = null;

        // ----------------
        // Process

        // Extract key & path
        if (TypeManager::isString($key)) {
            // Split key by the delimiter '.'
            $keys = ValueManager::split($key, '.');

            // Get first key
            $key = array_shift($keys);

            // Join the rest of the key
            $path = ValueManager::join($keys, '.');
        }

        // Check if the key is null
        if (TypeManager::isNull($key)) {
            return;
        }

        // Recursive system
        if (!empty($path)) {
            // Get parameter for the key
            $keyParameters = $parameters[$key] ?? [];

            // Call process
            $this->processSet($keyParameters, $path, $value);

            // Set value
            $value = $keyParameters;
        }

        // Set value
        $parameters[$key] = $value;
    }

    /**
     * Process : Key Sort.
     *
     * @param array $array
     * @param bool  $desc
     *
     * @return array
     */
    protected function processKeySort(array $array, bool $desc = false): array
    {
        // ----------------
        // Process

        // Recursive system
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value       = $this->processKeySort($value, $desc);
                $array[$key] = $value;
            }
        }

        // Sort
        ($desc) ? krsort($array) : ksort($array);

        return $array;
    }

    /**
     * Process : Value Sort.
     *
     * @param array $array
     * @param bool  $desc
     *
     * @return array
     */
    protected function processValueSort(array $array, bool $desc = false): array
    {
        // ----------------
        // Process

        // Recursive system
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value       = $this->processValueSort($value, $desc);
                $array[$key] = $value;
            }
        }

        // Sort
        ($desc) ? arsort($array) : asort($array);

        return $array;
    }
}
