<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Bag.php
 * @Created_at  : 25/05/2018
 * @Update_at   : 25/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

use Luna\Component\Bag\Exception\BagException;

class ReferenceBag extends Bag
{
    # --------------------------------
    # Constructor

    /**
     * @param array $parameters
     */
    public function __construct(array &$parameters = [])
    {
        // Call parent constructor
        parent::__construct();

        // Set attributes
        $this->parameters =& $parameters;
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     */
    public function &all(): array
    {
        return $this->parameters;
    }

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function change(mixed $parameters): static
    {
        throw new BagException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     */
    public function clear(): static
    {
        array_splice($this->parameters, 0);
        return $this;
    }

    # --------------------------------
    # Sort methods

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function keySort(): static
    {
        throw new BagException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function keySortDesc(): static
    {
        throw new BagException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function valueSort(): static
    {
        throw new BagException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function valueSortDesc(): static
    {
        throw new BagException("This method isn't supported.");
    }

    # --------------------------------
    # Utils methods

    /**
     * @inheritDoc
     *
     * @throws BagException This method cannot be supported.
     */
    public function unique(int $flags = SORT_STRING): static
    {
        throw new BagException("This method isn't supported.");
    }
}
