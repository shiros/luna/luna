<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ReadOnlyBag.php
 * @Created_at  : 03/05/2023
 * @Update_at   : 31/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

use Luna\Component\Bag\Exception\ImmutableBagException;

class ReadOnlyBag extends Bag
{
    # --------------------------------
    # Constants

    protected const MESSAGE_IMMUTABLE_EXCEPTION = ImmutableBagException::DEFAULT_MESSAGE;

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function change(mixed $parameters): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**-
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function replace(...$arrays): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function replaceRecursive(...$arrays): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function merge(...$arrays): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function mergeRecursive(...$arrays): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function set(mixed $key, mixed $value): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function remove(int|string $key): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function clear(): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    # --------------------------------
    # Heap methods

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function unshift(mixed ...$values): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function push(mixed ...$values): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function shift(): mixed
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function pop(): mixed
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }

    # --------------------------------
    # Utils methods

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ImmutableBagException The bag is immutable, and no changes can be made.
     */
    public function unique(int $flags = SORT_STRING): static
    {
        throw new ImmutableBagException(static::MESSAGE_IMMUTABLE_EXCEPTION);
    }
}
