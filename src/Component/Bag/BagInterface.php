<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Bag.php
 * @Created_at  : 07/12/2022
 * @Update_at   : 01/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Bag;

use ArrayAccess;
use Countable;
use Iterator;
use IteratorAggregate;
use JsonSerializable;
use Stringable;

interface BagInterface extends ArrayAccess, Countable, IteratorAggregate, Stringable, JsonSerializable
{
    # --------------------------------
    # Constants

    public const SEPARATOR = ',';

    # --------------------------------
    # Core methods

    /**
     * Returns the parameters.
     *
     * @return array
     */
    public function all(): array;

    /**
     * Returns the parameter keys.
     * Only on the top level.
     *
     * @return array
     */
    public function keys(): array;

    /**
     * Returns the parameter values.
     * Only on the top level.
     *
     * @return array
     */
    public function values(): array;

    /**
     * Replaces the current parameters by a new set.
     *
     * @param mixed $parameters
     *
     * @return static
     */
    public function change(mixed $parameters): static;

    /**
     * Replace some elements in the bag.
     *
     * @param array[] $arrays
     *
     * @return static
     */
    public function replace(...$arrays): static;

    /**
     * Replace some elements in the bag. (Recursive)
     *
     * @param array[] $arrays
     *
     * @return static
     */
    public function replaceRecursive(...$arrays): static;

    /**
     * Add some elements in the bag.
     *
     * @param array[] $arrays
     *
     * @return static
     */
    public function merge(...$arrays): static;

    /**
     * Add some elements in the bag. (Recursive)
     *
     * @param array[] $arrays
     *
     * @return static
     */
    public function mergeRecursive(...$arrays): static;

    /**
     * Check if the Bag is empty.
     *
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * Get the index of the given value.
     * Returns null if no index is found or if the index isn't an int.
     *
     * @param mixed $value
     *
     * @return int|null
     */
    public function indexOf(mixed $value): int|null;

    /**
     * Check if an element exist in the bag.
     *
     * @param mixed $key
     * @param bool  $nullable
     *
     * @return bool
     */
    public function has(mixed $key, bool $nullable = false): bool;

    /**
     * Check if value exist in the bag.
     * Only on the top level.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function contains(mixed $value): bool;

    /**
     * Get a bag's element. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function get(mixed $key, mixed $default = null): mixed;

    /**
     * Get a bag's element as string. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed       $key
     * @param string|null $default
     *
     * @return string|null
     */
    public function getAsString(mixed $key, ?string $default = ''): ?string;

    /**
     * Get a bag's element as integer. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed    $key
     * @param int|null $default
     *
     * @return int|null
     */
    public function getAsInteger(mixed $key, ?int $default = 0): ?int;

    /**
     * Get a bag's element as float. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed      $key
     * @param float|null $default
     *
     * @return float|null
     */
    public function getAsFloat(mixed $key = null, ?float $default = 0): ?float;

    /**
     * Get a bag's element as boolean. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed     $key
     * @param bool|null $default
     *
     * @return bool|null
     */
    public function getAsBoolean(mixed $key, ?bool $default = false): ?bool;

    /**
     * Get a bag's element as array. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed      $key
     * @param array|null $default
     *
     * @return array|null
     */
    public function getAsArray(mixed $key = null, ?array $default = []): ?array;

    /**
     * Get a bag's element as bag. (Recursive)
     * If the element doesn't exist a default value is returned.
     *
     * @param mixed             $key
     * @param BagInterface|null $default
     *
     * @return BagInterface|null
     */
    public function getAsBag(mixed $key = null, ?BagInterface $default = new Bag()): ?BagInterface;

    /**
     * Get the first element of the bag.
     * Returns null is the bag is empty.
     */
    public function getFirst(): mixed;


    /**
     * Get the last element of the bag.
     * Returns null is the bag is empty.
     */
    public function getLast(): mixed;

    /**
     * Add an element in the Bag with key. (Recursive)
     *
     * @param int|string $key
     * @param mixed      $value
     *
     * @return static
     */
    public function set(mixed $key, mixed $value): static;

    /**
     * Removes a parameter of the bag.
     *
     * @param int|string $key
     *
     * @return static
     */
    public function remove(int|string $key): static;

    /**
     * Clear parameters.
     *
     * @return static
     */
    public function clear(): static;

    # --------------------------------
    # Heap methods

    /**
     * Prepend element(s) at the start of the bag.
     *
     * @see  array_unshift()
     * @link https://php.net/manual/en/function.array-unshift.php
     *
     * @param mixed ...$values
     *
     * @return static
     */
    public function unshift(mixed ...$values): static;

    /**
     * Push element(s) at the end of the bag.
     *
     * @see  array_push()
     * @link https://php.net/manual/en/function.array-push.php
     *
     * @param mixed ...$values
     *
     * @return static
     */
    public function push(mixed ...$values): static;

    /**
     * Gets the first element from the bag and remove it.
     *
     * @see  array_shift()
     * @link https://php.net/manual/en/function.array-shift.php
     *
     * @return mixed
     */
    public function shift(): mixed;

    /**
     * Gets the last element from the bag and remove it.
     *
     * @see  array_pop()
     * @link https://php.net/manual/en/function.array-pop.php
     *
     * @return mixed
     */
    public function pop(): mixed;

    # --------------------------------
    # Sort methods

    /**
     * Sort array by key. (Ascending)
     *
     * @return static
     */
    public function keySort(): static;

    /**
     * Sort array by key. (Descending)
     *
     * @return static
     */
    public function keySortDesc(): static;

    /**
     * Sort array by value. (Ascending)
     *
     * @return static
     */
    public function valueSort(): static;

    /**
     * Sort array by value. (Descending)
     *
     * @return static
     */
    public function valueSortDesc(): static;

    # --------------------------------
    # Utils methods

    /**
     * Remove duplicates in the bag.
     * Makes unique each values.
     *
     * @param int $flags
     *
     * @return static
     */
    public function unique(int $flags = SORT_STRING): static;

    /**
     * Join the bag by a specific separator.
     * Returns bag as string.
     *
     * @param mixed $separator
     *
     * @return string
     */
    public function join(array|string $separator = self::SEPARATOR): string;

    /**
     * Get the max key length in the bag.
     *
     * @return int
     */
    public function maxKeyLength(): int;

    /**
     * Get the max value length in the bag.
     * If the bag is an object list, you can pass the property name on which you want to do the process.
     *
     * @param string|null $propertyName
     *
     * @return int
     */
    public function maxValueLength(?string $propertyName = null): int;

    # --------------------------------
    # Access methods

    /**
     * Implementation of ArrayAccess interface.
     *
     * Returns whether the given offset exists.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see ArrayAccess::offsetExists()
     *
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists(mixed $offset): bool;

    /**
     * Implementation of ArrayAccess interface.
     *
     * Returns the value of the given offset.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see ArrayAccess::offsetGet()
     *
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetGet(mixed $offset): mixed;

    /**
     * Implementation of ArrayAccess interface.
     *
     * Set value on the given offset.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see ArrayAccess::offsetSet()
     *
     * @param mixed $offset
     * @param mixed $value
     *
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void;

    /**
     * Implementation of ArrayAccess interface.
     *
     * Remove value for the given offset.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see ArrayAccess::offsetUnset()
     *
     * @param mixed $offset
     *
     * @return void
     */
    public function offsetUnset(mixed $offset): void;

    # --------------------------------
    # Countable methods

    /**
     * Implementation of Countable interface.
     *
     * Returns the size of the bag.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see Countable::count()
     *
     * @return int
     */
    public function count(): int;

    # --------------------------------
    # Serialize methods

    /**
     * Implementation of JsonSerializable interface.
     *
     * Returns parameters as valid JSON object.
     *
     * @see JsonSerializable::jsonSerialize()
     *
     * @return mixed
     */
    public function jsonSerialize(): mixed;

    # --------------------------------
    # Stringable methods

    /**
     * Implementation of Stringable interface.
     *
     * Returns parameters as string.
     *
     * @see Stringable::__toString()
     *
     * @return Iterator
     */
    public function __toString(): string;

    # --------------------------------
    # Traversable methods

    /**
     * Implementation of IteratorAggregate interface.
     *
     * Returns an iterator for parameters.
     * Use of existing bag methods to satisfy implementation.
     *
     * @see IteratorAggregate::getIterator()
     *
     * @return Iterator
     */
    public function getIterator(): Iterator;
}
