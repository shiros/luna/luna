<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : BagException.php
 * @Created_at  : 02/02/2023
 * @Update_at   : 01/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Bag\Exception;

use Luna\Exception\LunaException;

class BagException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the bag process.';
}
