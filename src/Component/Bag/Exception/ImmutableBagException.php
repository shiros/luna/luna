<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ImmutableBagException.php
 * @Created_at  : 25/05/2024
 * @Update_at   : 01/11/2024
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Bag\Exception;

use Luna\Exception\LunaException;

class ImmutableBagException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = "You can't modify the bag, it's only accessible in reading";
}
