<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ContainerException.php
 * @Created_at  : 06/10/2022
 * @Update_at   : 14/01/2023
 * --------------------------------------------------------------------------
 */

namespace Luna\Component\Container\Exception;

use Luna\Exception\LunaException;

class ContainerException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the container process.';
}
