<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaContainer.php
 * @Created_at  : 08/05/2018
 * @Update_at   : 31/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Container;

use Luna\Component\Bag\Bag;
use Luna\Component\Bag\BagInterface;
use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Dispatcher\DispatcherInterface;
use Luna\Component\Logger\LunaLogger;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Config\LunaConfig;
use Luna\Environment\EnvironmentInterface;
use Luna\KernelInterface;
use Throwable;

class LunaContainer extends Bag
{
    # --------------------------------
    # Constants

    // Base
    public const KEY_KERNEL      = '_kernel';
    public const KEY_ENVIRONMENT = '_environment';
    public const KEY_LOCK        = '_lock';
    public const KEY_MODULES     = '_modules';

    // Components
    public const KEY_CONFIG     = '_config';
    public const KEY_LOGGER     = '_logger';
    public const KEY_DISPATCHER = '_dispatcher';


    # --------------------------------
    # Attributes

    protected static ?self $_instance = null;

    # --------------------------------
    # Constructor

    /**
     * LunaContainer constructor.
     *
     * @param BagInterface|array $parameters
     */
    protected function __construct(BagInterface|array $parameters = [])
    {
        // Call parent constructor
        parent::__construct($parameters);
    }

    /**
     * Built the container if it isn't.
     * You can force the construction if there is already an instance, BUT you will lose all the data in it.
     *
     * @param BagInterface|array $container
     * @param bool               $force
     *
     * @return self
     * @throws ContainerException
     */
    public static function build(BagInterface|array $container = [], bool $force = false): self
    {
        // ----------------
        // Process

        // Security check : Check instance
        if (!TypeManager::isEmpty(static::$_instance) && !$force) {
            throw new ContainerException("You can't build another container.");
        }

        // Build it
        static::$_instance = new static($container);

        return static::getInstance();
    }

    /**
     * Get the container instance.
     * If no instance was found, an empty container is created.
     *
     * @return self
     */
    public static function getInstance(): self
    {
        // ----------------
        // Process

        // Build empty container
        if (TypeManager::isNull(static::$_instance)) {
            try {
                return static::build([], true);
            } catch (Throwable) {
                // Ignore the exception, because it is impossible to get here.
                // Also disables ignore the exception raise in the documentation's generation.
            }
        }

        return static::$_instance;
    }

    # --------------------------------
    # Internal getters

    /**
     * Get Luna kernel.
     *
     * @return KernelInterface
     * @throws ContainerException
     */
    public function getKernel(): KernelInterface
    {
        // ----------------
        // Vars

        // Get instance
        $kernel = $this->get(static::KEY_KERNEL);

        // ----------------
        // Process

        // Security check : Instance isn't null
        if (TypeManager::isNull($kernel)) {
            throw new ContainerException(
                "You must set the kernel during the initialization of the container before getting it."
            );
        }

        // Security check : Instance type
        if (!ClassManager::implement(KernelInterface::class, $kernel)) {
            throw new ContainerException(
                "Container initialization misconfigured, instance doesn't implement 'KernelInterface'."
            );
        }

        return $kernel;
    }

    /**
     * Get Luna environment.
     *
     * @return EnvironmentInterface
     * @throws ContainerException
     */
    public function getEnvironment(): EnvironmentInterface
    {
        // ----------------
        // Vars

        // Get instance
        $environment = $this->get(static::KEY_ENVIRONMENT);

        // ----------------
        // Process

        // Security check : Instance isn't null
        if (TypeManager::isNull($environment)) {
            throw new ContainerException(
                "You must set the environment during the initialization of the container before getting it."
            );
        }

        // Security check : Instance type
        if (!ClassManager::implement(EnvironmentInterface::class, $environment)) {
            throw new ContainerException(
                "Container initialization misconfigured, instance doesn't implement 'EnvironmentInterface'."
            );
        }

        return $environment;
    }

    /**
     * Get Luna configuration.
     *
     * @return LunaConfig
     * @throws ContainerException
     */
    public function getConfig(): LunaConfig
    {
        // ----------------
        // Vars

        // Get instance
        $config = $this->get(static::KEY_CONFIG);

        // ----------------
        // Process

        // Security check : Instance isn't null
        if (TypeManager::isNull($config)) {
            throw new ContainerException(
                "You must set the config during the initialization of the container before getting it."
            );
        }

        // Security check : Instance type
        if (!ClassManager::is(LunaConfig::class, $config)) {
            throw new ContainerException(
                "Container initialization misconfigured, instance isn't 'LunaConfig'."
            );
        }

        return $config;
    }

    /**
     * Get Luna logger.
     *
     * @return LunaLogger
     * @throws ContainerException
     */
    public function getLogger(): LunaLogger
    {
        // ----------------
        // Vars

        // Get instance
        $logger = $this->get(static::KEY_LOGGER);

        // ----------------
        // Process

        // Security check : Instance isn't null
        if (TypeManager::isNull($logger)) {
            throw new ContainerException(
                "You must set the logger during the initialization of the container before getting it."
            );
        }

        // Security check : Instance type
        if (!ClassManager::is(LunaLogger::class, $logger)) {
            throw new ContainerException(
                "Container initialization misconfigured, instance isn't 'LunaLogger'."
            );
        }

        return $logger;
    }

    /**
     * Get Luna dispatcher.
     *
     * @return DispatcherInterface
     * @throws ContainerException
     */
    public function getDispatcher(): DispatcherInterface
    {
        // ----------------
        // Vars

        // Get instance
        $dispatcher = $this->get(static::KEY_DISPATCHER);

        // ----------------
        // Process

        // Security check : Instance isn't null
        if (TypeManager::isNull($dispatcher)) {
            throw new ContainerException(
                "You must set the dispatcher during the initialization of the container before getting it."
            );
        }

        // Security check : Instance type
        if (!ClassManager::implement(DispatcherInterface::class, $dispatcher)) {
            throw new ContainerException(
                "Container initialization misconfigured, instance doesn't implement 'DispatcherInterface'."
            );
        }

        return $dispatcher;
    }

    # --------------------------------
    # Bag methods

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function change(mixed $parameters): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function replace(...$arrays): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function replaceRecursive(...$arrays): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function merge(...$arrays): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function mergeRecursive(...$arrays): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function remove(int|string $key): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function clear(): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function unshift(mixed ...$values): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function push(mixed ...$values): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function shift(): mixed
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function pop(): mixed
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function keySort(): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function keySortDesc(): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function valueSort(): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function valueSortDesc(): static
    {
        throw new ContainerException("This method isn't supported.");
    }

    /**
     * @inheritDoc
     * @deprecated
     *
     * @throws ContainerException
     */
    public function unique(int $flags = SORT_STRING): static
    {
        throw new ContainerException("This method isn't supported.");
    }
}
