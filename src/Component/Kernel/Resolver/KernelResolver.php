<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KernelResolver.php
 * @Created_at  : 07/10/2024
 * @Update_at   : 04/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\Kernel\Resolver;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\LOCKFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\Lock\Exception\LockException;
use Luna\Component\Lock\Lock;
use Luna\Component\Lock\LunaLock;
use Luna\Component\Logger\Builder\LoggerBuilder;
use Luna\Component\Logger\LunaLogger;
use Luna\Component\Manager\ClassManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Config\Builder\ConfigBuilder;
use Luna\Config\LunaConfig;
use Luna\Environment\Builder\EnvironmentBuilder;
use Luna\Environment\EnvironmentInterface;
use Luna\KernelInterface;

class KernelResolver
{
    # --------------------------------
    # Core methods

    /**
     * Resolve kernel environment.
     * The value could be :
     *  - String : Environment file path
     *  - Array  : Environment variables
     *  - Bag    : Environment variables
     *  - Object : Environment class
     *
     * By default, the environment is generated from the application's root path.
     *
     * @see EnvironmentInterface
     * @see EnvironmentBuilder
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $applicationPath = '/var/www/html';
     *
     * // Resolve environment
     * $environment = $resolver->resolveEnvironment($applicationPath);
     * $environment = $resolver->resolveEnvironment($applicationPath, '/var/www/html/environment');
     * $environment = $resolver->resolveEnvironment($applicationPath, [ 'key_1' => 'value_1' ]);
     * $environment = $resolver->resolveEnvironment($applicationPath, new BagParameter([ 'key_1' => 'value_1' ]));
     * $environment = $resolver->resolveEnvironment($applicationPath, new Environment());
     *
     * // Output is an environment object
     * ```
     *
     * @param string $applicationPath
     * @param mixed  $value
     *
     * @return EnvironmentInterface
     */
    public function resolveEnvironment(
        string $applicationPath,
        mixed  $value = null
    ): EnvironmentInterface {
        // ----------------
        // Vars

        // Get class information
        $bagInterface = BagInterface::class;
        $envInterface = EnvironmentInterface::class;

        // Default environment
        $defaultEnvironment = ValueManager::join(
            value    : [$applicationPath, 'env'],
            separator: DIRECTORY_SEPARATOR
        );

        // ----------------
        // Process

        // Create environment
        return match (true) {
            TypeManager::isString($value)                  => EnvironmentBuilder::buildFromFiles($value),
            TypeManager::isArray($value),
            ClassManager::implement($bagInterface, $value) => EnvironmentBuilder::build($value),
            ClassManager::implement($envInterface, $value) => $value,
            default                                        => EnvironmentBuilder::buildFromFiles($defaultEnvironment)
        };
    }

    /**
     * Resolve lock file.
     * The value could be :
     *  - String    : Lock file path
     *  - Object    : Luna lock object
     *
     * By default, the lock is generated from the application's root path. (luna.lock)
     *
     * @see Lock
     *
     * Usage :
     * ```
     * // Get resolver
     * $resolver = ...;
     * $applicationPath = '/var/www/html';
     *
     * // Resolve lock file
     * $lock = $resolver->resolveEnvironment($applicationPath);
     * $lock = $resolver->resolveEnvironment($applicationPath, '/var/www/html/lock/app.lock');
     *
     * // Output is a lock object
     * ```
     *
     * @param string $applicationPath
     * @param mixed  $value
     *
     * @return LunaLock
     * @throws FileWriterException
     * @throws LOCKFileReaderException
     * @throws LockException
     */
    public function resolveLock(
        string $applicationPath,
        mixed  $value = null
    ): LunaLock {
        // ----------------
        // Vars

        // Get class information
        $lunaLockClass = LunaLock::class;

        // Default lock
        $defaultLock = ValueManager::join(
            value    : [$applicationPath, 'luna.lock'],
            separator: DIRECTORY_SEPARATOR
        );

        // ----------------
        // Process

        // Create lock
        return match (true) {
            TypeManager::isString($value)            => new LunaLock(path: $value),
            ClassManager::is($lunaLockClass, $value) => $value,
            default                                  => new LunaLock(path: $defaultLock)
        };
    }

    /**
     * Resolve kernel configuration.
     *
     * @see LunaConfig
     * @see ConfigBuilder
     *
     * Usage :
     * ```
     * // Get information
     * $resolver = ...;
     * $environment = ...;
     *
     * // Resolve configuration
     * $config = $resolver->resolveConfiguration($environment);
     *
     * // Output is a luna configuration object
     * ```
     *
     * @param EnvironmentInterface $environment
     *
     * @return LunaConfig
     */
    public function resolveConfiguration(
        EnvironmentInterface $environment
    ): LunaConfig {
        // ----------------
        // Process

        // Create configuration
        return ConfigBuilder::buildLuna(
            environment: $environment
        );
    }

    /**
     * Resolve logger.
     *
     * @see LunaLogger
     * @see LoggerBuilder
     *
     * Usage :
     * ```
     * // Get information
     * $resolver = ...;
     *
     * // Resolve configuration
     * $logger = $resolver->resolveLogger('/var/www/html/log');
     *
     * // Output is a luna logger object
     * ```
     *
     * @param string $path
     *
     * @return LunaLogger
     */
    public function resolveLogger(
        string $path
    ): LunaLogger {
        // ----------------
        // Vars

        // Logger information
        $loggerName = KernelInterface::NAME;
        $loggerPath = ValueManager::join(
            value    : [$path, 'app.log'],
            separator: DIRECTORY_SEPARATOR
        );

        // ----------------
        // Process

        // Create logger
        return LoggerBuilder::buildLuna(
            name: $loggerName,
            path: $loggerPath
        );
    }
}
