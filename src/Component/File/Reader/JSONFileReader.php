<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : JSONFileReader.php
 * @Created_at  : 28/07/2021
 * @Update_at   : 06/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Exception\Reader\JSONFileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\JSONManager;
use Luna\Component\Manager\ValueManager;

class JSONFileReader extends AbstractFileReader
{
    # --------------------------------
    # Constants

    /**
     * Define the contents structure.
     * If this option is enabled, the content will be returned as an associative array.
     * Default is 'false'.
     *
     * @see JSONManager::decode()
     */
    public const OPT_ASSOCIATIVE = 'json_associative';

    /**
     * Define JSON decoding flags.
     * Default is '0'.
     *
     * @see JSONManager::decode()
     */
    public const OPT_FLAGS = 'json_flags';

    /**
     * Define JSON decoding depth.
     * Default is '512'.
     *
     * @see JSONManager::decode()
     */
    public const OPT_DEPTH = 'json_depth';

    # --------------------------------
    # Attributes

    /**
     * File reader instance.
     */
    protected FileReader $reader;

    # --------------------------------
    # Constructor

    /**
     * JSONFileReader constructor.
     */
    public function __construct()
    {
        $this->reader = new FileReader();
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Read JSON file.
     * The read process use JSON manager & file reader.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - FileReader options
     * - OPT_ASSOCIATIVE    (JSONFileReader)
     * - OPT_FLAGS          (JSONFileReader)
     * - OPT_DEPTH          (JSONFileReader)
     *
     * @see FileReader::read()
     * @see JSONManager::decode()
     *
     * @return mixed
     * @throws JSONFileReaderException
     */
    public function read(
        string             $path,
        BagInterface|array $options = []
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $options = ValueManager::getBag($options);

        // Options
        $associative = $options->getAsBoolean(self::OPT_ASSOCIATIVE);
        $flags       = $options->getAsInteger(self::OPT_FLAGS);
        $depth       = $options->getAsInteger(self::OPT_DEPTH, 512);

        // ----------------
        // Process

        try {
            // Security check : It's a JSON file
            if (!FileManager::isJSONFile($path)) {
                throw new JSONFileReaderException(
                    message   : "The file '{$path}' isn't a JSON file or doesn't exists.",
                    parameters: [
                        'path' => $path
                    ]
                );
            }

            // Read contents
            $contents = $this->contents(
                path   : $path,
                options: $options
            );

            // Transform to array
            return JSONManager::decode(
                json       : $contents,
                associative: $associative,
                flags      : $flags,
                depth      : $depth
            );
        } catch (FileReaderException $exception) {
            throw new JSONFileReaderException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'        => $path,
                    'associative' => $associative,
                    'flags'       => $flags,
                    'depth'       => $depth
                ],
                previous  : $exception,
            );
        }
    }
}
