<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : YAMLFileReader.php
 * @Created_at  : 24//09/2021
 * @Update_at   : 06/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Exception\Reader\YAMLFileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\Exception\ManagerException;
use Luna\Component\Manager\ValueManager;
use Luna\Component\Manager\YAMLManager;

class YAMLFileReader extends AbstractFileReader
{
    # --------------------------------
    # Constants

    /**
     * Decoding flags.
     *
     * @see YAMLManager::decode() for more information
     */
    public const OPT_FLAGS = 'yaml_flags';

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Read JSON file.
     * The read process use JSON manager & file reader.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - FileReader options
     * - OPT_FLAGS          (YAMLFileReader)
     *
     * @see FileReader::read()
     * @see YAMLManager::decode()
     *
     * @return mixed
     * @throws YAMLFileReaderException
     */
    public function read(
        string             $path,
        BagInterface|array $options = []
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $options = ValueManager::getBag($options);

        // Options
        $flags = $options->getAsInteger(self::OPT_FLAGS);

        // ----------------
        // Process

        try {
            // Security check : It's a YAML file
            if (!FileManager::isYAMLFile($path)) {
                throw new YAMLFileReaderException(
                    message   : "The file '{$path}' isn't a YAML file or doesn't exists.",
                    parameters: [
                        'path' => $path
                    ]
                );
            }

            // Read contents
            $yaml = $this->contents(
                path   : $path,
                options: $options
            );

            // Decoding YAML
            return YAMLManager::decode(
                yaml : $yaml,
                flags: $flags,
            );
        } catch (FileReaderException|ManagerException $exception) {
            throw new YAMLFileReaderException(
                message   : $exception->getMessage(),
                parameters: [
                    'path'    => $path,
                    'options' => $options
                ],
                previous  : $exception
            );
        }
    }
}
