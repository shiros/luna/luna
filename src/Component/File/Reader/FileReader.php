<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileReader.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 06/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\Manager\ValueManager;

class FileReader extends AbstractFileReader
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @see AbstractFileReader::contents()
     *
     * @return string
     * @throws FileReaderException
     */
    public function read(
        string             $path,
        BagInterface|array $options = []
    ): string {
        return ValueManager::getString(
            value: $this->contents(
                path   : $path,
                options: $options
            )
        );
    }
}
