<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileReader.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use DirectoryIterator;
use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;

abstract class AbstractFileReader implements FileReaderInterface
{
    # --------------------------------
    # Constants

    /**
     * Allows you to use the 'FILE_USE_INCLUDE_PATH' PHP constants.
     * Default is 'false'.
     */
    public const OPT_INCLUDE_PATH = 'include_path';

    /**
     * Allows you the offset where the reading starts.
     */
    public const OPT_OFFSET = 'offset';

    /**
     * Allows you to define the maximum length of data read.
     * Default is to read until end of file is reached.
     */
    public const OPT_LENGTH = 'length';

    # --------------------------------
    # Core methods

    /**
     * Read the provided file.
     * The read process use PHP function 'file_get_contents'.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - OPT_INCLUDE_PATH
     * - OPT_OFFSET
     * - OPT_LENGTH
     *
     * @link https://php.net/manual/en/function.file-get-contents.php
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return string
     * @throws FileReaderException
     */
    protected function contents(
        string             $path,
        BagInterface|array $options = []
    ): string {
        // ----------------
        // Vars

        // Attributes
        $path    = PathManager::sanitize($path);
        $options = ValueManager::getBag($options);

        // Options
        $includePath = $options->getAsBoolean(static::OPT_INCLUDE_PATH);
        $offset      = $options->getAsInteger(static::OPT_OFFSET);
        $length      = $options->getAsInteger(static::OPT_LENGTH, null);

        // ----------------
        // Process

        // Security check : Path doesn't exist
        if (!FileManager::exists($path)) {
            throw new FileReaderException(
                message   : "Failed to read '{$path}', path doesn't exist.",
                parameters: [
                    'path'        => $path,
                    'includePath' => $includePath,
                    'offset'      => $offset,
                    'length'      => $length
                ]
            );
        }

        // Security check : It's not a file
        if (!FileManager::isFile($path)) {
            throw new FileReaderException(
                message   : "Failed to read '{$path}', this isn't a file.",
                parameters: [
                    'path'        => $path,
                    'includePath' => $includePath,
                    'offset'      => $offset,
                    'length'      => $length
                ]
            );
        }

        // Read file
        $content = file_get_contents($path, $includePath, null, $offset, $length);

        // Check content
        if ($content === false) {
            throw new FileReaderException(
                message   : "Failed to read file '{$path}'",
                parameters: [
                    'path'        => $path,
                    'includePath' => $includePath,
                    'offset'      => $offset,
                    'length'      => $length
                ]
            );
        }

        return $content;
    }
}
