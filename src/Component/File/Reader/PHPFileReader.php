<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PHPFileReader.php
 * @Created_at  : 17/07/2021
 * @Update_at   : 06/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\PHPFileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\ValueManager;

class PHPFileReader extends AbstractFileReader
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Read PHP file.
     * The read process use PHP function 'require'.
     *
     * No options required.
     *
     * @see JSONManager::decode()
     *
     * @return array
     * @throws PHPFileReaderException
     */
    public function read(
        string             $path,
        BagInterface|array $options = []
    ): array {
        // ----------------
        // Process

        // Security check : It's a PHP file
        if (!FileManager::isPHPFile($path)) {
            throw new PHPFileReaderException(
                message   : "The file '{$path}' isn't a PHP file or doesn't exists.",
                parameters: [
                    'path' => $path
                ]
            );
        }

        // Read contents
        $contents = require($path);

        // Transform to array
        return ValueManager::getArray($contents);
    }
}
