<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DirectoryReader.php
 * @Created_at  : 07/01/2025
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use DirectoryIterator;
use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;

class DirectoryReader extends AbstractFileReader
{
    # --------------------------------
    # Core methods

    /**
     * Read a directory.
     * Returns a directory iterator.
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return DirectoryIterator
     * @throws FileReaderException
     */
    public function read(
        string             $path,
        BagInterface|array $options = []
    ): DirectoryIterator {

        // ----------------
        // Vars

        // Attributes
        $directory = PathManager::sanitize($path);

        // ----------------
        // Process

        // Security check : Path doesn't exist
        if (!FileManager::exists($directory)) {
            throw new FileReaderException(
                message   : "Failed to read '{$directory}', path doesn't exist.",
                parameters: ['directory' => $directory]
            );
        }

        // Security check : It's not a directory
        if (!FileManager::isDirectory($directory)) {
            throw new FileReaderException(
                message   : "Failed to read '{$directory}', this isn't a directory.",
                parameters: ['directory' => $directory]
            );
        }

        // Generate directory iterator
        return new DirectoryIterator($directory);
    }
}
