<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileReaderInterface.php
 * @Created_at  : 22/11/2022
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Reader;

use DirectoryIterator;
use Luna\Component\Bag\BagInterface;

interface FileReaderInterface
{
    # --------------------------------
    # Core methods

    /**
     * Read a file/folder, depending on the class instance.
     * To have more information about options or process, look at the children classes.
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return mixed
     */
    public function read(
        string $path,
        BagInterface|array $options = []
    ): mixed;
}
