<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LOCKFileWriterException.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 26/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Writer;

use Luna\Exception\LunaException;

class LOCKFileWriterException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the write process of the LOCK file.';
}
