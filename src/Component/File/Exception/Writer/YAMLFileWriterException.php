<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 * @Contributor : Maxime Mazet
 *
 * @File        : YAMLFileWriterException.php
 * @Created_at  : 10/06/2022
 * @Update_at   : 13/06/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Writer;

use Luna\Exception\LunaException;

class YAMLFileWriterException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the write process of the YAML file.';
}
