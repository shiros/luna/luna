<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PHPFileWriterException.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 13/06/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Writer;

use Luna\Exception\LunaException;

class PHPFileWriterException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the write process of the PHP file.';
}
