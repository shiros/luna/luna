<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : TemplateException.php
 * @Created_at  : 27/07/2021
 * @Update_at   : 14/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Template;

use Luna\Exception\LunaException;

class TemplateException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the file template process.';
}
