<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PHPFileReaderException.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 13/06/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Reader;

use Luna\Exception\LunaException;

class PHPFileReaderException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the read process of the PHP file.';
}
