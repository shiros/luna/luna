<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LOCKFileReaderException.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 26/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Exception\Reader;

use Luna\Exception\LunaException;

class LOCKFileReaderException extends LunaException
{
    # --------------------------------
    # Constants

    public const DEFAULT_MESSAGE = 'An error is occurred during the read process of the LOCK file.';
}
