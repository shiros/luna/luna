<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AbstractTemplate.php
 * @Created_at  : 27/12/2024
 * @Update_at   : 27/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Template;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Template\TemplateException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

abstract class AbstractTemplate implements TemplateInterface
{
    # --------------------------------
    # Attributes

    /**
     * This is the path of the original file.
     */
    protected string $originalFile;

    /**
     * This is the path of the template file.
     */
    protected string $file;

    # --------------------------------
    # Constructor

    /**
     * Template constructor.
     *
     * @param string $file
     *
     * @throws TemplateException
     */
    public function __construct(string $file)
    {
        // Set attributes
        $this->setFile(file: $file);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     */
    public abstract function read(BagInterface|array $variables = []): ?string;

    /**
     * @inheritDoc
     */
    public abstract function copy(
        string             $destination,
        BagInterface|array $variables = []
    ): self;

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getOriginalFile(): string
    {
        return $this->originalFile;
    }

    /**
     * @inheritDoc
     */
    public function getFile(): string
    {
        return $this->file;
    }

    # --------------------------------
    # Setters

    /**
     * @inheritDoc
     *
     * @throws TemplateException
     */
    public function setFile(string $file): self
    {
        // ----------------
        // Process

        // Sanitize path
        $file = PathManager::sanitize($file);
        $file = ValueManager::regexp_replace(pattern: "/\.template$/", replace: '', value: $file);

        // Get template file
        $templateFile = "{$file}.template";
        $templateFile = FileManager::isFile($templateFile) ? $templateFile : $file;

        // Security check : Checks if it's a file
        if (!FileManager::isFile($templateFile)) {
            throw new TemplateException(
                message   : "The file '{$file}' is not a file or doesn't exist.",
                parameters: ['file' => $file]
            );
        }

        // Set file
        $this->originalFile = $file;
        $this->file         = $templateFile;

        return $this;
    }

    # --------------------------------
    # Utils methods

    /**
     * Compute template's variables.
     * Returns an array with valid variables and formatted keys.
     *
     * @param BagInterface|array $variables
     *
     * @return array
     */
    protected function computeVariables(BagInterface|array $variables): array
    {
        // ----------------
        // Vars

        // Get information
        $_variables = [];

        // ----------------
        // Process

        // Browse provided variables
        foreach ($variables as $key => $value) {
            // Checks key's type
            if (!TypeManager::isString($key)
                && !TypeManager::isNumber($key)
            ) continue;

            // Checks value's type
            if (!TypeManager::isNull($value)
                && !TypeManager::isString($value)
                && !TypeManager::isNumber($value)
                && !TypeManager::isBoolean($value)
            ) continue;

            // Prepare key format
            $key = "{{{$key}}}";

            // Set value
            $_variables[$key] = $value;
        }

        return $_variables;
    }

    /**
     * Compute template's content.
     *
     * @param string $content
     * @param array  $variables
     *
     * @return string|null
     */
    protected function computeContent(
        string $content,
        array  $variables
    ): ?string {
        // ----------------
        // Process

        // Security check : If the content is null, get out of here!
        if (TypeManager::isEmpty($content)) return null;

        // Apply variables
        foreach ($variables as $key => $value) {
            $content = ValueManager::replace($key, $value, $content);
        }

        // Prevent characters
        return ValueManager::replace(
            ['\n', '\t', '\r'],
            ["\n", "\t", "\r"],
            $content
        );
    }
}
