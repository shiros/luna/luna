<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Template.php
 * @Created_at  : 27/07/2021
 * @Update_at   : 27/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Template;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Exception\Template\TemplateException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;

class Template extends AbstractTemplate
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws TemplateException
     */
    public function read(BagInterface|array $variables = []): ?string
    {
        // ----------------
        // Vars

        // Get information
        $file      = $this->getFile();
        $variables = $this->computeVariables($variables);

        // ----------------
        // Process

        try {
            // 'Read template file, and get its content'
            $content = FileManager::read($file);

            // Prepare template content and return it
            return $this->computeContent(
                content  : $content,
                variables: $variables
            );
        } catch (FileReaderException $exception) {
            throw new TemplateException(
                message   : "Failed to read file '{$file}'",
                code      : $exception->getCode(),
                parameters: [
                    'file'      => $file,
                    'variables' => $variables,
                ],
                previous  : $exception
            );
        }
    }

    /**
     * @inheritDoc
     *
     * @throws TemplateException
     */
    public function copy(
        string             $destination,
        BagInterface|array $variables = []
    ): self {
        // ----------------
        // Vars

        // Get information
        $source      = $this->getFile();
        $destination = PathManager::sanitize($destination);

        // ----------------
        // Process

        // Get template's content
        $contents = $this->read(variables: $variables);

        try {
            // Delete the destination
            if (FileManager::exists($destination)) {
                FileManager::delete($destination);
            }

            // Create the new file with the template's content
            FileManager::create(
                path    : $destination,
                contents: $contents
            );
        } catch (FileWriterException $exception) {
            throw new TemplateException(
                message   : "Failed to copy file '{$source}' to destination '{$destination}'.",
                code      : $exception->getCode(),
                parameters: [
                    'source'      => $source,
                    'destination' => $destination,
                    'variables'   => $variables,
                ],
                previous  : $exception
            );
        }

        return $this;
    }
}
