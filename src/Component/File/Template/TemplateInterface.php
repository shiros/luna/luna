<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : TemplateInterface.php
 * @Created_at  : 23/11/2022
 * @Update_at   : 26/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Template;

use Luna\Component\Bag\BagInterface;

interface TemplateInterface
{
    # --------------------------------
    # Core methods

    /**
     * Read the template file.
     * Variables are used by the reading process to inject data into the file.
     *
     * Usage :
     * ```
     * // Content of file :
     * // "I'm a template content file.\n I have also a variable '{{my-var}}'."
     *
     * // Get template
     * $template = new Template('/my/template/file');
     *
     * // Read file
     * $contents = $template->read();
     * // Output : "I'm a template content file.\n I have also a variable '{{my-var}}'."
     *
     * // Read file with variables
     * $contentsWithVariable = $template->read(['my-var' => 'Injected variable']);
     * // Output : "I'm a template content file.\n I have also a variable 'Injected variable'."
     * ```
     *
     * @param BagInterface|array $variables
     *
     * @return string|null
     */
    public function read(BagInterface|array $variables = []): ?string;

    /**
     * Copy the template file to the provided destination.
     * Variables are used by the reading process to inject data into the file.
     *
     * **WARNING :** If the destination already exists, it will be deleted.
     *
     * Usage :
     * ```
     * // Content of file :
     * // "I'm a template content file.\n I have also a variable '{{my-var}}'."
     *
     * // Get template
     * $template = new Template('/my/template/file');
     *
     * // Copy file
     * $template->copy('/my/copy/template/file');
     * // Output : File with content "I'm a template content file.\n I have also a variable '{{my-var}}'."
     *
     * // Copy file with variables
     * $template->copy('/my/copy/template/file', ['my-var' => 'Injected variable']);
     * // Output : File with content "I'm a template content file.\n I have also a variable 'Injected variable'."
     * ```
     *
     * @param string             $destination
     * @param BagInterface|array $variables
     *
     * @return self
     */
    public function copy(
        string             $destination,
        BagInterface|array $variables = []
    ): self;

    # --------------------------------
    # Getters

    /**
     * Returns the original file path.
     *
     * @return string
     */
    public function getOriginalFile(): string;

    /**
     * Returns the template file path.
     *
     * @return string
     */
    public function getFile(): string;

    # --------------------------------
    # Setters

    /**
     * Set the template file path.
     *
     * @param string $file
     *
     * @return self
     */
    public function setFile(string $file): self;
}
