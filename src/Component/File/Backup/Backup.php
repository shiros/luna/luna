<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Backup.php
 * @Created_at  : 27/07/2021
 * @Update_at   : 27/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Backup;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Backup\BackupException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\ValueManager;

class Backup extends AbstractBackup
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Available options :
     *  - OPT_BACKUP_PREFIX
     *  - OPT_BACKUP_NAME
     *
     * @throws BackupException
     * @throws FileWriterException
     */
    public function backup(
        ?string            $destination = null,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Get information
        $source  = $this->getSource();
        $options = ValueManager::getBag($options);

        // Extract options
        $_prefix = $options->getAsString(self::OPT_BACKUP_PREFIX, 'backup-');
        $_name   = $options->getAsString(self::OPT_BACKUP_NAME, null);

        // ----------------
        // Process

        // Security check : Checks if the source exists
        if (!FileManager::exists($source)) {
            throw new BackupException(
                message   : "The source '{$source}' doesn't exists.",
                parameters: ['source' => $source]
            );
        }

        // Get destination path
        $destination = self::generateDestination(
            path  : $destination,
            prefix: $_prefix,
            name  : $_name
        );

        // Perform the backup
        match (true) {
            $this->isFile()      => FileManager::copy(
                source     : $source,
                destination: $destination
            ),
            $this->isDirectory() => FileManager::copyDirectory(
                source     : $source,
                destination: $destination
            ),
            default              => new BackupException(
                message   : "The source isn't a file or a folder.",
                parameters: ['source' => $source]
            )
        };

        return $this;
    }
}
