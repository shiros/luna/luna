<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Backup.php
 * @Created_at  : 27/07/2021
 * @Update_at   : 03/11/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Backup;

use Luna\Component\Bag\BagInterface;

interface BackupInterface
{
    # --------------------------------
    # Constants

    /**
     * Allows you to define the date format for the backup.
     */
    public const DATE_FORMAT = 'Y-m-d_H-i-s';

    /**
     * Allows you to define the prefix of your backup.
     * Default is 'backup-'.
     *
     * **WARNING :** Couldn't be used with 'OPT_BACKUP_NAME'.
     */
    public const OPT_BACKUP_PREFIX = 'prefix';

    /**
     * Allows you to define the name of your backup.
     *
     * **WARNING :** Couldn't be used with 'OPT_BACKUP_PREFIX'.
     */
    public const OPT_BACKUP_NAME = 'name';

    # --------------------------------
    # Core methods

    /**
     * Backup the file or a folder.
     * If no destination is defined, the backup is performed at the same location as the source.
     *
     * To have more information about options or process, look at the children classes.
     *
     * @param string|null        $destination
     * @param BagInterface|array $options
     *
     * @return self
     */
    public function backup(
        ?string            $destination = null,
        BagInterface|array $options = []
    ): self;

    # --------------------------------
    # Getters

    /**
     * Returns the source's path.
     *
     * @return string
     */
    public function getSource(): string;

    /**
     * Returns whether the source is a file.
     *
     * @return bool
     */
    public function isFile(): bool;

    /**
     * Returns whether the source is a directory.
     *
     * @return bool
     */
    public function isDirectory(): bool;

    # --------------------------------
    # Getters

    /**
     * Set the source's path.
     *
     * @param string $source
     *
     * @return self
     */
    public function setSource(string $source): self;

    # --------------------------------
    # Utils methods

    /**
     * Generate destination path.
     *
     * If no path is provided, the source directory is used as the destination.
     * The name of the backup must be the same as that of the source, but with '.BAK' and possibly the prefix, if
     * indicated.
     *
     * If a name is supplied, it will be used as a backup name concatenated with '.BAK'.
     *
     * **WARNING :** The prefix and the name can't be used at the same time. The name has a higher priority.
     *
     * Usage :
     * ```
     * // Get the backup instance
     * $backup = new Backup('/my/folder');
     *
     * // Generate with no parameters
     * $destination = $backup->generateDestination();
     * // Output : '/my/folder.2024-01-01_12-00-00.BAK'
     *
     * // Generate with path
     * $destination = $backup->generateDestination(path: '/backup');
     * // Output : '/backup/folder.2024-01-01_12-00-00.BAK'
     *
     * // Generate with prefix
     * $destination = $backup->generateDestination(prefix: 'save');
     * // Output : '/my/save-folder.2024-01-01_12-00-00.BAK'
     *
     * // Generate with name
     * $destination = $backup->generateDestination(name: 'backup');
     * // Output : '/my/backup.2024-01-01_12-00-00.BAK'
     * ```
     *
     * @param string|null $path
     * @param string|null $prefix
     * @param string|null $name
     *
     * @return string
     */
    public function generateDestination(
        ?string $path = null,
        ?string $prefix = null,
        ?string $name = null,
    ): string;
}
