<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Backup.php
 * @Created_at  : 27/12/2024
 * @Update_at   : 27/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Backup;

use DateTime;
use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Backup\BackupException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;

abstract class AbstractBackup implements BackupInterface
{
    # --------------------------------
    # Attributes

    /**
     * This is the source's path.
     * Could be a file or a folder.
     */
    protected string $source;

    # --------------------------------
    # Constructor

    /**
     * Backup constructor.
     *
     * @param string $source
     */
    public function __construct(string $source)
    {
        // Set attributes
        $this->setSource(source: $source);
    }

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     */
    public abstract function backup(
        ?string            $destination = null,
        BagInterface|array $options = []
    ): self;

    # --------------------------------
    # Getters

    /**
     * @inheritDoc
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @inheritDoc
     */
    public function isFile(): bool
    {
        return FileManager::isFile(path: $this->source);
    }

    /**
     * @inheritDoc
     */
    public function isDirectory(): bool
    {
        return FileManager::isDirectory(path: $this->source);
    }

    # --------------------------------
    # Setters

    /**
     * @inheritDoc
     */
    public function setSource(string $source): self
    {
        $this->source = PathManager::sanitize($source);
        return $this;
    }

    # --------------------------------
    # Utils methods

    /**
     * @inheritDoc
     *
     * @throws BackupException
     */
    public function generateDestination(
        ?string $path = null,
        ?string $prefix = null,
        ?string $name = null,
    ): string {
        // ----------------
        // Vars

        // Get information
        $path       = PathManager::sanitize($path);
        $source     = $this->getSource();
        $sourceName = basename($source);

        // Dates
        $date         = new DateTime();
        $dateToString = $date->format(format: self::DATE_FORMAT);

        // ----------------
        // Process

        // Generate destination directory
        $destinationDir = TypeManager::isEmpty($path)
            ? dirname($source)
            : $path;

        // Security check : The destination isn't a directory
        if (!FileManager::isDirectory($destinationDir)) {
            throw new BackupException(
                message   : "The destination '{$destinationDir}' need to be a directory.",
                parameters: [
                    'source'      => $source,
                    'path'        => $path,
                    'destination' => $destinationDir,
                ]
            );
        }

        // Generate destination
        $destination = TypeManager::isEmpty($name)
            ? "{$destinationDir}/{$prefix}{$sourceName}.{$dateToString}.BAK"
            : "{$destinationDir}/{$name}.{$dateToString}.BAK";

        return PathManager::sanitize($destination);
    }
}
