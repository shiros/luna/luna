<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : JSONFileWriter.php
 * @Created_at  : 25/10/2021
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\JSONFileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\JSONManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

class JSONFileWriter extends FileWriter
{
    # --------------------------------
    # Constants

    /**
     * Define JSON encoding flags.
     * Default is '0'.
     *
     * @see JSONManager::encode()
     */
    public const OPT_JSON_FLAGS = 'json_flags';

    /**
     * Define JSON encoding depth.
     * Default is '512'.
     *
     * @see JSONManager::encode()
     */
    public const OPT_JSON_DEPTH = 'json_depth';

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Write JSON file.
     * The write process use JSON manager & file writer.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - FileWriter options
     * - OPT_JSON_FLAGS  (JSONFileWriter)
     * - OPT_JSON_DEPTH  (JSONFileWriter)
     *
     * @see FileWriter::create()
     * @see JSONManager::encode()
     *
     * @return self
     * @throws JSONFileWriterException
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $options = ValueManager::getBag($options);

        // Options
        $contents = $options->get(key: self::OPT_CONTENTS);
        $flags    = $options->getAsInteger(key: self::OPT_JSON_FLAGS);
        $depth    = $options->getAsInteger(key: self::OPT_JSON_DEPTH, default: 512);

        // ----------------
        // Process

        try {
            // Security check : It's not a JSON file
            if (!FileManager::isJSONFile(path: $path, extensionOnly: true)) {
                throw new JSONFileWriterException(
                    message   : "The file '{$path}' isn't a JSON file.",
                    parameters: ['path' => $path]
                );
            }

            // Security check : The contents is a resource
            if (TypeManager::isResource($contents)) {
                throw new JSONFileWriterException(
                    message: "Incorrect data. They can be any type except a resource."
                );
            }

            // Encode data
            $data = JSONManager::encode(
                value: $contents,
                flags: $flags,
                depth: $depth
            );

            // Set file contents
            $options->set(key: self::OPT_CONTENTS, value: $data);

            // Write data
            parent::create(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new JSONFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'     => $path,
                    'contents' => $contents,
                    'flags'    => $flags,
                    'depth'    => $depth,
                    'options'  => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Copy a JSON file.
     *
     * @see FileWriter::copy()
     *
     * @return self
     * @throws JSONFileWriterException
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $source      = PathManager::sanitize($source);
        $destination = PathManager::sanitize($destination);

        // ----------------
        // Process

        try {
            // Security check : Source isn't a JSON file
            if (!FileManager::isJSONFile($source)) {
                throw new JSONFileWriterException(
                    message   : "The source file '{$source}' isn't a JSON file or doesn't exists.",
                    parameters: ['source' => $source]
                );
            }

            // Security check : Destination isn't a JSON file
            if (!FileManager::isJSONFile(path: $destination, extensionOnly: true)) {
                throw new JSONFileWriterException(
                    message   : "The destination file '{$destination}' isn't a JSON file.",
                    parameters: ['destination' => $destination]
                );
            }

            // Copy file
            parent::copy(
                source     : $source,
                destination: $destination,
                options    : $options
            );
        } catch (FileWriterException $exception) {
            throw new JSONFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'source'      => $source,
                    'destination' => $destination,
                    'options'     => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Delete a JSON file.
     *
     * @see FileWriter::delete()
     *
     * @return self
     * @throws JSONFileWriterException
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path = PathManager::sanitize($path);

        // ----------------
        // Process

        try {
            // Security check : Path isn't a JSON file
            if (!FileManager::isJSONFile($path)) {
                throw new JSONFileWriterException(
                    message   : "The file '{$path}' isn't a JSON file or doesn't exists.",
                    parameters: ['path' => $path]
                );
            }

            // Copy file
            parent::delete(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new JSONFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'    => $path,
                    'options' => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }
}
