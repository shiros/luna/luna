<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PHPFileWriter.php
 * @Created_at  : 14/06/2023
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\PHPFileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;

class PHPFileWriter extends FileWriter
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Write PHP file.
     * The write process use file writer.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - FileWriter options
     *
     * @see FileWriter::create()
     *
     * @return self
     * @throws PHPFileWriterException
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Process

        try {
            // Security check : It's not a PHP file
            if (!FileManager::isPHPFile(path: $path, extensionOnly: true)) {
                throw new PHPFileWriterException(
                    message   : "The file '{$path}' isn't a PHP file.",
                    parameters: ['path' => $path]
                );
            }

            // Write data
            parent::create(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new PHPFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'    => $path,
                    'options' => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Copy a PHP file.
     *
     * @see FileWriter::copy()
     *
     * @return self
     * @throws PHPFileWriterException
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $source      = PathManager::sanitize($source);
        $destination = PathManager::sanitize($destination);

        // ----------------
        // Process

        try {
            // Security check : Source isn't a PHP file
            if (!FileManager::isPHPFile($source)) {
                throw new PHPFileWriterException(
                    message   : "The source file '{$source}' isn't a PHP file or doesn't exists.",
                    parameters: ['source' => $source]
                );
            }

            // Security check : Destination isn't a PHP file
            if (!FileManager::isPHPFile(path: $destination, extensionOnly: true)) {
                throw new PHPFileWriterException(
                    message   : "The destination file '{$destination}' isn't a PHP file.",
                    parameters: ['destination' => $destination]
                );
            }

            // Copy file
            parent::copy(
                source     : $source,
                destination: $destination,
                options    : $options
            );
        } catch (FileWriterException $exception) {
            throw new PHPFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'source'      => $source,
                    'destination' => $destination,
                    'options'     => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Delete a PHP file.
     *
     * @see FileWriter::delete()
     *
     * @return self
     * @throws PHPFileWriterException
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path = PathManager::sanitize($path);

        // ----------------
        // Process

        try {
            // Security check : Path isn't a PHP file
            if (!FileManager::isPHPFile($path)) {
                throw new PHPFileWriterException(
                    message   : "The file '{$path}' isn't a PHP file or doesn't exists.",
                    parameters: ['path' => $path]
                );
            }

            // Copy file
            parent::delete(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new PHPFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'    => $path,
                    'options' => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }
}
