<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DirectoryWriter.php
 * @Created_at  : 07/01/2025
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use DirectoryIterator;
use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\File\Reader\DirectoryReader;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;

class DirectoryWriter implements FileWriterInterface
{
    # --------------------------------
    # Constants

    /**
     * Allows you to the directory permissions.
     *
     * By default, the mode is 0755.
     * For more information on modes, read the details on the chmod page.
     *
     * **WARNING :** Permissions are ignored on Windows.
     */
    public const OPT_PERMISSIONS = 'permissions';

    /**
     * Allows the creation of nested directories specified in the pathname.
     * By default, the value is set to false.
     */
    public const OPT_RECURSIVE = 'recursive';

    # --------------------------------
    # Attributes

    /**
     * Directory reader.
     * Used to get directory's files.
     */
    protected DirectoryReader $directoryReader;

    /**
     * File writer.
     */
    protected FileWriter $fileWriter;

    # --------------------------------
    # Constructor

    /**
     * DirectoryWriter constructor.
     */
    public function __construct(
        DirectoryReader $directoryReader,
        FileWriter      $fileWriter,
    ) {
        // Set attributes
        $this->directoryReader = $directoryReader;
        $this->fileWriter      = $fileWriter;
    }

    # --------------------------------
    # Core methods

    /**
     * Create a new directory.
     * The copy process use PHP function 'mkdir'.
     *
     * Available options :
     * - OPT_PERMISSIONS
     * - OPT_RECURSIVE
     *
     * @link https://php.net/manual/en/function.mkdir.php
     *
     * @throws FileWriterException
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path    = PathManager::sanitize($path);
        $options = ValueManager::getBag($options);

        // Options
        $permissions = $options->getAsInteger(key: self::OPT_PERMISSIONS, default: 0755);
        $recursive   = $options->getAsBoolean(key: self::OPT_RECURSIVE);

        // ----------------
        // Process

        // Security check : The directory already exist
        if (FileManager::isDirectory($path)) {
            throw new FileWriterException(
                message   : "Failed to create directory '{$path}', it already exists.",
                parameters: ['path' => $path]
            );
        }

        // Create directory
        $status = mkdir(
            directory  : $path,
            permissions: $permissions,
            recursive  : $recursive
        );

        // Security check : Check process status
        if ($status === false) {
            throw new FileWriterException(
                message   : "Failed to create directory '{$path}'.",
                parameters: ['path' => $path]
            );
        }

        return $this;
    }

    /**
     * Copy a directory to a specific destination.
     *
     * Available options :
     * - OPT_PERMISSIONS
     *
     * @param string             $source
     * @param string             $destination
     * @param BagInterface|array $options
     *
     * @return DirectoryWriter
     * @throws FileWriterException
     * @throws FileReaderException
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $source      = PathManager::sanitize($source);
        $destination = PathManager::sanitize($destination);
        $options     = ValueManager::getBag($options);

        // Options
        $permissions = $options->getAsInteger(key: self::OPT_PERMISSIONS, default: 0755);

        // ----------------
        // Process

        // Security check : Source isn't a directory
        if (!FileManager::isDirectory($source)) {
            throw new FileWriterException(
                message   : "Failed to copy '{$source}'. Source isn't a directory or not exist.",
                parameters: ['source' => $source]
            );
        }

        // Create the destination, if it doesn't exist
        if (!FileManager::isDirectory($destination)) {
            $this->create(
                path   : $destination,
                options: [
                    self::OPT_PERMISSIONS => $permissions,
                    self::OPT_RECURSIVE   => true
                ]
            );
        }

        // Get files
        $files = $this->directoryReader->read(path: $source);

        /**
         * Iterate through the source directory.
         *
         * @var DirectoryIterator $file
         */
        foreach ($files as $file) {
            // Get information
            $path         = $file->getPathname();
            $name         = $file->getFilename();
            $_destination = PathManager::sanitize(path: "{$destination}/{$name}");

            // Security check : Is dot file
            if ($file->isDot()) continue;

            /**
             * Case : Is a directory
             * Recursively copy subdirectory.
             */
            if ($file->isDir()) {
                $this->copy(
                    source     : $path,
                    destination: $_destination,
                    options    : $options
                );
            }

            /**
             * Case : Is a file
             */
            if ($file->isFile()) {
                $this->fileWriter->copy(
                    source     : $path,
                    destination: $_destination,
                    options    : $options
                );
            }
        }

        return $this;
    }

    /**
     * Delete a directory.
     * The delete process use PHP function 'rmdir'.
     *
     * @link https://php.net/manual/en/function.rmdir.php
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return DirectoryWriter
     * @throws FileReaderException
     * @throws FileWriterException
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path = PathManager::sanitize($path);

        // ----------------
        // Process

        // Security check : This isn't a directory
        if (!FileManager::isDirectory($path)) {
            throw new FileWriterException(
                message   : "Failed to delete '{$path}'. This isn't a directory or not exist.",
                parameters: ['path' => $path]
            );
        }

        // Get files
        $files = $this->directoryReader->read(path: $path);

        /**
         * Iterate through the source directory.
         *
         * @var DirectoryIterator $file
         */
        foreach ($files as $file) {
            // Get information
            $filePath = $file->getPathname();

            // Security check : Is dot file
            if ($file->isDot()) continue;

            /**
             * Case : Is a directory
             * Recursively delete subdirectory.
             */
            if ($file->isDir()) {
                $this->delete(
                    path   : $filePath,
                    options: $options
                );
            }

            /**
             * Case : Is a file
             */
            if ($file->isFile()) {
                $this->fileWriter->delete(
                    path   : $filePath,
                    options: $options
                );
            }
        }

        // Remove the empty directory
        rmdir(directory: $path);

        return $this;
    }
}
