<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileWriterInterface.php
 * @Created_at  : 22/11/2022
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use Luna\Component\Bag\BagInterface;

interface FileWriterInterface
{
    # --------------------------------
    # Core methods

    /**
     * Write content into a file/folder, depending on the class instance.
     * To have more information about options or process, look at the children classes.
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return self
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self;

    /**
     * Copy a file/folder to a specific destination, depending on the class instance.
     * To have more information about options or process, look at the children classes.
     *
     * @param string             $source
     * @param string             $destination
     * @param BagInterface|array $options
     *
     * @return self
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self;

    /**
     * Delete a file/folder, depending on the class instance.
     * To have more information about options or process, look at the children classes.
     *
     * @param string             $path
     * @param BagInterface|array $options
     *
     * @return self
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self;
}
