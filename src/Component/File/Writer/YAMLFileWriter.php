<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : YAMLFileWriter.php
 * @Created_at  : 24/09/2021
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\YAMLFileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;
use Luna\Component\Manager\YAMLManager;

class YAMLFileWriter extends FileWriter
{
    # --------------------------------
    # Constants

    /**
     * The level where you switch to inline YAML
     *
     * @see YAMLManager::encode() for more information
     */
    public const OPT_YAML_INLINE = 'yaml_inline';

    /**
     * The indentation used for nested nodes.
     *
     * @see YAMLManager::encode() for more information
     */
    public const OPT_YAML_INDENT = 'yaml_indent';

    /**
     * The indentation used for nested nodes.
     *
     * @see YAMLManager::encode() for more information
     */
    public const OPT_YAML_FLAGS = 'yaml_flags';

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * Write JSON file.
     * The write process use YAML manager & file writer.
     *
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - FileWriter options
     * - OPT_YAML_INLINE (YAMLFileWriter)
     * - OPT_YAML_INDENT (YAMLFileWriter)
     * - OPT_YAML_FLAGS  (YAMLFileWriter)
     *
     * @see FileWriter::create()
     * @see YAMLManager::encode()
     *
     * @return self
     * @throws YAMLFileWriterException
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $options = ValueManager::getBag($options);

        // Options
        $contents = $options->get(key: self::OPT_CONTENTS);
        $inline   = $options->getAsInteger(self::OPT_YAML_INLINE, 2);
        $indent   = $options->getAsInteger(self::OPT_YAML_INDENT, 4);
        $flags    = $options->getAsInteger(self::OPT_YAML_FLAGS);

        // ----------------
        // Process

        try {
            // Security check : It's not a YAML file
            if (!FileManager::isYAMLFile(path: $path, extensionOnly: true)) {
                throw new YAMLFileWriterException(
                    message   : "The file '{$path}' isn't a YAML file.",
                    parameters: ['path' => $path]
                );
            }

            // Encoding data
            $data = YAMLManager::encode(
                value : $contents,
                inline: $inline,
                indent: $indent,
                flags : $flags
            );

            // Set file contents
            $options->set(key: self::OPT_CONTENTS, value: $data);

            // Write data
            parent::create(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new YAMLFileWriterException(
                message   : $exception->getMessage(),
                parameters: [
                    'path'    => $path,
                    'options' => $options
                ],
                previous  : $exception
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Copy a YAML file.
     *
     * @see FileWriter::copy()
     *
     * @return self
     * @throws YAMLFileWriterException
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $source      = PathManager::sanitize($source);
        $destination = PathManager::sanitize($destination);

        // ----------------
        // Process

        try {
            // Security check : Source isn't a YAML file
            if (!FileManager::isYAMLFile($source)) {
                throw new YAMLFileWriterException(
                    message   : "The source file '{$source}' isn't a YAML file or doesn't exists.",
                    parameters: ['source' => $source]
                );
            }

            // Security check : Destination isn't a YAML file
            if (!FileManager::isYAMLFile(path: $destination, extensionOnly: true)) {
                throw new YAMLFileWriterException(
                    message   : "The destination file '{$destination}' isn't a YAML file.",
                    parameters: ['destination' => $destination]
                );
            }

            // Copy file
            parent::copy(
                source     : $source,
                destination: $destination,
                options    : $options
            );
        } catch (FileWriterException $exception) {
            throw new YAMLFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'source'      => $source,
                    'destination' => $destination,
                    'options'     => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Delete a YAML file.
     *
     * @see FileWriter::delete()
     *
     * @return self
     * @throws YAMLFileWriterException
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path = PathManager::sanitize($path);

        // ----------------
        // Process

        try {
            // Security check : Path isn't a YAML file
            if (!FileManager::isYAMLFile($path)) {
                throw new YAMLFileWriterException(
                    message   : "The file '{$path}' isn't a YAML file or doesn't exists.",
                    parameters: ['path' => $path]
                );
            }

            // Copy file
            parent::delete(
                path   : $path,
                options: $options
            );
        } catch (FileWriterException $exception) {
            throw new YAMLFileWriterException(
                message   : $exception->getMessage(),
                code      : $exception->getCode(),
                parameters: [
                    'path'    => $path,
                    'options' => $options->all()
                ],
                previous  : $exception,
            );
        }

        return $this;
    }
}
