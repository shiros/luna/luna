<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileWriter.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Writer;

use Luna\Component\Bag\BagInterface;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Manager\FileManager;
use Luna\Component\Manager\PathManager;
use Luna\Component\Manager\ValueManager;

class FileWriter implements FileWriterInterface
{
    # --------------------------------
    # Constants

    /**
     * Allows you to define the file contents.
     *
     * By default, the contents is an empty string.
     */
    public const OPT_CONTENTS = 'contents';

    /**
     * Allows you to define the write flags.
     * The flag values can be any combination of the following flags, joined with the binary OR (|) operator.
     *
     * Available flags :
     * - FILE_USE_INCLUDE_PATH  : Search for filename in the include directory.
     *                            See include_path for more information.
     * - FILE_APPEND            : If file filename already exists, append the data to the file instead of overwriting
     * it. Mutually exclusive with LOCK_EX since appends are atomic and thus there is no reason to lock.
     * - LOCK_EX                : Acquire an exclusive lock on the file while proceeding to the writing.
     *                            Mutually exclusive with FILE_APPEND.
     */
    public const OPT_FLAGS = 'flags';

    /**
     * Allows you to the directory permissions.
     *
     * By default, the mode is 0755.
     * For more information on modes, read the details on the chmod page.
     *
     * **WARNING :** Permissions are ignored on Windows.
     */
    public const OPT_PERMISSIONS = 'permissions';

    /**
     * Allows the creation of nested directories specified in the pathname.
     * By default, the value is set to false.
     */
    public const OPT_RECURSIVE = 'recursive';

    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * The write process use PHP function 'file_put_contents'.
     * To have more information about options or process, look at the children classes.
     *
     * Available options :
     * - OPT_CONTENTS
     * - OPT_FLAGS
     *
     * @link https://php.net/manual/en/function.file-put-contents.php
     *
     * @throws FileWriterException
     */
    public function create(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path    = PathManager::sanitize($path);
        $options = ValueManager::getBag($options);

        // Options
        $contents = $options->getAsString(key: self::OPT_CONTENTS);
        $flags    = $options->getAsInteger(key: self::OPT_FLAGS);

        // ----------------
        // Process

        /**
         * Writing file.
         *
         * If file doesn't exist, it's created.
         * Otherwise, the existing file is overwritten, unless the FILE_APPEND flag is set.
         */
        $status = file_put_contents(
            filename: $path,
            data    : $contents,
            flags   : $flags
        );

        // Security check : Check process status
        if ($status === false) {
            throw new FileWriterException(
                message   : "Failed to create file '{$path}'.",
                parameters: ['path' => $path],
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * The copy process use PHP function 'copy'.
     *
     * @link https://php.net/manual/en/function.copy.php
     *
     * @throws FileWriterException
     */
    public function copy(
        string             $source,
        string             $destination,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $source      = PathManager::sanitize($source);
        $destination = PathManager::sanitize($destination);

        // ----------------
        // Process

        // Security check : Source doesn't exist
        if (!FileManager::isFile($source)) {
            throw new FileWriterException(
                message   : "Failed to copy '{$source}'. Source doesn't exist.",
                parameters: ['source' => $source]
            );
        }

        // Security check : Destination already exist
        if (FileManager::isFile($destination)) {
            throw new FileWriterException(
                message   : "Failed to copy '{$source}' to '{$destination}'. Destination already exist.",
                parameters: ['source' => $source, 'destination' => $destination]
            );
        }

        // Copy file/folder
        $status = copy(
            from: $source,
            to  : $destination,
        );

        // Security check : Check process status
        if ($status === false) {
            throw new FileWriterException(
                message   : "Failed to copy '{$source}' to '{$destination}'.",
                parameters: ['source' => $source, 'destination' => $destination]
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * The delete process use PHP function 'unlink'.
     *
     * @link https://php.net/manual/en/function.unlink.php
     *
     * @throws FileWriterException
     */
    public function delete(
        string             $path,
        BagInterface|array $options = []
    ): self {
        // ----------------
        // Vars

        // Attributes
        $path = PathManager::sanitize($path);

        // ----------------
        // Process

        // Security check : Path doesn't exist
        if (!FileManager::exists($path)) {
            throw new FileWriterException(
                message   : "Failed to delete '{$path}'. The path doesn't exist.",
                parameters: ['path' => $path]
            );
        }

        // Delete file/folder
        $status = unlink(filename: $path);

        // Check status
        if ($status === false) {
            throw new FileWriterException(
                message   : "Failed to delete '{$path}'.",
                parameters: ['path' => $path]
            );
        }

        return $this;
    }
}
