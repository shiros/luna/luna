<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : JSONFileManager.php
 * @Created_at  : 25/10/2021
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Manager;

use Luna\Component\File\Exception\Reader\JSONFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\JSONFileWriterException;
use Luna\Component\File\Reader\JSONFileReader;
use Luna\Component\File\Writer\JSONFileWriter;
use Luna\Component\Manager\ValueManager;

class JSONFileManager
{
    # --------------------------------
    # Core methods

    /**
     * Read a JSON file.
     *
     * @see JSONFileReader::read()
     *
     * @param mixed     $path
     * @param bool|null $associative
     * @param int       $flags
     * @param int       $depth
     *
     * @return mixed
     * @throws JSONFileReaderException
     */
    public static function read(
        ?string $path,
        ?bool   $associative = false,
        int     $flags = 0,
        int     $depth = 512
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new JSONFileReader();

        // ----------------
        // Process

        // Generate options
        $options = [
            JSONFileReader::OPT_ASSOCIATIVE => $associative,
            JSONFileReader::OPT_FLAGS       => $flags,
            JSONFileReader::OPT_DEPTH       => $depth
        ];

        // Read JSON file
        return $reader->read(
            path   : $path,
            options: $options
        );
    }

    /**
     * Create a JSON file.
     * If content is provided, it will be added to the file.
     *
     * @see JSONFileWriter::create()
     *
     * @param mixed $path
     * @param mixed $contents JSON data
     * @param int   $depth    Set the maximum depth. Must be greater than zero.
     * @param int   $flags    Encoding flags, to define JSON string generation
     *
     * @return void
     * @throws FileWriterException
     * @throws JSONFileWriterException
     */
    public static function create(
        ?string $path,
        mixed   $contents = null,
        int     $depth = 512,
        int     $flags = 0
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new JSONFileWriter();

        // ----------------
        // Process

        // Generate options
        $options = [
            JSONFileWriter::OPT_CONTENTS   => $contents,
            JSONFileWriter::OPT_JSON_FLAGS => $flags,
            JSONFileWriter::OPT_JSON_DEPTH => $depth
        ];

        // Write JSON file
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a file/folder.
     *
     * @see JSONFileReader::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     *
     * @return void
     * @throws FileWriterException
     * @throws JSONFileWriterException
     */
    public static function copy(
        ?string $source,
        ?string $destination
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $writer = new JSONFileWriter();

        // ----------------
        // Process

        // Copy JSON file
        $writer->copy(
            source     : $source,
            destination: $destination
        );
    }

    /**
     * Delete a file/folder.
     *
     * @see JSONFileReader::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileWriterException
     * @throws JSONFileWriterException
     */
    public static function delete(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new JSONFileWriter();

        // ----------------
        // Process

        // Deletes JSON file
        $writer->delete(path: $path);
    }
}
