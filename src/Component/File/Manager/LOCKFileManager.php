<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LOCKFileManager.php
 * @Created_at  : 26/10/2024
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Manager;

use Luna\Component\File\Exception\Reader\LOCKFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\LOCKFileWriterException;
use Luna\Component\File\Reader\LOCKFileReader;
use Luna\Component\File\Writer\LOCKFileWriter;
use Luna\Component\Manager\ValueManager;

class LOCKFileManager
{
    # --------------------------------
    # Core methods

    /**
     * Read a LOCK file.
     *
     * @see LOCKFileReader::read()
     *
     * @param mixed     $path
     * @param bool|null $associative
     * @param int       $flags
     * @param int       $depth
     *
     * @return mixed
     * @throws LOCKFileReaderException
     */
    public static function read(
        ?string $path,
        ?bool   $associative = false,
        int     $flags = 0,
        int     $depth = 512
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new LOCKFileReader();

        // ----------------
        // Process

        // Generate options
        $options = [
            LOCKFileReader::OPT_ASSOCIATIVE => $associative,
            LOCKFileReader::OPT_FLAGS       => $flags,
            LOCKFileReader::OPT_DEPTH       => $depth
        ];

        // Read LOCK file
        return $reader->read(
            path   : $path,
            options: $options
        );
    }

    /**
     * Create a LOCK file.
     * If content is provided, it will be added to the file.
     *
     * @see LOCKFileWriter::create()
     *
     * @param mixed $path
     * @param mixed $contents JSON data
     * @param int   $depth    Set the maximum depth. Must be greater than zero.
     * @param int   $flags    Encoding flags, to define JSON string generation
     *
     * @return void
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public static function create(
        ?string $path,
        mixed   $contents = null,
        int     $depth = 512,
        int     $flags = 0
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new LOCKFileWriter();

        // ----------------
        // Process

        // Generate options
        $options = [
            LOCKFileWriter::OPT_CONTENTS   => $contents,
            LOCKFileWriter::OPT_JSON_FLAGS => $flags,
            LOCKFileWriter::OPT_JSON_DEPTH => $depth
        ];

        // Write LOCK file
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a file/folder.
     *
     * @see LOCKFileReader::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     *
     * @return void
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public static function copy(
        ?string $source,
        ?string $destination
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $writer = new LOCKFileWriter();

        // ----------------
        // Process

        // Copy LOCK file
        $writer->copy(
            source     : $source,
            destination: $destination
        );
    }

    /**
     * Delete a file/folder.
     *
     * @see LOCKFileReader::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileWriterException
     * @throws LOCKFileWriterException
     */
    public static function delete(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new LOCKFileWriter();

        // ----------------
        // Process

        // Deletes LOCK file
        $writer->delete(path: $path);
    }
}
