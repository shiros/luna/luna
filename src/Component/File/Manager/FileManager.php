<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileManager.php
 * @Created_at  : 06/04/2022
 * @Update_at   : 12/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Manager;

use DirectoryIterator;
use Luna\Component\File\Exception\Reader\FileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Reader\AbstractFileReader;
use Luna\Component\File\Reader\DirectoryReader;
use Luna\Component\File\Reader\FileReader;
use Luna\Component\File\Writer\DirectoryWriter;
use Luna\Component\File\Writer\FileWriter;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;
use Luna\Constant\LunaConstant;

class FileManager
{
    # --------------------------------
    # Core methods

    /**
     * Returns whether the file/folder exists.
     *
     * @param string|null $path
     *
     * @return bool
     */
    public static function exists(?string $path): bool
    {
        return !TypeManager::isEmpty($path) && file_exists($path);
    }

    # --------------------------------
    # File methods

    /**
     * Read a file.
     *
     * @see FileReader::read()
     *
     * @param string|null $path
     * @param bool        $use_include_path
     * @param int         $offset
     * @param int|null    $length
     *
     * @return string
     * @throws FileReaderException
     */
    public static function read(
        ?string $path,
        bool    $use_include_path = false,
        int     $offset = 0,
        ?int    $length = null
    ): string {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new FileReader();

        // ----------------
        // Process

        // Generate options
        $options = [
            AbstractFileReader::OPT_INCLUDE_PATH => $use_include_path,
            AbstractFileReader::OPT_OFFSET       => $offset,
            AbstractFileReader::OPT_LENGTH       => $length,
        ];

        // Read file
        return $reader->read(
            path   : $path,
            options: $options
        );
    }

    /**
     * Create a file.
     * If content is provided, it will be added to the file.
     *
     * @see FileWriter::create()
     *
     * @param string|null $path
     * @param mixed       $contents
     * @param int         $flags
     *
     * @return void
     * @throws FileWriterException
     */
    public static function create(
        ?string $path,
        mixed   $contents = null,
        int     $flags = 0
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new FileWriter();

        // ----------------
        // Process

        // Generate options
        $options = [
            FileWriter::OPT_CONTENTS => $contents,
            FileWriter::OPT_FLAGS    => $flags,
        ];

        // Create a file
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a file.
     *
     * @see FileWriter::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     *
     * @return void
     * @throws FileWriterException
     */
    public static function copy(
        ?string $source,
        ?string $destination
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $writer = new FileWriter();

        // ----------------
        // Process

        // Copy source into destination
        $writer->copy(
            source     : $source,
            destination: $destination
        );
    }

    /**
     * Delete a file.
     *
     * @see FileWriter::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileWriterException
     */
    public static function delete(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new FileWriter();

        // ----------------
        // Process

        // Deletes a file
        $writer->delete(path: $path);
    }

    # --------------------------------
    # Directory methods

    /**
     * Read a directory.
     *
     * @see FileReader::readDirectory()
     *
     * @param string|null $path
     *
     * @return DirectoryIterator
     * @throws FileReaderException
     */
    public static function readDirectory(?string $path): DirectoryIterator
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new DirectoryReader();

        // ----------------
        // Process

        // Read file
        return $reader->read(path: $path);
    }

    /**
     * Create a directory.
     *
     * @see DirectoryWriter::create()
     *
     * @param string|null $path
     * @param int         $permissions
     * @param bool        $recursive
     *
     * @return void
     * @throws FileWriterException
     */
    public static function createDirectory(
        ?string $path,
        int     $permissions = 0755,
        bool    $recursive = false
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $directoryReader = new DirectoryReader();
        $fileWriter      = new FileWriter();
        $writer          = new DirectoryWriter(
            directoryReader: $directoryReader,
            fileWriter     : $fileWriter,
        );

        // ----------------
        // Process

        // Generate options
        $options = [
            FileWriter::OPT_PERMISSIONS => $permissions,
            FileWriter::OPT_RECURSIVE   => $recursive,
        ];

        // Create the directory
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a directory.
     *
     * @see DirectoryWriter::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     * @param int         $permissions
     *
     * @return void
     * @throws FileReaderException
     * @throws FileWriterException
     */
    public static function copyDirectory(
        ?string $source,
        ?string $destination,
        int     $permissions = 0755,
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $directoryReader = new DirectoryReader();
        $fileWriter      = new FileWriter();
        $writer          = new DirectoryWriter(
            directoryReader: $directoryReader,
            fileWriter     : $fileWriter,
        );

        // ----------------
        // Process

        // Generate options
        $options = [
            FileWriter::OPT_PERMISSIONS => $permissions
        ];

        // Copy source into destination
        $writer->copy(
            source     : $source,
            destination: $destination,
            options    : $options
        );
    }

    /**
     * Delete a directory.
     *
     * @see DirectoryWriter::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileReaderException
     * @throws FileWriterException
     */
    public static function deleteDirectory(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $directoryReader = new DirectoryReader();
        $fileWriter      = new FileWriter();
        $writer          = new DirectoryWriter(
            directoryReader: $directoryReader,
            fileWriter     : $fileWriter,
        );

        // ----------------
        // Process

        // Copy source into destination
        $writer->delete(path: $path);
    }

    # --------------------------------
    # Utils methods

    /**
     * Returns whether the path is a file.
     *
     * @param string|null $path
     *
     * @return bool
     */
    public static function isFile(?string $path): bool
    {
        return self::exists($path) && is_file($path);
    }

    /**
     * Returns whether the path is a directory.
     *
     * @param string|null $path
     *
     * @return bool
     */
    public static function isDirectory(?string $path): bool
    {
        return self::exists($path) && is_dir($path);
    }

    /**
     * Returns whether the path is a LOCK file.
     *
     * @param string|null $path
     * @param bool        $extensionOnly
     *
     * @return bool
     */
    public static function isLOCKFile(
        ?string $path,
        bool    $extensionOnly = false
    ): bool {
        // ----------------
        // Vars

        // Conditions
        $isFile              = self::isFile($path);
        $hasCorrectExtension = ValueManager::match(LunaConstant::REGEXP_LOCK_FILE, $path);

        // ----------------
        // Process

        return $extensionOnly
            ? $hasCorrectExtension
            : $isFile && $hasCorrectExtension;
    }

    /**
     * Returns whether the path is a PHP file.
     *
     * @param string|null $path
     * @param bool        $extensionOnly
     *
     * @return bool
     */
    public static function isPHPFile(
        ?string $path,
        bool    $extensionOnly = false
    ): bool {
        // ----------------
        // Vars

        // Conditions
        $isFile              = self::isFile($path);
        $hasCorrectExtension = ValueManager::match(LunaConstant::REGEXP_PHP_FILE, $path);

        // ----------------
        // Process

        return $extensionOnly
            ? $hasCorrectExtension
            : $isFile && $hasCorrectExtension;
    }

    /**
     * Returns whether the path is a JSON file.
     *
     * @param string|null $path
     * @param bool        $extensionOnly
     *
     * @return bool
     */
    public static function isJSONFile(
        ?string $path,
        bool    $extensionOnly = false
    ): bool {
        // ----------------
        // Vars

        // Conditions
        $isFile              = self::isFile($path);
        $hasCorrectExtension = ValueManager::match(LunaConstant::REGEXP_JSON_FILE, $path);

        // ----------------
        // Process

        return $extensionOnly
            ? $hasCorrectExtension
            : $isFile && $hasCorrectExtension;
    }

    /**
     * Returns whether the path is a YAML file.
     *
     * @param string|null $path
     * @param bool        $extensionOnly
     *
     * @return bool
     */
    public static function isYAMLFile(
        ?string $path,
        bool    $extensionOnly = false
    ): bool {
        // ----------------
        // Vars

        // Conditions
        $isFile              = self::isFile($path);
        $hasCorrectExtension = ValueManager::match(LunaConstant::REGEXP_YAML_FILE, $path);

        // ----------------
        // Process

        return $extensionOnly
            ? $hasCorrectExtension
            : $isFile && $hasCorrectExtension;
    }
}
