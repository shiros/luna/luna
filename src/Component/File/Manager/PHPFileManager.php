<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : PHPFileManager.php
 * @Created_at  : 13/06/2023
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Manager;

use Luna\Component\File\Exception\Reader\PHPFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\PHPFileWriterException;
use Luna\Component\File\Reader\PHPFileReader;
use Luna\Component\File\Writer\FileWriter;
use Luna\Component\File\Writer\PHPFileWriter;
use Luna\Component\Manager\ValueManager;

class PHPFileManager
{
    # --------------------------------
    # Core methods

    /**
     * Read a PHP file.
     *
     * @see PHPFileReader::read()
     *
     * @param string|null $path
     *
     * @return array
     * @throws PHPFileReaderException
     */
    public static function read(
        ?string $path
    ): array {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new PHPFileReader();

        // ----------------
        // Process

        return $reader->read(path: $path);
    }

    /**
     * Create a PHP file.
     * If content is provided, it will be added to the file.
     *
     * @see PHPFileWriter::create()
     *
     * @param string|null $path
     * @param mixed       $contents
     * @param int         $flags
     *
     * @return void
     * @throws FileWriterException
     * @throws PHPFileWriterException
     */
    public static function create(
        ?string $path,
        mixed   $contents = null,
        int     $flags = 0
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new PHPFileWriter();

        // ----------------
        // Process

        // Generate options
        $options = [
            FileWriter::OPT_CONTENTS => $contents,
            FileWriter::OPT_FLAGS    => $flags,
        ];

        // Write PHP file
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a file/folder.
     *
     * @see PHPFileWriter::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     *
     * @return void
     * @throws FileWriterException
     * @throws PHPFileWriterException
     */
    public static function copy(
        ?string $source,
        ?string $destination
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $writer = new PHPFileWriter();

        // ----------------
        // Process

        // Copy PHP file
        $writer->copy(
            source     : $source,
            destination: $destination
        );
    }

    /**
     * Delete a file/folder.
     *
     * @see PHPFileWriter::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileWriterException
     * @throws PHPFileWriterException
     */
    public static function delete(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new PHPFileWriter();

        // ----------------
        // Process

        // Deletes file/folder
        $writer->delete(path: $path);
    }
}
