<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : YAMLFileManager.php
 * @Created_at  : 25/10/2021
 * @Update_at   : 07/01/2025
 * ----------------------------------------------------------------
 */

namespace Luna\Component\File\Manager;

use Luna\Component\File\Exception\Reader\YAMLFileReaderException;
use Luna\Component\File\Exception\Writer\FileWriterException;
use Luna\Component\File\Exception\Writer\YAMLFileWriterException;
use Luna\Component\File\Reader\YAMLFileReader;
use Luna\Component\File\Writer\YAMLFileWriter;
use Luna\Component\Manager\ValueManager;

class YAMLFileManager
{
    # --------------------------------
    # Core methods

    /**
     * Read a YAML file.
     *
     * @see YAMLFileReader::read()
     *
     * @param string|null $path
     * @param int         $flags
     *
     * @return mixed
     * @throws YAMLFileReaderException
     */
    public static function read(
        ?string $path,
        int     $flags = 0
    ): mixed {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get reader
        $reader = new YAMLFileReader();

        // ----------------
        // Process

        // Generate options
        $options = [
            YAMLFileReader::OPT_FLAGS => $flags
        ];

        // Read YAML file
        return $reader->read(
            path   : $path,
            options: $options
        );
    }

    /**
     * Create a YAML file.
     * If content is provided, it will be added to the file.
     *
     * @see YAMLFileWriter::create()
     *
     * @param string|null $path
     * @param mixed       $contents YAML data
     * @param int         $inline   The level where you switch to inline YAML
     * @param int         $indent   The indentation used for nested nodes
     * @param int         $flags    Encoding flags, to define YAML string generation
     *
     * @return void
     * @throws FileWriterException
     * @throws YAMLFileWriterException
     */
    public static function create(
        ?string $path,
        mixed   $contents = null,
        int     $inline = 2,
        int     $indent = 4,
        int     $flags = 0
    ): void {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new YAMLFileWriter();

        // ----------------
        // Process

        // Generate options
        $options = [
            YAMLFileWriter::OPT_CONTENTS    => $contents,
            YAMLFileWriter::OPT_YAML_INLINE => $inline,
            YAMLFileWriter::OPT_YAML_INDENT => $indent,
            YAMLFileWriter::OPT_YAML_FLAGS  => $flags
        ];

        // Write YAML file
        $writer->create(
            path   : $path,
            options: $options
        );
    }

    /**
     * Copy a file/folder.
     *
     * @see YAMLFileReader::copy()
     *
     * @param string|null $source
     * @param string|null $destination
     *
     * @return void
     * @throws FileWriterException
     * @throws YAMLFileWriterException
     */
    public static function copy(
        ?string $source,
        ?string $destination
    ): void {
        // ----------------
        // Vars

        // Attributes
        $source      = ValueManager::getString($source);
        $destination = ValueManager::getString($destination);

        // Get components
        $writer = new YAMLFileWriter();

        // ----------------
        // Process

        // Copy YAML file
        $writer->copy(
            source     : $source,
            destination: $destination
        );
    }

    /**
     * Delete a file/folder.
     *
     * @see YAMLFileReader::delete()
     *
     * @param string|null $path
     *
     * @return void
     * @throws FileWriterException
     * @throws YAMLFileWriterException
     */
    public static function delete(?string $path): void
    {
        // ----------------
        // Vars

        // Attributes
        $path = ValueManager::getString($path);

        // Get components
        $writer = new YAMLFileWriter();

        // ----------------
        // Process

        // Deletes YAML file
        $writer->delete(path: $path);
    }
}
