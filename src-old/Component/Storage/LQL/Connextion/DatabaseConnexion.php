<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : DatabaseConnexion.php
 * @Created_at  : 10/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\Connextion;

use Luna\Component\Storage\LQL\MySQLDatabase;
use Luna\Config\Config;
use PDO;

class DatabaseConnexion
{
    protected const DRIVER_DEFAULT   = 0;
    protected const DRIVER_PDO_MYSQL = 1;

    /** @var DatabaseConnexion */
    protected static $instance;

    /** @var Config */
    protected $ConfigModule;

    /** @var PDO */
    protected $DBModule;

    /**
     * DatabaseConnexion constructor.
     */
    protected function __construct()
    {
        $this->ConfigModule = Config::getInstance();
        $this->prepareConnexion();
    }

    /**
     * Get the Database Connexion instance (Singleton)
     *
     * @return DatabaseConnexion
     */
    public static function getInstance(): DatabaseConnexion
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /* -------------------------------------------------------------------------- */
    /* CONNEXION */

    /**
     * Return a configured connexion
     *
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->DBModule;
    }


    /* -------------------------------------------------------------------------- */
    /* CONFIGURATION */

    /**
     * Prepare the connexion according to the selected driver
     *
     * @throws \Luna\Component\Exception\ConfigException
     */
    protected function prepareConnexion()
    {
        if (is_null($this->DBModule)) {
            switch ($this->getDriver()) {
                case static::DRIVER_PDO_MYSQL:
                    $this->DBModule = $this->getMySqlDatabaseConnexion();
                    break;

                default:
                    $this->DBModule = $this->getMySqlDatabaseConnexion();
                    break;
            }
        }
    }

    /**
     * @return int|null
     * @throws \Luna\Component\Exception\ConfigException
     */
    protected function getDriver()
    {
        if ($this->ConfigModule->getDatabase('Driver.PDO_MYSQL')) {
            return static::DRIVER_PDO_MYSQL;
        } else {
            return null;
        }
    }

    /**
     * Get the MySql Connexion
     *
     * @return PDO
     * @throws \Luna\Component\Exception\ConfigException
     */
    protected function getMySqlDatabaseConnexion(): PDO
    {
        $mySql = new MySQLDatabase(
            $this->ConfigModule->getDatabase('Connect.dbName'),
            $this->ConfigModule->getDatabase('Connect.dbUser'),
            $this->ConfigModule->getDatabase('Connect.dbPass'),
            $this->ConfigModule->getDatabase('Connect.dbHost')
        );

        return $mySql->getPDO();
    }
}
