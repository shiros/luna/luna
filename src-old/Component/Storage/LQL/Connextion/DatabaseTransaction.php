<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : DatabaseTransaction.php
 * @Created_at  : 17/06/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\Connextion;

class DatabaseTransaction
{
    /** @var DatabaseConnexion */
    protected $dbConnexion;

    /**
     * DatabaseTransaction constructor.
     */
    public function __construct()
    {
        $this->dbConnexion = DatabaseConnexion::getInstance()->getConnection();
    }

    /**
     * Start Db Transaction
     */
    public function begin()
    {
        $this->dbConnexion->beginTransaction();
    }

    /**
     * Rollback Db Transaction
     */
    public function rollback()
    {
        $this->dbConnexion->rollBack();
    }

    /**
     * Check if we are in Db Transaction
     *
     * @return bool
     */
    public function hasIn(): bool
    {
        return $this->dbConnexion->inTransaction();
    }

    /**
     * Commit Db Transaction
     */
    public function commit()
    {
        $this->dbConnexion->commit();
    }
}
