<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : LQLComponent.php
 * @Created_at  : 29/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql\QueryComponent;

use LQL\QueryBuilder\MySql\QueryBuilder;

class LQLComponent extends AbstractComponent
{
    /**
     * LQLComponent constructor.
     *
     * @param QueryBuilder $builder
     * @param string $queryString
     */
    public function __construct(QueryBuilder $builder, string $queryString)
    {
        parent::__construct($builder);
        $this->queryString = $queryString;
    }
}
