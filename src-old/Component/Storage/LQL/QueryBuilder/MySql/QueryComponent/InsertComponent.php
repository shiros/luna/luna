<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : InsertComponent.php
 * @Created_at  : 29/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql\QueryComponent;

use LQL\QueryBuilder\MySql\Query;
use LQL\QueryBuilder\MySql\QueryBuilder;
use LQL\QueryBuilder\MySql\QueryComponent\Traits\Values;
use Luna\Component\Utils\ClassDefinition\ClassManager;
use Luna\Entity\Entity;

class InsertComponent extends AbstractComponent
{
    /* -------------------------------------------------------------------------- */
    /* TRAITS */

    use Values;


    /* -------------------------------------------------------------------------- */
    /* ATTRIBUTES */

    /** @var string */
    protected $insertQuery;

    /** @var array */
    protected $insertPart = [];


    /**
     * SelectComponent constructor.
     *
     * @param QueryBuilder $builder
     * @param string $table
     */
    public function __construct(QueryBuilder $builder, string $table)
    {
        parent::__construct($builder);
        $this->insert($table);
    }


    /* -------------------------------------------------------------------------- */
    /* QUERY */

    /**
     * INSERT Part
     *
     * @param string $table
     *
     * @return InsertComponent
     */
    protected function insert(string $table): InsertComponent
    {
        if (ClassManager::exist($table) && ClassManager::is(Entity::class, $table)) {
            /** @var Entity $entity */
            $entity = new $table();
            $table  = $entity->getTable();
        }

        $this->insertPart = $table;

        return $this;
    }


    /* -------------------------------------------------------------------------- */
    /* PREPARE QUERY */

    protected function prepareInsertPart()
    {
        $columns = implode(', ', $this->insertColumns);
        $columns = str_replace(':', '', "({$columns})");

        $this->insertQuery = " INSERT INTO {$this->insertPart} {$columns} ";
    }


    /* -------------------------------------------------------------------------- */
    /* VALIDATE */

    /**
     * Validate the Query and build it
     *
     * @return AbstractComponent
     * @throws \Luna\Exception\Database\QueryComponentException
     */
    public function validate(): AbstractComponent
    {
        $this->prepareInsertPart();
        $this->prepareValuesPart();

        $this->queryString = $this->insertQuery . $this->valuesQuery;

        return $this;
    }

    /**
     * @return Query
     * @throws \Luna\Exception\Database\QueryComponentException
     */
    public function getQuery(): Query
    {
        $query = parent::getQuery();
        $query->setParameters($this->paramsBag);

        return $query;
    }
}
