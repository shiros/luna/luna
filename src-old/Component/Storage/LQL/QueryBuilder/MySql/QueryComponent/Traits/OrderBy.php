<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : OrderBy.php
 * @Created_at  : 29/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql\QueryComponent\Traits;

trait OrderBy
{
    /** @var string */
    protected $orderByQuery;

    /** @var array */
    protected $orderByPart = [];

    /* -------------------------------------------------------------------------- */
    /* QUERY */

    /**
     * ORDER BY Part
     *
     * @param string $column
     *
     * @return self
     */
    public function orderBy(string $column): self
    {
        array_push($this->orderByPart, $column);

        return $this;
    }


    /* -------------------------------------------------------------------------- */
    /* PREPARE QUERY */

    protected function prepareOrderByPart()
    {
        $this->orderByQuery = 'ORDER BY ';
        $this->orderByQuery .= implode(', ', $this->orderByPart);
        $this->orderByQuery = " {$this->orderByQuery} ";
    }
}
