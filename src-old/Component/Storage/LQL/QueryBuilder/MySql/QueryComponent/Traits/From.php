<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : From.php
 * @Created_at  : 29/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql\QueryComponent\Traits;

use Luna\Component\Utils\ClassDefinition\ClassManager;
use Luna\Entity\Entity;

trait From
{
    /** @var string */
    protected $fromQuery;

    /** @var array */
    protected $fromPart = [];

    /* -------------------------------------------------------------------------- */
    /* QUERY */

    /**
     * FROM Part
     *
     * @param string $table
     * @param string|null $alias
     *
     * @return self
     */
    public function from(string $table, string $alias = null): self
    {
        if (ClassManager::exist($table) && ClassManager::is(Entity::class, $table)) {
            /** @var Entity $entity */
            $entity = new $table();
            $table  = $entity->getTable();
        }

        $fromQuery = $table;
        if (!is_null($alias)) {
            $fromQuery .= " {$alias}";
        }

        array_push($this->fromPart, $fromQuery);

        return $this;
    }


    /* -------------------------------------------------------------------------- */
    /* PREPARE QUERY */

    protected function prepareFromPart()
    {
        $this->fromQuery = 'FROM ';
        $this->fromQuery .= implode(', ', $this->fromPart);
        $this->fromQuery = " {$this->fromQuery} ";
    }
}
