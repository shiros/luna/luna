<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : AbstractComponent.php
 * @Created_at  : 29/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql\QueryComponent;

use LQL\QueryBuilder\MySql\Query;
use LQL\QueryBuilder\MySql\QueryBuilder;
use Luna\Exception\Database\QueryComponentException;

abstract class AbstractComponent
{
    /** @var string */
    protected $queryString;

    /** @var QueryBuilder */
    protected $builder;

    /**
     * AbstractComponent constructor.
     *
     * @param QueryBuilder $builder
     */
    public function __construct(QueryBuilder $builder)
    {
        $this->builder     = $builder;
        $this->queryString = null;
    }

    /**
     * Validate the Query and build it
     *
     * @return AbstractComponent
     */
    public function validate(): AbstractComponent
    {
        return $this;
    }

    /**
     * @return Query
     * @throws QueryComponentException
     */
    public function getQuery(): Query
    {
        $query = trim($this->queryString);

        if (empty($this->queryString) || is_null($this->queryString)) {
            throw new QueryComponentException("You can't retrieve the query while it is empty");
        }

        return new Query($this->builder->getEntityName(), $query);
    }
}
