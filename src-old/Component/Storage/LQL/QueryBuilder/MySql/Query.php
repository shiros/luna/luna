<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Query.php
 * @Created_at  : 11/03/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace LQL\QueryBuilder\MySql;

use LQL\Connextion\DatabaseConnexion;
use LQL\Connextion\DatabaseTransaction;
use Luna\Component\Exception\DatabaseException;
use Luna\Component\Storage\LQL\QueryBuilder\MySQL\HttpException;
use PDO;
use PDOStatement;

class Query
{
    /**
     * Specifies that the fetch method shall return each row as an array indexed
     * by column name as returned in the corresponding result set. If the result
     * set contains multiple columns with the same name,
     * <b>PDO::FETCH_ASSOC</b> returns
     * only a single value per column name.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_ASSOC = PDO::FETCH_ASSOC;

    /**
     * Specifies that the fetch method shall return each row as an array indexed
     * by both column name and number as returned in the corresponding result set,
     * starting at column 0.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_BOTH = PDO::FETCH_BOTH;

    /**
     * Specifies that the fetch method shall return TRUE and assign the values of
     * the columns in the result set to the PHP variables to which they were
     * bound with the <b>PDOStatement::bindParam</b> or
     * <b>PDOStatement::bindColumn</b> methods.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_BOUND = PDO::FETCH_BOUND;

    /**
     * Specifies that the fetch method shall return a new instance of the
     * requested class, mapping the columns to named properties in the class.
     * The magic
     * <b>__set</b>
     * method is called if the property doesn't exist in the requested class
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_CLASS = PDO::FETCH_CLASS;

    /**
     * Specifies that the fetch method shall update an existing instance of the
     * requested class, mapping the columns to named properties in the class.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_INTO = PDO::FETCH_INTO;

    /**
     * Specifies that the fetch method shall return each row as an object with
     * variable names that correspond to the column names returned in the result
     * set. <b>PDO::FETCH_LAZY</b> creates the object variable names as they are accessed.
     * Not valid inside <b>PDOStatement::fetchAll</b>.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_LAZY = PDO::FETCH_LAZY;

    /**
     * Specifies that the fetch method shall return each row as an array indexed
     * by column name as returned in the corresponding result set. If the result
     * set contains multiple columns with the same name,
     * <b>PDO::FETCH_NAMED</b> returns
     * an array of values per column name.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_NAMED = PDO::FETCH_NAMED;

    /**
     * Specifies that the fetch method shall return each row as an array indexed
     * by column number as returned in the corresponding result set, starting at
     * column 0.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_NUM = PDO::FETCH_NUM;

    /**
     * Specifies that the fetch method shall return each row as an object with
     * property names that correspond to the column names returned in the result
     * set.
     *
     * @link http://php.net/manual/en/pdo.constants.php
     */
    public const FETCH_OBJ = PDO::FETCH_OBJ;

    /** @var DatabaseConnexion */
    protected $connexion;

    /** @var DatabaseTransaction */
    protected $transaction;

    /** @var int */
    protected $fetchMode;

    /** @var string */
    protected $entityName;

    /** @var string */
    protected $queryString;

    /** @var array */
    protected $parameters;


    /** @var bool */
    protected $isExecuted;

    /** @var PDOStatement */
    protected $request;

    /** @var bool */
    protected $requestStatus;

    /**
     * Query constructor.
     *
     * @param string $entityName
     * @param string $queryString
     */
    public function __construct(string $entityName, string $queryString)
    {
        $this->connexion   = DatabaseConnexion::getInstance()->getConnection();
        $this->transaction = new DatabaseTransaction();

        $this->parameters  = [];
        $this->entityName  = $entityName;
        $this->queryString = trim($queryString);

        $this->isExecuted = false;
    }

    /**
     * Allow to set an parameter in the query string
     *
     * Example :
     *
     *  Query : SELECT * FROM TestTable TT WHERE TT.id = :id
     *  After : $query->setParameter('id', 0);
     *
     *
     * @param string $key
     * @param $value
     *
     * @return Query
     */
    public function setParameter(string $key, $value): Query
    {
        $key   = trim($key);
        $value = trim($value);

        if (stripos($key, ':') !== 0) {
            $key = ":{$key}";
        }
        $this->parameters[$key] = $value;

        return $this;
    }

    /**
     * That the same thing of 'setParameter' method, but for an array of values
     *
     * @param array $params
     *
     * @return Query
     */
    public function setParameters(array $params): Query
    {
        foreach ($params as $key => $value) {
            $this->setParameter($key, $value);
        }

        return $this;
    }

    /**
     * Permit to change the fetch mode of PDO
     *
     * @param int $mode
     *
     * @return Query
     */
    public function setFetchMode(int $mode): Query
    {
        $this->fetchMode = $mode;

        return $this;
    }

    /**
     * Get the LQL Query String
     *
     * @return string
     */
    public function getLQL(): string
    {
        return $this->queryString;
    }

    /**
     * Retrieve the identifier of the last inserted object
     *
     * @return int
     */
    public function getLastInsertId(): int
    {
        return $this->connexion->lastInsertId();
    }

    /**
     * Execute the query.
     * If an error is occured, a rollback is performed.
     *
     * @return void
     *
     * @throws HttpException
     */
    public function execute(): void
    {
        try {
            $this->transaction->begin();

            // Check if the query have parameters
            if (empty($this->parameters)) {
                $this->request       = $this->connexion->query($this->queryString); // Because user don't interact with query
                $this->requestStatus = $this->request->execute();
            } else {
                $this->request       = $this->connexion->prepare($this->queryString); // Because user interact with query
                $this->requestStatus = $this->request->execute($this->parameters);
            }

            $this->transaction->commit();
            $this->isExecuted = true;
        } catch (\PDOException $PDOException) {
            $this->transaction->rollback();
            throw $PDOException;
        }
    }

    /**
     * Retrieves the status of the request.
     *
     * @return bool
     *
     * @throws HttpException
     */
    public function getStatus(): bool
    {
        if (!$this->isExecuted) {
            throw new DatabaseException('Execute query before retreive the status');
        }

        return $this->requestStatus;
    }

    /**
     * Retrieves the result of the request.
     *
     * @param bool $one
     *
     * @return mixed
     *
     * @throws HttpException
     */
    public function getResult(bool $one = false)
    {
        if (!$this->isExecuted) {
            throw new DatabaseException('Execute query before retreive the result');
        }

        $class = $this->entityName;
        switch ($this->fetchMode) {
            case static::FETCH_ASSOC:
                $this->request->setFetchMode(static::FETCH_ASSOC);
                break;

            case static::FETCH_BOTH:
                $this->request->setFetchMode(static::FETCH_BOTH);
                break;

            case static::FETCH_BOUND:
                $this->request->setFetchMode(static::FETCH_BOUND);
                break;

            case static::FETCH_CLASS:
                $this->request->setFetchMode(static::FETCH_CLASS, $class);
                break;

            case static::FETCH_INTO:
                $class = new $class();
                $this->request->setFetchMode(static::FETCH_INTO, $class);
                break;

            case static::FETCH_LAZY:
                $this->request->setFetchMode(static::FETCH_LAZY);
                break;

            case static::FETCH_NAMED:
                $this->request->setFetchMode(static::FETCH_NAMED);
                break;

            case static::FETCH_NUM:
                $this->request->setFetchMode(static::FETCH_NUM);
                break;

            case static::FETCH_OBJ:
                $this->request->setFetchMode(static::FETCH_OBJ);
                break;

            default:
                $this->request->setFetchMode(static::FETCH_CLASS);
                break;
        }

        return (($one) ? $this->request->fetch() : $this->request->fetchAll());
    }
}
