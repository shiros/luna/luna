<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormFactory.php
 * @Created_at  : 04/09/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Factory;

use Form\Child\FormChild;
use Form\Child\FormChildInterface;
use Form\Resolver\FormOptionResolver;
use Form\Resolver\FormOptionResolverInterface;
use Form\Type\FormTypeInterface;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormFactoryException;
use Luna\Component\Exception\FormOptionsResolverException;
use Luna\Component\Utils\ClassDefinition\ClassManager;

class FormFactory
{
    /**
     * Construct a Form Type
     *
     * @param string $type
     * @param mixed $data
     * @param array $options
     *
     * @return FormTypeInterface
     */
    public static function createType(string $type, $data, array $options): FormTypeInterface
    {
        // Check Type
        if (!ClassManager::implement(FormTypeInterface::class, $type)) {
            throw new FormFactoryException(
                "Type '{$type}' doesn't implement " . FormTypeInterface::class
            );
        }

        // Create Type
        /** @var FormTypeInterface $type */
        try {
            $type = DependencyInjector::callConstructor($type);
            $type->init($data, $options);
            return $type;
        } catch (DependencyInjectorException $throwable) {
            throw new FormFactoryException(
                $throwable->getMessage(),
                FormFactoryException::DEFAULT_CODE,
                $throwable
            );
        }
    }

    /**
     * Create Form Option Resolver
     *
     * @param array $options
     *
     * @return FormChildInterface
     */
    public static function createOptionResolver(
        array $options,
        ?string $optionResolverClassName = null
    ): FormChildInterface {
        if (is_null($optionResolverClassName)) {
            $optionResolver = new FormOptionResolver($options);
        } else {
            if (!ClassManager::implement(FormOptionResolverInterface::class, $optionResolverClassName)) {
                throw new FormFactoryException(
                    "{$optionResolverClassName} don't implemennt FormOptionResolverInterface::class"
                );
            }

            $var = compact('options');
            try {
                $optionResolver = DependencyInjector::callConstructor($optionResolverClassName, $var);
            } catch (DependencyInjectorException $throwable) {
                throw new FormFactoryException(
                    $throwable->getMessage(),
                    FormFactoryException::DEFAULT_CODE,
                    $throwable
                );
            }
        }

        try {
            $optionResolver->resolve();
        } catch (FormOptionsResolverException $throwable) {
            throw new FormFactoryException(
                $throwable->getMessage(),
                FormFactoryException::DEFAULT_CODE,
                $throwable
            );
        }

        return $optionResolver;
    }

    /**
     * Create Form Child
     *
     * @param string $name
     * @param string $type
     * @param ParameterBag $options
     *
     * @return FormChildInterface
     */
    public static function createChild(string $name, string $type, ParameterBag $options): FormChildInterface
    {
        return new FormChild($name, $type, $options);
    }
}
