<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : CheckboxType.php
 * @Created_at  : 13/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilderInterface;
use Form\Resolver\FormOptionResolverInterface;
use Form\Resolver\ListFormOptionResolver;
use Form\View\FormViewInterface;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Exception\FormOptionsResolverException;

class CheckboxType extends FormType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     * @throws FormOptionsResolverException
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new ListFormOptionResolver($options, true);
    }

    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $child = $this->createChild();

        /** @var ListFormOptionResolver $optionResolver */

        // Get Name & Id
        $id   = $optionResolver->getFieldName();
        $name = $id;

        // Prepare Attributes
        $multiple = $optionResolver->getMultiple();
        $items    = $optionResolver->getItems();
        $value    = $this->getData();

        $errorClass   = $optionResolver->getErrorClass();
        $errorMessage = $optionResolver->getErrorMessage();
        $error        = !is_null($errorMessage);

        $fieldClass = $optionResolver->getFieldClass($error);

        // Prepare Attributes
        if ($multiple) {
            $name = "{$name}[]";
        }

        // Prepare Field
        $tagField = '';
        foreach ($items as $key => $item) {
            $selected = '';

            if ($optionResolver->isSelected($item, $value)) {
                $selected = 'checked="checked"';
            }

            $labelClass = $optionResolver->getLabelClass($error);
            $tagLabel   = "<label {$labelClass} for='{$id}'>{$item}</label>";
            $tagInput   = "<input type='checkbox' {$fieldClass} id='{$id}' name='{$name}' value='{$item}' {$selected} />";
            $tagField   .= "{$tagInput} {$tagLabel}";
        }
        $tagField .= '</select>';

        // Prepare Error
        if (!is_null($errorMessage)) {
            $tagError = "<span {$errorClass}>{$errorMessage}</span>";

            $tagField = "{$tagField} {$tagError}";
        }

        // Set Tag
        $child->setContents($tagField);

        $view->add($child->getName(), $child);
    }
}
