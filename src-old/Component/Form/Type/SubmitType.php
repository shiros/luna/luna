<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : SubmitType.php
 * @Created_at  : 07/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Resolver\FormOptionResolverInterface;
use Form\Resolver\SubmitButtonFormOptionResolver;
use Luna\Component\Exception\FormOptionsResolverException;

class SubmitType extends ButtonType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     * @throws FormOptionsResolverException
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new SubmitButtonFormOptionResolver($options);
    }
}
