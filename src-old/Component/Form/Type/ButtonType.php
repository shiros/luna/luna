<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ButtonType.php
 * @Created_at  : 07/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilderInterface;
use Form\Resolver\ButtonFormOptionResolver;
use Form\Resolver\FormOptionResolverInterface;
use Form\View\FormViewInterface;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Exception\FormOptionsResolverException;

class ButtonType extends FormType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new ButtonFormOptionResolver($options);
    }

    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $child = $this->createChild();

        /** @var ButtonFormOptionResolver $optionResolver */

        // Get Name
        $name = $optionResolver->getFieldName();

        // Prepare Attributes
        $type  = $optionResolver->getType();
        $label = $optionResolver->getLabel();
        $value = $this->getData();

        $errorMessage = $optionResolver->getErrorMessage();
        $fieldClass   = $optionResolver->getFieldClass(!is_null($errorMessage));

        // Prepare Attributes
        if (!is_null($type)) {
            $type = "type='{$type}'";
        }
        if (!is_null($value)) {
            $value = "value='{$value}'";
        }
        if (is_null($label)) {
            $label = $name;
        }

        // Prepare Button
        $tag = "<button {$type} {$fieldClass} id='{$name}' name='{$name}' {$value}>{$label}</button>";

        // Set Tag
        $child->setContents($tag);

        $view->add($child->getName(), $child);
    }
}
