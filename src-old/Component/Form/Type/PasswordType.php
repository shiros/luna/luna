<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : PasswordType.php
 * @Created_at  : 07/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Resolver\FormOptionResolverInterface;
use Form\Resolver\InputFormOptionResolver;
use Luna\Component\Exception\FormOptionsResolverException;

class PasswordType extends InputType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     * @throws FormOptionsResolverException
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new InputFormOptionResolver($options, 'password');
    }
}
