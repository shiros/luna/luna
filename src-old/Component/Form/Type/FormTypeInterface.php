<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormTypeInterface.php
 * @Created_at  : 05/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Resolver\FormOptionResolverInterface;
use Form\View\FormViewInterface;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\HTTP\Request\RequestInterface;

interface FormTypeInterface
{
    # -------------------------------------------------------------
    #   Handle Request
    # -------------------------------------------------------------

    /**
     * Handle the Request and validate form.
     *
     * @param RequestInterface $request
     *
     * @return void
     */
    public function handleRequest(RequestInterface $request): void;


    # -------------------------------------------------------------
    #   Init & Configure
    # -------------------------------------------------------------

    /**
     * Init Form.
     *
     * @param mixed $data
     * @param array $options
     *
     * @return void
     */
    public function init($data = null, array $options = []): void;


    # -------------------------------------------------------------
    #   Bind
    # -------------------------------------------------------------

    /**
     * Bind the Post Value with Data
     *
     * @param ParameterBag $bag
     *
     * @return void
     */
    public function bindData(ParameterBag $bag): void;


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * @return bool
     */
    public function isInit(): bool;

    /**
     * Get the parent name of form
     *
     * @return null|string
     */
    public function getParentName(): ?string;

    /**
     * Get the name of form
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Return boolean to know if this is a form or view component
     *
     * @return bool
     */
    public function isForm(): bool;

    /**
     * Get the Form Children
     *
     * @return ParameterBag
     */
    public function getChildren(): ParameterBag;

    /**
     * Get data of form
     *
     * @return mixed
     */
    public function getData();

    /**
     * Get the Form Option Resolver
     *
     * @return FormOptionResolverInterface
     */
    public function getOptionResolver(): FormOptionResolverInterface;

    /**
     * Get the View of the current form
     *
     * @return FormViewInterface
     */
    public function getView(): FormViewInterface;


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set data of form
     *
     * @param mixed $data
     *
     * @return FormTypeInterface
     */
    public function setData($data): FormTypeInterface;

    /**
     * Set options of form
     *
     * @param array $options
     *
     * @return FormTypeInterface
     */
    public function setOptions(array $options): FormTypeInterface;
}
