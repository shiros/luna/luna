<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : TextAreaType.php
 * @Created_at  : 07/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilderInterface;
use Form\Resolver\FormOptionResolver;
use Form\Resolver\FormOptionResolverInterface;
use Form\View\FormViewInterface;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Exception\FormOptionsResolverException;

class TextAreaType extends FormType
{
    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $child = $this->createChild();

        /** @var FormOptionResolver $optionResolver */

        // Get Name
        $name = $optionResolver->getFieldName();

        // Prepare Attributes
        $placeholder = $optionResolver->getPlaceholder();
        $required    = $optionResolver->getRequired();
        $value       = $this->getData();

        $errorClass   = $optionResolver->getErrorClass();
        $errorMessage = $optionResolver->getErrorMessage();
        $error        = !is_null($errorMessage);

        $fieldClass = $optionResolver->getFieldClass($error);

        // Prepare Field
        $tagField = "
            <textarea {$fieldClass} id='{$name}' name='{$name}' placeholder='{$placeholder}' {$required}>
                {$value}
            </textarea>
        ";

        // Prepare Label
        $label = $optionResolver->getLabel();
        if (!is_null($label)) {
            $labelClass = $optionResolver->getLabelClass($error);
            $tagLabel   = "<label {$labelClass} for='{$name}'>{$label}</label>";

            $tagField = "{$tagLabel} {$labelClass}";
        }

        // Prepare Error
        if (!is_null($errorMessage)) {
            $tagError = "<span {$errorClass}>{$errorMessage}</span>";

            $tagField = "{$tagField} {$tagError}";
        }

        // Set Tag
        $child->setContents($tagField);

        $view->add($child->getName(), $child);
    }
}
