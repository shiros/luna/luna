<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : InputType.php
 * @Created_at  : 07/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilderInterface;
use Form\Child\FormChildInterface;
use Form\Resolver\FormOptionResolverInterface;
use Form\Resolver\InputFormOptionResolver;
use Form\View\FormViewInterface;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Exception\FormOptionsResolverException;

class InputType extends FormType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new InputFormOptionResolver($options);
    }

    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        /** @var InputFormOptionResolver $optionResolver */

        $type = $optionResolver->getType();

        switch ($type) {
            case 'checkbox':
                $this->prepareCheckboxInput($view, $optionResolver);
                break;

            default:
                $this->prepareInput($view, $builder, $optionResolver);
                break;
        }
    }


    /**
     * Prepare checkbox input.
     *
     * @param FormViewInterface $view
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     */
    public function prepareCheckboxInput(FormViewInterface $view, FormOptionResolverInterface $optionResolver): void
    {
        $type = new CheckboxType();
        $type->init($this->getData(), $optionResolver->getOptions());
        $children = $type->getChildren();

        /** @var FormChildInterface $child */
        foreach ($children as $child) {
            $view->add($child->getName(), $child);
        }
    }


    /**
     * Prepare basic input.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return string
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function prepareInput(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $child = $this->createChild();

        /** @var InputFormOptionResolver $optionResolver */

        // Get Name
        $name = $optionResolver->getFieldName();

        // Prepare Attributes
        $type        = $optionResolver->getType();
        $placeholder = $optionResolver->getPlaceholder();
        $required    = $optionResolver->getRequired();
        $value       = $this->getData();

        $errorClass   = $optionResolver->getErrorClass();
        $errorMessage = $optionResolver->getErrorMessage();
        $error        = !is_null($errorMessage);

        $fieldClass = $optionResolver->getFieldClass($error);

        // Prepare Attributes
        if (!is_null($value)) {
            $value = "value='{$value}'";
        }

        // Prepare Field
        // phpcs:disable
        $tagField = "<input type='{$type}' {$fieldClass} id='{$name}' name='{$name}' {$placeholder} {$value} {$required}/>";
        // phpcs:enable

        // Prepare Label
        $label = $optionResolver->getLabel();
        if (!is_null($label)) {
            $labelClass = $optionResolver->getLabelClass($error);
            $tagLabel   = "<label {$labelClass} for='{$name}'>{$label}</label>";

            $tagField = "{$tagLabel} {$tagField}";
        }

        // Prepare Error
        if (!is_null($errorMessage)) {
            $tagError = "<span {$errorClass}>{$errorMessage}</span>";

            $tagField = "{$tagField} {$tagError}";
        }

        // Set Tag
        $child->setContents($tagField);

        $view->add($child->getName(), $child);
    }
}
