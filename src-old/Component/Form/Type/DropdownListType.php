<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : DropdownListType.php
 * @Created_at  : 13/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilderInterface;
use Form\Resolver\FormOptionResolverInterface;
use Form\Resolver\ListFormOptionResolver;
use Form\View\FormViewInterface;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Exception\FormOptionsResolverException;
use Luna\Component\Form\Type\ListType;

class DropdownListType extends ListType
{
    /**
     * Create the Form Option Resolver
     *
     * @param array $options
     *
     * @return FormOptionResolverInterface
     */
    public function createOptionResolver(array $options): FormOptionResolverInterface
    {
        return new ListFormOptionResolver($options);
    }

    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     *
     * @throws DependencyInjectorException
     * @throws FormException
     * @throws FormOptionsResolverException
     */
    public function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $child = $this->createChild();

        /** @var ListFormOptionResolver $optionResolver */

        // Get Name & Id
        $id   = $optionResolver->getFieldName();
        $name = $id;

        // Prepare Attributes
        $items    = $optionResolver->getItems();
        $value    = $this->getData();
        $required = $optionResolver->getRequired();
        $multiple = $optionResolver->getMultiple();
        $size     = $optionResolver->getSize();

        $errorClass   = $optionResolver->getErrorClass();
        $errorMessage = $optionResolver->getErrorMessage();
        $error        = !is_null($errorMessage);

        $fieldClass = $optionResolver->getFieldClass($error);

        // Prepare Attributes
        if ($multiple) {
            $multiple = 'multiple';
            $name     = "{$name}[]";
        }
        if ($size) {
            $size = "size={$size}";
        }

        // Prepare Field
        $tagField = "<select {$fieldClass} id='{$id}' name='{$name}' {$size} {$multiple} {$required}>";
        foreach ($items as $key => $item) {
            $selected = '';

            if ($this->isSelected($item, $value)) {
                $selected = 'selected';
            }

            $tagField .= "<option value='{$item}' {$selected}>{$key}</option>";
        }
        $tagField .= '</select>';

        // Prepare Label
        $label = $optionResolver->getLabel();
        if (!is_null($label)) {
            $labelClass = $optionResolver->getLabelClass($error);
            $tagLabel   = "<label {$labelClass} for='{$id}'>{$label}</label>";

            $tagField = "{$tagLabel} {$tagField}";
        }

        // Prepare Error
        if (!is_null($errorMessage)) {
            $tagError = "<span {$errorClass}>{$errorMessage}</span>";

            $tagField = "{$tagField} {$tagError}";
        }

        // Set Tag
        $child->setContents($tagField);

        $view->add($child->getName(), $child);
    }
}
