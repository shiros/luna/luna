<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormType.php
 * @Created_at  : 05/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Type;

use Form\Builder\FormBuilder;
use Form\Builder\FormBuilderInterface;
use Form\Child\FormChildInterface;
use Form\Factory\FormFactory;
use Form\Resolver\FormOptionResolver;
use Form\Resolver\FormOptionResolverInterface;
use Form\View\FormView;
use Form\View\FormViewInterface;
use Luna\Component\Bag\Bag;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\Exception\BindException;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\HTTP\Request\RequestInterface;
use Luna\Component\Utils\ClassDefinition\ClassManager;
use Luna\Component\Utils\Object\ObjectManager;
use Validation\ValidationInterface;

abstract class FormType implements FormTypeInterface
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    protected const VALIDATION_TYPE = null;


    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var bool */
    protected $isInit = false;

    /** @var ParameterBag */
    protected $children;

    /** @var mixed */
    protected $data;

    /** @var FormOptionResolverInterface */
    protected $optionResolver;

    /** @var FormBuilderInterface */
    protected $formBuilder;

    /** @var FormViewInterface */
    protected $formView;

    # -------------------------------------------------------------
    #   Handle Request
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to be init type before use it
     *
     * Handle the Request and validate form.
     *
     * @param RequestInterface $request
     *
     * @return void
     * @throws BindException
     * @throws FormException
     */
    public function handleRequest(RequestInterface $request): void
    {
        $this->checkIfInit();

        if ($request->isPostRequest()) {
            $this->bindData($request->getPostRequest());

            if (!is_null(static::VALIDATION_TYPE)) {
                $validationOptions = null; // TODO : Validate Data. Call Validator
                $this->optionResolver->set(FormOptionResolverInterface::OPTION_VALIDATION, $validationOptions);
            }
        }
    }

    # -------------------------------------------------------------
    #   Init & Configure
    # -------------------------------------------------------------

    /**
     * Init Form.
     *
     * @param mixed $data
     * @param array $options
     *
     * @return void
     *
     * @throws FormException
     */
    public function init($data = null, array $options = []): void
    {
        $this->isInit = false;

        // Check if the form have validation rules
        if (!is_null(static::VALIDATION_TYPE)) {
            if (!ClassManager::implement(ValidationInterface::class, static::VALIDATION_TYPE)) {
                throw new FormException(static::VALIDATION_TYPE . " doesn't implement " . ValidationInterface::class);
            }

            // TODO : Use DI to construct validator
        }

        // Init vars
        $this->children = new ParameterBag();
        $this
            ->setData($data)
            ->setOptions($options)
        ;

        // Build Form
        $this->formBuilder = new FormBuilder($this);
        $this->buildForm($this->formBuilder, $this->optionResolver);

        // Get Children
        $this->children->replace($this->formBuilder->all());

        // Configure Children
        $this->configureChildren();

        // Set Init State
        $this->isInit = true;
    }

    /**
     * Return an Form Option Resolver Class
     *
     * @return string
     */
    protected function optionResolverClass(): string
    {
        return FormOptionResolver::class;
    }

    /**
     * Allow to configure each child after build
     */
    protected function configureChildren(): void
    {
        /** @var FormChildInterface $child */
        foreach ($this->children as $child) {
            // Get Child Name
            $name = $child->getName();

            // Get information
            $data              = $this->getValue($name);
            $childOptions      = $child->getOptions();
            $validationOptions = $this->optionResolver->get(FormOptionResolverInterface::OPTION_VALIDATION);

            // Prepare information
            if (is_array($validationOptions)) {
                $validationOptions = new ParameterBag($validationOptions);
                $validationOptions = $validationOptions->get($name);

                $childOptions->set(FormOptionResolverInterface::OPTION_VALIDATION, $validationOptions);
            }

            // Set information in child
            $child
                ->setData($data)
                ->setOptions($childOptions)
            ;;
        }
    }

    /**
     * Check if options are resolved
     *
     * @throws FormException
     */
    protected function checkIfInit()
    {
        if (!$this->isInit()) {
            throw new FormException('You have to init the form type before');
        }
    }


    # -------------------------------------------------------------
    #   Bind
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to be init type before use it
     *
     * Bind the Post Value with Data.
     *
     * @param ParameterBag $bag
     *
     * @return void
     * @throws BindException
     * @throws FormException
     */
    public function bindData(ParameterBag $bag): void
    {
        $this->checkIfInit();

        if (!ClassManager::is(Bag::class, $this->data) or is_object($this->data)) {
            throw new BindException('Bind Fail, data must be an array, an object or a bag.');
        }

        $this->bindProcess($this->children, $bag);
        $this->configureChildren();
    }

    /**
     * @param ParameterBag $children
     * @param ParameterBag $requestBag
     *
     * @throws BindException
     */
    protected function bindProcess(ParameterBag $children, ParameterBag $requestBag)
    {
        /** @var FormChildInterface $child */
        foreach ($children as $child) {
            $name        = $child->getName();
            $requestData = $requestBag->get($name);

            if ($child->hasChildren() && is_array($requestData)) {
                $this->bindProcess($child->getChildren(), $requestData);
            }

            if (ClassManager::is(ParameterBag::class, $this->data)) {
                $this->data->set($name, $requestData);
            } elseif (is_object($this->data)) {
                ObjectManager::bindValue($this->data, $name, $requestData);
            }
        }
    }



    # -------------------------------------------------------------
    #   Build
    # -------------------------------------------------------------

    /**
     * Build the Form through a Builder.
     * Adding elements, like inputs, buttons, drop-down lists, ...
     *
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     */
    protected function buildForm(FormBuilderInterface $builder, FormOptionResolverInterface $optionResolver): void
    {
        // Redefine in sub-class
    }

    /**
     * Build the Form View.
     *
     * @param FormViewInterface $view
     * @param FormBuilderInterface $builder
     * @param FormOptionResolverInterface $optionResolver
     *
     * @return void
     */
    protected function buildView(
        FormViewInterface $view,
        FormBuilderInterface $builder,
        FormOptionResolverInterface $optionResolver
    ): void {
        $children = $builder->all();

        /**
         * @var string $name
         * @var FormChildInterface $child
         */
        foreach ($children as $name => $child) {
            $view->add($name, $child);
        }
    }


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * @return bool
     */
    public function isInit(): bool
    {
        return $this->isInit;
    }

    /**
     * @IMPORTANT : Need to be init type before use it
     *
     * Get the parent name of form
     *
     * @return null|string
     * @throws FormException
     */
    public function getParentName(): ?string
    {
        $this->checkIfInit();
        return $this->optionResolver->getParentName();
    }

    /**
     * Get the name of form
     *
     * @return string
     */
    public function getName(): string
    {
        try {
            return (new \ReflectionClass($this))->getShortName();
        } catch (\Throwable $throwable) {
            return get_class($this);
        }
    }

    /**
     * Return boolean to know if this is a form or view component
     *
     * @return bool
     * @throws FormException
     */
    public function isForm(): bool
    {
        return !$this->children->isEmpty() && $this->isInit;
    }

    /**
     * Get the Form Children
     *
     * @return ParameterBag
     * @throws FormException
     */
    public function getChildren(): ParameterBag
    {
        return $this->children;
    }

    /**
     * Get data of form
     *
     * @return mixed
     * @throws FormException
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @IMPORTANT : Need to be init type before use it
     *
     * Get the Form Option Resolver
     *
     * @return FormOptionResolverInterface
     * @throws FormException
     */
    public function getOptionResolver(): FormOptionResolverInterface
    {
        $this->checkIfInit();

        return $this->optionResolver;
    }

    /**
     * @IMPORTANT : Need to be init type before use it
     *
     * Get the View of the current form
     *
     * @return FormViewInterface
     * @throws DependencyInjectorException
     * @throws FormException
     */
    public function getView(): FormViewInterface
    {
        $this->checkIfInit();

        // Prepare View Instance
        /** @var FormViewInterface formView */
        $this->formView = DependencyInjector::callConstructor(FormView::class);

        // Build Form View
        $this->buildView($this->formView, $this->formBuilder, $this->optionResolver);

        return $this->formView;
    }

    /**
     * Get value for field
     *
     * @param string $name
     * @param null $default
     *
     * @return mixed
     */
    protected function getValue(string $name, $default = null)
    {
        if (ClassManager::is(ParameterBag::class, $this->data)) {
            return $this->data->get($name);
        } elseif (is_object($this->data) and is_string($name)) {
            return ObjectManager::getValue($this->data, $name, $default);
        }

        return $default;
    }


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set data of form
     *
     * @param mixed $data
     *
     * @return FormTypeInterface
     */
    public function setData($data): FormTypeInterface
    {
        // Prepare Data
        if (is_array($data)) {
            $data = new ParameterBag($data);
        }

        // Set Data
        $this->data = $data;

        // Configure Children
        $this->configureChildren();

        return $this;
    }

    /**
     * Set options of form
     *
     * @param array $options
     *
     * @return FormTypeInterface
     */
    public function setOptions(array $options): FormTypeInterface
    {
        // Check Options
        if (!array_key_exists(FormOptionResolverInterface::FIELD_NAME, $options)) {
            $options[FormOptionResolverInterface::FIELD_NAME] = $this->getName();
        }

        // Create Option Resolver
        $this->optionResolver = FormFactory::createOptionResolver($options, $this->optionResolverClass());

        // Configure Children
        $this->configureChildren();

        return $this;
    }


    # -------------------------------------------------------------
    #   Utils
    # -------------------------------------------------------------

    /**
     * @return FormChildInterface
     */
    protected function createChild(): FormChildInterface
    {
        return FormFactory::createChild(
            $this->optionResolver->getFieldName(),
            get_class($this),
            $this->optionResolver->getOptions()
        );
    }
}
