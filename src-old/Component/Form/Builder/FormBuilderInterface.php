<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormBuilderInterface.php
 * @Created_at  : 05/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Builder;

use Luna\Component\Bag\ParameterBag;

interface FormBuilderInterface
{
    /**
     * Add field to forms
     *
     * @param string $name
     * @param string $type
     * @param array $options
     *
     * @return FormBuilderInterface
     */
    public function add(string $name, string $type, array $options): FormBuilderInterface;

    /**
     * Get all fields
     *
     * @return ParameterBag
     */
    public function all(): ParameterBag;
}
