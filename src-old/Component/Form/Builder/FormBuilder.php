<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormBuilder.php
 * @Created_at  : 06/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Builder;

use Form\Factory\FormFactory;
use Form\Resolver\FormOptionResolverInterface;
use Form\Type\FormTypeInterface;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Utils\ClassDefinition\ClassManager;
use Luna\Component\Utils\Object\ObjectManager;

class FormBuilder implements FormBuilderInterface
{
    /** @var FormTypeInterface */
    protected $formType;

    /** @var ParameterBag */
    protected $children;

    /**
     * FormBuilder constructor.
     *
     * @param FormTypeInterface $formType
     */
    public function __construct(FormTypeInterface $formType)
    {
        $this->formType = $formType;
        $this->children = new ParameterBag();
    }

    /**
     * Add field to forms
     *
     * @param string $name
     * @param string $type
     * @param array $options
     *
     * @return FormBuilderInterface
     *
     * @throws DependencyInjectorException
     * @throws FormException
     */
    public function add(string $name, string $type, array $options): FormBuilderInterface
    {
        if (!ClassManager::implement(FormTypeInterface::class, $type)) {
            throw new FormException("Type '{$type}' doesn't implement " . FormTypeInterface::class);
        }

        // Prepare Options
        $options = $this->prepareOptions($name, $options);

        // Prepare form child
        $child = FormFactory::createChild($name, $type, $options);
        $this->children->set($name, $child);

        return $this;
    }

    /**
     * Get all fields
     *
     * @return ParameterBag
     */
    public function all(): ParameterBag
    {
        return $this->children;
    }

    # -------------------------------------------------------------
    #   Prepare
    # -------------------------------------------------------------

    /**
     * Prepare Form Options
     *
     * @param string $name
     * @param array $options
     *
     * @return ParameterBag
     */
    protected function prepareOptions(string $name, array $options): ParameterBag
    {
        $options = new ParameterBag($options);

        $formName       = $this->formType->getName();
        $formParentName = $this->formType->getParentName();

        $fieldPrefix = $formName;
        if (!is_null($formParentName) && $this->formType->isForm()) {
            $fieldPrefix = "{$formParentName}[{$formName}]";
        }

        $options
            ->set(FormOptionResolverInterface::PARENT_FORM_NAME, $formName)
            ->set(FormOptionResolverInterface::FIELD_PREFIX, $fieldPrefix)
            ->set(FormOptionResolverInterface::FIELD_NAME, $name)
        ;

        return $options;
    }
}
