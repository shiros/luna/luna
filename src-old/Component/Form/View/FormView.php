<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormView.php
 * @Created_at  : 05/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\View;

use Form\Child\FormChildInterface;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\UrlException;
use Luna\Component\HTTP\Request\RequestInterface;
use Url\Url;

class FormView implements FormViewInterface
{
    /** @var Url */
    protected $urlModule;

    /** @var ParameterBag */
    protected $children;

    /**
     * FormView constructor.
     *
     * @param Url $urlModule
     */
    public function __construct(Url $urlModule)
    {
        $this->urlModule = $urlModule;
        $this->children  = new ParameterBag();
    }


    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * Add value for a child.
     *
     * @param string $key
     * @param FormChildInterface $child
     *
     * @return FormViewInterface
     */
    public function add(string $key, FormChildInterface $child): FormViewInterface
    {
        $this->children->set($key, $child);

        return $this;
    }


    # -------------------------------------------------------------
    #   Getter
    # -------------------------------------------------------------

    /**
     * Return the body of form
     *
     * @return string
     */
    public function getBody(): string
    {
        $body = '';

        /** @var FormChildInterface $child */
        foreach ($this->children as $child) {
            $body .= $child->getContents();
        }

        $body = trim($body);

        return $body;
    }


    # -------------------------------------------------------------
    #   Render Functions
    # -------------------------------------------------------------

    /**
     * Generate the start form tag.
     * Like :
     *  <form method="POST" action="...">
     *
     * @param string $method
     * @param string|null $action
     *
     * @return void
     */
    public function start(string $method = RequestInterface::POST, string $action = null): void
    {
        $formTag = '<form method="' . $method . '" ';

        if ($action != null) {
            try {
                $action = $this->urlModule->get($action);
            } catch (UrlException $e) {
            }

            $formTag .= 'action="' . $action . '" ';
        }

        $formTag .= '>';

        echo $formTag;
    }

    /**
     * Render of form body.
     *
     * @return void
     */
    public function body(): void
    {
        echo $this->getBody();
    }

    /**
     * Render of the wanted element.
     *
     * @param string $name
     *
     * @return void
     */
    public function child(string $name): void
    {
        $child = $this->children->get($name);

        if (!is_null($child)) {
            /** @var FormChildInterface $child */
            $contents = $child->getContents();
            $contents = trim($contents);

            echo $contents;
        }
    }

    /**
     * Generate the closure  form tag.
     * Like :
     *  </form>
     *
     * @return void
     */
    public function end(): void
    {
        $formTag = '</form>';

        echo $formTag;
    }
}
