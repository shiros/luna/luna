<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormViewInterface.php
 * @Created_at  : 05/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\View;

use Form\Child\FormChildInterface;
use Luna\Component\HTTP\Request\RequestInterface;

interface FormViewInterface
{
    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * Add value for a child.
     *
     * @param string $key
     * @param FormChildInterface $child
     *
     * @return FormViewInterface
     */
    public function add(string $key, FormChildInterface $child): FormViewInterface;


    # -------------------------------------------------------------
    #   Getter
    # -------------------------------------------------------------

    /**
     * Return the body of form
     *
     * @return string
     */
    public function getBody(): string;


    # -------------------------------------------------------------
    #   Render Functions
    # -------------------------------------------------------------

    /**
     * Generate the start form tag.
     * Like :
     *  <form method="POST" action="...">
     *
     * @param string $method
     * @param string|null $action
     *
     * @return void
     */
    public function start(string $method = RequestInterface::POST, string $action = null): void;

    /**
     * Render of form body.
     *
     * @return void
     */
    public function body(): void;

    /**
     * Render of the wanted element.
     *
     * @param string $name
     *
     * @return void
     */
    public function child(string $name): void;

    /**
     * Generate the closure  form tag.
     * Like :
     *  </form>
     *
     * @return void
     */
    public function end(): void;
}
