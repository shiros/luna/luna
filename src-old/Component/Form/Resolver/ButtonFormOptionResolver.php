<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ButtonFormOptionResolver.php
 * @Created_at  : 21/08/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Exception\FormOptionsResolverException;

class ButtonFormOptionResolver extends FormOptionResolver
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    private const   FORM_OPT_TYPE  = 'type';
    private const   FORM_OPT_VALUE = 'value';
    protected const TYPE           = null;


    # -------------------------------------------------------------
    #   Resolve Process
    # -------------------------------------------------------------

    /**
     * Resolve Options
     *
     * @return void
     * @throws FormOptionsResolverException
     */
    public function resolve(): void
    {
        parent::resolve();
        $this->isResolved = false;

        $options = $this->options;

        // Define button type
        if (!is_null(static::TYPE)) {
            $options->set(self::FORM_OPT_TYPE, static::TYPE);
        }

        $this->isResolved = true;
    }


    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field type
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getType(): ?string
    {
        $this->checkOptionsResolved();

        $options = $this->options;
        $type    = $options->get(self::FORM_OPT_TYPE);

        if (!is_null($type)) {
            $type = trim($type);
        }

        return $type;
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field type
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getValue()
    {
        $this->checkOptionsResolved();

        $options = $this->options;
        $value   = $options->get(self::FORM_OPT_VALUE);

        if (!is_null($value)) {
            $value = trim($value);
        }

        return $value;
    }
}
