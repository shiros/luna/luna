<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormOptionResolver.php
 * @Created_at  : 21/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\FormOptionsResolverException;

class FormOptionResolver implements FormOptionResolverInterface
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    private const FORM_OPT_LABEL    = 'label';
    private const FORM_OPT_REQUIRED = 'required';

    private const FORM_OPT_LABEL_ATTR    = 'label_attr';
    private const FORM_OPT_FIELD_ATTR    = 'field_attr';
    private const FORM_OPT_ERROR_ATTR    = 'error_attr';
    private const FORM_OPT_SURROUND_ATTR = 'surround_attr';

    private const FORM_OPT_ATTR_TYPE        = 'type';
    private const FORM_OPT_ATTR_CLASS       = 'class';
    private const FORM_OPT_ATTR_CLASS_ERROR = 'class_error';
    private const FORM_OPT_ATTR_PLACEHOLDER = 'placeholder';

    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var bool */
    protected $isResolved;

    /** @var ParameterBag */
    protected $options;

    /** @var ParameterBag */
    protected $validationOptions;

    /**
     * FormOptionResolver constructor.
     *
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->isResolved = false;
        $this->options    = new ParameterBag($options);
    }


    # -------------------------------------------------------------
    #   Resolve Process
    # -------------------------------------------------------------

    /**
     * Resolve Options
     *
     * @return void
     * @throws FormOptionsResolverException
     */
    public function resolve(): void
    {
        $options = $this->options;

        // Check that non-optional options are present
        if (!$options->has(self::FIELD_NAME)) {
            throw new FormOptionsResolverException("Configure component failed");
        }

        // Check if 'Required' Options exist
        if (!$options->has(self::FORM_OPT_REQUIRED)) {
            $options->set(self::FORM_OPT_REQUIRED, true);
        }

        // Check if 'Label Attributes' Options exist
        if ($options->has(self::FORM_OPT_LABEL_ATTR)) {
            $this->resolveAttributes(self::FORM_OPT_LABEL_ATTR, $options);
        }

        // Check if 'Field Attributes' Options exist
        if ($options->has(self::FORM_OPT_FIELD_ATTR)) {
            $this->resolveAttributes(self::FORM_OPT_FIELD_ATTR, $options);
        }

        // Check if 'Error Attributes' Options exist
        if ($options->has(self::FORM_OPT_ERROR_ATTR)) {
            $this->resolveAttributes(self::FORM_OPT_ERROR_ATTR, $options);
        }

        // Check if 'Surround Attributes' Options exist
        if ($options->has(self::FORM_OPT_SURROUND_ATTR)) {
            $this->resolveAttributes(self::FORM_OPT_SURROUND_ATTR, $options);
        }

        $this->isResolved = true;
    }

    /**
     * Resolve Attributes
     *
     * @param string $name
     * @param ParameterBag $options
     *
     * @throws FormOptionsResolverException
     */
    protected function resolveAttributes(string $name, ParameterBag $options)
    {
        $attrOption = $options->get($name);

        if (!is_array($attrOption)) {
            throw new FormOptionsResolverException("Attribute '{$name}' must be an array");
        }
    }

    /**
     * @return bool
     */
    public function isResolved(): bool
    {
        return $this->isResolved;
    }


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Return options
     *
     * @return ParameterBag
     */
    public function getOptions(): ParameterBag
    {
        return $this->options;
    }

    /**
     * Return form option, but if doesn't exist a default value is returned
     *
     * @param mixed $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->options->get($key, $default);
    }


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set value in the options
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return FormOptionResolverInterface
     * @throws FormOptionsResolverException
     */
    public function set($key, $value): FormOptionResolverInterface
    {
        $this->options->set($key, $value);
        $this->resolve();

        return $this;
    }


    # -------------------------------------------------------------
    #   Render Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Add Surround to the render of tag
     *
     * @param string $html
     *
     * @return string
     * @throws FormOptionsResolverException
     */
    public function getSurround(string $html): string
    {
        $this->checkOptionsResolved();

        $options = $this->options;

        if (!$options->has(self::FORM_OPT_SURROUND_ATTR)) {
            $surround = "<div>{$html}</div>";
            $surround = trim($surround);

            return $surround;
        }

        $surroundOptions = new ParameterBag($options->get(self::FORM_OPT_SURROUND_ATTR));

        $surroundType  = $surroundOptions->get(self::FORM_OPT_ATTR_TYPE, 'div');
        $surroundClass = $this->getClass($surroundOptions);

        $tag    = $surroundType;
        $tagEnd = $surroundType;

        $surround = "<{$tag} {$surroundClass}>{$html}</{$tagEnd}>";
        $surround = trim($surround);

        return $surround;
    }



    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * Check if options are resolved
     *
     * @throws FormOptionsResolverException
     */
    protected function checkOptionsResolved()
    {
        if (!$this->isResolved()) {
            throw new FormOptionsResolverException('You have to resolve the options before');
        }
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Class Attribute
     *
     * @param ParameterBag $options
     * @param bool $error
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    protected function getClass(ParameterBag $options, bool $error = false): ?string
    {
        $this->checkOptionsResolved();

        $classAttribute    = null;
        $firstClassElement = true;

        $class       = $options->get(self::FORM_OPT_ATTR_CLASS);
        $class_error = $options->get(self::FORM_OPT_ATTR_CLASS_ERROR);

        if (!is_null($class)) {
            $classAttribute    = 'class="' . $class;
            $firstClassElement = false;
        }

        if (!is_null($class_error) && $error) {
            if ($firstClassElement) {
                $classAttribute = 'class="' . $class_error;
            } else {
                $classAttribute .= ' ' . $class_error;
            }
        }

        return is_null($classAttribute) ? $classAttribute : $classAttribute . '"';
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Label Class Attribute
     *
     * @param bool $error
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getLabelClass(bool $error = false): ?string
    {
        $this->checkOptionsResolved();

        $options     = $this->options;
        $attrOptions = new ParameterBag($options->get(self::FORM_OPT_LABEL_ATTR));

        return $this->getClass($attrOptions, $error);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Label Class Attribute
     *
     * @param bool $error
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getFieldClass(bool $error = false): ?string
    {
        $this->checkOptionsResolved();

        $options     = $this->options;
        $attrOptions = new ParameterBag($options->get(self::FORM_OPT_FIELD_ATTR));

        return $this->getClass($attrOptions, $error);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Error Class Attribute
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getErrorClass(): ?string
    {
        $this->checkOptionsResolved();

        $options     = $this->options;
        $attrOptions = new ParameterBag($options->get(self::FORM_OPT_ERROR_ATTR));

        return $this->getClass($attrOptions, false);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get attribute to define if the field is required
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getRequired(): ?string
    {
        $this->checkOptionsResolved();

        $options = $this->options;

        $required = $options->get(self::FORM_OPT_REQUIRED, true);
        return $required ? 'required' : null;
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get the name of the parent form
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getParentName(): ?string
    {
        $this->checkOptionsResolved();

        $options = $this->options;
        return $options->get(self::PARENT_FORM_NAME);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field name
     *
     * @return string
     * @throws FormOptionsResolverException
     */
    public function getFieldName(): string
    {
        $this->checkOptionsResolved();

        $options = $this->options;
        $name    = $options->get(self::FIELD_NAME);

        if ($options->has(self::FIELD_PREFIX)) {
            $prefix = $options->get(self::FIELD_PREFIX);
            $name   = "{$prefix}[{$name}]";
        }

        return trim($name);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field label
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getLabel(): ?string
    {
        $this->checkOptionsResolved();

        $options = $this->options;
        $label   = $options->get(self::FORM_OPT_LABEL);

        if (!is_null($label)) {
            $label = trim($label);
        }

        return $label;
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field placeholder
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getPlaceholder(): ?string
    {
        $this->checkOptionsResolved();

        $options     = $this->options;
        $attrOptions = new ParameterBag($options->get(self::FORM_OPT_FIELD_ATTR));
        $placeholder = $attrOptions->get(self::FORM_OPT_ATTR_PLACEHOLDER);

        if (!is_null($placeholder)) {
            $placeholder = trim($placeholder);
        }

        return $placeholder;
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field error message
     *
     * @return null|string
     * @throws FormOptionsResolverException
     */
    public function getErrorMessage(): ?string
    {
        $this->checkOptionsResolved();

        $options           = $this->options;
        $validationOptions = new ParameterBag($options->get(self::OPTION_VALIDATION));
        $errorMessage      = $validationOptions->get(self::VALIDATION_OPT_ERROR_MESSAGES);

        if (!is_null($errorMessage)) {
            $errorMessage = trim($errorMessage);
        }

        return $errorMessage;
    }
}
