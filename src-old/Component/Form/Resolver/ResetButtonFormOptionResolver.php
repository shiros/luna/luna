<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ResetButtonFormOptionResolver.php
 * @Created_at  : 11/10/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Exception\FormOptionsResolverException;

class ResetButtonFormOptionResolver extends ButtonFormOptionResolver
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    protected const TYPE = 'reset';
}
