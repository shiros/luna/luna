<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ListFormOptionResolver.php
 * @Created_at  : 03/09/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\DI\DependencyInjector;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormOptionsResolverException;

class ListFormOptionResolver extends FormOptionResolver
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    private const FORM_OPT_ITEMS    = 'items';
    private const FORM_OPT_SIZE     = 'size';
    public const  FORM_OPT_MULTIPLE = 'multiple';

    /** @var bool */
    private $multiple;

    /**
     * ListFormOptionResolver constructor.
     *
     * @param array $options
     * @param bool $multiple
     */
    public function __construct(array $options, bool $multiple = false)
    {
        parent::__construct($options);

        $this->multiple = $multiple;
    }




    # -------------------------------------------------------------
    #   Resolve Process
    # -------------------------------------------------------------

    /**
     * Resolve Options
     *
     * @return void
     * @throws DependencyInjectorException
     * @throws FormOptionsResolverException
     */
    public function resolve(): void
    {
        parent::resolve();
        $this->isResolved = false;

        $options = $this->options;

        if (!$this->options->has(self::FORM_OPT_ITEMS)) {
            $options->set(self::FORM_OPT_ITEMS, []);
        } else {
            $items = $options->get(self::FORM_OPT_ITEMS);

            if (is_callable($items)) {
                $items = DependencyInjector::callFunction($items);

                if (!is_array($items)) {
                    $items = [];
                }
            }

            $options->set(self::FORM_OPT_ITEMS, $items);
        }

        if (!$options->has(self::FORM_OPT_SIZE)) {
            $options->set(self::FORM_OPT_SIZE, false);
        }

        if (!$options->has(self::FORM_OPT_MULTIPLE)) {
            $options->set(self::FORM_OPT_MULTIPLE, $this->multiple);
        }

        $this->isResolved = true;
    }


    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Item List
     *
     * @return array
     * @throws FormOptionsResolverException
     */
    public function getItems(): array
    {
        $this->checkOptionsResolved();

        return $this->options->get(self::FORM_OPT_ITEMS, []);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Item List
     *
     * Return type :
     *  - int : Size Number
     *  - bool : False (If Size Option doesn't exist)
     *
     * @return int|bool
     * @throws FormOptionsResolverException
     */
    public function getSize()
    {
        $this->checkOptionsResolved();

        $size = $this->options->get(self::FORM_OPT_SIZE, false);

        if (!is_numeric($size) or !is_bool($size)) {
            $size = false;
        }

        return $size;
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Multiple Attributes
     *
     * @return bool
     * @throws FormOptionsResolverException
     */
    public function getMultiple(): bool
    {
        $this->checkOptionsResolved();

        return $this->options->get(self::FORM_OPT_MULTIPLE, false);
    }

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Check if key is selected
     *
     * @param $key
     * @param $value
     *
     * @return bool
     * @throws FormOptionsResolverException
     */
    public function isSelected($key, $value): bool
    {
        $this->checkOptionsResolved();

        $multiple = $this->getMultiple();

        if ($multiple && is_array($value)) {
            $value = new ParameterBag($value);

            return $value->has($key);
        }

        return $key == $value;
    }
}
