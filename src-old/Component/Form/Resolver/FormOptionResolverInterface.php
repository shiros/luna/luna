<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormOptionResolverInterface.php
 * @Created_at  : 21/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Bag\ParameterBag;

interface FormOptionResolverInterface
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    public const PARENT_FORM_NAME = 'ParentFormName';

    public const FIELD_PREFIX = 'FieldPrefix';
    public const FIELD_NAME   = 'FieldName';

    public const OPTION_VALIDATION = 'Validation';

    public const VALIDATION_OPT_ERROR_MESSAGES = 'error_messages';

    # -------------------------------------------------------------
    #   Resolve Process
    # -------------------------------------------------------------

    /**
     * Resolve Options
     *
     * @return void
     */
    public function resolve(): void;

    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Return options
     *
     * @return ParameterBag
     */
    public function getOptions(): ParameterBag;

    /**
     * Return form option, but if doesn't exist a default value is returned
     *
     * @param mixed $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = null);

    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set value in the options
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return FormOptionResolverInterface
     */
    public function set($key, $value): FormOptionResolverInterface;

    # -------------------------------------------------------------
    #   Render Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Add Surround to the render of tag
     *
     * @param string $html
     *
     * @return string
     */
    public function getSurround(string $html): string;

    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Label Class Attribute
     *
     * @param bool $error
     *
     * @return null|string
     */
    public function getLabelClass(bool $error = false): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Label Class Attribute
     *
     * @param bool $error
     *
     * @return null|string
     */
    public function getFieldClass(bool $error = false): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Error Class Attribute
     *
     * @return null|string
     */
    public function getErrorClass(): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get the name of the parent form
     *
     * @return null|string
     */
    public function getParentName(): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get attribute to define if the field is required
     *
     * @return string
     */
    public function getRequired(): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field name
     *
     * @return string
     */
    public function getFieldName(): string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field label
     *
     * @return null|string
     */
    public function getLabel(): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field placeholder
     *
     * @return null|string
     */
    public function getPlaceholder(): ?string;

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get field error message
     *
     * @return null|string
     */
    public function getErrorMessage(): ?string;
}
