<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : InputFormOptionResolver.php
 * @Created_at  : 03/09/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Resolver;

use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormOptionsResolverException;

class InputFormOptionResolver extends FormOptionResolver
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    private const FORM_OPT_TYPE = 'type';
    private const TYPE          = 'text';

    /** @var ?string */
    private $type;

    /**
     * ListFormOptionResolver constructor.
     *
     * @param array $options
     * @param bool $multiple
     */
    public function __construct(array $options, ?string $type = null)
    {
        parent::__construct($options);

        $this->type = $type;
    }


    # -------------------------------------------------------------
    #   Resolve Process
    # -------------------------------------------------------------

    /**
     * Resolve Options
     *
     * @return void
     * @throws DependencyInjectorException
     * @throws FormOptionsResolverException
     */
    public function resolve(): void
    {
        parent::resolve();
        $this->isResolved = false;

        $options = $this->options;

        if (!$options->has(self::FORM_OPT_TYPE)) {
            $options->set(self::FORM_OPT_TYPE, self::TYPE);
        }

        if (!is_null($this->type)) {
            $options->set(self::FORM_OPT_TYPE, $this->type);
        }

        $this->isResolved = true;
    }


    # -------------------------------------------------------------
    #   Treatment Functions
    # -------------------------------------------------------------

    /**
     * @IMPORTANT : Need to resolve options before use it
     *
     * Get Field Type
     *
     * @return string
     * @throws FormOptionsResolverException
     */
    public function getType(): string
    {
        $this->checkOptionsResolved();

        return $this->options->get(static::FORM_OPT_TYPE, static::TYPE);
    }
}
