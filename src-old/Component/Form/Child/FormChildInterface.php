<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormChildInterface.php
 * @Created_at  : 20/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Child;

use Form\Type\FormTypeInterface;
use Luna\Component\Bag\ParameterBag;

interface FormChildInterface
{
    # -------------------------------------------------------------
    #   Checks
    # -------------------------------------------------------------

    /**
     * Check if the child have children
     *
     * @return bool
     */
    public function hasChildren(): bool;


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Get the child's name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get the child's type
     *
     * @return FormTypeInterface
     */
    public function getType(): FormTypeInterface;

    /**
     * Get the child's data
     *
     * @return mixed
     */
    public function getData();

    /**
     * Get the child's options
     *
     * @return ParameterBag
     */
    public function getOptions(): ParameterBag;

    /**
     * Get the child's contents
     *
     * @return string
     */
    public function getContents(): string;

    /**
     * Get the child's children
     *
     * @return ParameterBag
     */
    public function getChildren(): ParameterBag;


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set options of child
     *
     * @param ParameterBag $options
     *
     * @return FormChildInterface
     */
    public function setOptions(ParameterBag $options): FormChildInterface;

    /**
     * Set data of child
     *
     * @param mixed $data
     *
     * @return FormChildInterface
     */
    public function setData($data): FormChildInterface;

    /**
     * Set Contents of child
     *
     * @param string $contents
     *
     * @return FormChildInterface
     */
    public function setContents(string $contents): FormChildInterface;
}
