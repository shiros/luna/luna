<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FormChild.php
 * @Created_at  : 20/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Form\Child;

use Form\Factory\FormFactory;
use Form\Type\FormTypeInterface;
use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\DependencyInjectorException;
use Luna\Component\Exception\FormException;
use Luna\Component\Utils\ClassDefinition\ClassManager;

class FormChild implements FormChildInterface
{
    /** @var string */
    protected $name;

    /** @var FormTypeInterface */
    protected $type;

    /** @var ParameterBag */
    protected $options;

    /** @var mixed */
    protected $data;

    /** @var ?string */
    protected $contents;

    /** @var ParameterBag */
    protected $children;

    /**
     * FormChild constructor.
     *
     * @param string $name
     * @param string $type
     * @param ParameterBag $options
     */
    public function __construct(string $name, string $type, ParameterBag $options)
    {
        $this->name     = $name;
        $this->options  = $options;
        $this->data     = null;
        $this->contents = null;

        // Create Type
        $this->type = FormFactory::createType($type, $this->data, $this->options->all());

        // Get Children
        $this->children = $this->type->getChildren();
    }

    # -------------------------------------------------------------
    #   Checks
    # -------------------------------------------------------------

    /**
     * Check if the child have children
     *
     * @return bool
     */
    public function hasChildren(): bool
    {
        return !$this->children->isEmpty();
    }


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Get the child's name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the child's type
     *
     * @return FormTypeInterface
     */
    public function getType(): FormTypeInterface
    {
        return $this->type;
    }

    /**
     * Get the child's data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the child's options
     *
     * @return ParameterBag
     */
    public function getOptions(): ParameterBag
    {
        return $this->options;
    }

    /**
     * Get the child's contents
     *
     * @return string
     */
    public function getContents(): string
    {
        // Get Option Resolver
        $optionResolver = $this->type->getOptionResolver();

        // Check if contents is null
        if (is_null($this->contents)) {
            // Get View
            $view = $this->type->getView();

            // Get Contents
            $this->contents = $view->getBody();
        }

        // Get Tag
        return $optionResolver->getSurround($this->contents);
    }

    /**
     * Get the child's children
     *
     * @return ParameterBag
     */
    public function getChildren(): ParameterBag
    {
        return $this->children;
    }


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * Set options of child
     *
     * @param ParameterBag $options
     *
     * @return FormChildInterface
     */
    public function setOptions(ParameterBag $options): FormChildInterface
    {
        $this->options = $options;
        $this->type->setOptions($this->options->all());

        return $this;
    }

    /**
     * Set data of child
     *
     * @param mixed $data
     *
     * @return FormChildInterface
     */
    public function setData($data): FormChildInterface
    {
        $this->data = $data;
        $this->type->setData($this->data);

        return $this;
    }

    /**
     * Set Contents of child
     *
     * @param string $contents
     *
     * @return FormChildInterface
     */
    public function setContents(string $contents): FormChildInterface
    {
        $this->contents = $contents;

        return $this;
    }
}
