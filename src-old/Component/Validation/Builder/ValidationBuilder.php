<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ValidationBuilder.php
 * @Created_at  : 07/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Builder;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\ValidationException;
use Luna\Component\Utils\ClassDefinition\ClassManager;
use Validation\Type\ValidationType;
use Validation\ValidationComponent;

class ValidationBuilder implements ValidationBuilderInterface
{
    /**
     * @var ParameterBag
     */
    protected $checkList;

    /**
     * Validation constructor.
     */
    public function __construct()
    {
        $this->checkList = new ParameterBag();
    }


    # -------------------------------------------------------------
    #   Validation Rule Method
    # -------------------------------------------------------------

    /**
     * Add Validation Rule
     *
     * @param string $name
     * @param string $type
     * @param array $options
     *
     * @return ValidationBuilder
     * @throws ValidationException
     */
    public function add(string $name, string $type, array $options = []): ValidationBuilder
    {
        if (!ClassManager::implement(ValidationType::class, $type)) {
            throw new ValidationException("$type doesn't implement " . ValidationType::class);
        }

        $validationComponent = new ValidationComponent(new $type(), $options);
        $this->checkList->set($name, $validationComponent);

        return $this;
    }


    # -------------------------------------------------------------
    #   Getter
    # -------------------------------------------------------------

    /**
     * Return Check List
     *
     * @return ParameterBag
     */
    public function getCheckList(): ParameterBag
    {
        return $this->checkList;
    }
}
