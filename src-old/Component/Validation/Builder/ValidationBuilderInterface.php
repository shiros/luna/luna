<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ValidationBuilderInterface.php
 * @Created_at  : 07/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Builder;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\ValidationException;
use Luna\Component\Utils\ClassDefinition\ClassManager;

interface ValidationBuilderInterface
{
    # -------------------------------------------------------------
    #   Validation Rule Method
    # -------------------------------------------------------------

    /**
     * Add Validation Rule
     *
     * @param string $name
     * @param string $type
     * @param array $options
     *
     * @return ValidationBuilder
     * @throws ValidationException
     */
    public function add(string $name, string $type, array $options = []): ValidationBuilder;


    # -------------------------------------------------------------
    #   Getter
    # -------------------------------------------------------------

    /**
     * Return Check List
     *
     * @return ParameterBag
     */
    public function getCheckList(): ParameterBag;
}
