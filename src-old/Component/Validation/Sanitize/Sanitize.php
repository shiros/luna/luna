<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Sanitize.php
 * @Created_at  : 08/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Sanitize;

class Sanitize
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    public const SANITIZE           = 0;
    public const SANITIZE_SEARCH    = 1;
    public const SANITIZE_COMMENTS  = 2;
    public const SANITIZE_CHARACTER = 3;
    public const SANITIZE_URL       = 4;


    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var string */
    protected $sanitizeMethod;

    /** @var bool */
    protected $sanitizeCode;

    /** @var string */
    protected $type;

    /** @var array $prohibitedCharacter */
    protected $prohibitedCharacters;

    /**
     * Sanitize constructor.
     */
    public function __construct()
    {
        $this->setSanitizeType();
        $this->setSanitizeMethod();
        $this->setProhibitedCharacters();
    }


    # -------------------------------------------------------------
    #   Setters
    # -------------------------------------------------------------

    /**
     * @param int $sanitizeType
     */
    public function setSanitizeType(int $sanitizeType = FILTER_SANITIZE_STRING)
    {
        $this->type = $sanitizeType;
    }

    /**
     * @param array $prohibitedCharacters
     */
    public function setProhibitedCharacters(array $prohibitedCharacters = [])
    {
        $this->prohibitedCharacters = $prohibitedCharacters;
    }

    /**
     * @param int $sanitizeMethod
     */
    public function setSanitizeMethod(int $sanitizeMethod = self::SANITIZE)
    {
        $this->sanitizeCode = $sanitizeMethod;

        switch ($this->sanitizeCode) {
            case self::SANITIZE:
                $this->sanitizeMethod = 'sanitizeField';
                break;

            case self::SANITIZE_SEARCH:
                $this->sanitizeMethod = 'sanitizeSearch';
                break;

            case self::SANITIZE_COMMENTS:
                $this->sanitizeMethod = 'sanitizeComments';
                break;

            case self::SANITIZE_CHARACTER:
                $this->sanitizeMethod = 'sanitizeCharacter';
                break;
                break;

            case self::SANITIZE_URL:
                $this->sanitizeMethod = 'sanitizeUrl';
                break;

            default:
                $this->sanitizeMethod = 'sanitizeField';
                break;
        }
    }


    # -------------------------------------------------------------
    #   Sanitize Method
    # -------------------------------------------------------------

    /**
     * Prepare & Satinize Field
     *
     * @param $field
     *
     * @return mixed
     */
    public function sanitize($field)
    {
        $sanitizeMethod = $this->sanitizeMethod;

        if (is_array($field)) {
            foreach ($field as $key => $value) {
                $field[$key] = $this->$sanitizeMethod($value);
            }
        } else {
            $field = $this->$sanitizeMethod($field);
        }

        return $field;
    }


    # -------------------------------------------------------------
    #   Configuration
    # -------------------------------------------------------------

    /**
     * Apply Filter to the field
     *
     * @param string $field
     *
     * @return string
     */
    protected function sanitizeField(string $field): string
    {
        return filter_var($field, $this->type);
    }

    /**
     * Apply Filter to Search Field
     *
     * @param string $field
     *
     * @return string
     */
    protected function sanitizeSearch(string $field): string
    {
        $this->type = FILTER_SANITIZE_FULL_SPECIAL_CHARS;
        return $this->sanitizeField($field);
    }

    /**
     * Apply Filter to Comments Field
     *
     * @param string $field
     *
     * @return string
     */
    protected function sanitizeComments(string $field): string
    {
        /* -- Suppression des Balises Js -- */
        $field = preg_replace('@<script[^>]*?>.*?</script>@si', '', $field);


        /* -- Suppression des Balises Css -- */
        $field = preg_replace('@<link [^>]*?>@si', '', $field);
        $field = preg_replace('@<style[^>]*?>.*?</style>@si', '', $field);

        /* -- Nettoyage des Signatures -- */
        $field = preg_replace('@<p class="signature">.*?</p>@si', '', $field);

        return $field;
    }

    /**
     * Apply Filter (Special characters) to the Field
     *
     * @param string $field
     *
     * @return string
     */
    protected function sanitizeCharacter(string $field): string
    {
        foreach ($this->prohibitedCharacters as $character) {
            switch ($character) {
                case '/':
                    $field = str_replace($character, ':', $field);
                    break;

                case '.':
                    $field = str_replace($character, '', $field);
                    break;

                default:
                    $field = str_replace($character, '-', $field);
                    break;
            }
        }

        return $this->sanitizeField($field);
    }

    /**
     * Apply Filter (Url Encode) to the Field
     *
     * @param string $field
     *
     * @return string
     */
    protected function sanitizeUrl(string $field): string
    {
        $field = urlencode($field);
        return $this->sanitizeField($field);
    }
}
