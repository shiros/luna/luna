<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Validation.php
 * @Created_at  : 24/11/2016
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\BindException;
use Luna\Component\Utils\Object\ObjectManager;
use Validation\Sanitize\Sanitize;
use Validation\Type\PhoneType;
use Validation\Type\StringType;
use Validation\Type\UrlType;
use Validation\Type\ValidationType;

class ValidationProcess
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    protected const TYPE_BAG    = 'Bag';
    protected const TYPE_OBJECT = 'Object';

    // ----------------
    // Messages

    // TODO : See how to make messages editable

    protected const ERROR_MESSAGE_EMPTY    = "Le champ doit être renseigné";
    protected const ERROR_MESSAGE_TYPE     = "Le champ ne correspond pas au type voulu";
    protected const ERROR_MESSAGE_EQUAL_TO = "Le champ ne correspond pas avec les valeurs attendues";


    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var string */
    protected $objectType;

    /** @var ParameterBag */
    protected $checkList;

    /** @var mixed */
    protected $data;

    /** @var ParameterBag */
    protected $values;

    /** @var ParameterBag */
    protected $rawValues;

    /** @var ParameterBag */
    protected $errors;

    /** @var bool */
    protected $valid;

    /**
     * Types to use with 'trim()' function
     *
     * @var array
     */
    protected $typeToTrim = [
        StringType::class,
        UrlType::class,
        PhoneType::class
    ];

    /**
     * ValidationProcess constructor.
     *
     * @param ParameterBag $checkList
     */
    public function __construct(ParameterBag $checkList)
    {
        $this->checkList = $checkList;

        $this->errors    = new ParameterBag();
        $this->rawValues = new ParameterBag();
        $this->valid     = true;
    }


    # -------------------------------------------------------------
    #   Access & Prepare Data
    # -------------------------------------------------------------

    /**
     * Prepare data
     * - Transform array in ParameterBag
     *
     * @param $data
     */
    protected function prepareData($data)
    {
        if (is_array($data)) {
            $this->objectType = self::TYPE_BAG;

            $data         = new ParameterBag($data);
            $this->values = new ParameterBag();
        } elseif (is_object($data)) {
            $this->objectType = self::TYPE_OBJECT;
        }

        $this->data = $data;
    }

    /**
     * Check if key exist in data
     *
     * @param mixed $key
     * @param $value
     *
     * @return void
     * @throws BindException
     */
    protected function setDataValue($key, $value): void
    {
        switch ($this->objectType) {
            case self::TYPE_BAG:
                $this->values->set($key, $value);
                break;

            case self::TYPE_OBJECT && is_string($key):
                ObjectManager::bindValue($this->data, $key, $value);
                break;

            default:
                break;
        }
    }

    /**
     * Check if key exist in data
     *
     * @param mixed $key
     *
     * @return mixed
     */
    protected function hasDataValue($key)
    {
        switch ($this->objectType) {
            case self::TYPE_BAG:
                return $this->data->has($key);
            case self::TYPE_OBJECT && is_string($key):
                return ObjectManager::hasAttribute($this->data, $key);
            default:
                return false;
        }
    }

    /**
     * Get value for key in data
     *
     * @param mixed $key
     * @param null $default
     *
     * @return mixed
     */
    protected function getDataValue($key, $default = null)
    {
        switch ($this->objectType) {
            case self::TYPE_BAG:
                return $this->data->get($key);
            case self::TYPE_OBJECT && is_string($key):
                return ObjectManager::getValue($this->data, $key, $default);
            default:
                return $default;
        }
    }

    /**
     * Prepare Value
     * - Trim Processing
     *
     * @param ValidationType $type
     * @param $value
     *
     * @return string
     */
    protected function prepareValue(ValidationType $type, $value): string
    {
        if (in_array(get_class($type), $this->typeToTrim, true)) {
            $value = trim($value);
        }

        return $value;
    }


    # -------------------------------------------------------------
    #   Validate
    # -------------------------------------------------------------

    /**
     * This function try to validate and sanitize datas
     *
     * @param mixed $data
     *
     * @return void
     * @throws BindException
     */
    public function validate($data): void
    {
        $this->prepareData($data);

        /**
         * @var $key
         * @var ValidationComponent $component
         */
        foreach ($this->checkList as $key => $component) {
            // Get Params
            $type     = $component->getType();
            $required = $component->getOptionRequired();

            // Get Field Value
            $value = $this->getDataValue($key);
            $value = $this->prepareValue($type, $value);

            // Process & Check
            $this->requiredProcess($required, $key, $value);

            if ($required or !is_null($value)) {
                $message = $component->getOptionMessage();
                $equalTo = $component->getOptionSanitizeEqualTo();

                $this->equalToProcess($equalTo, $key, $value);
                $this->validateProcess($type, $key, $value, $message);
            }

            // Set Raw Value
            $this->rawValues->set($key, $value);

            // Sanitize Process
            $value = $this->sanitizeProcess($component, $key, $value);
            $this->setDataValue($key, $value);
        }
    }


    # -------------------------------------------------------------
    #   Process
    # -------------------------------------------------------------

    /**
     * Test if the field is required
     *
     * @param bool $required
     * @param $key
     * @param $value
     *
     * @return void
     */
    protected function requiredProcess(bool $required, $key, $value): void
    {
        if ($required) {
            if (!$this->hasDataValue($key) || $this->isEmpty($value)) {
                $this->errors->set($key, self::ERROR_MESSAGE_EMPTY);
                $this->valid = false;
            }
        }
    }

    /**
     * Check the value of the field
     *
     * @param null|array $equalTo
     * @param $key
     * @param $value
     *
     * @return void
     */
    protected function equalToProcess(?array $equalTo, $key, $value): void
    {
        if (is_array($equalTo)) {
            if ($this->isNotEqual($value, $equalTo)) {
                $this->errors->set($key, self::ERROR_MESSAGE_EQUAL_TO);
                $this->valid = false;
            }
        }
    }

    /**
     * Check the type
     *
     * @param ValidationType $type
     * @param $key
     * @param $value
     * @param string $message
     *
     * @return void
     */
    protected function validateProcess(ValidationType $type, $key, $value, string $message): void
    {
        if (!$type->validate($value)) {
            $this->errors[$key] = ((!empty($message)) ? $message : self::ERROR_MESSAGE_TYPE);
            $this->valid        = false;
        }
    }

    /**
     * Sanitize
     *
     * @param ValidationComponent $component
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    protected function sanitizeProcess(ValidationComponent $component, $key, $value)
    {
        if ($component->hasOptionSanitize()) {
            $sanitizeType         = $component->getOptionSanitizeType();
            $sanitizeMethod       = $component->getOptionSanitizeMethod();
            $prohibitedCharacters = $component->getOptionSanitizeProhibitedCharacters();

            $SanitizeModule = new Sanitize();
            $SanitizeModule->setSanitizeType($sanitizeType);
            $SanitizeModule->setSanitizeMethod($sanitizeMethod);
            $SanitizeModule->setProhibitedCharacters($prohibitedCharacters);

            $value = $SanitizeModule->sanitize($value);
        }

        return $value;
    }


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Return Errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors->all();
    }

    /**
     * Return Sanitize Values
     *
     * @return mixed
     */
    public function getValues()
    {
        switch ($this->objectType) {
            case self::TYPE_BAG:
                return $this->values->all();
            case self::TYPE_OBJECT:
                return $this->data;
            default:
                return null;
        }
    }

    /**
     * Return Raw Values
     *
     * @return array
     */
    public function getRawValues(): array
    {
        return $this->rawValues->all();
    }

    /**
     * Return Sanitize Values
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }



    # -------------------------------------------------------------
    #   Check
    # -------------------------------------------------------------

    /**
     * Check if the field is empty
     *
     * @param $field
     *
     * @return bool
     */
    protected function isEmpty($field): bool
    {
        if ($field === '0') {
            return false;
        }

        return empty($field);
    }

    /**
     * Check if the field is not equal to an entry or list of entry
     *
     * @param string $field
     * @param array $equalTo
     *
     * @return bool
     */
    protected function isNotEqual(string $field, array $equalTo): bool
    {
        return !in_array($field, $equalTo, true);
    }
}
