<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : NumericType.php
 * @Created_at  : 08/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Type;

class PhoneType implements ValidationType
{
    /**
     * Regex
     *
     * @var string
     */
    protected const REGEX_PHONE = "#^((\d{2}?){4}\d{2})$#";

    /**
     * Check if the $field is the defined type
     */
    public function validate($field)
    {
        return preg_match(self::REGEX_PHONE, $field);
    }
}
