<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : IntType.php
 * @Created_at  : 08/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Type;

use function Luna\Component\Validation\Type\ctype_digit;

class IntType implements ValidationType
{
    /**
     * Check if the $field is the defined type
     */
    public function validate($field)
    {
        return ctype_digit($field);
    }
}
