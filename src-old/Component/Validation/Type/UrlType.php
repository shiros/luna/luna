<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : StringType.php
 * @Created_at  : 10/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Type;

class UrlType implements ValidationType
{
    /**
     * Regex
     *
     * @var string
     */
    protected const REGEX_URL = "#(https?|ftp|ssh|mailto):\/\/[a-z0-9\/:%_+.,\#?!@&=-]+#i";

    /**
     * Check if the $field is the defined type
     */
    public function validate($field)
    {
        return preg_match(self::REGEX_URL, $field);
    }
}
