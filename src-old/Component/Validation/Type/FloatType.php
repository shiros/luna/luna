<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : FloatType.php
 * @Created_at  : 08/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Type;

class FloatType implements ValidationType
{
    /**
     * Check if the $field is the defined type
     */
    public function validate($field)
    {
        return (is_numeric($field)) ? is_float(floatval($field)) : false;
    }
}
