<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : BooleanType.php
 * @Created_at  : 08/12/2017
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation\Type;

class BooleanType implements ValidationType
{
    /**
     * Check if the $field is the defined type
     */
    public function validate($field)
    {
        if (!is_bool($field)) {
            $field = strtolower($field);
            return ($field == "false" || $field == "true");
        } else {
            return true;
        }
    }
}
