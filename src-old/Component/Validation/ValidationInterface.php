<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ValidationInterface.php
 * @Created_at  : 16/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation;

use Validation\Builder\ValidationBuilderInterface;

interface ValidationInterface
{
    # -------------------------------------------------------------
    #   Process
    # -------------------------------------------------------------

    /**
     * Validation Builder Function
     * Allow to add different check for the fields
     *
     * @param ValidationBuilderInterface $builder
     */
    public function buildValidation(ValidationBuilderInterface $builder);


    /**
     * This function try to validate and sanitize fields
     *
     * @param $fields
     */
    public function validate($fields);


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * Return Errors
     *
     * @return array
     */
    public function getErrors(): array;

    /**
     * Return Sanitize Values
     *
     * @return array
     */
    public function getValues(): array;

    /**
     * Return Raw Values
     *
     * @return array
     */
    public function getRawValues(): array;

    /**
     * Return Sanitize Values
     *
     * @return bool
     */
    public function isValid(): bool;
}
