<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : Validation.php
 * @Created_at  : 24/11/2016
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation;

use Luna\Component\Bag\ParameterBag;
use Luna\Component\Exception\BindException;
use Luna\Component\Exception\ConfigException;
use Luna\Config\Config;
use Validation\Builder\ValidationBuilder;
use Validation\Type\PhoneType;
use Validation\Type\StringType;
use Validation\Type\UrlType;

abstract class Validation implements ValidationInterface
{
    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var Config */
    protected $ConfigModule;

    /** @var ValidationProcess */
    protected $ProcessModule;

    /** @var ValidationBuilder */
    protected $BuilderModule;

    /** @var ParameterBag */
    protected $checkList;

    /**
     * Types to use with 'trim()' function
     *
     * @var array
     */
    protected $typeToTrim = [
        StringType::class,
        UrlType::class,
        PhoneType::class
    ];

    /**
     * Validation constructor.
     *
     * @throws ConfigException
     */
    public function __construct()
    {
        $this->BuilderModule = new ValidationBuilder();
        $this->ConfigModule  = Config::getInstance();

        $this->buildValidation($this->BuilderModule);

        $this->checkList     = $this->BuilderModule->getCheckList();
        $this->ProcessModule = new ValidationProcess($this->checkList);
    }


    # -------------------------------------------------------------
    #   Process
    # -------------------------------------------------------------


    /**
     * This function try to validate forms, and sanitize field
     *
     * @param mixed $fields
     *
     * @return void
     * @throws BindException
     */
    public function validate($fields): void
    {
        $this->ProcessModule->validate($fields);
    }
}
