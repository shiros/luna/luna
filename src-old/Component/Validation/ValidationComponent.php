<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2023
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : ValidationComponent.php
 * @Created_at  : 19/07/2018
 * @Update_at   : 01/06/2020
 * --------------------------------------------------------------------------
 */

namespace Validation;

use Luna\Component\Bag\ParameterBag;
use Validation\Type\ValidationType;

class ValidationComponent
{
    # -------------------------------------------------------------
    #   Constants
    # -------------------------------------------------------------

    protected const PARAM_TYPE     = 'Type';
    protected const PARAM_MESSAGE  = 'Message';
    protected const PARAM_REQUIRED = 'Required';

    protected const PARAM_SANITIZE                       = 'Sanitize';
    protected const PARAM_SANITIZE_TYPE                  = 'SanitizeType';
    protected const PARAM_SANITIZE_METHOD                = 'SanitizeMethod';
    protected const PARAM_SANITIZE_EQUAL_TO              = 'SanitizeEqualTo';
    protected const PARAM_SANITIZE_PROHIBITED_CHARACTERS = 'SanitizeProhibitedCharacters';

    # -------------------------------------------------------------
    #   Vars
    # -------------------------------------------------------------

    /** @var ValidationType */
    protected $type;

    /** @var ParameterBag */
    protected $options;

    /**
     * ValidationComponent constructor.
     *
     * @param ValidationType $type
     * @param array $options
     */
    public function __construct(ValidationType $type, array $options = [])
    {
        $this->type    = $type;
        $this->options = new ParameterBag($options);
    }


    /**
     * @param string $name
     * @param null $default
     *
     * @return mixed
     */
    protected function getSanitizeOptions(string $name, $default = null)
    {
        if ($this->hasOptionSanitize()) {
            $sanitizeOptions = $this->options->get(self::PARAM_SANITIZE);
            $sanitizeOptions = (is_array($sanitizeOptions)) ? new ParameterBag($sanitizeOptions) : new ParameterBag();

            return $sanitizeOptions->get($name, $default);
        }

        return $default;
    }


    # -------------------------------------------------------------
    #   Getters
    # -------------------------------------------------------------

    /**
     * @return ValidationType
     */
    public function getType(): ValidationType
    {
        return $this->type;
    }

    /**
     * @return null|string
     */
    public function getOptionMessage(): ?string
    {
        return $this->options->get(self::PARAM_MESSAGE);
    }

    /**
     * @return bool
     */
    public function getOptionRequired(): bool
    {
        return $this->options->get(self::PARAM_REQUIRED, true);
    }

    /**
     * @return bool
     */
    public function hasOptionSanitize(): bool
    {
        return $this->options->has(self::PARAM_SANITIZE);
    }

    /**
     * @return null|string
     */
    public function getOptionSanitizeType(): ?string
    {
        return $this->getSanitizeOptions(self::PARAM_SANITIZE_TYPE);
    }

    /**
     * @return null|string
     */
    public function getOptionSanitizeMethod(): ?string
    {
        return $this->getSanitizeOptions(self::PARAM_SANITIZE_METHOD);
    }

    /**
     * @return mixed
     */
    public function getOptionSanitizeEqualTo()
    {
        return $this->getSanitizeOptions(self::PARAM_SANITIZE_EQUAL_TO);
    }

    /**
     * @return null|array
     */
    public function getOptionSanitizeProhibitedCharacters(): ?array
    {
        return $this->getSanitizeOptions(self::PARAM_SANITIZE_PROHIBITED_CHARACTERS, []);
    }
}
