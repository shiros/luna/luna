<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : services.php
 * @Created_at  : 23/07/2024
 * @Update_at   : 23/07/2024
 * ----------------------------------------------------------------
 */

/**
 * Services configuration.
 */
return [
    # --------------------------------
    # General

    'key_1' => 'service_value',

    # --------------------------------
    # Authentication

    'Authentication' => [
        'key_1' => 'authentication_value'
    ],
];
