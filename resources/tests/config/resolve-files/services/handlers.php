<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : handlers.php
 * @Created_at  : 23/07/2024
 * @Update_at   : 23/07/2024
 * ----------------------------------------------------------------
 */

/**
 * Handler configuration.
 */
return [
    'key_1' => 'handler_value'
];
