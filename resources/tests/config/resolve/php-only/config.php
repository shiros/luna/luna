<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : config.php
 * @Created_at  : 11/11/2018
 * @Update_at   : 09/01/2023
 * ----------------------------------------------------------------
 */

/**
 * General configuration.
 */
return [
    'key_1' => 'value_of_key_1',
    'key_2' => [
        'element_1' => 'value_of_element_1',
        'element_2' => 'value_of_element_2',
    ]
];
