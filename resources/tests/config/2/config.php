<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : config.php
 * @Created_at  : 23/07/2024
 * @Update_at   : 23/07/2024
 * ----------------------------------------------------------------
 */

/**
 * General configuration.
 */
return [
    'key_1' => 'general_value',
    'key_2' => [
        'element_1' => 'value_of_element_1',
        'element_2' => 'value_of_element_2',
    ]
];
