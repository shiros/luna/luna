<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : env.php
 * @Created_at  : 24/07/2024
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

/**
 * Environment parameters.
 *
 * Allow to configure the kernel system.
 */
return [
    /**
     * Environment name.
     *
     * Available values :
     *  - Production       => 'prod'
     *  - Pre-Production   => 'pre-prod'
     *  - Development      => 'dev'
     *  - Test             => 'test'
     */
    'Environment' => 'test',

    # --------------------------------
    # Parameters

    /**
     * The parameters format looks like :
     *  - 'Key' => 'SecretValue'
     */

    'Key1' => 'Value1',
    'Key2' => 'Value2'
];
