<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigReaderTest.php
 * @Created_at  : 23/07/2024
 * @Update_at   : 23/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Config\Resolver;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Bag\Bag;
use Luna\Component\Test\LunaTestCase;
use Luna\Config\Reader\ConfigReader;
use Luna\Config\Resolver\ConfigResolver;
use Luna\Exception\LunaException;
use ReflectionException;

class ConfigResolverTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Configuration reader.
     */
    protected ConfigReader $reader;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Set attributes
        $this->reader = new ConfigReader();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Tests
        $this->assertNotNull($resolver);
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Resolve files.
     *
     * Check if 'resolveFiles' method return a bag with settings.
     */
    public function test_resolveFiles(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path  = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve-files');
        $files = [
            'config',
            'Routing'         => 'routing',
            'Service'         => 'services/services',
            'Service.Handler' => 'services/handlers',
        ];

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFiles(
            path : $path,
            files: $files
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('Routing', $settings);
        $this->assertArrayHasKey('key_1', $settings->get('Routing'));
        $this->assertArrayHasKey('Service', $settings);
        $this->assertArrayHasKey('key_1', $settings->get('Service'));
        $this->assertArrayHasKey('Authentication', $settings->get('Service'));
        $this->assertArrayHasKey('key_1', $settings->get('Service.Authentication'));
        $this->assertArrayHasKey('Handler', $settings->get('Service'));

        // Tests - Data
        $this->assertEquals('general_value', $settings->get('key_1'));
        $this->assertEquals('routing_value', $settings->get('Routing.key_1'));
        $this->assertEquals('service_value', $settings->get('Service.key_1'));
        $this->assertEquals('authentication_value', $settings->get('Service.Authentication.key_1'));
        $this->assertEquals('handler_value', $settings->get('Service.Handler.key_1'));
    }

    /**
     * Test : Resolve files - Files as bag.
     *
     * Check if 'resolveFiles' method return a bag with settings.
     */
    public function test_resolveFiles_filesAsBag(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path  = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve-files');
        $files = new Bag([
            'config',
            'Routing'         => 'routing',
            'Service'         => 'services/services',
            'Service.Handler' => 'services/handlers',
        ]);

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFiles(
            path : $path,
            files: $files
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('Routing', $settings);
        $this->assertArrayHasKey('key_1', $settings->get('Routing'));
        $this->assertArrayHasKey('Service', $settings);
        $this->assertArrayHasKey('key_1', $settings->get('Service'));
        $this->assertArrayHasKey('Authentication', $settings->get('Service'));
        $this->assertArrayHasKey('key_1', $settings->get('Service.Authentication'));
        $this->assertArrayHasKey('Handler', $settings->get('Service'));

        // Tests - Data
        $this->assertEquals('general_value', $settings->get('key_1'));
        $this->assertEquals('routing_value', $settings->get('Routing.key_1'));
        $this->assertEquals('service_value', $settings->get('Service.key_1'));
        $this->assertEquals('authentication_value', $settings->get('Service.Authentication.key_1'));
        $this->assertEquals('handler_value', $settings->get('Service.Handler.key_1'));
    }

    /**
     * Test : Resolve files - Wrong path.
     *
     *  Check if 'resolveFiles' method return an empty bag.
     */
    public function test_resolveFiles_wrongPath(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path  = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve-files-wrong');
        $files = [
            'config',
            'Routing'         => 'routing',
            'Service'         => 'services/services',
            'Service.Handler' => 'services/handlers',
        ];

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFiles(
            path : $path,
            files: $files
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }

    /**
     * Test : Resolve file - PHP file.(Only)
     *
     * Check if 'resolveFile' method return a PHP file.
     * Existing files in path :
     *  - PHP
     */
    public function test_resolveFile_phpOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/php-only/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('key_2', $settings);
        $this->assertArrayHasKey('element_1', $settings->get('key_2'));
        $this->assertArrayHasKey('element_2', $settings->get('key_2'));
        $this->assertEquals('value_of_key_1', $settings->get('key_1'));
        $this->assertEquals('value_of_element_1', $settings->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $settings->get('key_2.element_2'));
    }

    /**
     * Test : Resolve file - JSON file.(Only)
     *
     * Check if 'resolveFile' method return a JSON file.
     * Existing files in path :
     *  - JSON
     */
    public function test_resolveFile_jsonOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/json-only/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('key_2', $settings);
        $this->assertArrayHasKey('element_1', $settings->get('key_2'));
        $this->assertArrayHasKey('element_2', $settings->get('key_2'));
        $this->assertEquals('value_of_key_1', $settings->get('key_1'));
        $this->assertEquals('value_of_element_1', $settings->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $settings->get('key_2.element_2'));
    }

    /**
     * Test : Resolve file - YAML file. (Only)
     *
     * Check if 'resolveFile' method return a JSON file.
     * Existing files in path :
     *  - YAML
     */
    public function test_resolveFile_yamlOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/yaml-only/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('key_2', $settings);
        $this->assertArrayHasKey('element_1', $settings->get('key_2'));
        $this->assertArrayHasKey('element_2', $settings->get('key_2'));
        $this->assertEquals('value_of_key_1', $settings->get('key_1'));
        $this->assertEquals('value_of_element_1', $settings->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $settings->get('key_2.element_2'));
    }

    /**
     * Test : Resolve file - PHP file.
     *
     * Check if 'resolveFile' method return a PHP file.
     * Existing files in path :
     *  - PHP
     *  - JSON
     *  - YAML
     */
    public function test_resolveFile_php(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/php/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('key_2', $settings);
        $this->assertArrayHasKey('element_1', $settings->get('key_2'));
        $this->assertArrayHasKey('element_2', $settings->get('key_2'));
        $this->assertEquals('value_of_key_1', $settings->get('key_1'));
        $this->assertEquals('value_of_element_1', $settings->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $settings->get('key_2.element_2'));
    }

    /**
     * Test : Resolve file - JSON file.
     *
     * Check if 'resolveFile' method return a JSON file.
     * Existing files in path :
     *  - JSON
     *  - YAML
     */
    public function test_resolveFile_json(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/json/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('key_1', $settings);
        $this->assertArrayHasKey('key_2', $settings);
        $this->assertArrayHasKey('element_1', $settings->get('key_2'));
        $this->assertArrayHasKey('element_2', $settings->get('key_2'));
        $this->assertEquals('value_of_key_1', $settings->get('key_1'));
        $this->assertEquals('value_of_element_1', $settings->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $settings->get('key_2.element_2'));
    }

    /**
     * Test : Resolve file - Wrong file.
     *
     * Check if 'resolveFile' method return null.
     */
    public function test_resolveFile_wrongFile(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/txt/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }

    /**
     * Test : Resolve file - No file.
     *
     * Check if 'resolveFile' method return null.
     */
    public function test_resolveFile_noFile(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/config');

        // ----------------
        // Process

        // Resolve file
        $settings = $resolver->resolveFile(
            path: $path
        );

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }

    /**
     * Test : Resolve path - PHP file.(Only)
     *
     * Check if 'resolvePath' method return a PHP file.
     * Existing files in path :
     *  - PHP
     */
    public function test_resolvePath_phpOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/php-only/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNotNull($realPath);
        $this->assertIsString($realPath);
        $this->assertMatchesRegularExpression('/\.php$/', $realPath);
    }

    /**
     * Test : Resolve path - JSON file.(Only)
     *
     * Check if 'resolvePath' method return a JSON file.
     * Existing files in path :
     *  - JSON
     */
    public function test_resolvePath_jsonOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/json-only/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNotNull($realPath);
        $this->assertIsString($realPath);
        $this->assertMatchesRegularExpression('/\.json$/', $realPath);
    }

    /**
     * Test : Resolve path - YAML file. (Only)
     *
     * Check if 'resolvePath' method return a JSON file.
     * Existing files in path :
     *  - YAML
     */
    public function test_resolvePath_yamlOnly(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/yaml-only/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNotNull($realPath);
        $this->assertIsString($realPath);
        $this->assertMatchesRegularExpression('/\.yaml$/', $realPath);
    }

    /**
     * Test : Resolve path - PHP file.
     *
     * Check if 'resolvePath' method return a PHP file.
     * Existing files in path :
     *  - PHP
     *  - JSON
     *  - YAML
     */
    public function test_resolvePath_php(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/php/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNotNull($realPath);
        $this->assertIsString($realPath);
        $this->assertMatchesRegularExpression('/\.php$/', $realPath);
    }

    /**
     * Test : Resolve path - JSON file.
     *
     * Check if 'resolvePath' method return a JSON file.
     * Existing files in path :
     *  - JSON
     *  - YAML
     */
    public function test_resolvePath_json(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/json/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNotNull($realPath);
        $this->assertIsString($realPath);
        $this->assertMatchesRegularExpression('/\.json$/', $realPath);
    }

    /**
     * Test : Resolve path - Wrong file.
     *
     * Check if 'resolvePath' method return null.
     */
    public function test_resolvePath_wrongFile(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/txt/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNull($realPath);
    }

    /**
     * Test : Resolve path - No file.
     *
     * Check if 'resolvePath' method return null.
     */
    public function test_resolvePath_noFile(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // Path
        $path = $this->computePath(LUNA_ROOT . '/resources/tests/config/resolve/config');

        // ----------------
        // Process

        // Resolve path
        $realPath = $resolver->resolvePath(
            path: $path
        );

        // Tests
        $this->assertNull($realPath);
    }

    # --------------------------------
    # Test - Utils methods

    /**
     * Test : Sanitize path.
     *
     * Check if 'sanitizePath' method return a correct path.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_sanitizePath(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Sanitize path
        $path = $this->call(
            object    : $resolver,
            method    : 'sanitizePath',
            parameters: ['path' => './good/path']
        );

        // Tests
        $this->assertNotNull($path);
        $this->assertIsString($path);
        $this->assertEquals('./good/path', $path);
    }

    /**
     * Test : Sanitize path - Slash terminated.
     *
     * Check if 'sanitizePath' method return a correct path.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_sanitizePath_slashTerminated(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Sanitize path
        $path = $this->call(
            object    : $resolver,
            method    : 'sanitizePath',
            parameters: ['path' => './slash-terminated/path/']
        );

        // Tests
        $this->assertNotNull($path);
        $this->assertIsString($path);
        $this->assertEquals('./slash-terminated/path', $path);
    }

    /**
     * Test : Sanitize path - With slashes different from system.
     *
     * Check if 'sanitizePath' method return a correct path.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_sanitizePath_withSlashesDifferentFromSystem(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Sanitize path
        $path = $this->call(
            object    : $resolver,
            method    : 'sanitizePath',
            parameters: ['path' => '.\path\with\slashes\different\from\system']
        );

        // Tests
        $this->assertNotNull($path);
        $this->assertIsString($path);
        $this->assertEquals('./path/with/slashes/different/from/system', $path);
    }

    /**
     * Test : Generate settings.
     *
     * Check if 'generateSettings' method return a bag with settings.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_generateSettings(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Generate settings
        $settings = $this->call(
            object    : $resolver,
            method    : 'generateSettings',
            parameters: [
                'key'    => 'Test.Multiple.Keys',
                'config' => ['welcome-message' => 'Hello World !']
            ]
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('Test', $settings);
        $this->assertArrayHasKey('Multiple', $settings->get('Test'));
        $this->assertArrayHasKey('Keys', $settings->get('Test.Multiple'));
        $this->assertArrayHasKey('welcome-message', $settings->get('Test.Multiple.Keys'));
        $this->assertEquals('Hello World !', $settings->get('Test.Multiple.Keys.welcome-message'));
    }

    /**
     * Test : Generate settings - Key as integer.
     *
     * Check if 'generateSettings' method return a bag with settings.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_generateSettings_keyAsInt(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Generate settings
        $settings = $this->call(
            object    : $resolver,
            method    : 'generateSettings',
            parameters: [
                'key'    => 0,
                'config' => ['welcome-message' => 'Hello World !']
            ]
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('welcome-message', $settings);
        $this->assertEquals('Hello World !', $settings->get('welcome-message'));
    }

    /**
     * Test : Generate settings - Empty key.
     *
     * Check if 'generateSettings' method return a bag with settings.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_generateSettings_emptyKey(): void
    {
        // ----------------
        // Vars

        // Get resolver
        $resolver = new ConfigResolver(
            reader: $this->reader
        );

        // ----------------
        // Process

        // Generate settings
        $settings = $this->call(
            object    : $resolver,
            method    : 'generateSettings',
            parameters: [
                'key'    => '',
                'config' => ['welcome-message' => 'Hello World !']
            ]
        );

        // Tests - Common
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);

        // Tests - Data
        $this->assertArrayHasKey('welcome-message', $settings);
        $this->assertEquals('Hello World !', $settings->get('welcome-message'));
    }
}
