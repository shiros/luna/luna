<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigBuilderTest.php
 * @Created_at  : 11/11/2018
 * @Update_at   : 12/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Config\Builder;

use Luna\Component\Test\LunaTestCase;
use Luna\Config\Builder\ConfigBuilder;
use Luna\Config\Config;
use Luna\Environment\Environment;

class ConfigBuilderTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Configuration data.
     */
    protected array $data;

    /**
     * Configuration file.
     */
    protected string $path;

    /**
     * Files structure.
     */
    protected array $files;

    /**
     * Environment instance.
     */
    protected Environment $environment;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate configuration data
        $this->data = [
            'key_1' => 'value_of_key_1',
            'key_2' => [
                'element_1' => 'value_of_element_1',
                'element_2' => 'value_of_element_2',
            ]
        ];

        // Generate files structure
        $this->files = [
            'config',
            'Service' => 'services/services'
        ];

        // Generate paths
        $this->path = $this->computePath(LUNA_ROOT . '/resources/tests/config/1');

        // Set environment
        $this->environment = new Environment();
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Build.
     *
     * Check if 'build' method return a correct config.
     */
    public function test_build(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::build(
            data       : $this->data,
            environment: $this->environment
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNotNull($config->getEnvironment());
        $this->assertTrue(is_a($config->getEnvironment(), Environment::class));

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('key_2', $config);

        // Tests - Data
        $this->assertEquals('value_of_key_1', $config->get('key_1'));
        $this->assertEquals('value_of_element_1', $config->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $config->get('key_2.element_2'));
    }

    /**
     * Test : Build - Without environment.
     *
     * Check if 'build' method return a correct config.
     */
    public function test_build_withoutEnvironment(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::build(
            data: $this->data
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('key_2', $config);

        // Tests - Data
        $this->assertEquals('value_of_key_1', $config->get('key_1'));
        $this->assertEquals('value_of_element_1', $config->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $config->get('key_2.element_2'));
    }

    /**
     * Test : Build - Empty.
     *
     * Check if 'build' method return a correct config.
     */
    public function test_build_empty(): void
    {
        // ----------------
        // Vars

        // Get data
        $data = [];

        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::build(
            data: $data
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    /**
     * Test : Build - Without parameters.
     *
     * Check if 'build' method return a correct config.
     */
    public function test_build_withoutParameters(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::build();

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    /**
     * Test : Build from files.
     *
     * Check if 'buildFromFiles' method return a correct config.
     */
    public function test_buildFromFiles(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::buildFromFiles(
            path       : $this->path,
            files      : $this->files,
            environment: $this->environment
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNotNull($config->getEnvironment());
        $this->assertTrue(is_a($config->getEnvironment(), Environment::class));

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('Service', $config);
        $this->assertArrayHasKey('key_1', $config->get('Service'));
        $this->assertArrayHasKey('Authentication', $config->get('Service'));
        $this->assertArrayHasKey('key_1', $config->get('Service.Authentication'));

        // Tests - Data
        $this->assertEquals('general_value', $config->get('key_1'));
        $this->assertEquals('service_value', $config->get('Service.key_1'));
        $this->assertEquals('authentication_value', $config->get('Service.Authentication.key_1'));
    }

    /**
     * Test : Build from files - Without environment.
     *
     * Check if 'buildFromFiles' method return a correct config.
     */
    public function test_buildFromFiles_withoutEnvironment(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::buildFromFiles(
            path : $this->path,
            files: $this->files
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());

        // Tests - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('Service', $config);
        $this->assertArrayHasKey('key_1', $config->get('Service'));
        $this->assertArrayHasKey('Authentication', $config->get('Service'));
        $this->assertArrayHasKey('key_1', $config->get('Service.Authentication'));

        // Tests - Data
        $this->assertEquals('general_value', $config->get('key_1'));
        $this->assertEquals('service_value', $config->get('Service.key_1'));
        $this->assertEquals('authentication_value', $config->get('Service.Authentication.key_1'));
    }

    /**
     * Test : Build from files - Wrong path.
     *
     * Check if 'buildFromFiles' method return an empty config.
     */
    public function test_buildFromFiles_wrongPath(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::buildFromFiles(
            path : './config/wrong/path',
            files: $this->files
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    /**
     * Test : Build from files - Empty files structure.
     *
     * Check if 'buildFromFiles' method return an empty config.
     */
    public function test_buildFromFiles_emptyFilesStructure(): void
    {
        // ----------------
        // Process

        // Build config
        $config = ConfigBuilder::buildFromFiles(
            path : $this->path,
            files: []
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertIsObject($config);
        $this->assertTrue(is_a($config, Config::class));
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }
}
