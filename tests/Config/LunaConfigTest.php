<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaConfigTest.php
 * @Created_at  : 01/06/2024
 * @Update_at   : 06/12/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Config;

use Luna\Component\Test\LunaTestCase;
use Luna\Config\LunaConfig;
use Luna\Exception\LunaException;
use ReflectionException;

class LunaConfigTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Luna settings.
     */
    protected array $settings;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate settings
        $this->settings = [
            'Services' => [
                'DI'      => [
                    'key_1' => 'di_value'
                ],
                'Handler' => [
                    'key_1' => 'handler_value'
                ]
            ]
        ];
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new LunaConfig(
            data: $this->settings
        );

        // Tests
        $this->assertNotNull($config);
    }

    # --------------------------------
    # Test - Getters

    /**
     * Test : Get DI.
     *
     * Check if 'getDI' method works as expected.
     * Returns the value corresponding to the given key inside the DI path.
     */
    public function test_getDI(): void
    {
        // ----------------
        // Vars

        // Get config
        $config = new LunaConfig(
            data: $this->settings
        );

        // ----------------
        // Process

        // Get value
        $value = $config->getDI('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals('di_value', $value);
    }

    /**
     * Test : Get handler.
     *
     * Check if 'getHandler' method works as expected.
     * Returns the value corresponding to the given key inside the handler path.
     */
    public function test_getHandler(): void
    {
        // ----------------
        // Vars

        // Get config
        $config = new LunaConfig(
            data: $this->settings
        );

        // ----------------
        // Process

        // Get value
        $value = $config->getHandler('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals('handler_value', $value);
    }

    # --------------------------------
    # Test - Utils methods

    /**
     * Test : Compute key.
     *
     * Check if 'computeKey' method works as expected.
     * Returns the concatenated key to the group.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_computeKey(): void
    {
        // ----------------
        // Vars

        // Get config
        $config = new LunaConfig();

        // Get information
        $group = 'my-group';
        $key   = 'my-key';

        // ----------------
        // Process

        // Get value
        $value = $this->call($config, 'computeKey', [$group, $key]);

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals('my-group.my-key', $value);
    }

    /**
     * Test : Compute key - Without key.
     *
     * Check if 'computeKey' method works as expected.
     * Returns only the group.
     *
     * @throws LunaException
     * @throws ReflectionException
     */
    public function test_computeKey_withoutKey(): void
    {
        // ----------------
        // Vars

        // Get config
        $config = new LunaConfig();

        // Get information
        $group = 'my-group';

        // ----------------
        // Process

        // Get value
        $value = $this->call($config, 'computeKey', [$group]);

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals('my-group', $value);
    }
}
