<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ConfigBuilderTest.php
 * @Created_at  : 12/11/2018
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Config;

use Luna\Component\Test\LunaTestCase;
use Luna\Config\Config;
use Luna\Environment\Environment;

class ConfigTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Configuration data.
     */
    protected array $data;

    /**
     * Files structure.
     */
    protected array $files;

    /**
     * Initial configuration path.
     */
    protected string $path;

    /**
     * Secondary configuration path.
     */
    protected string $pathSecondary;

    /**
     * Environment instance.
     */
    protected Environment $environment;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate configuration data
        $this->data = [
            'key_1' => 'value_of_key_1',
            'key_2' => [
                'element_1' => 'value_of_element_1',
                'element_2' => 'value_of_element_2',
            ]
        ];

        // Generate files structure
        $this->files = [
            'config',
            'Service' => 'services/services'
        ];

        // Generate paths
        $this->path          = $this->computePath(LUNA_ROOT . '/resources/tests/config/1');
        $this->pathSecondary = $this->computePath(LUNA_ROOT . '/resources/tests/config/2');

        // Set environment
        $this->environment = new Environment();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new Config(
            data       : $this->data,
            environment: $this->environment
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNotNull($config->getEnvironment());
    }

    /**
     * Test : Construct - Without environment.
     *
     * Check if the constructor process went well.
     */
    public function test_construct_withoutEnvironment(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new Config(
            data: $this->data,
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertNotEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    /**
     * Test : Construct - Empty data.
     *
     * Check if the constructor process went well.
     */
    public function test_construct_emptyData(): void
    {
        // ----------------
        // Vars

        // Get data
        $data = [];

        // ----------------
        // Process

        // Build config
        $config = new Config(
            data: $data
        );

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    /**
     * Test : Construct - Without data.
     *
     * Check if the constructor process went well.
     */
    public function test_construct_withoutData(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new Config();

        // Tests - Common
        $this->assertNotNull($config);
        $this->assertEmpty($config);

        // Tests - Environment
        $this->assertNull($config->getEnvironment());
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Build from files.
     *
     * Check if 'buildFromFiles' method return a correct config.
     */
    public function test_load(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new Config();

        // Tests (Initialize) - Common
        $this->assertNotNull($config);
        $this->assertEmpty($config);

        // Load file
        $config->load(
            path : $this->path,
            files: $this->files
        );

        // Tests (After load) - Common
        $this->assertNotEmpty($config);

        // Tests (After load) - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('Service', $config);
        $this->assertArrayHasKey('key_1', $config->get('Service'));
        $this->assertArrayHasKey('Authentication', $config->get('Service'));
        $this->assertArrayHasKey('key_1', $config->get('Service.Authentication'));

        // Tests (After load) - Data
        $this->assertEquals('general_value', $config->get('key_1'));
        $this->assertEquals('service_value', $config->get('Service.key_1'));
        $this->assertEquals('authentication_value', $config->get('Service.Authentication.key_1'));
    }

    /**
     * Test : Build from files.
     *
     * Check if 'buildFromFiles' method return a correct config.
     */
    public function test_load_multiple(): void
    {
        // ----------------
        // Process

        // Build config
        $config = new Config();

        // Tests (Initialize) - Common
        $this->assertNotNull($config);
        $this->assertEmpty($config);

        // Load first file
        $config->load(
            path : $this->path,
            files: $this->files
        );

        // Tests (First load) - Common
        $this->assertNotEmpty($config);

        // Tests (First load) - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayNotHasKey('key_2', $config);
        $this->assertArrayHasKey('Service', $config);
        $this->assertArrayHasKey('key_1', $config->get('Service'));
        $this->assertArrayHasKey('Authentication', $config->get('Service'));
        $this->assertArrayHasKey('key_1', $config->get('Service.Authentication'));

        // Tests (First load) - Data
        $this->assertEquals('general_value', $config->get('key_1'));
        $this->assertEquals('service_value', $config->get('Service.key_1'));
        $this->assertEquals('authentication_value', $config->get('Service.Authentication.key_1'));

        // Load second file
        $config->load(
            path : $this->pathSecondary,
            files: $this->files
        );

        // Tests (Second load) - Common
        $this->assertNotEmpty($config);

        // Tests (Second load) - Keys
        $this->assertArrayHasKey('key_1', $config);
        $this->assertArrayHasKey('key_2', $config);
        $this->assertArrayHasKey('element_1', $config->get('key_2'));
        $this->assertArrayHasKey('element_2', $config->get('key_2'));
        $this->assertArrayHasKey('Service', $config);
        $this->assertArrayHasKey('key_1', $config->get('Service'));
        $this->assertArrayHasKey('Authentication', $config->get('Service'));
        $this->assertArrayHasKey('key_1', $config->get('Service.Authentication'));

        // Tests (Second load) - Data
        $this->assertEquals('general_value', $config->get('key_1'));
        $this->assertEquals('value_of_element_1', $config->get('key_2.element_1'));
        $this->assertEquals('value_of_element_2', $config->get('key_2.element_2'));
        $this->assertEquals('service_value', $config->get('Service.key_1'));
        $this->assertEquals('authentication_value_2', $config->get('Service.Authentication.key_1'));
    }
}
