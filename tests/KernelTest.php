<?php

/****
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KernelTest.php
 * @Created_at  : 12/10/2018
 * @Update_at   : 09/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests;

use Luna\Component\Test\LunaTestCase;
use Luna\Kernel;

class KernelTest extends LunaTestCase
{
    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the construction is done without problems.
     */
    public function test_construct(): void
    {
        // ----------------
        // Vars

        // Get kernel
        $kernel = new Kernel();

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($kernel);
        $this->assertIsObject($kernel);
    }

    # --------------------------------
    # Test - Constants

    /**
     * Test : Constants of Kernel.
     *
     * Check if the creation of constant is done without problems.
     */
    public function test_constantsKernel(): void
    {
        // ----------------
        // Vars

        // Get kernel
        $kernel = new Kernel();

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($kernel);
        $this->assertTrue(defined('LUNA_ROOT'));
        $this->assertTrue(defined('LUNA_CONFIG_DIR'));
        $this->assertTrue(defined('APP_ROOT'));
        $this->assertTrue(defined('APP_ENV'));
        $this->assertTrue(defined('APP_CONFIG_DIR'));
    }
}
