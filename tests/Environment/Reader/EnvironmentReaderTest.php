<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentReaderTest.php
 * @Created_at  : 13/01/2023
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Environment\Reader;

use Luna\Component\Bag\BagInterface;
use Luna\Component\Test\LunaTestCase;
use Luna\Environment\Reader\EnvironmentReader;

class EnvironmentReaderTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * PHP Configuration path.
     */
    protected string $pathPHP;

    /**
     * JSON Configuration path.
     */
    protected string $pathJSON;

    /**
     * YAML Configuration path.
     */
    protected string $pathYAML;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate paths
        $this->pathPHP  = $this->computePath(LUNA_ROOT . '/resources/tests/environment/extensions/env.php');
        $this->pathJSON = $this->computePath(LUNA_ROOT . '/resources/tests/environment/extensions/env.json');
        $this->pathYAML = $this->computePath(LUNA_ROOT . '/resources/tests/environment/extensions/env.yaml');
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build reader
        $reader = new EnvironmentReader();

        // Tests
        $this->assertNotNull($reader);
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Read - PHP file.
     *
     * Check if 'read' method return a bag with environment variables.
     */
    public function test_read_php(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read($this->pathPHP);

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);
    }

    /**
     * Test : Read - JSON file.
     *
     * Check if 'read' method return a bag with environment variables.
     */
    public function test_read_json(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read($this->pathJSON);

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);
    }

    /**
     * Test : Read - YAML file.
     *
     * Check if 'read' method return a bag with environment variables.
     */
    public function test_read_yaml(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read($this->pathYAML);

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertNotEmpty($settings);
    }

    /**
     * Test : Read - Wrong path.
     *
     * Check if 'read' method return an empty bag.
     */
    public function test_read_wrongPath(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // Paths
        $wrongPath = $this->computePath(LUNA_ROOT . '/resources/tests/config/extensions/config.txt');

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read($wrongPath);

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }

    /**
     * Test : Read - Null path.
     *
     * Check if 'read' method return an empty bag.
     */
    public function test_read_nullPath(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read(null);

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }

    /**
     * Test : Read - Empty path.
     *
     * Check if 'read' method return an empty bag.
     */
    public function test_read_emptyPath(): void
    {
        // ----------------
        // Vars

        // Get reader
        $reader = new EnvironmentReader();

        // ----------------
        // Process

        // Launch build
        $settings = $reader->read('');

        // Tests
        $this->assertNotNull($settings);
        $this->assertIsObject($settings);
        $this->assertTrue(is_a($settings, BagInterface::class));
        $this->assertEmpty($settings);
    }
}
