<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentBuilderTest.php
 * @Created_at  : 24/07/2024
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Environment\Builder;

use Luna\Component\Test\LunaTestCase;
use Luna\Environment\Builder\EnvironmentBuilder;
use Luna\Environment\Environment;

class EnvironmentBuilderTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Environment parameters.
     */
    protected array $parameters;

    /**
     * Environment file.
     */
    protected string $path;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate environment parameters
        $this->parameters = [
            'Environment' => 'test',
            'key_1'       => 'value_1'
        ];

        // Generate paths
        $this->path = $this->computePath(LUNA_ROOT . '/resources/tests/environment/env.php');
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Build.
     *
     * Check if 'build' method return a correct environment.
     */
    public function test_build(): void
    {
        // ----------------
        // Process

        // Build environment
        $environment = EnvironmentBuilder::build(
            parameters: $this->parameters
        );

        // Tests - Common
        $this->assertNotNull($environment);
        $this->assertIsObject($environment);
        $this->assertTrue(is_a($environment, Environment::class));
        $this->assertNotEmpty($environment);

        // Tests - Keys
        $this->assertArrayHasKey('Environment', $environment);
        $this->assertArrayHasKey('key_1', $environment);

        // Tests - Data
        $this->assertEquals('test', $environment->get('Environment'));
        $this->assertEquals('value_1', $environment->get('key_1'));
    }

    /**
     * Test : Build - Empty.
     *
     * Check if 'build' method return an empty environment.
     */
    public function test_build_empty(): void
    {
        // ----------------
        // Vars

        // Get data
        $parameters = [];

        // ----------------
        // Process

        // Build environment
        $environment = EnvironmentBuilder::build(
            parameters: $parameters
        );

        // Tests
        $this->assertNotNull($environment);
        $this->assertIsObject($environment);
        $this->assertTrue(is_a($environment, Environment::class));
        $this->assertEmpty($environment);
    }

    /**
     * Test : Build - Without parameters.
     *
     * Check if 'build' method return an empty environment.
     */
    public function test_build_withoutParameters(): void
    {
        // ----------------
        // Process

        // Build environment
        $environment = EnvironmentBuilder::build();

        // Tests
        $this->assertNotNull($environment);
        $this->assertIsObject($environment);
        $this->assertTrue(is_a($environment, Environment::class));
        $this->assertEmpty($environment);
    }

    /**
     * Test : Build from files.
     *
     * Check if 'buildFromFiles' method return a correct config.
     */
    public function test_buildFromFiles(): void
    {
        // ----------------
        // Process

        // Build environment
        $environment = EnvironmentBuilder::buildFromFiles(
            path       : $this->path
        );

        // Tests
        $this->assertNotNull($environment);
        $this->assertIsObject($environment);
        $this->assertTrue(is_a($environment, Environment::class));
        $this->assertNotEmpty($environment);

        // Tests - Keys
        $this->assertArrayHasKey('Environment', $environment);
        $this->assertArrayHasKey('Key1', $environment);
        $this->assertArrayHasKey('Key2', $environment);

        // Tests - Data
        $this->assertEquals('test', $environment->get('Environment'));
        $this->assertEquals('Value1', $environment->get('Key1'));
        $this->assertEquals('Value2', $environment->get('Key2'));
    }

    /**
     * Test : Build from files - Wrong path.
     *
     * Check if 'buildFromFiles' method return an empty config.
     */
    public function test_buildFromFiles_wrongPath(): void
    {
        // ----------------
        // Process

        // Build environment
        $environment = EnvironmentBuilder::buildFromFiles(
            path : './config/wrong/path'
        );

        // Tests
        $this->assertNotNull($environment);
        $this->assertIsObject($environment);
        $this->assertTrue(is_a($environment, Environment::class));
        $this->assertEmpty($environment);
    }
}
