<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : EnvironmentTest.php
 * @Created_at  : 13/01/2023
 * @Update_at   : 24/07/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Environment;

use Luna\Component\Bag\Exception\ImmutableBagException;
use Luna\Component\Test\LunaTestCase;
use Luna\Environment\Environment;
use Luna\Environment\EnvironmentInterface;

class EnvironmentTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    /**
     * Environment parameters.
     */
    protected array $parameters;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Start Kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Generate environment parameters
        $this->parameters = [
            'Environment' => 'test',
            'key_1'       => 'value_1'
        ];
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // Tests
        $this->assertNotNull($environment);
        $this->assertNotEmpty($environment);
    }

    # --------------------------------
    # Test - Getters

    /**
     * Test : Get - Key exists.
     *
     * Check if 'get' method return the right value of a key.
     */
    public function test_get_keyExists(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Get variable
        $value = $environment->get('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_1', $value);
    }

    /**
     * Test : Get - Key not exists.
     *
     * Check if 'get' method return the right value of a non-existent key.
     */
    public function test_get_keyNotExists(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Get variable
        $value = $environment->get('key_250');

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get - Default value.
     *
     * Check if 'get' method return the default value when a non-existent key is used.
     */
    public function test_get_defaultValue(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Get variable
        $value = $environment->get('key_250', 'NOT_FOUND');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('NOT_FOUND', $value);
    }

    /**
     * Test : Get environment.
     *
     * Check if 'getEnv' method return the environment string.
     */
    public function test_getEnv(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process
        $env = $environment->getEnv();

        // Tests
        $this->assertNotNull($env);
        $this->assertNotEmpty($env);
        $this->assertEquals(EnvironmentInterface::TEST, $env);
    }

    # --------------------------------
    # Test - Bag methods

    /**
     * Test : Change.
     *
     * Check if 'change' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_change(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->change([]);
    }

    /**
     * Test : Replace.
     *
     * Check if 'replace' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_replace(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->replace([]);
    }

    /**
     * Test : Replace recursive.
     *
     * Check if 'replaceRecursive' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_replaceRecursive(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->replaceRecursive([]);
    }

    /**
     * Test : Merge.
     *
     * Check if 'merge' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_merge(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->merge([]);
    }

    /**
     * Test : Merge recursive.
     *
     * Check if 'mergeRecursive' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_mergeRecursive(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->mergeRecursive([]);
    }

    /**
     * Test : Setter.
     *
     * Check if 'set' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_set(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->set('Key1', 'Test Value');
    }

    /**
     * Test : Remove.
     *
     * Check if 'remove' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_remove(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->remove('Key1');
    }

    /**
     * Test : Clear.
     *
     * Check if 'clear' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_clear(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->clear();
    }

    /**
     * Test : Push.
     *
     * Check if 'push' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_push(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->push('Test Value');
    }

    /**
     * Test : Shift.
     *
     * Check if 'shift' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_shift(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->shift();
    }

    /**
     * Test : Pop.
     *
     * Check if 'pop' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_pop(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->pop();
    }

    /**
     * Test : Unique.
     *
     * Check if 'unique' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_unique(): void
    {
        // ----------------
        // Vars

        // Get environment
        $environment = new Environment(
            parameters: $this->parameters
        );

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the environment settings.");

        // Call method
        $environment->unique();
    }
}
