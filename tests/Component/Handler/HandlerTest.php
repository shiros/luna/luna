<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : HandlerTest.php
 * @Created_at  : 28/11/2023
 * @Update_at   : 28/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Handler;

use Luna\Component\Handler\Handler;
use Luna\Component\Test\LunaTestCase;

class HandlerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build handler
        $handler = new Handler();

        // Tests
        $this->assertNotNull($handler);
    }
}
