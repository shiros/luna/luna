<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : JSONManagerTest.php
 * @Created_at  : 02/12/2023
 * @Update_at   : 02/12/2023
 * ----------------------------------------------------------------
 */

namespace Component\Manager;

use Luna\Component\Test\LunaTestCase;

class JSONManagerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test

    /**
     * Test :
     */
    public function test_(): void
    {
        $this->assertNull($this->doNothing());
    }
}
