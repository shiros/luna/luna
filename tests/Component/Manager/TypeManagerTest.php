<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : TypeManagerTest.php
 * @Created_at  : 02/12/2023
 * @Update_at   : 02/12/2023
 * ----------------------------------------------------------------
 */

namespace Component\Manager;

use Luna\Component\Bag\Bag;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Test\LunaTestCase;

class TypeManagerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Primary methods

    /**
     * Test : Is Null - Null returns true.
     *
     * Check if 'isNull' method returns true when null is given.
     */
    public function test_isNull_nullReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isNull(null);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Null - Non-null value returns false.
     *
     * Check if 'isNull' method returns false when a non-null value is given.
     */
    public function test_isNull_nonNullValueReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $stringCondition  = TypeManager::isNull('Luna');
        $intCondition     = TypeManager::isNull(24);
        $booleanCondition = TypeManager::isNull(true);
        $arrayCondition   = TypeManager::isNull([]);

        // Tests
        $this->assertFalse($stringCondition);
        $this->assertFalse($intCondition);
        $this->assertFalse($booleanCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Empty - Null returns true.
     *
     * Check if 'isEmpty' method returns true when null is given.
     */
    public function test_isEmpty_nullReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isEmpty(null);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Empty - Empty value returns true.
     *
     * Check if 'isEmpty' method returns true when an empty value is given.
     */
    public function test_isEmpty_emptyValueReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $stringCondition = TypeManager::isEmpty('');
        $arrayCondition  = TypeManager::isEmpty([]);
        $bagCondition    = TypeManager::isEmpty(new Bag());

        // Tests
        $this->assertTrue($stringCondition);
        $this->assertTrue($arrayCondition);
        $this->assertTrue($bagCondition);
    }

    /**
     * Test : Is String - String returns true.
     *
     * Check if 'isString' method returns true when a string is given.
     */
    public function test_isString_stringReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isString('Luna');

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is String - Non-string value returns false.
     *
     * Check if 'isString' method returns false when a non-string value is given.
     */
    public function test_isString_nonStringReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition  = TypeManager::isString(null);
        $intCondition   = TypeManager::isString(24);
        $arrayCondition = TypeManager::isString([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($intCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Number - Integer returns true.
     *
     * Check if 'isNumber' method returns true when a integer is given.
     */
    public function test_isNumber_intReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isNumber(24);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Number - Float returns true.
     *
     * Check if 'isNumber' method returns true when a float is given.
     */
    public function test_isNumber_floatReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isNumber(24.5);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Number - Non-number value returns false.
     *
     * Check if 'isNumber' method returns false when a non-number value is given.
     */
    public function test_isNumber_nonNumberReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isNumber(null);
        $stringCondition = TypeManager::isNumber('Luna');
        $arrayCondition  = TypeManager::isNumber([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Integer - Integer returns true.
     *
     * Check if 'isInteger' method returns true when a integer is given.
     */
    public function test_isInteger_integerReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isInteger(24);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Integer - Non-integer value returns false.
     *
     * Check if 'isInteger' method returns false when a non-integer value is given.
     */
    public function test_isInteger_nonIntegerReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isInteger(null);
        $stringCondition = TypeManager::isInteger('Luna');
        $floatCondition  = TypeManager::isInteger(24.5);
        $arrayCondition  = TypeManager::isInteger([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($floatCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Long - Long returns true.
     *
     * Check if 'isLong' method returns true when a long is given.
     */
    public function test_isLong_longReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isLong(24000000000);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Long - Non-long value returns false.
     *
     * Check if 'isLong' method returns false when a non-long value is given.
     */
    public function test_isLong_nonLongReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isLong(null);
        $stringCondition = TypeManager::isLong('Luna');
        $floatCondition  = TypeManager::isLong(24.5);
        $arrayCondition  = TypeManager::isLong([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($floatCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Float - Float returns true.
     *
     * Check if 'isFloat' method returns true when a float is given.
     */
    public function test_isFloat_floatReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isFloat(24.5);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Float - Non-float value returns false.
     *
     * Check if 'isFloat' method returns false when a non-float value is given.
     */
    public function test_isFloat_nonFloatReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isFloat(null);
        $stringCondition = TypeManager::isFloat('Luna');
        $intCondition    = TypeManager::isFloat(24);
        $arrayCondition  = TypeManager::isFloat([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($intCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Double - Double returns true.
     *
     * Check if 'isDouble' method returns true when a double is given.
     */
    public function test_isDouble_doubleReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $value = TypeManager::isDouble(24000000000.5);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Is Double - Non-double value returns false.
     *
     * Check if 'isDouble' method returns false when a non-double value is given.
     */
    public function test_isDouble_nonDoubleReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isDouble(null);
        $stringCondition = TypeManager::isDouble('Luna');
        $intCondition    = TypeManager::isDouble(24);
        $arrayCondition  = TypeManager::isDouble([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($intCondition);
        $this->assertFalse($arrayCondition);
    }

    /**
     * Test : Is Boolean - Boolean returns true.
     *
     * Check if 'isBoolean' method returns true when a boolean is given.
     */
    public function test_isBoolean_booleanReturnsTrue(): void
    {
        // ----------------
        // Process

        // Get value
        $condition1 = TypeManager::isBoolean(true);
        $condition2 = TypeManager::isBoolean(false);

        // Tests
        $this->assertTrue($condition1);
        $this->assertTrue($condition2);
    }

    /**
     * Test : Is Boolean - Non-boolean value returns false.
     *
     * Check if 'isBoolean' method returns false when a non-boolean value is given.
     */
    public function test_isBoolean_nonBooleanReturnsFalse(): void
    {
        // ----------------
        // Process

        // Get value
        $nullCondition   = TypeManager::isBoolean(null);
        $stringCondition = TypeManager::isBoolean('Luna');
        $intCondition    = TypeManager::isBoolean(24);
        $arrayCondition  = TypeManager::isBoolean([]);

        // Tests
        $this->assertFalse($nullCondition);
        $this->assertFalse($stringCondition);
        $this->assertFalse($intCondition);
        $this->assertFalse($arrayCondition);
    }

    # --------------------------------
    # Test - Complex methods
}
