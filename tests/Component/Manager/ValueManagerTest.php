<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Maxime Mazet
 * @Contributor : Alexandre Caillot
 *
 * @File        : ValueManagerTest.php
 * @Created_at  : 19/09/2021
 * @Update_at   : 25/08/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Manager;

use Luna\Component\Manager\ValueManager;
use Luna\Component\Test\LunaTestCase;

class ValueManagerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Get - String return string.
     *
     * Check if 'get' method return a correct string when a string is given.
     */
    public function test_get_stringReturnsString(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getString('luna');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame('luna', $value);
    }

    /**
     * Test : Get - Integer return string.
     *
     * Check if 'get' method return a correct string when an integer is given.
     */
    public function test_get_integerReturnsString(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getString(5);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame('5', $value);
        $this->assertNotSame(5, $value);
    }

    /**
     * Test : Get - Boolean return string.
     *
     * Check if 'get' method return a correct string when a boolean is given.
     */
    public function test_get_booleanReturnsString(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getString(true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame('true', $value);
        $this->assertNotSame(true, $value);
    }

    /**
     * Test : Get - Integer return integer.
     *
     * Check if 'get' method return a correct integer when an integer is given.
     */
    public function test_get_integerReturnsInteger(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getInteger(5);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(5, $value);
    }

    /**
     * Test : Get - String return integer.
     *
     * Check if 'get' method return a correct integer when a string is given.
     */
    public function test_get_stringReturnsInteger(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getInteger('5');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(5, $value);
        $this->assertNotSame('5', $value);
    }

    /**
     * Test : Get - Boolean return integer.
     *
     * Check if 'get' method return a correct integer when a boolean is given.
     */
    public function test_get_booleanReturnsInteger(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getInteger(true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(1, $value);
        $this->assertNotSame(true, $value);
    }

    /**
     * Test : Get - Float return float.
     *
     * Check if 'get' method return a correct float when a float is given.
     */
    public function test_get_floatReturnsFloat(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getFloat(5.5);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(5.5, $value);
    }

    /**
     * Test : Get - String return float.
     *
     * Check if 'get' method return a correct float when a string is given.
     */
    public function test_get_stringReturnsFloat(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getFloat('5.5');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(5.5, $value);
        $this->assertNotSame('5.5', $value);
    }

    /**
     * Test : Get - Boolean return float.
     *
     * Check if 'get' method return a correct float when a boolean is given.
     */
    public function test_get_booleanReturnsFloat(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getFloat(true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(1.0, $value);
        $this->assertNotSame(true, $value);
    }

    /**
     * Test : Get - Boolean return boolean.
     *
     * Check if 'get' method return a correct boolean when a boolean is given.
     */
    public function test_get_booleanReturnsBoolean(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getBoolean(true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(true, $value);
    }

    /**
     * Test : Get - String return boolean.
     *
     * Check if 'get' method return a correct boolean when a string is given.
     */
    public function test_get_stringReturnsBoolean(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getBoolean('true');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(true, $value);
        $this->assertNotSame('true', $value);
    }

    /**
     * Test : Get - Integer return boolean.
     *
     * Check if 'get' method return a correct boolean when an integer is given.
     */
    public function test_get_integerReturnsBoolean(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::getBoolean(1);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame(true, $value);
        $this->assertNotSame(1, $value);
    }

    # --------------------------------
    # Test - Utils methods

    /**
     * Test : UcFirst.
     *
     * Check if 'ucFirst' method return a string with first letter in uppercase.
     */
    public function test_ucFirst(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::ucFirst('maxime');

        // Tests
        $this->assertEquals("Maxime", $value);
        $this->assertNotEquals("maxime", $value);
    }

    /**
     * Test : Random Integer.
     *
     * Check if 'randomInteger' method return an integer between min and max.
     */
    public function test_randomInteger(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::randomInteger(0, 10);

        // Tests
        $this->assertGreaterThanOrEqual(0, $value);
        $this->assertLessThanOrEqual(10, $value);
    }

    /**
     * Test : Truncate.
     *
     * Check if 'truncate' method return a string with the correct size and content.
     */
    public function test_truncate(): void
    {
        // ----------------
        // Process

        // Get values
        $value1 = ValueManager::truncate('maxime', 0);
        $value2 = ValueManager::truncate('maxime', 3);
        $value3 = ValueManager::truncate('maxime', 7);

        // Tests - Value 1
        $this->assertEquals('', $value1);

        // Tests - Value 2
        $this->assertEquals('max', $value2);

        // Tests - Value 2
        $this->assertEquals('maxime', $value3);
    }

    /**
     * Test : Split.
     *
     * Check if 'split' method return an array string.
     */
    public function test_split(): void
    {
        // ----------------
        // Process

        // Get values
        $value1 = ValueManager::split('bonjour,je,suis,maxime');
        $value2 = ValueManager::split('bonjour;je;suis;maxime', ';');

        // Tests - Value 1
        $this->assertIsArray($value1);
        $this->assertCount(4, $value1);
        $this->assertEquals('bonjour', $value1[0]);
        $this->assertEquals('je', $value1[1]);
        $this->assertEquals('suis', $value1[2]);
        $this->assertEquals('maxime', $value1[3]);

        // Tests - Value 2
        $this->assertIsArray($value2);
        $this->assertCount(4, $value2);
        $this->assertEquals('bonjour', $value2[0]);
        $this->assertEquals('je', $value2[1]);
        $this->assertEquals('suis', $value2[2]);
        $this->assertEquals('maxime', $value2[3]);
    }

    /**
     * Test : Join.
     *
     * Check if 'join' method return a string.
     */
    public function test_join(): void
    {
        // ----------------
        // Vars

        $array = ['bonjour', 'je', 'suis', 'maxime'];

        // ----------------
        // Process

        // Get values
        $value1 = ValueManager::join($array);
        $value2 = ValueManager::join($array, ';');

        // Tests - Value 1
        $this->assertIsString($value1);
        $this->assertEquals('bonjour,je,suis,maxime', $value1);

        // Tests - Value 2
        $this->assertIsString($value2);
        $this->assertEquals('bonjour;je;suis;maxime', $value2);
    }

    /**
     * Test : Match.
     *
     * Check if 'join' method works as expected.
     */
    public function test_match(): void
    {
        // ----------------
        // Vars

        $pattern = "#^[0-9]$#";

        // ----------------
        // Process

        // Tests
        $this->assertTrue(ValueManager::match($pattern, "1"));
        $this->assertFalse(ValueManager::match($pattern, "155"));
    }

    /**
     * Test : Contains.
     *
     * Check if 'contains' method works as expected.
     */
    public function test_contains(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ValueManager::contains('Bonjiour', 'jiour');

        // Tests
        $this->assertTrue($value);
    }
}
