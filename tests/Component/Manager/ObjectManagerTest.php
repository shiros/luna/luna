<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ObjectManagerTest.php
 * @Created_at  : 06/04/2020
 * @Update_at   : 12/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Manager;

use Luna\Component\Test\LunaTestCase;

class ObjectManagerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test

    /**
     * Test :
     */
    public function test_(): void
    {
        $this->assertNull($this->doNothing());
    }
}
