<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ClassManagerTest.php
 * @Created_at  : 06/04/2020
 * @Update_at   : 02/12/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Manager;

use Luna\Component\Manager\ClassManager;
use Luna\Component\Test\LunaTestCase;

interface InterfaceMyClassTest
{

}

abstract class AbstractMyClassTest implements InterfaceMyClassTest
{

}

class MyClassTest extends AbstractMyClassTest
{
    public function myMethod(): void
    {

    }
}

class ClassManagerTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    protected MyClassTest $myClassTest;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();

        // Set attributes
        $this->myClassTest = new MyClassTest();
    }

    # --------------------------------
    # Test - Naming methods

    /**
     * Test : Name of.
     *
     * Check if 'nameOf' method returns the name of the class.
     */
    public function test_nameOf(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::nameOf($this->myClassTest);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertSame('Luna\Tests\Component\Manager\MyClassTest', $value);
    }

    # --------------------------------
    # Test - Naming methods

    /**
     * Test : Exist.
     *
     * Check if 'exist' method returns true when an existing class is given.
     */
    public function test_exist(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::exist('Luna\Tests\Component\Manager\MyClassTest');

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Exist - Not existing class.
     *
     * Check if 'exist' method returns false when a non-existing class is given.
     */
    public function test_exist_nonExistingClass(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::exist('MyClassTest2');

        // Tests
        $this->assertFalse($value);
    }

    /**
     * Test : Exist - Not existing class.
     *
     * Check if 'exist' method returns true when the class is the same as the requested class.
     */
    public function test_is(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::is(MyClassTest::class, $this->myClassTest);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Exist - Not same class.
     *
     * Check if 'exist' method returns false when the class is not the same as the requested class.
     */
    public function test_is_notSameClass(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::is('MyClassTest2', $this->myClassTest);

        // Tests
        $this->assertFalse($value);
    }

    /**
     * Test : Implement.
     *
     * Check if 'exist' method returns true when the class implement the interface.
     */
    public function test_implement(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::implement(InterfaceMyClassTest::class, $this->myClassTest);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Implement - Not implemented interface.
     *
     * Check if 'exist' method returns false when the class not implement the interface.
     */
    public function test_implement_notImplementedInterface(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::implement('InterfaceMyClassTest2', $this->myClassTest);

        // Tests
        $this->assertFalse($value);
    }

    /**
     * Test : Extend.
     *
     * Check if 'exist' method returns true when the class extend the interface.
     */
    public function test_extend(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::extend(AbstractMyClassTest::class, $this->myClassTest);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Extend - Not extended interface.
     *
     * Check if 'exist' method returns false when the class not extend the interface.
     */
    public function test_extend_notExtendedInterface(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::extend('AbstractMyClassTest2', $this->myClassTest);

        // Tests
        $this->assertFalse($value);
    }

    # --------------------------------
    # Test - Has/Is methods

    /**
     * Test : Has Method.
     *
     * Check if 'exist' method returns true when the class has the given method.
     */
    public function test_hasMethod(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::hasMethod('myMethod', $this->myClassTest);

        // Tests
        $this->assertTrue($value);
    }

    /**
     * Test : Has Method - Non-existing method.
     *
     * Check if 'hasMethod' method returns false when the class haven't the given method.
     */
    public function test_hasMethod_nonExistingMethod(): void
    {
        // ----------------
        // Process

        // Get value
        $value = ClassManager::hasMethod('myMethod2', $this->myClassTest);

        // Tests
        $this->assertFalse($value);
    }
}
