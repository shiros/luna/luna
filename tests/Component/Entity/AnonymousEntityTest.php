<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AnonymousEntityTest.php
 * @Created_at  : 26/09/2020
 * @Update_at   : 08/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Entity;

use Luna\Component\Test\LunaTestCase;
use Luna\Component\Entity\AnonymousEntity;

class AnonymousEntityTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build entity
        $entity = new AnonymousEntity();

        // Tests
        $this->assertNotNull($entity);
    }
}
