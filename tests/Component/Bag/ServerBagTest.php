<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ServerBagTest.php
 * @Created_at  : 08/01/2023
 * @Update_at   : 08/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Bag;

use Luna\Component\Bag\ServerBag;
use Luna\Component\Test\LunaTestCase;

class ServerBagTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Setup PHP environment variables (Simulate system environment variables)
        $_SERVER["env1"] = "Mon env";
        $_SERVER["env2"] = 24;
        $_SERVER["env3"] = "24";
        $_SERVER["env4"] = true;
        $_SERVER["env5"] = "true";
        $_SERVER["env6"] = false;

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build bag
        $bag = new ServerBag();

        // Tests
        $this->assertNotNull($bag);
    }

    # --------------------------------
    # Test - Core methods
//
//    /**
//     * Test : Get.
//     *
//     * Check if 'get' method return correct environment variable value.
//     */
//    public function test_get(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::get('env1');
//
//        // Tests
//        $this->assertSame('Mon env', $value);
//    }
//
//    /**
//     * Test : Get. (Wrong key)
//     *
//     * Check if 'get' method return the default value for wrong key.
//     */
//    public function test_get_wrongKey(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::get('env999', 'Default value');
//
//        // Tests
//        $this->assertSame('Default value', $value);
//    }
//
//    /**
//     * Test : Get as integer.
//     *
//     * Check if 'getAsInteger' method return the environment variable value as integer.
//     */
//    public function test_getAsInteger(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::getAsInteger('env2');
//
//        // Tests
//        $this->assertSame(24, $value);
//    }
//
//    /**
//     * Test : Get as integer. (For string value)
//     *
//     * Check if 'getAsInteger' method return the environment variable value as integer from a string value.
//     */
//    public function test_getAsInteger_forStringValue(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::getAsInteger('env3');
//
//        // Tests
//        $this->assertSame(24, $value);
//    }
//
//    /**
//     * Test : Get as boolean.
//     *
//     * Check if 'getAsBoolean' method return the environment variable value as boolean.
//     */
//    public function test_getAsBoolean(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::getAsBoolean('env4');
//
//        // Tests
//        $this->assertSame(true, $value);
//    }
//
//    /**
//     * Test : Get as boolean. (For string value)
//     *
//     * Check if 'getAsBoolean' method return the environment variable value as boolean from a string value.
//     */
//    public function test_getAsBoolean_forStringValue(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::getAsBoolean('env5');
//
//        // Tests
//        $this->assertSame(true, $value);
//    }
//
//    /**
//     * Test : Get as boolean. (For string value)
//     *
//     * Check if 'getAsBoolean' method return the environment variable value as boolean from a string value.
//     */
//    public function test_getAsBoolean_falseValue(): void
//    {
//        // ----------------
//        // Process
//
//        // Get environment variable
//        $value = Environment::getAsBoolean('env6');
//
//        // Tests
//        $this->assertSame(false, $value);
//    }
}
