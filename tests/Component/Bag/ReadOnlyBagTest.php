<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ReadOnlyBagTest.php
 * @Created_at  : 04/05/2023
 * @Update_at   : 25/05/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Bag;

use Luna\Component\Bag\Exception\ImmutableBagException;
use Luna\Component\Bag\Bag;
use Luna\Component\Bag\ReadOnlyBag;
use Luna\Component\Test\LunaTestCase;

class ReadOnlyBagTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    protected array $array;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Vars

        $this->array = [
            0      => 'Value0',
            'Key1' => 'Value1',
            'Key2' => 'Value2',
            'Key3' => [
                0        => 'Value0',
                'Key3-1' => 'Value1',
                'Key3-2' => 'Value2',
            ]
        ];

        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build bag
        $bag = new ReadOnlyBag();

        // Tests
        $this->assertNotNull($bag);
    }

    /**
     * Test : Construct - With array.
     *
     * Check if the constructor process went well.
     */
    public function test_construct_withArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($bag);
    }

    # --------------------------------
    # Test - Interface methods

    /**
     * Test : Iterator.
     *
     * Check if the iterator process went well.
     */
    public function test_iterator(): void
    {
        // ----------------
        // Vars

        $count = 0;

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Go through the table
        foreach ($bag as $item) {
            $count++;
        }

        // Tests
        $this->assertEquals(4, $count);
        $this->assertIsIterable($bag);
    }

    /**
     * Test : Count.
     *
     * Check if the count process went well.
     */
    public function test_count(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Tests
        $this->assertCount(4, $bag);
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : All.
     *
     * Check if 'all' method return all bag items.
     */
    public function test_all(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Get all items
        $result = $bag->all();

        // Tests
        $this->assertCount(4, $result);
        $this->assertSameSize($this->array, $result);
        $this->assertEquals($this->array, $result);
        $this->assertSame($this->array, $result);
    }

    /**
     * Test : Keys.
     *
     * Check if 'keys' method return all bag keys.
     */
    public function test_keys(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Get keys
        $keys         = $bag->keys();
        $originalKeys = array_keys($this->array);

        // Tests
        $this->assertCount(4, $keys);
        $this->assertSameSize($originalKeys, $keys);
        $this->assertEquals($originalKeys, $keys);
        $this->assertSame($originalKeys, $keys);
    }

    /**
     * Test : Values.
     *
     * Check if 'keys' method return all bag values.
     */
    public function test_values(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Get values
        $values         = $bag->values();
        $originalValues = array_values($this->array);

        // Tests
        $this->assertCount(4, $values);
        $this->assertSameSize($originalValues, $values);
        $this->assertEquals($originalValues, $values);
        $this->assertSame($originalValues, $values);
    }

    /**
     * Test : Change.
     *
     * Check if 'change' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_change(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->change([]);
    }

    /**
     * Test : Replace.
     *
     * Check if 'replace' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_replace(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->replace([]);
    }

    /**
     * Test : Replace Recursive.
     *
     * Check if 'replaceRecursive' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_replaceRecursive(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->replaceRecursive([]);
    }

    /**
     * Test : Merge.
     *
     * Check if 'merge' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_merge(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->merge([]);
    }

    /**
     * Test : Merge Recursive.
     *
     * Check if 'mergeRecursive' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_mergeRecursive(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->mergeRecursive([]);
    }

    /**
     * Test : Setter.
     *
     * Check if the setter process throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_set(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->set('Key1', 'Test Value');
    }

    /**
     * Test : Is not Empty.
     *
     * Check if 'empty' method returns false when bag is not empty.
     */
    public function test_isNotEmpty(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Tests
        $this->assertFalse($bag->isEmpty());
    }

    /**
     * Test : Is Empty - Without data.
     *
     * Check if 'empty' method returns true when the bag is initialized with an empty array.
     */
    public function test_isEmpty_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Tests
        $this->assertTrue($bag->isEmpty());
    }

    /**
     * Test : Remove.
     *
     * Check if 'remove' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_remove(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->remove('Key1');
    }

    /**
     * Test : Clear.
     *
     * Check if 'clear' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_clear(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->clear();
    }

    /**
     * Test : IndexOf.
     *
     * Check if 'indexOf' method works as expected.
     * Returns the value's index.
     */
    public function test_indexOf(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'test_0',
            'test_1',
            'test_2'
        ]);

        // ----------------
        // Process

        // Get indexes
        $index0 = $bag->indexOf('test_0');
        $index1 = $bag->indexOf('test_1');
        $index2 = $bag->indexOf('test_2');

        // Tests
        $this->assertEquals(0, $index0);
        $this->assertEquals(1, $index1);
        $this->assertEquals(2, $index2);
    }

    /**
     * Test : IndexOf - Without data.
     *
     * Check if 'indexOf' method works as expected.
     * Returns no index.
     */
    public function test_indexOf_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Get index
        $index = $bag->indexOf('test_0');

        // Tests
        $this->assertEquals(null, $index);
    }

    /**
     * Test : Has.
     *
     * Check if 'has' method works as expected.
     * Returns whether the key exist in the bag.
     */
    public function test_has(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1',
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_1');

        // Tests
        $this->assertTrue($has);
    }

    /**
     * Test : Has - Wrong key.
     *
     * Check if 'has' method works as expected.
     * Returns false because the key doesn't exist in the bag.
     */
    public function test_has_wrongKey(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1',
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_2');

        // Tests
        $this->assertFalse($has);
    }

    /**
     * Test : Has - Without data.
     *
     * Check if 'has' method works as expected.
     * Returns false because the bag is empty.
     */
    public function test_has_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_1');

        // Tests
        $this->assertFalse($has);
    }

    /**
     * Test : Contains.
     *
     * Check if 'contains' method works as expected.
     * Returns whether the value is contained in the bag.
     */
    public function test_contains(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'Test_1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_1');

        // Tests
        $this->assertTrue($contains);
    }

    /**
     * Test : Contains - Wrong value.
     *
     * Check if 'contains' method works as expected.
     * Returns false because the value isn't contained in the bag.
     */
    public function test_contains_wrongValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'Test_1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_2');

        // Tests
        $this->assertFalse($contains);
    }

    /**
     * Test : Contains - Without data.
     *
     * Check if 'contains' method works as expected.
     * Returns false because the bag is empty.
     */
    public function test_contains_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_1');

        // Tests
        $this->assertFalse($contains);
    }

    /**
     * Test : Get.
     *
     * Check if 'get' method works as expected.
     * Returns the value corresponding to the key in the bag.
     */
    public function test_get(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('Test 1', $value);
    }

    /**
     * Test : Get - Wrong key.
     *
     * Check if 'get' method works as expected.
     * Returns null because the key doesn't exist in the bag.
     */
    public function test_get_wrongKey(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_2');

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get - Wrong key with default value.
     *
     * Check if 'get' method works as expected.
     * Returns default value because the key doesn't exist in the bag.
     */
    public function test_get_wrongKeyWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_2', 'My default value');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('My default value', $value);
    }

    /**
     * Test : Get - Without data.
     *
     * Check if 'get' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_get_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_1');

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get as string.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'Test 1'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('Test 1', $value);
    }

    /**
     * Test : Get as string - Value is integer.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_valueIsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 10
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('10', $value);
    }

    /**
     * Test : Get as string - Value is boolean.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => true
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('true', $value);
    }

    /**
     * Test : Get as string - Wrong data.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
    }

    /**
     * Test : Get as string - Wrong data with default value.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1', 'My default value');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('My default value', $value);
    }

    /**
     * Test : Get as integer.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 10
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(10, $value);
    }

    /**
     * Test : Get as integer - Value is string.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => '5'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(5, $value);
    }

    /**
     * Test : Get as integer - Value is boolean.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => true
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(1, $value);
    }

    /**
     * Test : Get as integer - Wrong data.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(0, $value);
    }

    /**
     * Test : Get as integer - Wrong data with default value.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1', 100);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(100, $value);
    }

    /**
     * Test : Get as float.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 10.5
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(10.5, $value);
    }

    /**
     * Test : Get as float - Value is string.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => '5.5'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(5.5, $value);
    }

    /**
     * Test : Get as float - Wrong data.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(0, $value);
    }

    /**
     * Test : Get as float - Wrong data with default value.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1', 100);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(100, $value);
    }

    /**
     * Test : Get as boolean.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => true
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is string.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'true'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is integer.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 1
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is boolean.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => true
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Wrong data.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(false, $value);
    }

    /**
     * Test : Get as boolean - Wrong data with default value.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1', true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as array.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => ['key_1' => 'value_1']
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['key_1' => 'value_1'], $value);
    }

    /**
     * Test : Get as array - Value is array.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_valueIsBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => new ReadOnlyBag(['key_1' => 'value_1'])
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['key_1' => 'value_1'], $value);
    }

    /**
     * Test : Get as array - Wrong data.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
        $this->assertEquals([], $value);
    }

    /**
     * Test : Get as array - Wrong data with default value.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1', ['value_1']);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['value_1'], $value);
    }

    /**
     * Test : Get as bag.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => new ReadOnlyBag(['key_1' => 'value_1'])
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new ReadOnlyBag(['key_1' => 'value_1']), $value);
    }

    /**
     * Test : Get as bag - Value is array.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_valueIsArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => ['key_1' => 'value_1']
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new Bag(['key_1' => 'value_1']), $value);
    }

    /**
     * Test : Get as bag - Wrong data.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
        $this->assertEquals(new Bag(), $value);
    }

    /**
     * Test : Get as bag - Wrong data with default value.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => null
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1', new ReadOnlyBag(['value_1']));

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new ReadOnlyBag(['value_1']), $value);
    }

    /**
     * Test : Get first.
     *
     * Check if 'getFirst' method works as expected.
     * Returns the first value of the bag.
     */
    public function test_getFirst(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'value_1',
            'value_2'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_1', $value);
    }

    /**
     * Test : Get first - Key / Value Bag.
     *
     * Check if 'getFirst' method works as expected.
     * Returns the first value of the bag.
     */
    public function test_getFirst_keyValueBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'value_1',
            'key_2' => 'value_2'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_1', $value);
    }

    /**
     * Test : Get first - Without data.
     *
     * Check if 'getFirst' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_getFirst_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get last.
     *
     * Check if 'getLast' method works as expected.
     * Returns the last value of the bag.
     */
    public function test_getLast(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'value_1',
            'value_2'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_2', $value);
    }

    /**
     * Test : Get last - Key / Value Bag.
     *
     * Check if 'getLast' method works as expected.
     * Returns the last value of the bag.
     */
    public function test_getLast_keyValueBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag([
            'key_1' => 'value_1',
            'key_2' => 'value_2'
        ]);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_2', $value);
    }

    /**
     * Test : Get last - Without data.
     *
     * Check if 'getLast' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_getLast_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNull($value);
    }

    # --------------------------------
    # Test - Heap methods

    /**
     * Test : Push.
     *
     * Check if 'push' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_push(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->push('Test Value');
    }

    /**
     * Test : Shift.
     *
     * Check if 'shift' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_shift(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->shift();
    }

    /**
     * Test : Pop.
     *
     * Check if 'pop' methods throw an exception.
     *
     * @throws ImmutableBagException
     */
    public function test_pop(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReadOnlyBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ImmutableBagException::class);
        $this->expectExceptionMessage("You can't modify the bag, it's only accessible in reading");

        // Call method
        $bag->pop();
    }
}
