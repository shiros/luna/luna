<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ReferenceBagTest.php
 * @Created_at  : 11/04/2020
 * @Update_at   : 08/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Bag;

use Luna\Component\Bag\Exception\BagException;
use Luna\Component\Bag\Bag;
use Luna\Component\Bag\ReferenceBag;
use Luna\Component\Test\LunaTestCase;

class ReferenceBagTest extends LunaTestCase
{
    # --------------------------------
    # Attributes

    protected array $array;

    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Vars

        $this->array = [
            0      => 'Value0',
            'Key1' => 'Value1',
            'Key2' => 'Value2',
            'Key3' => [
                0        => 'Value0',
                'Key3-1' => 'Value1',
                'Key3-2' => 'Value2',
            ]
        ];

        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build bag
        $bag = new ReferenceBag();

        // Tests
        $this->assertNotNull($bag);
    }

    /**
     * Test : Construct - With array.
     *
     * Check if the constructor process went well.
     */
    public function test_construct_withArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($bag);
    }

    # --------------------------------
    # Test - References

    /**
     * Test : Reference.
     *
     * Check if the bag is a ref to the original array.
     */
    public function test_reference(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Add values
        $bag->push('Ma Super Value');

        // Get all items
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertCount(5, $bag);
        $this->assertSameSize($this->array, $bag);
    }

    # --------------------------------
    # Test - Interface methods

    /**
     * Test : Iterator.
     *
     * Check if the iterator process went well.
     */
    public function test_iterator(): void
    {
        // ----------------
        // Vars

        $count = 0;

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Go through the table
        foreach ($bag as $item) {
            $count++;
        }

        // Tests
        $this->assertEquals(4, $count);
        $this->assertIsIterable($bag);
    }

    /**
     * Test : Count.
     *
     * Check if the count process went well.
     */
    public function test_count(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Tests
        $this->assertCount(4, $bag);
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : All.
     *
     * Check if 'all' method return all bag items.
     */
    public function test_all(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get all items
        $result = $bag->all();

        // Tests
        $this->assertSame($this->array, $result);
        $this->assertCount(4, $result);
        $this->assertSameSize($this->array, $result);
        $this->assertEquals($this->array, $result);
    }

    /**
     * Test : Keys.
     *
     * Check if 'keys' method return all bag keys.
     */
    public function test_keys(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get keys
        $keys         = $bag->keys();
        $originalKeys = array_keys($this->array);

        // Tests
        $this->assertSame($originalKeys, $keys);
        $this->assertCount(4, $keys);
        $this->assertSameSize($originalKeys, $keys);
        $this->assertEquals($originalKeys, $keys);
    }

    /**
     * Test : Values.
     *
     * Check if 'keys' method return all bag values.
     */
    public function test_values(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get values
        $values         = $bag->values();
        $originalValues = array_values($this->array);

        // Tests
        $this->assertSame($originalValues, $values);
        $this->assertCount(4, $values);
        $this->assertSameSize($originalValues, $values);
        $this->assertEquals($originalValues, $values);
    }

    /**
     * Test : Change.
     *
     * Check if 'change' method throw an exception because this method isn't supported.
     *
     * @throws BagException
     */
    public function test_change(): void
    {
        // ----------------
        // Vars

        $newArray = [
            'Key1' => 'Value1',
            'Key2' => 'Value2'
        ];

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(BagException::class);
        $this->expectExceptionMessage("This method isn't supported.");

        // Call function
        $bag->change($newArray);
    }

    /**
     * Test : Replace.
     *
     * Check if 'replace' method change some values into the bag.
     */
    public function test_replace(): void
    {
        // ----------------
        // Vars

        $newArray = [
            'Key1' => 'V1',
            'Key2' => 'V2',
            'Key4' => 'Value4'
        ];

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Replace & get items
        $bag->replace($newArray);
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertCount(5, $bag);
        $this->assertArrayHasKey(0, $bagArray);
        $this->assertArrayHasKey('Key1', $bagArray);
        $this->assertArrayHasKey('Key2', $bagArray);
        $this->assertArrayHasKey('Key3', $bagArray);
        $this->assertArrayHasKey('Key4', $bagArray);

        // Tests - Key '0'
        $this->assertEquals('Value0', $bagArray[0]);

        // Tests - Key 'Key1'
        $this->assertEquals('V1', $bagArray['Key1']);

        // Tests - Key 'Key2'
        $this->assertEquals('V2', $bagArray['Key2']);

        // Tests - Key 'Key3'
        $this->assertIsArray($bagArray['Key3']);
        $this->assertCount(3, $bagArray['Key3']);

        // Tests - Key 'Key4'
        $this->assertEquals('Value4', $bagArray['Key4']);
    }

    /**
     * Test : Replace Recursive.
     *
     * Check if 'replaceRecursive' method change recursively some values into the bag.
     */
    public function test_replaceRecursive(): void
    {
        // ----------------
        // Vars

        $newArray = [
            'Key1' => 'V1',
            'Key2' => 'V2',
            'Key3' => [
                'Key3-1' => 'V1',
                'Key3-3' => 'V3'
            ],
            'Key4' => 'Value4'
        ];

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Replace & get items
        $bag->replaceRecursive($newArray);
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertCount(5, $bag);
        $this->assertArrayHasKey(0, $bagArray);
        $this->assertArrayHasKey('Key1', $bagArray);
        $this->assertArrayHasKey('Key2', $bagArray);
        $this->assertArrayHasKey('Key3', $bagArray);
        $this->assertArrayHasKey('Key4', $bagArray);

        // Tests - Key '0'
        $this->assertEquals('Value0', $bagArray[0]);

        // Tests - Key 'Key1'
        $this->assertEquals('V1', $bagArray['Key1']);

        // Tests - Key 'Key2'
        $this->assertEquals('V2', $bagArray['Key2']);

        // Tests - Key 'Key3'
        $this->assertCount(4, $bagArray['Key3']);
        $this->assertArrayHasKey(0, $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-1', $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-2', $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-3', $bagArray['Key3']);
        $this->assertEquals('Value0', $bagArray['Key3'][0]);
        $this->assertEquals('V1', $bagArray['Key3']['Key3-1']);
        $this->assertEquals('Value2', $bagArray['Key3']['Key3-2']);
        $this->assertEquals('V3', $bagArray['Key3']['Key3-3']);

        // Tests - Key 'Key4'
        $this->assertEquals('Value4', $bagArray['Key4']);
    }

    /**
     * Test : Merge.
     *
     * Check if 'merge' method change some values into the bag.
     */
    public function test_merge(): void
    {
        // ----------------
        // Vars

        $newArray = [
            0      => 'V0',
            'Key1' => 'V1',
            'Key2' => 'V2',
            'Key4' => 'Value4'
        ];

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Merge & get items
        $bag->merge($newArray);
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertCount(6, $bag);
        $this->assertArrayHasKey(0, $bagArray);
        $this->assertArrayHasKey(1, $bagArray);
        $this->assertArrayHasKey('Key1', $bagArray);
        $this->assertArrayHasKey('Key2', $bagArray);
        $this->assertArrayHasKey('Key3', $bagArray);
        $this->assertArrayHasKey('Key4', $bagArray);

        // Tests - Key '0'
        $this->assertEquals('Value0', $bagArray[0]);

        // Tests - Key '1'
        $this->assertEquals('V0', $bagArray[1]);

        // Tests - Key 'Key1'
        $this->assertEquals('V1', $bagArray['Key1']);

        // Tests - Key 'Key2'
        $this->assertEquals('V2', $bagArray['Key2']);

        // Tests - Key 'Key3'
        $this->assertIsArray($bagArray['Key3']);
        $this->assertCount(3, $bagArray['Key3']);

        // Tests - Key 'Key4'
        $this->assertEquals('Value4', $bagArray['Key4']);
    }

    /**
     * Test : Merge Recursive.
     *
     * Check if 'mergeRecursive' method change recursively some values into the bag.
     */
    public function test_mergeRecursive(): void
    {
        // ----------------
        // Vars

        $newArray = [
            0      => 'V0',
            'Key1' => 'V1',
            'Key2' => 'V2',
            'Key3' => [
                0        => 'V0',
                'Key3-1' => 'V1',
                'Key3-3' => 'V3'
            ],
            'Key4' => 'Value4'
        ];

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Merge & get items
        $bag->mergeRecursive($newArray);
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertCount(6, $bag);
        $this->assertArrayHasKey(0, $bagArray);
        $this->assertArrayHasKey(1, $bagArray);
        $this->assertArrayHasKey('Key1', $bagArray);
        $this->assertArrayHasKey('Key2', $bagArray);
        $this->assertArrayHasKey('Key3', $bagArray);
        $this->assertArrayHasKey('Key4', $bagArray);

        // Tests - Key '0'
        $this->assertEquals('Value0', $bagArray[0]);

        // Tests - Key '1'
        $this->assertEquals('V0', $bagArray[1]);

        // Tests - Key 'Key1'
        $this->assertIsArray($bagArray['Key1']);
        $this->assertCount(2, $bagArray['Key1']);
        $this->assertArrayHasKey(0, $bagArray['Key1']);
        $this->assertArrayHasKey(1, $bagArray['Key1']);
        $this->assertEquals('Value1', $bagArray['Key1'][0]);
        $this->assertEquals('V1', $bagArray['Key1'][1]);

        // Tests - Key 'Key2'
        $this->assertIsArray($bagArray['Key2']);
        $this->assertCount(2, $bagArray['Key2']);
        $this->assertArrayHasKey(0, $bagArray['Key2']);
        $this->assertArrayHasKey(1, $bagArray['Key2']);
        $this->assertEquals('Value2', $bagArray['Key2'][0]);
        $this->assertEquals('V2', $bagArray['Key2'][1]);

        // Tests - Key 'Key3'
        $this->assertIsArray($bagArray['Key3']);
        $this->assertCount(5, $bagArray['Key3']);
        $this->assertArrayHasKey(0, $bagArray['Key3']);
        $this->assertArrayHasKey(1, $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-1', $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-2', $bagArray['Key3']);
        $this->assertArrayHasKey('Key3-3', $bagArray['Key3']);
        $this->assertEquals('Value0', $bagArray['Key3'][0]);
        $this->assertEquals('V0', $bagArray['Key3'][1]);
        $this->assertEquals('Value2', $bagArray['Key3']['Key3-2']);
        $this->assertEquals('V3', $bagArray['Key3']['Key3-3']);

        // Tests - Key 'Key3.Key3-1'
        $this->assertIsArray($bagArray['Key3']['Key3-1']);
        $this->assertCount(2, $bagArray['Key3']['Key3-1']);

        $this->assertArrayHasKey(0, $bagArray['Key3']['Key3-1']);
        $this->assertArrayHasKey(1, $bagArray['Key3']['Key3-1']);
        $this->assertEquals('Value1', $bagArray['Key3']['Key3-1'][0]);
        $this->assertEquals('V1', $bagArray['Key3']['Key3-1'][1]);

        // Tests - Key 'Key4'
        $this->assertEquals('Value4', $bagArray['Key4']);
    }

    /**
     * Test : Is not Empty.
     *
     * Check if 'empty' method returns false when bag is not empty.
     */
    public function test_isNotEmpty(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get items
        $bagArray = &$bag->all();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertFalse($bag->isEmpty());
    }

    /**
     * Test : Is Empty - With call clear.
     *
     * Check if 'empty' method returns true when the 'clear' method is called.
     */
    public function test_isEmpty_withCallClear(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get items
        $bagArray = &$bag->all();

        // Clear bag
        $bag->clear();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertEmpty($this->array);
    }

    /**
     * Test : Is Empty - Without data.
     *
     * Check if 'empty' method returns true when the bag is initialized with an empty array.
     */
    public function test_isEmpty_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Tests
        $this->assertTrue($bag->isEmpty());
    }

    /**
     * Test : Clear.
     *
     * Check if 'clear' method works as expected.
     * The bag should be empty.
     */
    public function test_clear(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag($this->array);

        // ----------------
        // Process

        // Get items
        $bagArray = &$bag->all();

        // Clear bag
        $bag->clear();

        // Tests
        $this->assertSame($this->array, $bagArray);
        $this->assertTrue($bag->isEmpty());
    }

    /**
     * Test : Clear - Without data.
     *
     * Check if 'clear' method works as expected.
     * The bag should be empty.
     */
    public function test_clear_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Tests
        $this->assertTrue($bag->isEmpty());
    }


    /**
     * Test : IndexOf.
     *
     * Check if 'indexOf' method works as expected.
     * Returns the value's index.
     */
    public function test_indexOf(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = [
            'test_0',
            'test_1',
            'test_2'
        ];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Get indexes
        $index0 = $bag->indexOf('test_0');
        $index1 = $bag->indexOf('test_1');
        $index2 = $bag->indexOf('test_2');

        // Tests
        $this->assertEquals(0, $index0);
        $this->assertEquals(1, $index1);
        $this->assertEquals(2, $index2);
    }

    /**
     * Test : IndexOf - Without data.
     *
     * Check if 'indexOf' method works as expected.
     * Returns no index.
     */
    public function test_indexOf_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Get index
        $index = $bag->indexOf('test_0');

        // Tests
        $this->assertEquals(null, $index);
    }

    /**
     * Test : Has.
     *
     * Check if 'has' method works as expected.
     * Returns whether the key exist in the bag.
     */
    public function test_has(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_1');

        // Tests
        $this->assertTrue($has);
    }

    /**
     * Test : Has - Wrong key.
     *
     * Check if 'has' method works as expected.
     * Returns false because the key doesn't exist in the bag.
     */
    public function test_has_wrongKey(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_2');

        // Tests
        $this->assertFalse($has);
    }

    /**
     * Test : Has - Without data.
     *
     * Check if 'has' method works as expected.
     * Returns false because the bag is empty.
     */
    public function test_has_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Checks if key exist
        $has = $bag->has('key_1');

        // Tests
        $this->assertFalse($has);
    }

    /**
     * Test : Contains.
     *
     * Check if 'contains' method works as expected.
     * Returns whether the value is contained in the bag.
     */
    public function test_contains(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['Test_1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_1');

        // Tests
        $this->assertTrue($contains);
    }

    /**
     * Test : Contains - Wrong value.
     *
     * Check if 'contains' method works as expected.
     * Returns false because the value isn't contained in the bag.
     */
    public function test_contains_wrongValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_2');

        // Tests
        $this->assertFalse($contains);
    }

    /**
     * Test : Contains - Without data.
     *
     * Check if 'contains' method works as expected.
     * Returns false because the bag is empty.
     */
    public function test_contains_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Checks if key exist
        $contains = $bag->contains('Test_1');

        // Tests
        $this->assertFalse($contains);
    }

    /**
     * Test : Get.
     *
     * Check if 'get' method works as expected.
     * Returns the value corresponding to the key in the bag.
     */
    public function test_get(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('Test 1', $value);
    }

    /**
     * Test : Get - Wrong key.
     *
     * Check if 'get' method works as expected.
     * Returns null because the key doesn't exist in the bag.
     */
    public function test_get_wrongKey(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_2');

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get - Wrong key with default value.
     *
     * Check if 'get' method works as expected.
     * Returns default value because the key doesn't exist in the bag.
     */
    public function test_get_wrongKeyWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_2', 'My default value');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('My default value', $value);
    }

    /**
     * Test : Get - Without data.
     *
     * Check if 'get' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_get_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->get('key_1');

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get as string.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'Test 1'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('Test 1', $value);
    }

    /**
     * Test : Get as string - Value is integer.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_valueIsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 10];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('10', $value);
    }

    /**
     * Test : Get as string - Value is boolean.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => true];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('true', $value);
    }

    /**
     * Test : Get as string - Wrong data.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
    }

    /**
     * Test : Get as string - Wrong data with default value.
     *
     * Check if 'getAsString' method works as expected.
     * Returns the value as string.
     */
    public function test_getAsString_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsString('key_1', 'My default value');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsString($value);
        $this->assertEquals('My default value', $value);
    }

    /**
     * Test : Get as integer.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 10];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(10, $value);
    }

    /**
     * Test : Get as integer - Value is string.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => '5'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(5, $value);
    }

    /**
     * Test : Get as integer - Value is boolean.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => true];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(1, $value);
    }

    /**
     * Test : Get as integer - Wrong data.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(0, $value);
    }

    /**
     * Test : Get as integer - Wrong data with default value.
     *
     * Check if 'getAsInteger' method works as expected.
     * Returns the value as integer.
     */
    public function test_getAsInteger_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsInteger('key_1', 100);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsInt($value);
        $this->assertEquals(100, $value);
    }

    /**
     * Test : Get as float.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 10.5];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(10.5, $value);
    }

    /**
     * Test : Get as float - Value is string.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => '5.5'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(5.5, $value);
    }

    /**
     * Test : Get as float - Wrong data.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(0, $value);
    }

    /**
     * Test : Get as float - Wrong data with default value.
     *
     * Check if 'getAsFloat' method works as expected.
     * Returns the value as float.
     */
    public function test_getAsFloat_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsFloat('key_1', 100);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsFloat($value);
        $this->assertEquals(100, $value);
    }

    /**
     * Test : Get as boolean.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => true];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is string.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsString(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 'true'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is integer.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsInteger(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => 1];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Value is boolean.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_valueIsBoolean(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => true];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as boolean - Wrong data.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEquals(false, $value);
    }

    /**
     * Test : Get as boolean - Wrong data with default value.
     *
     * Check if 'getAsBoolean' method works as expected.
     * Returns the value as boolean.
     */
    public function test_getAsBoolean_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBoolean('key_1', true);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsBool($value);
        $this->assertEquals(true, $value);
    }

    /**
     * Test : Get as array.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => ['key_1' => 'value_1']];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['key_1' => 'value_1'], $value);
    }

    /**
     * Test : Get as array - Value is array.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_valueIsBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => new Bag(['key_1' => 'value_1'])];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['key_1' => 'value_1'], $value);
    }

    /**
     * Test : Get as array - Wrong data.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
        $this->assertEquals([], $value);
    }

    /**
     * Test : Get as array - Wrong data with default value.
     *
     * Check if 'getAsArray' method works as expected.
     * Returns the value as array.
     */
    public function test_getAsArray_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsArray('key_1', ['value_1']);

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsArray($value);
        $this->assertEquals(['value_1'], $value);
    }

    /**
     * Test : Get as bag.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => new Bag(['key_1' => 'value_1'])];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new Bag(['key_1' => 'value_1']), $value);
    }

    /**
     * Test : Get as bag - Value is array.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_valueIsArray(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => ['key_1' => 'value_1']];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new Bag(['key_1' => 'value_1']), $value);
    }

    /**
     * Test : Get as bag - Wrong data.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_wrongData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1');

        // Tests
        $this->assertNotNull($value);
        $this->assertEmpty($value);
        $this->assertEquals(new Bag(), $value);
    }

    /**
     * Test : Get as bag - Wrong data with default value.
     *
     * Check if 'getAsBag' method works as expected.
     * Returns the value as bag.
     */
    public function test_getAsBag_wrongDataWithDefaultValue(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['key_1' => null];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getAsBag('key_1', new Bag(['value_1']));

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertIsIterable($value);
        $this->assertEquals(new Bag(['value_1']), $value);
    }

    /**
     * Test : Get first.
     *
     * Check if 'getFirst' method works as expected.
     * Returns the first value of the bag.
     */
    public function test_getFirst(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['value_1', 'value_2'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_1', $value);
    }

    /**
     * Test : Get first - Key / Value Bag.
     *
     * Check if 'getFirst' method works as expected.
     * Returns the first value of the bag.
     */
    public function test_getFirst_keyValueBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = [
            'key_1' => 'value_1',
            'key_2' => 'value_2'
        ];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_1', $value);
    }

    /**
     * Test : Get first - Without data.
     *
     * Check if 'getFirst' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_getFirst_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getFirst();

        // Tests
        $this->assertNull($value);
    }

    /**
     * Test : Get last.
     *
     * Check if 'getLast' method works as expected.
     * Returns the last value of the bag.
     */
    public function test_getLast(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = ['value_1', 'value_2'];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_2', $value);
    }

    /**
     * Test : Get last - Key / Value Bag.
     *
     * Check if 'getLast' method works as expected.
     * Returns the last value of the bag.
     */
    public function test_getLast_keyValueBag(): void
    {
        // ----------------
        // Vars

        // Get bag
        $data = [
            'key_1' => 'value_1',
            'key_2' => 'value_2'
        ];
        $bag  = new ReferenceBag($data);

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNotNull($value);
        $this->assertNotEmpty($value);
        $this->assertEquals('value_2', $value);
    }

    /**
     * Test : Get last - Without data.
     *
     * Check if 'getLast' method works as expected.
     * Returns null because the bag is empty.
     */
    public function test_getLast_withoutData(): void
    {
        // ----------------
        // Vars

        // Get bag
        $bag = new ReferenceBag();

        // ----------------
        // Process

        // Checks if key exist
        $value = $bag->getLast();

        // Tests
        $this->assertNull($value);
    }

    # --------------------------------
    # Test - Heap methods

    # --------------------------------
    # Test - Sort methods
}
