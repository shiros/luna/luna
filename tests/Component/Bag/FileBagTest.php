<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileBagTest.php
 * @Created_at  : 08/01/2023
 * @Update_at   : 08/01/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Bag;

use Luna\Component\Bag\FileBag;
use Luna\Component\Test\LunaTestCase;

class FileBagTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build bag
        $bag = new FileBag();

        // Tests
        $this->assertNotNull($bag);
    }
}
