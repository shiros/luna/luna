<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : LunaContainerTest.php
 * @Created_at  : 05/04/2020
 * @Update_at   : 31/10/2024
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Container;

use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Container\LunaContainer;
use Luna\Component\Test\LunaTestCase;

class LunaContainerTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Construct

    /**
     * Test : Construct / Build.
     *
     * Check if the build process went well with force option.
     *
     * @throws ContainerException
     */
    public function test_construct_withForce(): void
    {
        // ----------------
        // Vars

        // Get bag
        $container = LunaContainer::build([], true);

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($container);
    }

    /**
     * Test : Construct / Build.
     *
     * Check if the build process throw an exception on multiple builds.
     *
     * @throws ContainerException
     */
    public function test_construct_multiple(): void
    {
        // ----------------
        // Process

        // Setup exception tests
        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage("You can't build another container.");

        // Tests - Another build
        LunaContainer::build();
    }

    # --------------------------------
    # Test - Instance

    /**
     * Test : Get instance.
     *
     * Check if the retrieval of the instance went well.
     */
    public function test_getInstance(): void
    {
        // ----------------
        // Vars

        // Get container
        $container = LunaContainer::getInstance();

        // ----------------
        // Process

        // Tests
        $this->assertNotNull($container);
    }

    /**
     * Test : Get two container instance.
     *
     * Check if the container instances are the same.
     */
    public function test_2Instance(): void
    {
        // ----------------
        // Vars

        // Get containers
        $container1 = LunaContainer::getInstance();
        $container2 = LunaContainer::getInstance();

        // ----------------
        // Process

        // Tests
        $this->assertIsObject($container1);
        $this->assertIsObject($container2);
        $this->assertSame($container1, $container2);
        $this->assertEquals($container1, $container2);
    }

    # --------------------------------
    # Test - Core methods

    /**
     * Test : Has key.
     *
     * Check if 'has' method return true when the item exists.
     */
    public function test_has(): void
    {
        // ----------------
        // Vars

        $key = 'MyKey';

        // Get container
        $container = LunaContainer::getInstance();

        // ----------------
        // Process

        // Set item
        $container->set($key, 'MyValue');

        // Tests
        $this->assertTrue($container->has($key));
    }

    /**
     * Test : Has key with a null value. (Nullable disable)
     *
     * Check if 'has' method return false when the key exists but the value is null. (Nullable option set to false)
     */
    public function test_has_withNullValueAndNullableDisable(): void
    {
        // ----------------
        // Vars

        $key = 'MyKey';

        // Get container
        $container = LunaContainer::getInstance();

        // ----------------
        // Process

        // Set item
        $container->set($key, null);

        // Tests
        $this->assertFalse($container->has($key));
    }

    /**
     * Test : Has key with a null value. (Nullable enable)
     *
     * Check if 'has' method return true when the key exists but the value is null. (Nullable option set to true)
     */
    public function test_has_withNullValueAndNullableEnable(): void
    {
        // ----------------
        // Vars

        $key = 'MyKey';

        // Get container
        $container = LunaContainer::getInstance();

        // ----------------
        // Process

        // Set item
        $container->set($key, null);

        // Tests
        $this->assertTrue($container->has($key, true));
    }

    /**
     * Test : Get/Set a key/value pair.
     *
     * Check if 'get' & 'set' method works well.
     */
    public function test_getSet(): void
    {
        // ----------------
        // Vars

        $key   = 'MyKey';
        $value = 'MyValue';

        // Get container
        $container = LunaContainer::getInstance();

        // ----------------
        // Process

        // Set key/value
        $container->set($key, $value);

        // Get data
        $data = $container->get($key);

        // Tests
        $this->assertEquals($value, $data);
    }
}
