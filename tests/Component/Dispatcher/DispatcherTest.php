<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2024
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DispatcherTest.php
 * @Created_at  : 28/11/2023
 * @Update_at   : 28/11/2023
 * ----------------------------------------------------------------
 */

namespace Luna\Tests\Component\Dispatcher;

use Luna\Component\Dispatcher\Dispatcher;
use Luna\Component\Test\LunaTestCase;

class DispatcherTest extends LunaTestCase
{
    # --------------------------------
    # Setup

    /**
     * Test setup.
     *
     * @return void
     */
    protected function setUp(): void
    {
        // ----------------
        // Process

        // Boot kernel
        $this->bootKernel();

        // Call parent setup
        parent::setUp();
    }

    # --------------------------------
    # Test - Constructor

    /**
     * Test : Construct.
     *
     * Check if the constructor process went well.
     */
    public function test_construct(): void
    {
        // ----------------
        // Process

        // Build dispatcher
        $dispatcher = new Dispatcher();

        // Tests
        $this->assertNotNull($dispatcher);
    }
}
